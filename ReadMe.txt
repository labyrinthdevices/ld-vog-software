
2015-09-15 by MAR

---------REQUIRED SOFTWARE---------

:::Visual Studio 2013:::

I used Community Edition, but the original software was made in Professional Edition.  I don't think it should matter.


:::Sandcastle:::

This is used for the documentation generated in Visual Studio.
You can download the exe here: https://shfb.codeplex.com/


:::nvcuda:::

This is only applicable if the PC does not have a CUDA graphics card

http://file.emgu.com/forum/viewtopic.php?f=4&t=4728
Re: Usage of wrapper without cuda dependencies
by Chris_Johnson » Wed Feb 05, 2014 3:21 am
Hi,

If I remember you only need the to show Open CV the nvcuda.dll I've uploaded a copy here:

http://sourceforge.net/projects/emguexample/files/nvcuda.dll/download 

The readme simply says:

If you are installing Emgu CV 2.4.9+ on a platform without Cuda support you will still 
need nvcuda.dll in the following directories.

Install nvcuda in:

x86
C:\Windows\System32 (I think this is right it's been a long time since I've used an x86 platform)

x64
C:\Windows\System32
C:\Windows\SysWOW64

This should enable your application let me know if there are any problems,

Cheers,e
Chris


:::StyleCop:::

https://stylecop.codeplex.com/

This is not necessary, but recommended since Jorge seems to be using this as a coding guideline