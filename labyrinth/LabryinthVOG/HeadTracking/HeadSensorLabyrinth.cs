﻿//-----------------------------------------------------------------------
// <copyright file="HeadSensorLabyrinth.cs" company="Labyrinth Devices">
//     Copyright (c) 2014- Labyrinth Devices, All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using OculomotorLab.VOG.HeadTracking;

    /// <summary>
    /// Analog accelerometer connected to a measurement computing DAQ used together with the micromedical googles.
    /// </summary>

    public partial class CameraEyePointGreyLabyrinth : ICameraEye
    {

        public class HeadSensorLabyrinth : IHeadSensor
        {
            /// <summary>
            /// Pointgrey camera used to get timestamps to sync the head data and the eye data.
            /// </summary>w
            private CameraEyePointGreyLabyrinth cameraSync;

            public enum HeadSensorState
            {
                Idle,
                Initialized,
                Tracking,
                Finalizing
            }

            public HeadSensorState State { get; private set; }


            /// <summary>
            /// Initializes a new instance of HeadSensorLabyrinth the class.
            /// </summary>
            /// <param name="cameraSync">The associated point grey camera.</param>
            internal HeadSensorLabyrinth(CameraEyePointGreyLabyrinth cameraSync)
            {
                this.cameraSync = cameraSync;
                this.State = HeadSensorState.Idle;
            }

            /// <summary>
            /// Interface for head tracking sensors
            /// </summary>
            public HeadData GrabHeadData(bool SoftwareTriggerOn)
            {
                HeadData headData;

                if(this.cameraSync.State != CameraState.Tracking) {
                    headData = new HeadData();
                    if (this.cameraSync.State == CameraState.Finalizing)
                        headData.DataResult = HeadData.Result.Terminated;
                    else
                        headData.DataResult = HeadData.Result.Missed;
                }
                else
                {
                    headData = this.cameraSync.ReadSensors();

                    if (SoftwareTriggerOn)
                    {
                        headData.SoftwareTrigger = 1;
                    }
                    else
                    {
                        headData.SoftwareTrigger = 0;
                    }
                }

                return headData;
            }

            public void StartTracking()
            {
                this.State = HeadSensorState.Initialized;
                //this.cameraSync.Clear_Buffer();
            }
            public void StopTracking()
            {
                this.State = HeadSensorState.Finalizing;
            }

            public void Dispose()
            {
                this.Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected void Dispose(bool disposing)
            {
                if (disposing)
                {

                }
            }
        }
    
    }
}
