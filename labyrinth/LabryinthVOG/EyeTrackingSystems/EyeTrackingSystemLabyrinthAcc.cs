﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemLabyrinth.cs" company="Labyrinth Devices">
//     Copyright (c) 2014- Labyrinth Devices, All rights reserved. 
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.HeadTracking;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Labyrinth system.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("LabyrinthAcc", 1, 2)]
    public class EyeTrackingSystemLabyrinthAcc : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider, IHeadSensorProvider
    {
        /// <summary>
        /// Camera, necesary to configure the head tracking.
        /// </summary>
        internal CameraEyePointGreyLabyrinth Camera { get; private set; }

        /// <summary>
        /// Distortion Correction Left Eye.
        /// </summary>
        private Matrix<float> Map1_Left, Map2_Left;

        /// <summary>
        /// Distortion Correction Right Eye.
        /// </summary>
        private Matrix<float> Map1_Right, Map2_Right;

        /// <summary>
        /// Distortion Correction Whole Image.
        /// </summary>
        private Matrix<float> Map1, Map2;

        /// <summary>
        /// Gets the cameras. In this case just one single camera.
        /// </summary>
        /// <returns>List containing the camera object.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            using (var busMgr = new ManagedBusManager())
            {
                uint numCameras = busMgr.GetNumOfCameras();

                if (numCameras < 1)
                {
                    return null;
                }

                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 256, 1264, 512), EyeTracker.Settings.ShutterDuration, true);
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 140, 1264, 512), EyeTracker.Settings.ShutterDuration, true);
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 176, 1264, 512), EyeTracker.Settings.ShutterDuration, true);
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 140, 1264, 512), EyeTracker.Settings.ShutterDuration, true);
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 124, 1264, 512), EyeTracker.Settings.ShutterDuration, true);
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 40, 1264, 512), EyeTracker.Settings.ShutterDuration, true);
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 70, 1264, 512), EyeTracker.Settings.ShutterDuration, true);

                // PJB
                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 56, 1264, 512), EyeTracker.Settings.ShutterDuration, true);

                //this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 120, 1264, 512), EyeTracker.Settings.ShutterDuration, true);

                this.Camera = new CameraEyePointGreyLabyrinth(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 142, 1264, 512), EyeTracker.Settings.ShutterDuration, true);


                this.Camera.Init();

                this.Load_Distortion_Mapping();

                // this.Camera.ShouldAdjustFrameRate = false;
                this.Camera.StartCapture();

                return new EyeCollection<ICameraEye>(this.Camera);
            }
        }

        String CamCalibFilePath = @"C:\VOG\Labyrinth VOG Software\lib\CamCalib\";
        private void Load_Distortion_Mapping()
        {
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1_Left.xml");
            Map1_Left = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2_Left.xml");
            Map2_Left = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));

            XmlDocument xDoc3 = new XmlDocument();
            xDoc3.Load(CamCalibFilePath + "Map1_Right.xml");
            Map1_Right = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc3));

            XmlDocument xDoc4 = new XmlDocument();
            xDoc4.Load(CamCalibFilePath + "Map2_Right.xml");
            Map2_Right = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc4));

            XmlDocument xDoc5 = new XmlDocument();
            xDoc5.Load(CamCalibFilePath + "Map1.xml");
            Map1 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc5));

            XmlDocument xDoc6 = new XmlDocument();
            xDoc6.Load(CamCalibFilePath + "Map2.xml");
            Map2 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc6));

        }

        /// <summary>
        /// Prepares the images for processing. Splits the single image into left and right eye
        /// and rotates them appropriately. 
        /// </summary>
        /// <param name="images">Raw image from the camera.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            Image<Gray, byte> imageLeft = null;
            Image<Gray, byte> imageRight = null;
            Image<Gray, byte> imageWhole = null;
            EyeCollection<ImageEye> newImages = null;
            try
            {
                // Total ROI is 1264 by 512
                //var roiLeft = new Rectangle(792, 0, 432, 512);
                //var roiRight = new Rectangle(96, 0, 432, 512);
                
                //var roiLeft = new Rectangle(812, 0, 432, 512);
                //var roiRight = new Rectangle(76, 0, 432, 512);
                
                //var roiLeft = new Rectangle(648, 0, 432, 512);
                //var roiRight = new Rectangle(250, 0, 432, 512);

                //var roiLeft = new Rectangle(708, 0, 432, 512);
                //var roiRight = new Rectangle(180, 0, 432, 512);

                //var roiLeft = new Rectangle(784, 0, 432, 512);
                //var roiRight = new Rectangle(52, 0, 432, 512);

                //var roiLeft = new Rectangle(824, 0, 432, 512);
                //var roiRight = new Rectangle(72, 0, 432, 512);

                //var roiLeft = new Rectangle(784, 0, 432, 512);
                //var roiRight = new Rectangle(32, 0, 432, 512);

                // 2016-10-18
                //var roiLeft = new Rectangle(824, 0, 432, 512);
                //var roiRight = new Rectangle(12, 0, 432, 512);

                // 2016-11-02
                var roiLeft = new Rectangle(740, 0, 432, 512);
                var roiRight = new Rectangle(50, 0, 432, 512);

                /*imageWhole = images[Eye.Both].GetCopy();

                //remap the image to the particular intrinsics
                //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
                //image is mapped backed, in the future this should be controllable through the flag '0'
                Image<Gray, Byte> tempImage = imageWhole.CopyBlank();

                CvInvoke.cvRemap(imageWhole, tempImage, Map1, Map2, 0, new MCvScalar(0));

                imageWhole = tempImage.Copy();

                Image<Gray, byte> imageLeftTemp = imageWhole.Copy(roiLeft);
                CvInvoke.cvTranspose(imageLeftTemp.Ptr, imageLeft.Ptr);

                Image<Gray, byte> imageRightTemp = imageWhole.Copy(roiRight);
                CvInvoke.cvTranspose(imageRightTemp.Ptr, imageRight.Ptr);

                //imageLeft = imageWhole.Copy(roiLeft);
                //imageRight = imageWhole.Copy(roiRight);

                //images[Eye.Both].LoadDistortionMapping();

                //images[Eye.Both].PerformDistortionCorrection();*/

                imageLeft = images[Eye.Both].GetCroppedTransposedImage(roiLeft);

                imageRight = images[Eye.Both].GetCroppedTransposedImage(roiRight);

                /*//remap the image to the particular intrinsics
                //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
                //image is mapped backed, in the future this should be controllable through the flag '0'
                Image<Gray, Byte> tempLeft = imageLeft.CopyBlank();
                Image<Gray, Byte> tempRight = imageRight.CopyBlank();

                CvInvoke.cvRemap(imageLeft, tempLeft, Map1_Left, Map2_Left, 0, new MCvScalar(0));
                CvInvoke.cvRemap(imageRight, tempRight, Map1_Right, Map2_Right, 0, new MCvScalar(0));

                imageLeft = tempLeft.Copy();
                imageRight = tempRight.Copy();*/
                
                imageRight = imageRight.Flip(Emgu.CV.CvEnum.FLIP.VERTICAL);
                imageRight = imageRight.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);

                // Copy the embedded fields into both images
                var bitsSeconds = BitConverter.GetBytes(images[Eye.Both].TimeStamp.Seconds);
                var bitsFrameNumber = BitConverter.GetBytes(images[Eye.Both].TimeStamp.FrameNumber);

                for (int i = 0; i < bitsSeconds.Length; i++)
                {
                    imageLeft.Data[i, 0, 0] = bitsSeconds[i];
                    imageLeft.Data[i, 0, 0] = bitsSeconds[i];
                }

                for (int i = 0; i < bitsFrameNumber.Length; i++)
                {
                    imageRight.Data[bitsSeconds.Length + i, 0, 0] = bitsFrameNumber[i];
                    imageRight.Data[bitsSeconds.Length + i, 0, 0] = bitsFrameNumber[i];
                }

                newImages = new EyeCollection<ImageEye>(
                    new ImageEye(imageLeft, Eye.Left, images[Eye.Both].TimeStamp),
                    new ImageEye(imageRight, Eye.Right, images[Eye.Both].TimeStamp));

                imageLeft = null;
                imageRight = null;
            }
            finally
            {
                if (imageLeft != null)
                {
                    imageLeft.Dispose();
                }

                if (imageRight != null)
                {
                    imageRight.Dispose();
                }
            }

            return newImages;
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <param name="fileNames">Filenames of the videos.</param>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> fileNames)
        {
            return new EyeCollection<VideoEye>(
                new VideoEyePointGrey(Eye.Left, fileNames[Eye.Left], VideoEyePointGrey.PositionOfEmbeddedInfo.TopLeftVertical),
                new VideoEyePointGrey(Eye.Right, fileNames[Eye.Right], VideoEyePointGrey.PositionOfEmbeddedInfo.TopLeftVertical));
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public IHeadSensor GetHeadSensor()
        {
            return new CameraEyePointGreyLabyrinth.HeadSensorLabyrinth(this.Camera);
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.10;
            settings.DistanceCameraToEyeMm = 70;

            return settings;
        }
    }
}
