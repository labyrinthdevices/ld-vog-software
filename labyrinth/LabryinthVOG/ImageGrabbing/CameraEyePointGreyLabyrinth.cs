﻿//-----------------------------------------------------------------------
// <copyright file="CameraEyePointGreyLabyrinth.cs" company="Labyrinth Devices">
//     Copyright (c) 2014- Labyrinth Devices, All rights reserved. 
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using FlyCapture2Managed;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Class to control a point grey camera in the context of eye tracking.
    /// </summary>
    public partial class CameraEyePointGreyLabyrinth : ICameraEye
    {

        #region serial port settings
        private const uint SerSettingReg = 0x70000;
        private const uint SerSettingVal = 0x90800FF;
        private const uint SerEnableReg = 0x70004;
        private const uint SerEnableVal = 0xC0000000;
        private const uint SerTrigReg = 0x70100;
        private const uint SerTrigVal = 0x67;
        private const uint SerTransReg = 0x7000C;
        private const uint SerRecReg = 0x70008;

        private const uint SerEnableValTrans = 0x40000000;
        private const uint SerEnableValRec = 0x80000000;

        //MAR and NSD added the following gyro related variable declarations
        private uint ReadSerSettings;
        private uint ReadSerEnable;
        private uint ReadSerTrans;
        private uint ReadSerReadReady;
        private uint ReadSerRec;
        private uint ReadSerNumBytes;
        private uint ReadSerNumBytes_Register;
        private uint[] SerReadBuffer = new uint[255];
        private uint[] SensorBuf = new uint[4];

        private short GyroX;
        private short GyroY;
        private short GyroZ;
        private double gyroScalar = 65.5; //NSD 5-8-14: 

        // MAR 2016-06-21
        private short Temper;
        private double temperScalar = 340;
        private double temperOffset = -521;

        private short AccX;
        private short AccY;
        private short AccZ;
        private double accScalar = 16384; // 8-22-2014 JB

        // Added by MAR for GPIO reading
        private const uint GPIO_Ctrl = 0x1100;
        private uint GPIO_Val;
        private uint GPIO_Data;

        #endregion


        #region fields

        /// <summary>
        /// Possible camera states
        /// </summary>
        public enum CameraState
        {
            /// <summary>
            /// Camera is not initialized
            /// </summary>
            Idle,
            /// <summary>
            /// Camera has completed initialization
            /// </summary>
            Initialized,
            /// <summary>
            /// Camera is currently tracking
            /// </summary>
            Tracking,
            /// <summary>
            /// Camera is ending tracking
            /// </summary>
            Finalizing
        }

        /// <summary>
        /// Camera object.
        /// </summary>
        private ManagedCamera camera;

        /// <summary>
        /// Gets the initial ROI of the camera.
        /// </summary>
        private Rectangle initialROI;

        /// <summary>
        /// Frame rate reported by the camera.
        /// </summary>
        private float reportedFrameRate = 0;

        /// <summary>
        /// Shutter time reported by the camera.
        /// </summary>
        private float reportedShutterDuration = 0;

        /// <summary>
        /// Last frame rate requested to the camera.
        /// </summary>
        private float lastFrameRateRequested = 0;

        /// <summary>
        /// Number of frame when the last change of frame rate was requested.
        /// </summary>
        private float lastFrameRateChangeFrameNumber = 0;

        /// <summary>
        /// Counter of how many times the frame rate has been changed.
        /// </summary>
        private long timesFrameRateChanged = 0;

        /// <summary>
        /// Maximum delay recorded between one frame timestamp and the expected time. For 
        /// instance the frame 110 at 100Hz should occur 1.100 seconds after the first frame.
        /// </summary>
        private double maxDelay = 0;

        /// <summary>
        /// How many cycles of 128 seconds (maximum raw timestamp) have happened. This is
        /// necessary to calculate good timestamps references to the beginning.
        /// </summary>
        private long cycles128sec = 0;

        /// <summary>
        /// Timestamp in seconds of the first frame (correcting for 128 sec cycles).
        /// </summary>
        private double firstSecondsTimestamp = -1;

        /// <summary>
        /// Timestamp in seconds of the last frame (correcting for 128 sec cycles).
        /// </summary>
        public double lastSecondsTimestamp = 0;

        /// <summary>
        /// Raw timestamp of the last frame.
        /// </summary>
        private TimeStamp lastRawTimeStamp = new TimeStamp();

        /// <summary>
        /// Frame number of the first frame.
        /// </summary>
        private long firstRawFrameNumber = -1;

        /// <summary>
        /// Frame number of the last frame.
        /// </summary>
        private long lastRawFrameNumber = 0;

        private long headDataFrameCounter = 0;

        private bool shouldUseAcc = true;

        private bool shouldUseAccTempGyro = true;

        private ManagedPGRGuid cameraID;

        private event EventHandler firstFrameAcquired;

        private bool cameraStopRequested = false;

        /// <summary>
        /// Distortion Correction Whole Image.
        /// </summary>
        private Matrix<float> Map1, Map2;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the CameraEyePointGreyLabyrinth class.
        /// </summary>
        /// <param name="whichEye">Left or right eye (or both).</param>
        /// <param name="cameraID">Camera ID from bus manager.</param>
        /// <param name="requestedFrameRate">Requested frame rate.</param>
        /// <param name="roi">Region of interest within the frame.</param>
        /// <param name="AutoExposure">Set exposure automatically?</param>
        public CameraEyePointGreyLabyrinth(Eye whichEye, ManagedPGRGuid cameraID, float requestedFrameRate, Rectangle roi, float shutterDuration, bool shouldUseAcc)
        {
            this.State = CameraState.Idle;

            this.WhichEye = whichEye;
            this.RequestedFrameRate = requestedFrameRate;
            this.RequestedShutterDuration = shutterDuration;
            if(shutterDuration <= 0) {
                this.AutoExposure = true;
            } else {
                this.AutoExposure = false;
            }
            
            this.AutoExposure = AutoExposure;

            this.PixelMode = Mode.Mode0;

            this.initialROI = roi;
            this.cameraID = cameraID;

            this.LoadDistortionMapping();

            this.camera = new ManagedCamera();
            this.camera.Connect(this.cameraID);

            this.camera.SetGPIOPinDirection(1, 0);
            this.shouldUseAcc = shouldUseAcc;

            // this.queue = new ImageEyeQueue(); //12-30-2014 JB - I don't think this is needed
        }

        #endregion

        #region public properties

        /// <summary>
        /// State of the camera. <see cref="CameraState" /> for more information.
        /// </summary>
        public CameraState State { get; private set; }

        /// <summary>
        /// Gets left or right eye (or both).
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        public ImageEyeQueue queue { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the camera is upside down. Images will be rotated 180 degrees.
        /// </summary>
        public bool IsUpsideDown { get; set; }

        /// <summary>
        /// Gets a value indicating whether looks at the eye trhough a mirror. Images will be horizontally flipped.
        /// </summary>
        public bool IsMirrored { get; set; }

        public int NumberOfBufferedHeadFrames { get;  private set; }

        /// <summary>
        /// Gets the diagnostics info.
        /// </summary>
        public string Diagnostics
        {
            get
            {
                string s = string.Empty;
                s = s + "cycles128sec".PadLeft(30) + " : " + this.cycles128sec.ToString() + "\r\n";

                s = s + "firstSeconds".PadLeft(30) + " : " + this.firstSecondsTimestamp.ToString() + "\r\n";
                s = s + "lastSeconds".PadLeft(30) + " : " + this.lastSecondsTimestamp.ToString() + "\r\n";

                s = s + "firstRawFrameNumber".PadLeft(30) + " : " + this.firstRawFrameNumber.ToString() + "\r\n";
                s = s + "lastRawFrameNumber".PadLeft(30) + " : " + this.lastRawFrameNumber.ToString() + "\r\n";
                s = s + "DroppedFramesCounter".PadLeft(30) + " : " + this.DroppedFramesCounter.ToString() + "\r\n";
                s = s + "timesFreqChanged".PadLeft(30) + " : " + this.timesFrameRateChanged.ToString() + "\r\n";
                s = s + "FramesCounter".PadLeft(30) + " : " + this.FrameCounter.ToString() + "\r\n";
                s = s + "HeadDataFramesCounter".PadLeft(30) + " : " + this.headDataFrameCounter.ToString() + "\r\n";
                s = s + "HeadFramesRemaining".PadLeft(30) + " : " + this.NumberOfBufferedHeadFrames.ToString() + "\r\n";

                var delay = (this.lastSecondsTimestamp - this.firstSecondsTimestamp) - ((this.lastRawFrameNumber - this.firstRawFrameNumber) / this.RequestedFrameRate);

                s = s + "delay".PadLeft(30) + " : " + delay.ToString() + "\r\n";
                s = s + "maxDelay".PadLeft(30) + " : " + this.maxDelay.ToString() + "\r\n";

                s = s + "lastFreq".PadLeft(30) + " : " + this.lastFrameRateRequested.ToString() + "\r\n";
                s = s + "reportedFrameRate".PadLeft(30) + " : " + this.reportedFrameRate.ToString() + "\r\n";

                s = s + "reportedShutterDuration".PadLeft(30) + " : " + this.reportedShutterDuration.ToString() + " \r\n";

                s = s + "\r\n";

                return s;
            }
        }

        /// <summary>
        /// Gets the frame rate requested to the camera.
        /// </summary>
        public double RequestedFrameRate { get; private set; }

        /// <summary>
        /// Gets the reported frame rate.
        /// </summary>
        public double ReportedFrameRate
        {
            get
            {
                //return this.camera.GetProperty(PropertyType.FrameRate).absValue;
                return reportedFrameRate; // Do this to prevent querying frame rate too frequently (Timestamp information is more important) 
            }
        }

        /// <summary>
        /// Gets the shutter duration requested to the camera.
        /// </summary>
        public float RequestedShutterDuration { get; private set; }

        /// <summary>
        /// Gets the reported shutter duration.
        /// </summary>
        public float ReportedShutterDuration
        {
            get
            {
                //return this.camera.GetProperty(PropertyType.FrameRate).absValue;
                return reportedShutterDuration;
            }
        }

        /// <summary>
        /// Gets the measured frame rate of the camera.
        /// Equals to total number of frames since starting capture divided by the
        /// amount of time (in seconds) since starting capturing.
        /// </summary>
        public double MeasuredFrameRate
        {
            get
            {
                return ((double)(this.lastRawFrameNumber - this.firstRawFrameNumber)) / (this.lastSecondsTimestamp - this.firstSecondsTimestamp);
            }
        }

        /// <summary>
        /// Frame counter starts at zero and increments every time a frame is captured.
        /// </summary>
        public long FrameCounter { get; protected set; }

        /// <summary>
        /// Dropped frame counter. Countes the number of frames grabbed by the camera but not received.
        /// </summary>
        public long DroppedFramesCounter { get; protected set; }

        /// <summary>
        /// Frame size of the camera. Do not abuse this property because it has 
        /// to request it from the camera.
        /// </summary>
        public Size FrameSize
        {
            get
            {
                return frameSize;
            }
        }
        private Size frameSize;

        /// <summary>
        /// Mode of the camera. 0 normal. 1 every four pixels. 2 skip half of the lines.
        /// </summary>
        public Mode PixelMode { get; set; }

        /// <summary>
        /// Sets whether exposure should be set automatically or manually
        /// </summary>
        public bool AutoExposure { get; set; }


        //JB 4-9-2015
        bool startedHead = false;

        #endregion

        #region public methods
        /// <summary>
        /// Initializes the camera.
        /// </summary>
        public void Init()
        {
            this.SetFormat7Settings();

            this.SetFrameRate((float)this.RequestedFrameRate);

            if (!this.AutoExposure)
            {
                var prop = this.camera.GetProperty(PropertyType.Shutter);
                prop.absControl = true;
                prop.absValue = this.RequestedShutterDuration;
                prop.autoManualMode = false;
                prop.onOff = true;
                this.camera.SetProperty(prop);
            }

            this.SetBufferMode(100);
            this.SetEmbeddedInfo();

            this.reportedFrameRate = this.camera.GetProperty(PropertyType.FrameRate).absValue;
            this.reportedShutterDuration = this.camera.GetProperty(PropertyType.Shutter).absValue;

            this.InitializeSerialRegisters();

            ClearBuffer();
            

            this.headDataFrameCounter = 0;

            //this.camera.SetGPIOPinDirection(1, 0);

            //this.camera.SetGPIOPinDirection(2, 1);
           
            

            
        }

        String CamCalibFilePath = @"C:\VOG\Labyrinth VOG Software\lib\CamCalib\";
        internal void LoadDistortionMapping()
        {
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1.xml");
            Map1 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2.xml");
            Map2 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));
        }

        // MAR
        /// <summary>
        /// Initialize camera serial registers.
        /// </summary>
        public void InitializeSerialRegisters()
        {
            this.camera.WriteRegister(SerSettingReg, SerSettingVal);
            ReadSerSettings = this.camera.ReadRegister(SerSettingReg);
            this.camera.WriteRegister(SerEnableReg, SerEnableValRec);

            ReadSerEnable = this.camera.ReadRegister(SerEnableReg);
        }
        /// <summary>
        /// Clear camera buffer (careful, this method will hang if the camera is unable to read from the buffer but it has contents)
        /// </summary>
        public void ClearBuffer()
        {
            InitializeSerialRegisters();
            lock (camera)
            {
                var waiting = false;
                while (((ReadSerEnable = this.camera.ReadRegister(SerEnableReg)) == 0x80200000 || ReadSerEnable == 0x80000000) && (ReadSerRec = this.camera.ReadRegister(SerRecReg)) > 0)
                {
                    if (ReadSerEnable != 0x80200000) // && ReadSerEnable != 0x80000000)
                    {
                        if (!waiting)
                        {
                            System.Diagnostics.Trace.WriteLine("Waiting for camera...");
                            waiting = true;
                        }
                        Thread.Sleep(1);
                        continue;
                    }
                    waiting = false;
                    if (ReadSerRec > 0)
                    {
                        this.camera.WriteRegister(SerRecReg, 0x00040000);
                        ReadSerNumBytes_Register = this.camera.ReadRegister(SerRecReg);
                        uint result = this.camera.ReadRegister(SerTrigReg);
                    }
                    else
                    {
                        break;
                    }
                    
                }
            }

        }

        public void ChangeShutterDuration(float shutterDuration)
        {
            this.RequestedShutterDuration = shutterDuration;
        }

        /// <summary>
        /// Starts capturing images. It is expected that the frame numbers start at 0 and they are always
        /// monotonic. There could be dropped frames though. The <see cref="EyeTrackerImageGrabber"/> object will take care of it.
        /// </summary>
        public void StartCapture()
        {
            
            this.ClearBuffer();
            
            this.headDataFrameCounter = this.FrameCounter;
            startedHead = false;
            this.firstFrameAcquired += StopCameraCallback;
            // this.camera.StartCapture(CameraCallback);
            cameraStopRequested = false;
            this.State = CameraState.Initialized;
            this.camera.StartCapture(CameraCallback);
            //this.camera.StopCapture();
            //System.Diagnostics.Trace.WriteLine("Camera fully initialized...restarting capture.");
            //this.State = CameraState.Tracking;
            //this.camera.StartCapture();
        }

        protected void OnFirstFrameAcquired(EventArgs e)
        {
            var handler = this.firstFrameAcquired;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void StopCameraCallback(object sender, EventArgs e)
        {
            try
            {
                this.StartStrobe(2);
                this.camera.SetCallback(null);
                // System.Diagnostics.Trace.WriteLine("Camera fully initialized.");
                this.State = CameraState.Tracking;
            }
            catch (Exception ex)
            {

            }
            
            
            this.camera.StartCapture();
        }

        private void CameraCallback(ManagedImage img)
        {
            if (!this.cameraStopRequested)
            {
                this.cameraStopRequested = true;
            }
            else
            {
                return;
            }
            Task.Run(() =>
            {
                // System.Diagnostics.Trace.WriteLine("Camera fully initialized...Restarting camera...");
                System.Diagnostics.Trace.WriteLine("Running camera callback.");
                this.camera.StopCapture();
                this.StartStrobe(2);
                this.ClearBuffer();
                
                this.camera.StartCapture();
                this.State = CameraState.Tracking;
                //this.camera.SetCallback(null);
                // System.Diagnostics.Trace.WriteLine("Camera fully initialized.");
                
            });
        }

        /// <summary>
        /// Stops capturing images.
        /// </summary>
        public void StopCapture()
        {
            this.State = CameraState.Finalizing;
            if (this.camera != null)
            {
                this.camera.StopCapture();
                this.StopStrobe(2);
                this.camera.Dispose();
            }
            this.State = CameraState.Idle;
        }

        /// <summary>
        /// Retrieves an image from the camera buffer.
        /// </summary>
        /// <returns>Image grabbed.</returns>
        public ImageEye GrabImageEye()
        {
            while (this.State == CameraState.Initialized)
            {
                Thread.Sleep(1);
            }
            if (this.State == CameraState.Finalizing)
            {
                return null;
            }
            if (this.lastFrameRateRequested == 0)
            {
                this.lastFrameRateRequested = (float)this.RequestedFrameRate;
            }

            using (var rawImage = new ManagedImage())
            {
                // Retrieve the image from the buffer
                try
                {
                    this.camera.RetrieveBuffer(rawImage);
                }
                catch (Exception e)
                {

                }
                

                this.FrameCounter++;
                var currentFrameNumber = rawImage.imageMetadata.embeddedFrameCounter;

                if (this.FrameCounter == 1)
                {
                    
                    this.firstRawFrameNumber = currentFrameNumber;
                    this.lastRawFrameNumber = this.firstRawFrameNumber - 1;
                    
                }

                //if(this.FrameCounter >= 1)
                //    this.StopStrobe(2);

                // Check for dropped frames
                this.DroppedFramesCounter += currentFrameNumber - (this.lastRawFrameNumber + 1);

                if (this.lastRawFrameNumber > currentFrameNumber && this.State != CameraState.Finalizing)
                {
                    System.Diagnostics.Trace.WriteLine(String.Format("Error details: lastRawFrameNumber: {0}, currentFrameNumber: {1}", this.lastRawFrameNumber, currentFrameNumber));
                    throw new InvalidOperationException("Frame counter is not consistently growing!.");
                }

                // Build the timestamp
                ImageEyeTimestamp timestamp = this.GetEyeTrackerTimeStamp(rawImage.timeStamp, currentFrameNumber);

                // Adjust the frame rate if it has drifted
                // this.AdjustFrameRate(rawImage.timeStamp, timestamp.Seconds);

                this.lastRawFrameNumber = currentFrameNumber;
                this.lastRawTimeStamp = rawImage.timeStamp;
                this.lastSecondsTimestamp = timestamp.Seconds;

                // Convert the image to OpenCV format
                unsafe
                {
                    //// TODO: make sure this is ok memorywise. I am afraid the rawImage object 
                    //// may be disposed and mess up with the image object
                    //// I actually don't understand very well why this works and never crashes. 
                    //// RawImage is inside a using so it should get disposed.
                    var image = new Image<Gray, byte>(
                             (int)rawImage.cols,
                             (int)rawImage.rows,
                             (int)rawImage.stride,
                             (IntPtr)rawImage.data);

                    //image = PerformDistortionCorrection(image);

                    var imageEye = new ImageEye(image.Clone(), this.WhichEye, timestamp);

                    //imageEye = EyeTrackerImageGrabber.CorrectImageOrientation(imageEye, this.IsMirrored, this.IsUpsideDown);

                    return imageEye;
                }
            }
        }

        /// <summary>
        /// Undistorts camera image
        /// </summary>
        /// <returns>Image</returns>
        private Image<Gray, byte> PerformDistortionCorrection(Image<Gray, byte> imageWhole)
        {

            //remap the image to the particular intrinsics
            //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
            //image is mapped backed, in the future this should be controllable through the flag '0'
            Image<Gray, Byte> tempImageWhole = imageWhole.CopyBlank();

            CvInvoke.cvRemap(imageWhole, tempImageWhole, Map1, Map2, 0, new MCvScalar(0));

            return tempImageWhole;
        }

        /// <summary>
        /// Read one frame of sensor data off the camera's data buffer
        /// </summary>
        /// <returns>A HeadData object containing the resulting sensor data</returns>
        public HeadData ReadSensors() //MAR, JB
        {
            var headData = new HeadData();
            GPIO_Val = this.camera.ReadRegister(GPIO_Ctrl);
            GPIO_Val = GPIO_Val & 0x00000001;
            headData.GPIO0 = GPIO_Val;

            // Added by Mehdi on 2019-12-23
            headData.CPUClockTime = DateTime.Now.ToString("HH:mm:ss.fff");
            if (this.State == CameraState.Idle)
            {
                headData.DataResult = HeadData.Result.Terminated;
                return headData;
            }
            /*if(this.State == CameraState.Finalizing)
            {
                headData.DataResult = HeadData.Result.Terminated;
                return headData;
            }*/

            ReadSerRec = this.camera.ReadRegister(SerRecReg);
            if (this.shouldUseAccTempGyro)
            {
                if ((ReadSerRec & 0xff000000) < 0xE000000)
                {
                    headData.DataResult = HeadData.Result.Empty;
                    // headData.TimestampSeconds = -1;
                    // System.Diagnostics.Trace.WriteLine("Missed head data (buffer is empty)");
                    return headData;
                }
                this.camera.WriteRegister(SerRecReg, 0xE0000);      // Push receive buffer          FLAG - this one - 0x70008

                ReadSerNumBytes_Register = this.camera.ReadRegister(SerRecReg);
                this.NumberOfBufferedHeadFrames = ((int)((ReadSerRec & 0xff000000) >> 24) - (int)((ReadSerNumBytes_Register & 0x00ff0000) >> 16)) / 12;
            }
            else if (this.shouldUseAcc)
            {
                if ((ReadSerRec & 0xff000000) < 0xC000000)
                {
                    headData.DataResult = HeadData.Result.Empty;
                    // headData.TimestampSeconds = -1;
                    // System.Diagnostics.Trace.WriteLine("Missed head data (buffer is empty)");
                    return headData;
                }
                this.camera.WriteRegister(SerRecReg, 0xC0000);      // Push receive buffer          FLAG - this one - 0x70008

                ReadSerNumBytes_Register = this.camera.ReadRegister(SerRecReg);
                this.NumberOfBufferedHeadFrames = ((int)((ReadSerRec & 0xff000000) >> 24) - (int)((ReadSerNumBytes_Register & 0x00ff0000) >> 16)) / 12;
            }
            else
            {
                if ((ReadSerRec & 0xff000000) < 0x6000000)
                {
                    headData.DataResult = HeadData.Result.Empty;
                    // headData.TimestampSeconds = -1;
                    // System.Diagnostics.Trace.WriteLine("Missed head data (buffer is empty)");
                    return headData;
                }
                this.camera.WriteRegister(SerRecReg, 0x60000);      // Push receive buffer          FLAG - this one - 0x70008

                ReadSerNumBytes_Register = this.camera.ReadRegister(SerRecReg);
                this.NumberOfBufferedHeadFrames = ((int)((ReadSerRec & 0xff000000) >> 24) - (int)((ReadSerNumBytes_Register & 0x00ff0000) >> 16)) / 6;
            }


            ReadSerNumBytes = (ReadSerNumBytes_Register & 0x00ff0000) >> 16;
            if (ReadSerNumBytes == 0)
            {
                headData.DataResult = HeadData.Result.Missed;
                // System.Diagnostics.Trace.WriteLine("Missed head data (read 0 bytes) -- buffer: " + (int)((ReadSerRec & 0xff000000) >> 24) + ", read: " +  (int)((ReadSerNumBytes & 0x00ff0000) >> 16));
                return headData;
            }
            if (!startedHead)
            {
                System.Diagnostics.Trace.WriteLine("Camera reports " + this.FrameCounter);
                startedHead = true;
            }
                
            ++this.headDataFrameCounter;

            SensorBuf[0] = this.camera.ReadRegister(SerTrigReg);
            SensorBuf[1] = this.camera.ReadRegister(SerTrigReg + 0x4);
            if (this.shouldUseAccTempGyro)
            {
                SensorBuf[2] = this.camera.ReadRegister(SerTrigReg + 0x8);
                SensorBuf[3] = this.camera.ReadRegister(SerTrigReg + 0xC);
            }
            else if (this.shouldUseAcc)
            {
                SensorBuf[2] = this.camera.ReadRegister(SerTrigReg + 0x8);
            }

            headData.NumSerBytesInBuffer = ReadSerNumBytes;

            GyroX = (short)(SensorBuf[0] >> 16); //(short)this.lastSecondsTimestamp; NSD - I can test the synchronization by passing along the timestamp value 
            GyroY = (short)(SensorBuf[0] & 0x0000FFFF);
            GyroZ = (short)(SensorBuf[1] >> 16);
            headData.GyroX = GyroX / gyroScalar;
            headData.GyroY = GyroY / gyroScalar;
            headData.GyroZ = GyroZ / gyroScalar;

            var timeStamp = new ImageEyeTimestamp();
            timeStamp.FrameNumber = this.headDataFrameCounter;
            headData.TimeStamp = timeStamp;

            if (this.shouldUseAccTempGyro)
            {
                Temper = (short)(SensorBuf[1] & 0x0000FFFF);
                AccX = (short)(SensorBuf[2] >> 16);
                AccY = (short)(SensorBuf[2] & 0x0000FFFF);
                AccZ = (short)(SensorBuf[3] >> 16);

                headData.Temperature = Temper;

                headData.AccelerometerX = AccX/accScalar;
                headData.AccelerometerY = AccY/accScalar;
                headData.AccelerometerZ = AccZ/accScalar;
            }
            // 8-22-2014 JB Added the following:
            else if (this.shouldUseAcc)
            {
                AccX = (short)(SensorBuf[1] & 0x0000FFFF);
                AccY = (short)(SensorBuf[2] >> 16);
                AccZ = (short)(SensorBuf[2] & 0x0000FFFF);
                headData.AccelerometerX = AccX / accScalar;
                headData.AccelerometerY = AccY / accScalar;
                headData.AccelerometerZ = AccZ / accScalar;
            }
            // System.Diagnostics.Trace.WriteLine("Head data recorded");
            // if (this.headDataFrameCounter < 5)
            //    System.Diagnostics.Trace.WriteLine(String.Format("{0}, {1}",(ReadSerNumBytes >> 24), headData.AccelerometerX));
            //    System.Diagnostics.Trace.WriteLine(String.Format("{4}: {0:F2},{1:F2},{2:F2} -- Gyro board reports {3}, {5} bytes in buffer, {6} frames acquired", headData.GyroX, headData.GyroY, headData.GyroZ, headData.AccelerometerX, this.headDataFrameCounter, (int)(ReadSerNumBytes >> 24), framesacquired));
            //headData.MagnetometerX = (int)(ReadSerNumBytes >> 24);

            //headData.GPIO0 = 10;


            //this.camera.SetGPIOPinDirection(1, 0);

            headData.DataResult = HeadData.Result.Normal;

            return headData;
        }

        public int CountRemainingHeadFrames()
        {
            ReadSerRec = this.camera.ReadRegister(SerRecReg);
            if (this.shouldUseAccTempGyro)
                return (int)((ReadSerRec & 0xff000000) >> 24) / 14;
            else if (this.shouldUseAcc)
                return (int)((ReadSerRec & 0xff000000) >> 24) / 12;
            else
                return (int)((ReadSerRec & 0xff000000) >> 24) / 6;
        }

        /// <summary>
        /// Read from a specified camera register.
        /// </summary>
        /// <param name="address">The address to read from</param>
        /// <returns>The value stored at specified address</returns>
        public uint ReadRegister(uint address)
        {
            return this.camera.ReadRegister(address);
        }
        public void ReadRegisterBlock(ushort addressHigh, uint addressLow, uint[] buffer)
        {
            this.camera.ReadRegisterBlock(addressHigh, addressLow, buffer);
        }

        /// <summary>
        /// Write a value to a specified camera register
        /// </summary>
        /// <param name="address">The address to write to</param>
        /// <param name="value">The value to write</param>
        public void WriteRegister(uint address, uint value)
        {
            this.camera.WriteRegister(address, value);
        }

        /// <summary>
        /// Start the strobe (1ms pulse on shutter close) through specified output pin
        /// </summary>
        /// <param name="pin">Pin to output strobe through</param>
        public void StartStrobe(int pin)
        {
            this.camera.SetGPIOPinDirection((uint)pin, 1);
            var strobeControl = this.camera.GetStrobe((uint)pin);
            if (!strobeControl.onOff)
            {
                /*strobeControl.onOff = true;
                strobeControl.duration = 0;
                strobeControl.polarity = 1;
                this.camera.SetStrobe(strobeControl);*/
                this.camera.WriteRegister(0x1508, 0x83000001);
                //this.camera.WriteRegister(0x1508, 0x83200001);
            }
        }
        /// <summary>
        /// Stop the strobe through specified output pin
        /// </summary>
        /// <param name="pin">Pin that strobe is outputted through</param>
        public void StopStrobe(int pin)
        {
            this.camera.SetGPIOPinDirection((uint)pin, 1);
            var strobeControl = this.camera.GetStrobe((uint)pin);
            strobeControl.onOff = false;
            this.camera.SetStrobe(strobeControl);
        }

        public void WriteRegisterBlock(ushort addressHigh, uint addressLow, uint[] buffer)
        {
            this.camera.WriteRegisterBlock(addressHigh, addressLow, buffer);
        }

        #endregion

        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        /// <param name="disposing">Value indicating wether it should disposes resources.</param>
        protected void Dispose(bool disposing)
        {
            if ( disposing)
            {

            }
        }

        #region private methods

        /// <summary>
        /// Sets the embedded info in the frame.
        /// </summary>
        private void SetEmbeddedInfo()
        {
            EmbeddedImageInfo embeddedInfo = this.camera.GetEmbeddedImageInfo();
            embeddedInfo.timestamp.onOff = true;
            embeddedInfo.brightness.onOff = true;
            embeddedInfo.exposure.onOff = true;
            embeddedInfo.frameCounter.onOff = true;
            embeddedInfo.gain.onOff = true;
            embeddedInfo.GPIOPinState.onOff = true;
            embeddedInfo.ROIPosition.onOff = true;
            embeddedInfo.shutter.onOff = true;
            embeddedInfo.strobePattern.onOff = true;
            embeddedInfo.whiteBalance.onOff = true;
            this.camera.SetEmbeddedImageInfo(embeddedInfo);
        }

        /// <summary>
        /// Sets the format7 image format (mainly ROI).
        /// </summary>
        private void SetFormat7Settings()
        {
            // Set the new Format 7 settings
            Format7ImageSettings newFmt7Settings = new Format7ImageSettings();

            switch (this.PixelMode)
            {
                case Mode.Mode0:
                    newFmt7Settings.mode = Mode.Mode0;
                    newFmt7Settings.offsetX = Convert.ToUInt32(this.initialROI.X);
                    newFmt7Settings.offsetY = Convert.ToUInt32(this.initialROI.Y);
                    newFmt7Settings.width = Convert.ToUInt32(this.initialROI.Width);
                    newFmt7Settings.height = Convert.ToUInt32(this.initialROI.Height);
                    break;
                case Mode.Mode1:
                    newFmt7Settings.mode = Mode.Mode1;
                    newFmt7Settings.offsetX = Convert.ToUInt32(this.initialROI.X / 2.0);
                    newFmt7Settings.offsetY = Convert.ToUInt32(this.initialROI.Y / 2.0);
                    newFmt7Settings.width = Convert.ToUInt32(this.initialROI.Width / 2.0);
                    newFmt7Settings.height = Convert.ToUInt32(this.initialROI.Height / 2.0);
                    break;
                case Mode.Mode2:
                    newFmt7Settings.mode = Mode.Mode2;
                    newFmt7Settings.offsetX = Convert.ToUInt32(this.initialROI.X);
                    newFmt7Settings.offsetY = Convert.ToUInt32(this.initialROI.Y / 2.0);
                    newFmt7Settings.width = Convert.ToUInt32(this.initialROI.Width);
                    newFmt7Settings.height = Convert.ToUInt32(this.initialROI.Height / 2.0);
                    break;
                default:
                    throw new Exception("Format7 settings are not good.");
            }
            // newFmt7Settings.pixelFormat = PixelFormat.PixelFormatMono8;
            newFmt7Settings.pixelFormat = PixelFormat.PixelFormatRaw8;

            bool good = true;
            Format7PacketInfo fmt7PacketInfo = this.camera.ValidateFormat7Settings(newFmt7Settings, ref good);
            if (good)
            {
                this.camera.SetFormat7Configuration(newFmt7Settings, fmt7PacketInfo.recommendedBytesPerPacket);
            }
            else
            {
                throw new Exception("Format7 settings are not good.");
            }
            frameSize = new Size(
                (int)newFmt7Settings.width,
                (int)newFmt7Settings.height);
        }

        /// <summary>
        /// Changes the frame rate of the camera.
        /// </summary>
        /// <param name="frameRate">Frame rate (Hz).</param>
        private void SetFrameRate(float frameRate)
        {
            var prop = this.camera.GetProperty(PropertyType.FrameRate);
            prop.absControl = true;
            prop.absValue = frameRate;
            prop.autoManualMode = false;
            prop.onOff = true;
            this.camera.SetProperty(prop);
        }

        /// <summary>
        /// Sets the camera to buffer frames mode.
        /// </summary>
        /// <remarks>
        /// Images accumulate in the user buffer, and the oldest image is grabbed for handling before 
        /// being discarded.
        /// This member can be used to guarantee that each image is seen. However, image processing 
        /// time must not exceed transmission time from the camera to the buffer. Grabbing blocks if 
        /// the camera has not finished transmitting the next available image. The buffer size is 
        /// by the number of Buffers parameter in the FC2Config struct. Note that this mode is the equivalent 
        /// of FlycaptureLockNext in earlier versions of the FlyCapture SDK.
        /// </remarks>
        /// <param name="bufferSize">Number of frames that can be buffered.</param>
        private void SetBufferMode(int bufferSize)
        {
            FC2Config config = this.camera.GetConfiguration();
            config.grabMode = GrabMode.BufferFrames;
            config.numBuffers = (uint)bufferSize;
            this.camera.SetConfiguration(config);
        }

        /// <summary>
        /// Converts the raw timestamps to timestamps in Seconds (accounting for 128 s cycles).
        /// </summary>
        /// <param name="timeStamp">Raw timestamp.</param>
        /// <param name="frameCounter">Frame counter.</param>
        /// <returns>Timestamp in seconds.</returns>
        private ImageEyeTimestamp GetEyeTrackerTimeStamp(TimeStamp timeStamp, long frameCounter)
        {

            var seconds = (double)timeStamp.cycleSeconds + (((double)timeStamp.cycleCount + ((double)timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
            seconds += this.cycles128sec * 128;

            var timestamp = new ImageEyeTimestamp();
            timestamp.Seconds = seconds;
            timestamp.DateTimeGrabbed = DateTime.Now;
            timestamp.FrameNumber = frameCounter - this.firstRawFrameNumber;
            timestamp.FrameNumberRaw = frameCounter;

            return timestamp;
        }
        #endregion
    }
}