﻿//-----------------------------------------------------------------------
// <copyright file="CameraEyeUEye.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Diagnostics;
    using System.Drawing;

    /// <summary>
    /// Class to control a uEye camera in the context of eye tracking.
    /// </summary>
    /// <remarks>These cameras are manufactured by IDS Imaging Development Systems GmbH, 
    /// Dimbacher Straße 6-8, 74182 Obersulm, Germany 
    /// http://en.ids-imaging.com/store/produkte/kameras/where/ids-family/le/ids-interface/usb-2.0.html.
    /// They have their our .NET api that is included in the uEyeDotNET.dll library.
    /// </remarks>
    internal class CameraEyeUEye : ICameraEye
    {
        /// <summary>
        /// Camera object.
        /// </summary>
        private uEye.Camera camera;

        /// <summary>
        /// Stopwatch used to create timestamps for the frames.
        /// </summary>
        private Stopwatch timecounter = new Stopwatch();

        /// <summary>
        /// Temporary buffer for frames.
        /// </summary>
        private Bitmap lastBitmap = null;

        /// <summary>
        /// Initializes a new instance of the CameraEyeUEye class.
        /// </summary>
        /// <param name="whichEye">Left or right eye (or both).</param>
        /// <param name="requestedFrameRate">Requested frame rate.</param>
        internal CameraEyeUEye(Eye whichEye, float requestedFrameRate)
        {
            this.WhichEye = whichEye;
            this.RequestedFrameRate = requestedFrameRate;

            this.queue = new ImageEyeQueue();
        }

        /// <summary>
        /// Gets left or right eye (or both).
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        public ImageEyeQueue queue { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the camera is upside down. Images will be rotated 180 degrees.
        /// </summary>
        public bool IsUpsideDown { get; set; }

        /// <summary>
        /// Gets a value indicating whether looks at the eye trhough a mirror. Images will be horizontally flipped.
        /// </summary>
        public bool IsMirrored { get; set; }

        /// <summary>
        /// Gets the diagnostics info.
        /// </summary>
        public string Diagnostics
        {
            get
            {
                string s = string.Empty;

                return s;
            }
        }

        /// <summary>
        /// Gets the frame rate requested to the camera.
        /// </summary>
        public double RequestedFrameRate { get; private set; }

        /// <summary>
        /// Frame rate of the camera.
        /// </summary>
        public double ReportedFrameRate
        {
            get
            {
                double frameRate = 0;
                this.camera.Timing.Framerate.Get(out frameRate);
                return frameRate;
            }
        }

        /// <summary>
        /// Frame counter starts at zero and increments every time a frame is captured.
        /// </summary>
        public long FrameCounter { get; protected set; }

        /// <summary>
        /// Dropped frame counter. Countes the number of frames grabbed by the camera but not received.
        /// </summary>
        public long DroppedFramesCounter { get; protected set; }

        /// <summary>
        /// Frame size of the camera.
        /// </summary>
        public Size FrameSize
        {
            get
            {
                Rectangle aoi = new Rectangle();
                this.camera.Size.AOI.Get(out aoi);
                return aoi.Size;
            }
        }

        /// <summary>
        /// Starts capturing images.
        /// </summary>
        public void StartCapture()
        {
            // Connect Event
            this.camera.EventFrame += OnFrameEvent;
        }

        /// <summary>
        /// Stops capturing images.
        /// </summary>
        public void StopCapture()
        {
            if (this.camera != null)
            {
                this.camera.Acquisition.Stop();
                this.camera = null;
            }

            if (this.lastBitmap != null)
            {
                this.lastBitmap.Dispose();
                this.lastBitmap = null;
            }
        }

        /// <summary>
        /// Retrieves an image from the camera buffer.
        /// </summary>
        /// <returns>Image grabbed.</returns>
        public ImageEye GrabImageEye()
        {
            while (lastBitmap == null)
            {
                System.Threading.Thread.Sleep(1);
            }

            // Build the timestamp
            // Set up the timestamp for the image
            ImageEyeTimestamp timestamp = new ImageEyeTimestamp();
            timestamp.FrameNumber = this.FrameCounter++;
            timestamp.Seconds = this.timecounter.ElapsedMilliseconds / 1000;

            var bitmap = lastBitmap;
            this.lastBitmap = null;

            // Convert the image to OpenCV format
            var imageEye = new ImageEye(bitmap, this.WhichEye, timestamp);

            imageEye = EyeTrackerImageGrabber.CorrectImageOrientation(imageEye, this.IsMirrored, this.IsUpsideDown);

            return imageEye;
        }

        /// <summary>
        /// Initializes the camera.
        /// </summary>
        public void Init()
        {
            this.camera = new uEye.Camera();

            uEye.Defines.Status statusRet = 0;

            // Open Camera
            statusRet = this.camera.Init();
            if (statusRet != uEye.Defines.Status.SUCCESS)
            {
                System.Diagnostics.Trace.WriteLine("Camera initializing failed");
                return;
            }

            // Allocate Memory
            Int32 s32MemID;
            statusRet = this.camera.Memory.Allocate(out s32MemID, true);
            if (statusRet != uEye.Defines.Status.SUCCESS)
            {
                System.Diagnostics.Trace.WriteLine("Allocate Memory failed");
                return;
            }

            // Start Live Video
            statusRet = this.camera.Acquisition.Capture();
            if (statusRet != uEye.Defines.Status.SUCCESS)
            {
                System.Diagnostics.Trace.WriteLine("Start Live Video failed");
                return;
            }

            this.camera.Size.AOI.Set(new Rectangle(220, 350, 800, 300));

            uEye.Types.Range<Double> range;

            statusRet = this.camera.Timing.Framerate.GetFrameRateRange(out range);

            Double value;
            statusRet = this.camera.Timing.Framerate.Get(out value);

            this.camera.Timing.Framerate.Set(60.0);

            double f;
            this.camera.Timing.Exposure.Get(out f);

            this.camera.Gain.Hardware.Scaled.SetMaster(10);

            this.camera.Focus.Zone.SetAOI(new Rectangle(100, 75, 200, 150));

            //// this.Timing.Exposure.Set(20);
        }

        
        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        /// <param name="disposing">Value indicating wether it should disposes resources.</param>
        protected void Dispose(bool disposing)
        {
            if ( disposing)
            {
            }
        }

        /// <summary>
        /// Handles the FrameEvent.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void OnFrameEvent(object sender, EventArgs e)
        {
            uEye.Camera Camera = sender as uEye.Camera;

            Int32 s32MemID;
            Camera.Memory.GetActive(out s32MemID);

            Bitmap bitmap = null;
            try
            {
                bitmap = new Bitmap(1280, 1024);
                this.camera.Memory.ToBitmap(s32MemID, out bitmap);

                this.lastBitmap = bitmap;
                bitmap = null;
            }
            finally
            {
                if (bitmap != null)
                {
                    bitmap.Dispose();
                }
            }
        }
    }
}
