﻿//-----------------------------------------------------------------------
// <copyright file="CameraEyePointGrey.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using FlyCapture2Managed;

    /// <summary>
    /// Class to control a point grey camera in the context of eye tracking.
    /// </summary>
    public class CameraEyePointGrey : ICameraEye, IMovableImageEyeSource, IDisposable
    {
        #region fields

        /// <summary>
        /// Camera object.
        /// </summary>
        private ManagedCamera camera;

        /// <summary>
        /// Gets the initial ROI of the camera.
        /// </summary>
        private Rectangle initialROI;

        /// <summary>
        /// Frame rate reported by the camera.
        /// </summary>
        private float reportedFrameRate = 0;

        /// <summary>
        /// Last frame rate requested to the camera.
        /// </summary>
        private float lastFrameRateRequested = 0;

        /// <summary>
        /// Number of frame when the last change of frame rate was requested.
        /// </summary>
        private float lastFrameRateChangeFrameNumber = 0;

        /// <summary>
        /// Counter of how many times the frame rate has been changed.
        /// </summary>
        private long timesFrameRateChanged = 0;

        /// <summary>
        /// Maximum delay recorded between one frame timestamp and the expected time. For 
        /// instance the frame 110 at 100Hz should occur 1.100 seconds after the first frame.
        /// </summary>
        private double maxDelay = 0;

        /// <summary>
        /// How many cycles of 128 seconds (maximum raw timestamp) have happened. This is
        /// necessary to calculate good timestamps references to the beginning.
        /// </summary>
        private long cycles128sec = 0;

        /// <summary>
        /// Timestamp in seconds of the first frame (correcting for 128 sec cycles).
        /// </summary>
        private double firstSecondsTimestamp = -1;

        /// <summary>
        /// Timestamp in seconds of the last frame (correcting for 128 sec cycles).
        /// </summary>
        public double lastSecondsTimestamp = 0;

        /// <summary>
        /// Raw timestamp of the last frame.
        /// </summary>
        private TimeStamp lastRawTimeStamp = new TimeStamp();

        /// <summary>
        /// Frame number of the first frame.
        /// </summary>
        private long firstRawFrameNumber = -1;

        /// <summary>
        /// Frame number of the last frame.
        /// </summary>
        private long lastRawFrameNumber = 0;

        private ManagedPGRGuid cameraID;

        #endregion

        #region constructor

        /// <summary>
        /// Initializes a new instance of the CameraEyePointGrey class.
        /// </summary>
        /// <param name="whichEye">Left or right eye (or both).</param>
        /// <param name="cameraID">Camera ID from bus manager.</param>
        /// <param name="requestedFrameRate">Requested frame rate.</param>
        /// <param name="roi">Region of interest within the frame.</param>
        public CameraEyePointGrey(Eye whichEye, ManagedPGRGuid cameraID, float requestedFrameRate, Rectangle roi)
        {
            this.WhichEye = whichEye;
            this.RequestedFrameRate = requestedFrameRate;
            this.AutoExposure = true;

            this.initialROI = roi;
            this.cameraID = cameraID;

            this.camera = new ManagedCamera();
            this.camera.Connect(this.cameraID);

            this.queue = new ImageEyeQueue();
        }

        #endregion

        #region public properties

        /// <summary>
        /// Gets left or right eye (or both).
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        public ImageEyeQueue queue { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the camera is upside down. Images will be rotated 180 degrees.
        /// </summary>
        public bool IsUpsideDown { get; set; }

        /// <summary>
        /// Gets a value indicating whether looks at the eye trhough a mirror. Images will be horizontally flipped.
        /// </summary>
        public bool IsMirrored { get; set; }

        /// <summary>
        /// Gets the diagnostics info.
        /// </summary>
        public string Diagnostics
        {
            get
            {
                string s = string.Empty;
                s = s + "cycles128sec".PadLeft(30) + " : " + this.cycles128sec.ToString() + "\r\n";

                s = s + "firstSeconds".PadLeft(30) + " : " + this.firstSecondsTimestamp.ToString() + "\r\n";
                s = s + "lastSeconds".PadLeft(30) + " : " + this.lastSecondsTimestamp.ToString() + "\r\n";

                s = s + "firstRawFrameNumber".PadLeft(30) + " : " + this.firstRawFrameNumber.ToString() + "\r\n";
                s = s + "lastRawFrameNumber".PadLeft(30) + " : " + this.lastRawFrameNumber.ToString() + "\r\n";
                s = s + "DroppedFramesCounter".PadLeft(30) + " : " + this.DroppedFramesCounter.ToString() + "\r\n";
                s = s + "timesFreqChanged".PadLeft(30) + " : " + this.timesFrameRateChanged.ToString() + "\r\n";
                s = s + "FramesCounter".PadLeft(30) + " : " + this.FrameCounter.ToString() + "\r\n";

                var delay = (this.lastSecondsTimestamp - this.firstSecondsTimestamp) - ((this.lastRawFrameNumber - this.firstRawFrameNumber) / this.RequestedFrameRate);

                s = s + "delay".PadLeft(30) + " : " + delay.ToString() + "\r\n";
                s = s + "maxDelay".PadLeft(30) + " : " + this.maxDelay.ToString() + "\r\n";

                s = s + "lastFreq".PadLeft(30) + " : " + this.lastFrameRateRequested.ToString() + "\r\n";
                s = s + "reportedFrameRate".PadLeft(30) + " : " + this.reportedFrameRate.ToString() + "\r\n";
                s = s + "\r\n";

                return s;
            }
        }

        /// <summary>
        /// Gets the frame rate requested to the camera.
        /// </summary>
        public double RequestedFrameRate { get; private set; }

        /// <summary>
        /// Gets the reported frame rate.
        /// </summary>
        public double ReportedFrameRate
        {
            get
            {
                return this.camera.GetProperty(PropertyType.FrameRate).absValue;
            }
        }

        /// <summary>
        /// Gets the measured frame rate of the camera.
        /// Equals to total number of frames since starting capture divided by the
        /// amount of time (in seconds) since starting capturing.
        /// </summary>
        public double MeasuredFrameRate
        {
            get
            {
                return ((double)(this.lastRawFrameNumber - this.firstRawFrameNumber)) / (this.lastSecondsTimestamp - this.firstSecondsTimestamp);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the camera should
        /// continuously monitor and adjust the frame rate.
        /// </summary>
        public bool ShouldAdjustFrameRate { get; set; }

        /// <summary>
        /// Frame counter starts at zero and increments every time a frame is captured.
        /// </summary>
        public long FrameCounter { get; protected set; }

        /// <summary>
        /// Dropped frame counter. Countes the number of frames grabbed by the camera but not received.
        /// </summary>
        public long DroppedFramesCounter { get; protected set; }

        /// <summary>
        /// Frame size of the camera.Do not abuse this property because it has 
        /// to request it from the camera.
        /// </summary>
        public Size FrameSize
        {
            get
            {
                VideoMode videoMode = VideoMode.NumberOfVideoModes;
                FrameRate frameRate = FrameRate.FrameRate1_875;

                this.camera.GetVideoModeAndFrameRate(ref videoMode, ref frameRate);

                switch (videoMode)
                {
                    case VideoMode.VideoMode1024x768Rgb:
                    case VideoMode.VideoMode1024x768Y16:
                    case VideoMode.VideoMode1024x768Y8:
                    case VideoMode.VideoMode1024x768Yuv422:
                        return new Size(1024, 768);
                    case VideoMode.VideoMode1280x960Rgb:
                    case VideoMode.VideoMode1280x960Y16:
                    case VideoMode.VideoMode1280x960Y8:
                    case VideoMode.VideoMode1280x960Yuv422:
                        return new Size(1280, 960);
                    case VideoMode.VideoMode1600x1200Rgb:
                    case VideoMode.VideoMode1600x1200Y16:
                    case VideoMode.VideoMode1600x1200Y8:
                    case VideoMode.VideoMode1600x1200Yuv422:
                        return new Size(1600, 1200);
                    case VideoMode.VideoMode160x120Yuv444:
                        return new Size(160, 120);
                    case VideoMode.VideoMode320x240Yuv422:
                        return new Size(320, 240);
                    case VideoMode.VideoMode640x480Rgb:
                    case VideoMode.VideoMode640x480Y16:
                    case VideoMode.VideoMode640x480Y8:
                    case VideoMode.VideoMode640x480Yuv411:
                    case VideoMode.VideoMode640x480Yuv422:
                        return new Size(640, 480);
                    case VideoMode.VideoMode800x600Rgb:
                    case VideoMode.VideoMode800x600Y16:
                    case VideoMode.VideoMode800x600Y8:
                    case VideoMode.VideoMode800x600Yuv422:
                        return new Size(800, 600);
                    case VideoMode.VideoModeFormat7:

                        Format7ImageSettings currFmt7Settings = new Format7ImageSettings();
                        uint currPacketSize = 0;
                        float percentage = 0.0f; // Don't need to keep this
                        this.camera.GetFormat7Configuration(currFmt7Settings, ref currPacketSize, ref percentage);

                        return new Size(
                            (int)currFmt7Settings.width,
                            (int)currFmt7Settings.height);
                    default:
                        return new Size();
                }
            }
        }

        /// <summary>
        /// Mode of the camera. 0 normal. 1 every four pixels. 2 skip half of the lines.
        /// </summary>
        public Mode PixelMode { get; set; }

        #endregion

        #region public methods

        /// <summary>
        /// Initializes the camera.
        /// </summary>
        public void Init()
        {
            this.SetFormat7Settings();

            this.SetFrameRate((float)this.RequestedFrameRate);

            if (!this.AutoExposure)
            {
                var prop = this.camera.GetProperty(PropertyType.Shutter);
                prop.absControl = true;
                prop.absValue = this.ShutterDuration;
                prop.autoManualMode = false;
                prop.onOff = true;
                this.camera.SetProperty(prop);

                prop = this.camera.GetProperty(PropertyType.Gain);
                prop.absControl = true;
                prop.absValue = this.Gain;
                prop.autoManualMode = false;
                prop.onOff = true;
                this.camera.SetProperty(prop);
            }
            //// TODO: I would like to make this more consistent across cameras
            //// right now it is a bit messy, when do they actually start to get images
        }

        public bool AutoExposure { get; set; }
        public float ShutterDuration { get; set; }
        public float Gain { get; set; }

        /// <summary>
        /// Starts capturing images. It is expected that the frame numbers start at 0 and they are always
        /// monotonic. There could be dropped frames though. The <see cref="EyeTrackerImageGrabber"/> object will take care of it.
        /// </summary>
        public void StartCapture()
        {
            this.SetBufferMode(100);
            this.SetEmbeddedInfo();

            this.camera.StartCapture();
        }

        /// <summary>
        /// Stops capturing images.
        /// </summary>
        public void StopCapture()
        {
            if (this.camera != null)
            {
                this.camera.StopCapture();
                this.camera.Dispose();
            }
        }

        /// <summary>
        /// Retrieves an image from the camera buffer.
        /// </summary>
        /// <returns>Image grabbed.</returns>
        public ImageEye GrabImageEye()
        {
            if (this.lastFrameRateRequested == 0)
            {
                this.lastFrameRateRequested = (float)this.RequestedFrameRate;
            }

            using (var rawImage = new ManagedImage())
            {
                // Retrieve the image from the buffer
                this.camera.RetrieveBuffer(rawImage);

                this.FrameCounter++;
                var currentFrameNumber = rawImage.imageMetadata.embeddedFrameCounter;

                if (this.FrameCounter == 1)
                {
                    this.firstRawFrameNumber = currentFrameNumber;
                    this.lastRawFrameNumber = this.firstRawFrameNumber - 1;
                }

                // Check for dropped frames
                this.DroppedFramesCounter += currentFrameNumber - (this.lastRawFrameNumber + 1);

                if (this.lastRawFrameNumber > currentFrameNumber)
                {
                    throw new InvalidOperationException("Frame counter is not consistently growing!.");
                }

                // Build the timestamp
                ImageEyeTimestamp timestamp = this.GetEyeTrackerTimeStamp(rawImage.timeStamp, currentFrameNumber);

                // Adjust the frame rate if it has drifted
                this.AdjustFrameRate(rawImage.timeStamp, timestamp.Seconds);

                this.lastRawFrameNumber = currentFrameNumber;
                this.lastRawTimeStamp = rawImage.timeStamp;
                this.lastSecondsTimestamp = timestamp.Seconds;

                // Convert the image to OpenCV format
                unsafe
                {
                    //// TODO: make sure this is ok memorywise. I am afraid the rawImage object 
                    //// may be disposed and mess up with the image object
                    //// I actually don't understand very well why this works and never crashes. 
                    //// RawImage is inside a using so it should get disposed.
                    var image = new Image<Gray, byte>(
                             (int)rawImage.cols,
                             (int)rawImage.rows,
                             (int)rawImage.stride,
                             (IntPtr)rawImage.data);

                    var imageEye = new ImageEye(image, this.WhichEye, timestamp);

                    imageEye = EyeTrackerImageGrabber.CorrectImageOrientation(imageEye, this.IsMirrored, this.IsUpsideDown);

                    return imageEye;
                }
            }
        }

        public uint ReadRegister(uint address)
        {
            return this.camera.ReadRegister(address);
        }
        public void ReadRegisterBlock(ushort addressHigh, uint addressLow, uint[] buffer)
        {
            this.camera.ReadRegisterBlock(addressHigh, addressLow, buffer);
        }
        public void WriteRegister(uint address, uint value)
        {
            this.camera.WriteRegister(address, value);
        }
        public void StartStrobe(int pin)
        {
            this.camera.SetGPIOPinDirection((uint)pin, 1);
            var strobeControl = this.camera.GetStrobe((uint)pin);
            if (!strobeControl.onOff)
            {
                strobeControl.onOff = true;
                strobeControl.duration = 1;
                strobeControl.polarity = 1;
                this.camera.SetStrobe(strobeControl);
            }
        }

        public virtual void WriteRegisterBlock(ushort addressHigh, uint addressLow, uint[] buffer)
        {
            this.camera.WriteRegisterBlock(addressHigh, addressLow, buffer);
        }

        /// <summary>
        /// Center camera (or ROI) around a point.
        /// </summary>
        /// <param name="centerPupil">Center of the eye.</param>
        public void Center(PointF centerPupil)
        {
            var info = this.camera.GetCameraInfo();

            var s = info.sensorResolution;

            var xpos = s.IndexOf("x");
            var rows = int.Parse(s.Substring(0, xpos));
            var cols = int.Parse(s.Substring(xpos + 1, s.Length - xpos - 1));

            Format7ImageSettings currFmt7Settings = new Format7ImageSettings();
            uint currPacketSize = 0;
            float percentage = 0.0f; // Don't need to keep this
            this.camera.GetFormat7Configuration(currFmt7Settings, ref currPacketSize, ref percentage);

            var xmax = (int)(rows - currFmt7Settings.width) - 4;
            var ymax = (int)(cols - currFmt7Settings.height) - 4;

            Point roiOrigin = new Point();

            // Round the center to a multiple of 4
            if (this.IsUpsideDown)
            {
                roiOrigin = new Point(
                    (int)(currFmt7Settings.offsetX + (Math.Round((centerPupil.X - (currFmt7Settings.width / 2)) / 4) * 4)),
                    (int)(currFmt7Settings.offsetY + (Math.Round((-centerPupil.Y + (currFmt7Settings.height / 2)) / 4) * 4)));
            }
            else
            {
                roiOrigin = new Point(
                    (int)(currFmt7Settings.offsetX + (Math.Round((-centerPupil.X + (currFmt7Settings.width / 2)) / 4) * 4)),
                    (int)(currFmt7Settings.offsetY + (Math.Round((centerPupil.Y - (currFmt7Settings.height / 2)) / 4) * 4)));
            }

            // Bound the center
            roiOrigin = new Point(
                (int)Math.Min(xmax, Math.Max(4, roiOrigin.X)),
                (int)Math.Min(ymax, Math.Max(4, roiOrigin.Y)));

            currFmt7Settings.offsetX = Convert.ToUInt32(roiOrigin.X);
            currFmt7Settings.offsetY = Convert.ToUInt32(roiOrigin.Y);

            bool isgood = false;

            try
            {
                // Validate the settings to make sure that they are valid
                this.camera.ValidateFormat7Settings(currFmt7Settings, ref isgood);

                if (!isgood)
                {
                    return;
                }

                this.camera.StopCapture();

                // Get the image settings from the page
                this.camera.SetFormat7Configuration(currFmt7Settings, currPacketSize);
            }
            catch (FC2Exception)
            {
                System.Diagnostics.Trace.WriteLine("There was an error setting the Format7 settings");
            }

            try
            {
                // Restart the camera if it was running beforehand.
                this.camera.StartCapture();
            }
            catch (FC2Exception)
            {
                System.Diagnostics.Trace.WriteLine("There was an error restarting the camera.");
            }
        }

        /// <summary>
        /// Move the camera in a given direction.
        /// </summary>
        /// <param name="direction">Direction to move.</param>
        public void Move(MovementDirection direction)
        {
            // TODO: try this one the micromedical camera
            int step = 24;

            if (this.IsUpsideDown)
            {
                switch (direction)
                {
                    ////case CameraMovementDirection.Up:
                    ////    direction = CameraMovementDirection.Down;
                    ////    break;
                    ////case CameraMovementDirection.Down:
                    ////    direction = CameraMovementDirection.Up;
                    ////    break;
                    case MovementDirection.Left:
                        direction = MovementDirection.Right;
                        break;
                    case MovementDirection.Right:
                        direction = MovementDirection.Left;
                        break;
                    default:
                        break;
                }
            }

            Format7ImageSettings currFmt7Settings = new Format7ImageSettings();
            uint currPacketSize = 0;
            float percentage = 0.0f; // Don't need to keep this

            this.camera.GetFormat7Configuration(currFmt7Settings, ref currPacketSize, ref percentage);
            switch (direction)
            {
                case MovementDirection.Up:
                    currFmt7Settings.offsetY = Convert.ToUInt32(Math.Min((int)currFmt7Settings.offsetY + step, 200));
                    break;
                case MovementDirection.Down:
                    currFmt7Settings.offsetY = Convert.ToUInt32(Math.Max((int)currFmt7Settings.offsetY - step, 4));
                    break;
                case MovementDirection.Left:
                    currFmt7Settings.offsetX = Convert.ToUInt32(Math.Max((int)currFmt7Settings.offsetX - step, 4));
                    break;
                case MovementDirection.Right:
                    currFmt7Settings.offsetX = Convert.ToUInt32(Math.Min((int)currFmt7Settings.offsetX + step, 300));
                    break;
                default:
                    break;
            }

            bool isgood = false;

            // Validate the settings to make sure that they are valid
            this.camera.ValidateFormat7Settings(currFmt7Settings, ref isgood);
            if (!isgood)
            {
                return;
            }

            try
            {
                this.camera.StopCapture();

                // Get the image settings from the page
                this.camera.SetFormat7Configuration(currFmt7Settings, currPacketSize);
            }
            catch (FC2Exception)
            {
                System.Diagnostics.Trace.WriteLine("There was an error setting the Format7 settings");
                return;
            }

            try
            {
                // Restart the camera if it was running beforehand.
                this.camera.StartCapture();
            }
            catch (FC2Exception)
            {
                System.Diagnostics.Trace.WriteLine("There was an error restarting the camera.");
            }
        }

        /// <summary>
        /// Sets the GPIOs.
        /// </summary>
        internal void SetGPIO()
        {
            // Disable the Micromedical LEDs;       
            this.camera.SetGPIOPinDirection(1, 1);
            this.camera.SetGPIOPinDirection(1, 0);

            //// // Sets the gpio number 1 to output the strobe
            ////var strobeControl = camera.GetStrobe(1);
            ////strobeControl.onOff = true;
            ////camera.SetStrobe(strobeControl);
        }

        /// <summary>
        /// Get the current timestamp at the camera.
        /// </summary>
        /// <returns>The seconds of the timestamp.</returns>
        public double GetCurrentSeconds()
        {
            var timeStamp = this.camera.GetCycleTime();

            var seconds = (double)timeStamp.cycleSeconds + (((double)timeStamp.cycleCount + ((double)timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
            seconds += this.cycles128sec * 128;

            return seconds;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Synchronizes two cameras so the shutter opens more or less at the same time.
        /// </summary>
        /// <param name="cameras">List of cameras to sync.</param>
        /// <param name="frameRate">Desired frame rate.</param>
        internal static void SyncCameras(CameraEyePointGrey[] cameras, float frameRate)
        {
            if (cameras.Length != 2)
            {
                throw new InvalidOperationException("Only two cameras can be synchronized.");
            }

            foreach (var camera in cameras)
            {
                camera.SetFrameRate(frameRate);
            }

            // Empty the buffers of the cameras
            // To do that check the current timestamp in the camera and compare it with the timestamps in
            // the frames. If the difference is large it means that the frame was captured long time ago
            // and it must come from the buffer. Wait until frames from both cameras are captured without delay
            // and then proceed with the syncrhonization.
            var delayCameraFrame = double.MaxValue;
            var delay0 = double.MaxValue;
            var delay1 = double.MaxValue;
            var counter0 = 0;
            var counter1 = 0;

            while (delayCameraFrame > 0.01)
            {

                if (delay0 > 0.01)
                {
                    using (var rawImage = new ManagedImage())
                    {
                        // Retrieve the image from the buffer
                        cameras[0].camera.RetrieveBuffer(rawImage);

                        var secondsFrame0 = (double)rawImage.timeStamp.cycleSeconds + (((double)rawImage.timeStamp.cycleCount + ((double)rawImage.timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
                        var secondsCamera0 = cameras[0].GetCurrentSeconds();

                        delay0 = secondsCamera0 - secondsFrame0;

                        counter0++;
                    }
                }

                if (delay1 > 0.01)
                {
                    using (var rawImage = new ManagedImage())
                    {
                        // Retrieve the image from the buffer
                        cameras[1].camera.RetrieveBuffer(rawImage);

                        var secondsFrame1 = (double)rawImage.timeStamp.cycleSeconds + (((double)rawImage.timeStamp.cycleCount + ((double)rawImage.timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
                        var secondsCamera1 = cameras[1].GetCurrentSeconds();

                        delay1 = secondsCamera1 - secondsFrame1;

                        counter1++;
                    }
                }

                delayCameraFrame = Math.Max(delay0, delay1);
            }

            System.Diagnostics.Trace.WriteLine(string.Format("Camera buffers cleared {0} from camera0 and {1} from camera1", counter0, counter1));


            for (int i = 0; i < 1000; i++)
            {
                var seconds0 = 0.0;
                var seconds1 = 0.0;

                using (var rawImage = new ManagedImage())
                {
                    // Retrieve the image from the buffer
                    cameras[0].camera.RetrieveBuffer(rawImage);
                    seconds0 = (double)rawImage.timeStamp.cycleSeconds + (((double)rawImage.timeStamp.cycleCount + ((double)rawImage.timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
                    cameras[0].lastRawFrameNumber = rawImage.imageMetadata.embeddedFrameCounter;
                }

                using (var rawImage = new ManagedImage())
                {
                    // Retrieve the image from the buffer
                    cameras[1].camera.RetrieveBuffer(rawImage);
                    seconds1 = (double)rawImage.timeStamp.cycleSeconds + (((double)rawImage.timeStamp.cycleCount + ((double)rawImage.timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
                    cameras[1].lastRawFrameNumber = rawImage.imageMetadata.embeddedFrameCounter;
                }

                var delay = seconds1 - seconds0;

                // Adjust the frame rate of the camera to fix the delay if bigger than 2 ms
                float newFrameRate0 = cameras[0].lastFrameRateRequested;
                float newFrameRate1 = cameras[1].lastFrameRateRequested;
                if (Math.Abs(delay) > 0.02)
                {
                    // speed up or slow down the camera
                    newFrameRate0 = cameras[0].lastFrameRateRequested - ((float)Math.Sign(delay) * 2f);
                    newFrameRate1 = cameras[1].lastFrameRateRequested + ((float)Math.Sign(delay) * 2f);

                    // Never go more than 10Hz above or below the requested frame rate
                    newFrameRate0 = (float)Math.Max(newFrameRate0, cameras[0].RequestedFrameRate - 20);
                    newFrameRate0 = (float)Math.Min(newFrameRate0, cameras[0].RequestedFrameRate + 20);
                    newFrameRate1 = (float)Math.Max(newFrameRate1, cameras[1].RequestedFrameRate - 20);
                    newFrameRate1 = (float)Math.Min(newFrameRate1, cameras[1].RequestedFrameRate + 20);
                }
                else
                {
                    if (Math.Abs(delay) > 0.002)
                    {
                        // speed up or slow down the camera
                        newFrameRate0 = cameras[0].lastFrameRateRequested - ((float)Math.Sign(delay) * 0.5f);
                        newFrameRate1 = cameras[1].lastFrameRateRequested + ((float)Math.Sign(delay) * 0.5f);

                        // Never go more than 1Hz above or below the requested frame rate
                        newFrameRate0 = (float)Math.Max(newFrameRate0, cameras[0].RequestedFrameRate - 5);
                        newFrameRate0 = (float)Math.Min(newFrameRate0, cameras[0].RequestedFrameRate + 5);
                        newFrameRate1 = (float)Math.Max(newFrameRate1, cameras[1].RequestedFrameRate - 5);
                        newFrameRate1 = (float)Math.Min(newFrameRate1, cameras[1].RequestedFrameRate + 5);
                    }
                    else
                    {
                        if (Math.Abs(delay) < 0.001)
                        {
                            // If the delay is very small go back to the requested framerate
                            cameras[0].SetFrameRate(frameRate);
                            cameras[1].SetFrameRate(frameRate);

                            cameras[0].timesFrameRateChanged++;
                            cameras[0].reportedFrameRate = cameras[0].camera.GetProperty(PropertyType.FrameRate).absValue;

                            cameras[1].timesFrameRateChanged++;
                            cameras[1].reportedFrameRate = cameras[0].camera.GetProperty(PropertyType.FrameRate).absValue;

                            break;
                        }
                    }
                }

                long rem = 0;
                Math.DivRem(i, 50, out rem);
                if (rem == 0)
                {
                    System.Diagnostics.Trace.WriteLine(delay.ToString());
                    System.Diagnostics.Trace.WriteLine(newFrameRate0.ToString());
                    System.Diagnostics.Trace.WriteLine(newFrameRate1.ToString());
                }

                // Only update the frame rate if the new frame rate is different and if at least 20 frames have passed
                // This is to avoid sending commands to the camera continuosly
                if (newFrameRate0 != cameras[0].lastFrameRateRequested && (cameras[0].lastRawFrameNumber - cameras[0].lastFrameRateChangeFrameNumber) > 5)
                {
                    // Change the frame rate
                    cameras[0].SetFrameRate(newFrameRate0);

                    // Save when the frame rate changed happened
                    cameras[0].lastFrameRateChangeFrameNumber = cameras[0].lastRawFrameNumber;

                    // Count how many times the frame rate has changed
                    cameras[0].timesFrameRateChanged++;

                    // Get the reported frame rate from the camera
                    cameras[0].reportedFrameRate = cameras[0].camera.GetProperty(PropertyType.FrameRate).absValue;
                }

                // Only update the frame rate if the new frame rate is different and if at least 20 frames have passed
                // This is to avoid sending commands to the camera continuosly
                if (newFrameRate1 != cameras[1].lastFrameRateRequested && (cameras[1].lastRawFrameNumber - cameras[1].lastFrameRateChangeFrameNumber) > 5)
                {
                    // Change the frame rate
                    cameras[1].SetFrameRate(newFrameRate1);

                    // Save when the frame rate changed happened
                    cameras[1].lastFrameRateChangeFrameNumber = cameras[1].lastRawFrameNumber;

                    // Count how many times the frame rate has changed
                    cameras[1].timesFrameRateChanged++;

                    // Get the reported frame rate from the camera
                    cameras[1].reportedFrameRate = cameras[0].camera.GetProperty(PropertyType.FrameRate).absValue;
                }
            }

            for (int i = 0; i < 20; i++)
            {
                cameras[0].SetFrameRate(frameRate);
                cameras[1].SetFrameRate(frameRate);

                var seconds0 = 0.0;
                var seconds1 = 0.0;
                using (var rawImage = new ManagedImage())
                {
                    // Retrieve the image from the buffer
                    cameras[0].camera.RetrieveBuffer(rawImage);
                    seconds0 = (double)rawImage.timeStamp.cycleSeconds + (((double)rawImage.timeStamp.cycleCount + ((double)rawImage.timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
                }

                using (var rawImage = new ManagedImage())
                {
                    // Retrieve the image from the buffer
                    cameras[1].camera.RetrieveBuffer(rawImage);
                    seconds1 = (double)rawImage.timeStamp.cycleSeconds + (((double)rawImage.timeStamp.cycleCount + ((double)rawImage.timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
                }

                var delay = seconds1 - seconds0;

                long rem = 0;
                Math.DivRem(i, 10, out rem);
                if (rem == 0)
                {
                    System.Diagnostics.Trace.WriteLine(delay.ToString());
                }
            }
        }

        #endregion
        
        #region IDisposable Members

        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        /// <param name="disposing">Value indicating wether it should disposes resources.</param>
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.camera != null)
                {
                    this.camera.Dispose();
                }
            }
        }

        #region private methods

        /// <summary>
        /// Sets the embedded info in the frame.
        /// </summary>
        private void SetEmbeddedInfo()
        {
            EmbeddedImageInfo embeddedInfo = this.camera.GetEmbeddedImageInfo();
            embeddedInfo.timestamp.onOff = true;
            embeddedInfo.brightness.onOff = true;
            embeddedInfo.exposure.onOff = true;
            embeddedInfo.frameCounter.onOff = true;
            embeddedInfo.gain.onOff = true;
            embeddedInfo.GPIOPinState.onOff = true;
            embeddedInfo.ROIPosition.onOff = true;
            embeddedInfo.shutter.onOff = true;
            embeddedInfo.strobePattern.onOff = true;
            embeddedInfo.whiteBalance.onOff = true;
            this.camera.SetEmbeddedImageInfo(embeddedInfo);
        }

        /// <summary>
        /// Sets the format7 image format (mainly ROI).
        /// </summary>
        private void SetFormat7Settings()
        {
            // Set the new Format 7 settings
            Format7ImageSettings newFmt7Settings = new Format7ImageSettings();

            switch (this.PixelMode)
            {
                case Mode.Mode0:
                    newFmt7Settings.mode = Mode.Mode0;
                    newFmt7Settings.offsetX = Convert.ToUInt32(this.initialROI.X);
                    newFmt7Settings.offsetY = Convert.ToUInt32(this.initialROI.Y);
                    newFmt7Settings.width = Convert.ToUInt32(this.initialROI.Width);
                    newFmt7Settings.height = Convert.ToUInt32(this.initialROI.Height);
                    break;
                case Mode.Mode1:
                    newFmt7Settings.mode = Mode.Mode1;
                    newFmt7Settings.offsetX = Convert.ToUInt32(this.initialROI.X / 2.0);
                    newFmt7Settings.offsetY = Convert.ToUInt32(this.initialROI.Y / 2.0);
                    newFmt7Settings.width = Convert.ToUInt32(this.initialROI.Width / 2.0);
                    newFmt7Settings.height = Convert.ToUInt32(this.initialROI.Height / 2.0);
                    break;
                case Mode.Mode2:
                    newFmt7Settings.mode = Mode.Mode2;
                    newFmt7Settings.offsetX = Convert.ToUInt32(this.initialROI.X);
                    newFmt7Settings.offsetY = Convert.ToUInt32(this.initialROI.Y / 2.0);
                    newFmt7Settings.width = Convert.ToUInt32(this.initialROI.Width);
                    newFmt7Settings.height = Convert.ToUInt32(this.initialROI.Height / 2.0);
                    break;
                default:
                    throw new Exception("Format7 settings are not good.");
                    break;
            }
            newFmt7Settings.pixelFormat = PixelFormat.PixelFormatMono8;

            bool good = true;
            Format7PacketInfo fmt7PacketInfo = this.camera.ValidateFormat7Settings(newFmt7Settings, ref good);
            if (good)
            {
                this.camera.SetFormat7Configuration(newFmt7Settings, fmt7PacketInfo.recommendedBytesPerPacket);
            }
            else
            {
                throw new Exception("Format7 settings are not good.");
            }
        }

        /// <summary>
        /// Changes the frame rate of the camera.
        /// </summary>
        /// <param name="frameRate">Frame rate (Hz).</param>
        private void SetFrameRate(float frameRate)
        {
            this.lastFrameRateRequested = frameRate;

            var prop = this.camera.GetProperty(PropertyType.FrameRate);
            prop.absControl = true;
            prop.absValue = frameRate;
            prop.autoManualMode = false;
            prop.onOff = true;
            this.camera.SetProperty(prop);
        }

        /// <summary>
        /// Adjusts the frame rate of the camera to match the requested frame rate.
        /// </summary>
        /// <remarks>
        /// The frame rate of the point grey cameras is never exactly the frame rate requested. For instance,
        /// if you request 100Hz the camera might run at 99.8 Hz. Thus, if recording is long the delay of the last 
        /// frame relative to the expect time will be long. Moreover, if two cameras are running they will drift
        /// from each other.
        /// To control for this, this function monitors the delay of the current frame with the expected frame time
        /// and adjusts the frame rate of the camera accordingly. Speeding it up or slowing it down temporarily.
        /// The result is a variable frame rate that on average gives the expected frame rate.
        /// </remarks>
        /// <param name="timeStamp">Raw timestamp of the current frame.</param>
        /// <param name="seconds">Timestamp in seconds.</param>
        private void AdjustFrameRate(TimeStamp timeStamp, double seconds)
        {
            // Keep track of the timestamp in seconds
            if (this.firstSecondsTimestamp < 0)
            {
                this.firstSecondsTimestamp = seconds;
            }

            if (!this.ShouldAdjustFrameRate)
            {
                return;
            }

            // Keep track of the last raw timestamp
            if (this.firstSecondsTimestamp > 0)
            {
                // If the current clycle seconds is smaller than the last one it must
                // be that at least one cycle has been completed
                if (this.lastRawTimeStamp.cycleSeconds > timeStamp.cycleSeconds)
                {
                    this.cycles128sec++;
                }
            }

            // Calculate the delay of the current frame
            var delay = (seconds - this.firstSecondsTimestamp) - ((this.lastRawFrameNumber - this.firstRawFrameNumber) / this.RequestedFrameRate);

            // Keep track of the maximum delay
            if (Math.Abs(delay) > Math.Abs(this.maxDelay))
            {
                this.maxDelay = delay;
            }

            // Adjust the frame rate of the camera to fix the delay if bigger than 2 ms
            float newFrameRate = this.lastFrameRateRequested;
            if (Math.Abs(delay) > 0.002)
            {
                // speed up or slow down the camera
                newFrameRate = this.lastFrameRateRequested + ((float)Math.Sign(delay) * 0.1f);

                // Never go more than 1Hz above or below the requested frame rate
                newFrameRate = (float)Math.Max(newFrameRate, this.RequestedFrameRate - 1);
                newFrameRate = (float)Math.Min(newFrameRate, this.RequestedFrameRate + 1);
            }
            else
            {
                // If the delay is very small go back to the requested framerate
                if (Math.Abs(delay) < 0.0005)
                {
                    newFrameRate = (float)this.RequestedFrameRate;
                }
            }

            // Only update the frame rate if the new frame rate is different and if at least 20 frames have passed
            // This is to avoid sending commands to the camera continuosly
            if (newFrameRate != this.lastFrameRateRequested && (this.lastRawFrameNumber - this.lastFrameRateChangeFrameNumber) > 20)
            {
                // Change the frame rate
                this.SetFrameRate(newFrameRate);
                this.lastFrameRateRequested = newFrameRate;

                // Save when the frame rate changed happened
                this.lastFrameRateChangeFrameNumber = this.lastRawFrameNumber;

                // Count how many times the frame rate has changed
                this.timesFrameRateChanged++;

                // Get the reported frame rate from the camera
                this.reportedFrameRate = this.camera.GetProperty(PropertyType.FrameRate).absValue;
            }
        }

        /// <summary>
        /// Sets the camera to buffer frames mode.
        /// </summary>
        /// <remarks>
        /// Images accumulate in the user buffer, and the oldest image is grabbed for handling before 
        /// being discarded.
        /// This member can be used to guarantee that each image is seen. However, image processing 
        /// time must not exceed transmission time from the camera to the buffer. Grabbing blocks if 
        /// the camera has not finished transmitting the next available image. The buffer size is 
        /// by the number of Buffers parameter in the FC2Config struct. Note that this mode is the equivalent 
        /// of FlycaptureLockNext in earlier versions of the FlyCapture SDK.
        /// </remarks>
        /// <param name="bufferSize">Number of frames that can be buffered.</param>
        private void SetBufferMode(int bufferSize)
        {
            FC2Config config = this.camera.GetConfiguration();
            config.grabMode = GrabMode.BufferFrames;
            config.numBuffers = (uint)bufferSize;
            this.camera.SetConfiguration(config);
        }

        /// <summary>
        /// Converts the raw timestamps to timestamps in Seconds (accounting for 128 s cycles).
        /// </summary>
        /// <param name="timeStamp">Raw timestamp.</param>
        /// <param name="frameCounter">Frame counter.</param>
        /// <returns>Timestamp in seconds.</returns>
        private ImageEyeTimestamp GetEyeTrackerTimeStamp(TimeStamp timeStamp, long frameCounter)
        {

            var seconds = (double)timeStamp.cycleSeconds + (((double)timeStamp.cycleCount + ((double)timeStamp.cycleOffset / 3072.0)) / 8000.0); // seconds
            seconds += this.cycles128sec * 128;

            var timestamp = new ImageEyeTimestamp();
            timestamp.Seconds = seconds;
            timestamp.DateTimeGrabbed = DateTime.Now;
            timestamp.FrameNumber = frameCounter - this.firstRawFrameNumber;
            timestamp.FrameNumberRaw = frameCounter;

            return timestamp;
        }

        #endregion
        #endregion
    }
}
