﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemEyeSeeCam.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.HeadTracking;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Micromedical system.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("EyeSeeCam", 2, 2)]
    public class EyeTrackingSystemEyeSeeCam : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider, IHeadSensorProvider
    {
        CameraEyePointGrey cameraLeftEye = null;
        CameraEyePointGrey cameraRightEye = null;

        /// <summary>
        /// Gets the cameras. In this case two, left and right eye. 
        /// </summary>
        /// <returns>List containing the camera object.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            using (var busMgr = new ManagedBusManager())
            {
                uint numCameras = busMgr.GetNumOfCameras();

                if (numCameras < 1)
                {
                    return null;
                }

                var roi = new System.Drawing.Rectangle(0, 0, 752, 480);
                ////EyeTracker.Settings.FrameRate
                var camera = new CameraEyePointGrey(Eye.Left, busMgr.GetCameraFromIndex(0u), 60.0f, roi);
                camera.IsMirrored = true;
                camera.IsUpsideDown = false;
                camera.StartCapture();

                ////camera.Init();
                ////camera.SetGPIO();
                ////camera.SetShutter(9);
                ////camera.SetGain(2);
                camera.ShouldAdjustFrameRate = false;

                this.cameraLeftEye = camera;


                //// Syncrhonize the cameras
                ////CameraEyePointGrey.SyncCameras(new CameraEyePointGrey[] { cameraLeftEye, cameraRightEye }, EyeTracker.Settings.FrameRate);

                return new EyeCollection<ICameraEye>(cameraLeftEye, cameraRightEye);
            }
        }

        /// <summary>
        /// Prepares the images for processing. 
        /// </summary>
        /// <param name="images">Raw image from the camera.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            return images;
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> filenames)
        {
            return new EyeCollection<VideoEye>(
                new VideoEyePointGrey(Eye.Left, filenames[Eye.Left], VideoEyePointGrey.PositionOfEmbeddedInfo.BottomLeftHorizontal),
                new VideoEyePointGrey(Eye.Right, filenames[Eye.Right], VideoEyePointGrey.PositionOfEmbeddedInfo.TopRightHorizontal));
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public IHeadSensor GetHeadSensor()
        {
            return null;
        }

        /// <summary>
        /// Gets additional data specific to the eye tracking system.
        /// </summary>
        /// <param name="procesedImages">Images that the data must correspond with.</param>
        /// <returns>The extra data to add to the frame data.</returns>
        public virtual ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            var data = new ExtraData();

            return data;
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.05;
            settings.DistanceCameraToEyeMm = 20;

            return settings;
        }
    }
}
