﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemEyeBrain.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using System.Collections.Generic;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// EyeBrain system.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("EyeBrain", 1, 2)]
    public class EyeTrackingSystemEyeBrain : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider
    {
        /// <summary>
        /// Gets the cameras. In this case just one single camera.
        /// </summary>
        /// <returns>List containing the camera object.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            //// TODO: It should check if the proper camera is really plugged to the computer
            var camera = new CameraEyeUEye(Eye.Both, EyeTracker.Settings.FrameRate);
            camera.Init();

            return new EyeCollection<ICameraEye>(camera);
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> fileNames)
        {
            if (fileNames.Count == 2)
            {
                return new EyeCollection<VideoEye>(
                        new VideoEye(Eye.Left, fileNames[Eye.Left]),
                        new VideoEye(Eye.Right, fileNames[Eye.Right]));
            }
            else
            {
                throw new InvalidOperationException("The number of video files should be two.");
            }
        }

        /// <summary>
        /// Prepares the images for processing. Splits the single image into left and right eye
        /// and rotates them appropriately. 
        /// </summary>
        /// <param name="images">Raw image from the camera.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            var imageLeft = images[Eye.Both].GetCroppedImage(new Rectangle(400, 0, 400, 300));
            var imageRight = images[Eye.Both].GetCroppedImage(new Rectangle(0, 0, 400, 300));

            var temp = imageLeft.SmoothBlur(30, 30);
            //temp._EqualizeHist();
            imageLeft = imageLeft - (temp - imageLeft.GetAverage());
            imageLeft._EqualizeHist();

            temp = imageRight.SmoothBlur(30, 30);
            //temp._EqualizeHist();
            imageRight = imageRight - (temp - imageRight.GetAverage());
            imageRight._EqualizeHist();

            return new EyeCollection<ImageEye>(
                new ImageEye(imageLeft, Eye.Left, images[Eye.Both].TimeStamp),
                new ImageEye(imageRight, Eye.Right, images[Eye.Both].TimeStamp));
        }


        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.20;
            settings.DistanceCameraToEyeMm = 50;

            return settings;
        }
    }
}
