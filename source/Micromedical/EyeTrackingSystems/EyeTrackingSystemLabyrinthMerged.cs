﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemLabyrinthMerged.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Labyrinth system, only for videos with a single image of both eyes.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("LabyrinthMergedJOM", 1, 1)]
    public class EyeTrackingSystemLabyrinthMerged : IEyeTrackingSystem, IVideoEyeProvider
    {
        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <param name="fileNames">Filenames of the videos.</param>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> fileNames)
        {
            return new EyeCollection<VideoEye>(
                new VideoEye(Eye.Both, fileNames[Eye.Both]));
        }

        /// <summary>
        /// Grabs the images from the video file.
        /// </summary>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            ImageEye imageLeftEye = null;
            ImageEye imageRightEye = null;

            var imageLeft = images[Eye.Both].GetCroppedImage(new System.Drawing.Rectangle(512, 0, 624, 512));
            var imageRight = images[Eye.Both].GetCroppedImage(new System.Drawing.Rectangle(0, 0, 624, 512));
            imageLeft = imageLeft.Rotate(90, new Gray(0));
            imageRight = imageRight.Rotate(270, new Gray(0));

            imageLeftEye = new ImageEye(imageLeft.Bitmap, Eye.Left, images[Eye.Both].TimeStamp);

            imageRightEye = new ImageEye(imageRight.Bitmap, Eye.Right, images[Eye.Both].TimeStamp);

            return new EyeCollection<ImageEye>(imageLeftEye, imageRightEye);
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.15;
            settings.DistanceCameraToEyeMm = 70;

            return settings;
        }
    }
}
