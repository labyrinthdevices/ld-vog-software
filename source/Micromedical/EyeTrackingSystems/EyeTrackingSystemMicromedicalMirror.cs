﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemMicromedicalMirror.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Micromedical system setup as a mirror.
    /// This is only for backwards compatibility with the videos initially recorded
    /// backwards.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("Micromedical mirrored", 2, 2)]
    public class EyeTrackingSystemMicromedicalMirror : IEyeTrackingSystem, IVideoEyeProvider
    {
        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> filenames)
        {
            var videoLeftEye = new VideoEyePointGrey(Eye.Left, filenames[Eye.Left], VideoEyePointGrey.PositionOfEmbeddedInfo.BottomRightHorizontal);
            videoLeftEye.IsMirrored = true;

            var videoRightEye = new VideoEyePointGrey(Eye.Right, filenames[Eye.Right], VideoEyePointGrey.PositionOfEmbeddedInfo.TopLeftHorizontal);
            videoRightEye.IsMirrored = true;

            return new EyeCollection<VideoEye>(videoLeftEye, videoRightEye);
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.15;
            settings.DistanceCameraToEyeMm = 70;

            return settings;
        }
    }
}
