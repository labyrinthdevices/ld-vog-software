﻿//-----------------------------------------------------------------------
// <copyright file="ImageEyeGrabberMicromedical.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.HeadTracking;

    /// <summary>
    /// Micromedical system.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("Micromedical", 2, 2)]
    public class EyeTrackingSystemMicromedical : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider, IHeadSensorProvider
    {
        protected CameraEyePointGrey cameraLeftEye = null;
        protected CameraEyePointGrey cameraRightEye = null;

        /// <summary>
        /// Gets the cameras. In this case two, left and right eye. 
        /// </summary>
        /// <returns>The list of cameras.</returns>
        public virtual EyeCollection<ICameraEye> GetCameras()
        {
            using (var busMgr = new ManagedBusManager())
            {
                var settings = (EyeTrackerSystemSettingsMicromedical)EyeTracker.Settings.EyeTrackingSystemSettings;

                uint numCameras = busMgr.GetNumOfCameras();

                if (numCameras < 2)
                {
                    throw new Exception("Not enough cameras connected.");
                }

                var roi = new System.Drawing.Rectangle(176, 112, 400, 260);

                this.cameraRightEye = new CameraEyePointGrey(Eye.Right, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, roi);
                cameraRightEye.IsMirrored = true;
                cameraRightEye.IsUpsideDown = false;
                cameraRightEye.ShouldAdjustFrameRate = true;
                cameraRightEye.AutoExposure = settings.AutoExposure;
                if (!settings.AutoExposure)
                {
                    cameraRightEye.ShutterDuration = settings.ShutterDuration;
                    cameraRightEye.Gain = settings.Gain;
                }

                cameraRightEye.PixelMode = Mode.Mode0;
                cameraRightEye.Init();
                cameraRightEye.StartCapture();
                cameraRightEye.SetGPIO();


                this.cameraLeftEye = new CameraEyePointGrey(Eye.Left, busMgr.GetCameraFromIndex(1u), EyeTracker.Settings.FrameRate, roi);
                cameraLeftEye.IsMirrored = true;
                cameraLeftEye.IsUpsideDown = true;
                cameraLeftEye.ShouldAdjustFrameRate = true;
                cameraLeftEye.AutoExposure = settings.AutoExposure;
                if (!settings.AutoExposure)
                {
                    cameraLeftEye.ShutterDuration = settings.ShutterDuration;
                    cameraLeftEye.Gain = settings.Gain;
                }

                cameraLeftEye.PixelMode = Mode.Mode0;
                cameraLeftEye.Init();
                cameraLeftEye.StartCapture();
                cameraLeftEye.SetGPIO();

                // Syncrhonize the cameras
                CameraEyePointGrey.SyncCameras(new CameraEyePointGrey[] { cameraLeftEye, cameraRightEye }, EyeTracker.Settings.FrameRate);

                return new EyeCollection<ICameraEye>(cameraLeftEye, cameraRightEye);
            }
        }

        /// <summary>
        /// Prepares the images for processing. 
        /// </summary>
        /// <param name="images">Raw image from the camera.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            return images;
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> filenames)
        {
            VideoEyePointGrey videoLeftEye = null;
            VideoEyePointGrey videoRightEye = null;
            if (filenames[Eye.Left].Length > 1)
            {
                videoLeftEye = new VideoEyePointGrey(Eye.Left, filenames[Eye.Left], VideoEyePointGrey.PositionOfEmbeddedInfo.BottomLeftHorizontal);
            }

            if (filenames[Eye.Right].Length > 1)
            {
                videoRightEye = new VideoEyePointGrey(Eye.Right, filenames[Eye.Right], VideoEyePointGrey.PositionOfEmbeddedInfo.TopRightHorizontal);
            }

            return new EyeCollection<VideoEye>(videoLeftEye, videoRightEye);
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public IHeadSensor GetHeadSensor()
        {
            //if (EyeTracker.Settings.UseDataAcquisition)
            //{

            //    return new HeadSensorMicromedicalMeasurmentComputing(this.cameraLeftEye);
            //}
            //else
            //{
            //    return null;
            //}

            return new HeadSensorMPUFTDI(this.cameraLeftEye);
        }

        /// <summary>
        /// Gets additional data specific to the eye tracking system.
        /// </summary>
        /// <param name="procesedImages">Images that the data must correspond with.</param>
        /// <returns>The extra data to add to the frame data.</returns>
        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            var data = new ExtraData();

            //if (EyeTracker.Settings.UseDataAcquisition)
            //{
            //    var daq = DataAcquisitionMeasurementComputing.Instance;

            //    if (daq != null)
            //    {
            //        data.Int0 = daq.ReadCounter();
            //    }
            //}

            return data;
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettingsMicromedical();
            settings.MmPerPixel = 0.15;
            settings.DistanceCameraToEyeMm = 70;
            settings.UseHeadSensor = true;
            
            return settings;
        }

        public class EyeTrackerSystemSettingsMicromedical : EyeTrackerSystemSettings
        {
            public bool UseHeadSensor
            {
                get
                {
                    return this.useHeadSensor;
                }
                set
                {
                    if (value != this.useHeadSensor)
                    {
                        this.useHeadSensor = value;
                        this.OnPropertyChanged("UseHeadSensor");
                    }
                }
            }
            private bool useHeadSensor = true;

            public bool AutoExposure
            {
                get
                {
                    return this.autoExposure;
                }
                set
                {
                    if (value != this.autoExposure)
                    {
                        this.autoExposure = value;
                        this.OnPropertyChanged("AutoExposure");
                    }
                }
            }
            private bool autoExposure = true;

            public float ShutterDuration
            {
                get
                {
                    return this.shutterDuration;
                }
                set
                {
                    if (value != this.shutterDuration)
                    {
                        this.shutterDuration = value;
                        this.OnPropertyChanged("ShutterDuration");
                    }
                }
            }
            private float shutterDuration = 4;


            public float Gain
            {
                get
                {
                    return this.gain;
                }
                set
                {
                    if (value != this.gain)
                    {
                        this.gain = value;
                        this.OnPropertyChanged("Gain");
                    }
                }
            }
            private float gain = 9;
        }
    }
}
