﻿//-----------------------------------------------------------------------
// <copyright file="ImageEyeGrabberMicromedical.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.HeadTracking;

    /// <summary>
    /// Micromedical system.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("Micromedical200Hz", 2, 2)]
    public class EyeTrackingSystemMicromedical200Hz : EyeTrackingSystemMicromedical
    {
        /// <summary>
        /// Gets the cameras. In this case two, left and right eye. 
        /// </summary>
        /// <returns>The list of cameras.</returns>
        public override EyeCollection<ICameraEye> GetCameras()
        {
            using (var busMgr = new ManagedBusManager())
            {
                uint numCameras = busMgr.GetNumOfCameras();

                if (numCameras < 2)
                {
                    throw new Exception("Not enough cameras connected.");
                }

                var roi = new System.Drawing.Rectangle(176, 112, 400, 260);

                this.cameraRightEye = new CameraEyePointGrey(Eye.Right, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, roi);
                cameraRightEye.IsMirrored = true;
                cameraRightEye.IsUpsideDown = false;
                cameraRightEye.ShouldAdjustFrameRate = true;
                //cameraRightEye.SetShutter(9);
                //cameraRightEye.SetGain(2);

                cameraRightEye.PixelMode = Mode.Mode1;
                cameraRightEye.Init();
                cameraRightEye.StartCapture();
                cameraRightEye.SetGPIO();


                this.cameraLeftEye = new CameraEyePointGrey(Eye.Left, busMgr.GetCameraFromIndex(1u), EyeTracker.Settings.FrameRate, roi);
                cameraLeftEye.IsMirrored = true;
                cameraLeftEye.IsUpsideDown = true;
                cameraLeftEye.ShouldAdjustFrameRate = true;
                //cameraLeftEye.SetShutter(9);
                //cameraLeftEye.SetGain(2);

                cameraLeftEye.PixelMode = Mode.Mode1;
                cameraLeftEye.Init();
                cameraLeftEye.StartCapture();
                cameraLeftEye.SetGPIO();

                // Syncrhonize the cameras
                CameraEyePointGrey.SyncCameras(new CameraEyePointGrey[] { cameraLeftEye, cameraRightEye }, EyeTracker.Settings.FrameRate);

                return new EyeCollection<ICameraEye>(cameraLeftEye, cameraRightEye);
            }
        }

    }
}
