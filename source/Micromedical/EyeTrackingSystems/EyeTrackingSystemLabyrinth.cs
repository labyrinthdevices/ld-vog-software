﻿//-----------------------------------------------------------------------
// <copyright file="ImageEyeGrabberLabyrinth.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using FlyCapture2Managed;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.HeadTracking;

    /// <summary>
    /// Labyrinth system.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("LabyrinthJOM", 1, 2)]
    public class EyeTrackingSystemLabyrinth : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider, IHeadSensorProvider
    {
        /// <summary>
        /// Camera, necesary to configure the head tracking.
        /// </summary>
        internal CameraEyePointGrey Camera { get; private set; }
        
        /// <summary>
        /// Gets the cameras. In this case just one single camera.
        /// </summary>
        /// <returns>List containing the camera object.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            using (var busMgr = new ManagedBusManager())
            {
                uint numCameras = busMgr.GetNumOfCameras();

                if (numCameras < 1)
                {
                    return null;
                }

                this.Camera = new CameraEyePointGrey(Eye.Both, busMgr.GetCameraFromIndex(0u), EyeTracker.Settings.FrameRate, new Rectangle(16 - 16, 256, 1264, 512));
                this.Camera.Init();
                this.Camera.ShouldAdjustFrameRate = false;
                this.Camera.StartCapture();

                return new EyeCollection<ICameraEye>(this.Camera);
            }
        }

        /// <summary>
        /// Prepares the images for processing. Splits the single image into left and right eye
        /// and rotates them appropriately. 
        /// </summary>
        /// <param name="images">Raw image from the camera.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            Image<Gray, byte> imageLeft = null;
            Image<Gray, byte> imageRight = null;
            EyeCollection<ImageEye> newImages = null;
            try
            {
                // Total ROI is 1264 by 512
                var roiLeft = new Rectangle(682, 0, 432, 512);
                var roiRight = new Rectangle(150, 0, 432, 512);

                imageLeft = images[Eye.Both].GetCroppedTransposedImage(roiLeft);

                imageRight = images[Eye.Both].GetCroppedTransposedImage(roiRight);
                imageRight = imageRight.Flip(Emgu.CV.CvEnum.FLIP.VERTICAL);
                imageRight = imageRight.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);

                // Copy the embedded fields into both images
                var bitsSeconds = BitConverter.GetBytes(images[Eye.Both].TimeStamp.Seconds);
                var bitsFrameNumber = BitConverter.GetBytes(images[Eye.Both].TimeStamp.FrameNumber);

                for (int i = 0; i < bitsSeconds.Length; i++)
                {
                    imageLeft.Data[i, 0, 0] = bitsSeconds[i];
                    imageLeft.Data[i, 0, 0] = bitsSeconds[i];
                }

                for (int i = 0; i < bitsFrameNumber.Length; i++)
                {
                    imageRight.Data[bitsSeconds.Length + i, 0, 0] = bitsFrameNumber[i];
                    imageRight.Data[bitsSeconds.Length + i, 0, 0] = bitsFrameNumber[i];
                }

                newImages = new EyeCollection<ImageEye>(
                    new ImageEye(imageLeft, Eye.Left, images[Eye.Both].TimeStamp),
                    new ImageEye(imageRight, Eye.Right, images[Eye.Both].TimeStamp));

                imageLeft = null;
                imageRight = null;
            }
            finally
            {
                if (imageLeft != null)
                {
                    imageLeft.Dispose();
                }

                if (imageRight != null)
                {
                    imageRight.Dispose();
                }
            }

            return newImages;
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <param name="fileNames">Filenames of the videos.</param>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> fileNames)
        {
            return new EyeCollection<VideoEye>(
                new VideoEyePointGrey(Eye.Left, fileNames[Eye.Left], VideoEyePointGrey.PositionOfEmbeddedInfo.TopLeftVertical),
                new VideoEyePointGrey(Eye.Right, fileNames[Eye.Right], VideoEyePointGrey.PositionOfEmbeddedInfo.TopLeftVertical));
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public IHeadSensor GetHeadSensor()
        {
            return new HeadSensorLabyrinthJOM(this.Camera);
            //return new HeadSensorMPUFTDI(this.Camera);
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.10;
            settings.DistanceCameraToEyeMm = 70;

            return settings;
        }
    }
}
