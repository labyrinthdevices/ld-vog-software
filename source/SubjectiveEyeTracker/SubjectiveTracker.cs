﻿//-----------------------------------------------------------------------
// <copyright file="SubjectiveTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace SubjectiveTracker
{
    using System;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using Emgu.CV;
    using Emgu.CV.CvEnum;
    using Emgu.CV.Structure;
    using OculomotorLab.VOG;

    public partial class SubjectiveTracker : Form
    {
        /// <summary>
        /// Default colors for the markers
        /// </summary>
        private Color[] markerColors;

        /// <summary>
        /// Data structure with all the markers.
        /// </summary>
        private SubjectiveTrackerData data;

        /// <summary>
        /// Video player to control the image acquisition.
        /// </summary>
        private VideoPlayer videoPlayer;

        /// <summary>
        /// Path of the data file.
        /// </summary>
        private string dataFile;

        /// <summary>
        /// Path of the video file
        /// </summary>
        private string videoFile;

        /// <summary>
        /// Value indicating wether the UI is currently updating. Used to avoid recurrent events while changing
        /// the data in the controls.
        /// </summary>
        private bool updating;

        /// <summary>
        /// Initializes a new instance of the SubjectiveTracker class.
        /// </summary>
        public SubjectiveTracker()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the data and the UI.
        /// </summary>
        private void Init()
        {
            /// Default colors
            this.markerColors = new Color[]
            {
                Color.Blue,
                Color.Red,
                Color.Green,
                Color.Yellow,
                Color.Purple,
                Color.Brown,
            };


            if (Properties.Settings.Default.LastDataFile.Length > 0)
            {
                var result = MessageBox.Show(
                    "Do you want to load the last data file? " + Properties.Settings.Default.LastDataFile,
                    "Load file?",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    this.dataFile = Properties.Settings.Default.LastDataFile;
                }
            }

            this.LoadDataFile();

            this.UpdateUI();
        }

        public void LoadDataFile()
        {
            if (this.dataFile != null)
            {
                if (!System.IO.File.Exists(this.dataFile))
                {
                    var result = MessageBox.Show(
                        "The data file does not exist, do you want to select another one?",
                        "Error",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Error);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        this.SelectDataFile();

                        if (this.dataFile != null)
                        {
                            this.LoadDataFile();
                        }

                        return;
                    }
                }

                this.data = SubjectiveTrackerData.Load(this.dataFile);

                if (this.data.VideoFile != null)
                {
                    this.videoFile = this.data.VideoFile;
                    this.LoadVideoFile();
                }
                else
                {
                    this.SelectVideoFile();
                    if (this.videoFile != null)
                    {
                        this.LoadVideoFile();
                    }
                }
            }
        }

        public void SelectDataFile()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Data Files (*.dat)|*.dat";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Title = "Select data file.";

            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            Properties.Settings.Default.LastDataFile = openFileDialog1.FileName;
            Properties.Settings.Default.Save();
            this.dataFile = Properties.Settings.Default.LastDataFile;
        }

        public void CreateNewDataFile()
        {
            // The video file has to be selected first
            if (this.videoFile == null)
            {
                return;
            }
            else
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Data Files (*.dat)|*.dat";
                saveFileDialog1.OverwritePrompt = true;

                if (saveFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }

                Properties.Settings.Default.LastDataFile = saveFileDialog1.FileName;
                Properties.Settings.Default.Save();
                this.dataFile = Properties.Settings.Default.LastDataFile;

                this.data = new SubjectiveTrackerData();
                this.data.VideoFile = this.videoFile;
                this.data.Save(this.dataFile);
            }
        }

        public void LoadVideoFile()
        {
            if (this.videoFile != null)
            {
                if (!System.IO.File.Exists(this.videoFile))
                {
                    var result = MessageBox.Show(
                        "The video file does not exist at the saved location. Select again.",
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);

                    this.SelectVideoFile();
                    return;
                }

                // Initialize the video grabbing
                this.videoPlayer = new OculomotorLab.VOG.VideoPlayer(this.videoFile);
                this.videoPlayer.Scroll(this.data.CurrentFrameNumber);
                this.videoPlayer.ImagesGrabbed += videoPlayer_ImagesGrabbed;
            }
        }

        public void SelectVideoFile()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Video Files (*.avi;*.mpg)|*.avi;*.mpg";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            this.videoFile = openFileDialog1.FileName;

            if (this.dataFile == null)
            {
                this.CreateNewDataFile();
            }

            if (this.dataFile != null)
            {
                this.data.VideoFile = openFileDialog1.FileName;
            }
        }

        private void UpdateUI()
        {
            if (this.updating)
            {
                return;
            }

            this.updating = true;

            try
            {
                if (this.data != null)
                {
                    this.listBoxMarkers.DataSource = null;
                    this.listBoxMarkers.DataSource = this.data.MarkerNames;
                    this.listBoxMarkers.SelectedIndex = this.listBoxMarkers.FindStringExact(this.data.SelectedMarker);

                    this.comboBoxLeftClick.Items.Clear();
                    this.comboBoxLeftClick.Items.Add("None");
                    this.comboBoxLeftClick.Items.AddRange(this.data.MarkerNames.ToArray());
                    if (this.data.Options.LeftClickAction == null)
                        this.data.Options.LeftClickAction = this.comboBoxLeftClick.Items[0].ToString();
                    if (this.data.Options.LeftClickAction != null)
                        this.comboBoxLeftClick.SelectedItem = this.data.Options.LeftClickAction;

                    this.comboBoxRightClick.Items.Clear();
                    this.comboBoxRightClick.Items.Add("None");
                    this.comboBoxRightClick.Items.AddRange(this.data.MarkerNames.ToArray());
                    if (this.data.Options.RightClickAction == null)
                        this.data.Options.RightClickAction = this.comboBoxRightClick.Items[0].ToString();
                    if (this.data.Options.RightClickAction != null)
                        this.comboBoxRightClick.SelectedItem = this.data.Options.RightClickAction;

                    this.comboBoxCtrlLeftClick.Items.Clear();
                    this.comboBoxCtrlLeftClick.Items.Add("None");
                    this.comboBoxCtrlLeftClick.Items.AddRange(this.data.MarkerNames.ToArray());
                    if (this.data.Options.CtrlLeftClickAction == null)
                        this.data.Options.CtrlLeftClickAction = this.comboBoxCtrlLeftClick.Items[0].ToString();
                    if (this.data.Options.CtrlLeftClickAction != null)
                        this.comboBoxCtrlLeftClick.SelectedItem = this.data.Options.CtrlLeftClickAction;

                    this.comboBoxCtrlRightClick.Items.Clear();
                    this.comboBoxCtrlRightClick.Items.Add("None");
                    this.comboBoxCtrlRightClick.Items.AddRange(this.data.MarkerNames.ToArray());
                    if (this.data.Options.CtrlRightClickAction == null)
                        this.data.Options.CtrlRightClickAction = this.comboBoxCtrlRightClick.Items[0].ToString();
                    if (this.data.Options.CtrlRightClickAction != null)
                        this.comboBoxCtrlRightClick.SelectedItem = this.data.Options.CtrlRightClickAction;


                    if (this.data.Options.AutoAdvanceLeftClick)
                        this.radioButtonLeftClick.Checked = true;
                    if (this.data.Options.AutoAdvanceRightClick)
                        this.radioButtonRightClick.Checked = true;
                    if (this.data.Options.AutoAdvanceCtrlLeftClick)
                        this.radioButtonCtrlLeftClick.Checked = true;
                    if (this.data.Options.AutoAdvanceCtrlRightClick)
                        this.radioButtonCtrlRightClick.Checked = true;
                    if (!this.data.Options.AutoAdvanceLeftClick && !this.data.Options.AutoAdvanceRightClick &&
                        !this.data.Options.AutoAdvanceCtrlLeftClick && !this.data.Options.AutoAdvanceCtrlRightClick)
                        this.radioButtonNoAutoAdvance.Checked = true;

                    this.numericUpDownSkipFrames.Value = this.data.Options.EveryOtherFrame;

                    if (this.videoPlayer != null)
                    {
                        this.videoPlayerUI1.Update(this.videoPlayer);
                    }

                    this.data.Save(this.dataFile);
                }

                if (this.dataFile != null)
                {
                    this.toolStripStatusLabel1.Text = "DATA FILE : " + this.dataFile;
                }
                if (this.videoFile != null)
                {
                    this.toolStripStatusLabel2.Text = "VIDEO FILE : " + this.videoFile;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                this.updating = false;
            }
        }
    
        private Image<Bgr, byte> DrawMarkers(Image<Bgr, byte> image)
        {
            var frameMarkers =
                from item in this.data.MarkerData
                where item.FrameNumber == this.data.CurrentFrameNumber
                select item;

            var color = Color.Black;

            foreach (var marker in frameMarkers)
            {
                for (int i = 0; i < this.data.MarkerNames.Count; i++)
                {
                    if (this.data.MarkerNames[i].Equals(marker.MarkerName))
                    {
                        color = this.markerColors[i];
                        break;
                    }
                }

                image.Draw(new CircleF(marker.Location, 5), new Bgr(color), 2);
            }

            return image;
        }

        private void Advance()
        {
            this.videoPlayer.Scroll(this.data.CurrentFrameNumber + 1);
        }

        /// <summary>
        /// From: http://www.codeproject.com/script/Articles/View.aspx?aid=859100
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="X0"></param>
        /// <param name="Y0"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private Point ConvertCoordinates(Point p)
        {
            var pic = this.imageBox;

            int pic_hgt = pic.ClientSize.Height;
            int pic_wid = pic.ClientSize.Width;
            int img_hgt = pic.Image.Size.Height;
            int img_wid = pic.Image.Size.Width;

            var x = p.X;
            var y = p.Y;

            var X0 = x;
            var Y0 = y;
            switch (pic.SizeMode)
            {
                case PictureBoxSizeMode.AutoSize:
                case PictureBoxSizeMode.Normal:
                    // These are okay. Leave them alone.
                    break;
                case PictureBoxSizeMode.CenterImage:
                    X0 = x - (pic_wid - img_wid) / 2;
                    Y0 = y - (pic_hgt - img_hgt) / 2;
                    break;
                case PictureBoxSizeMode.StretchImage:
                    X0 = (int)(img_wid * x / (float)pic_wid);
                    Y0 = (int)(img_hgt * y / (float)pic_hgt);
                    break;
                case PictureBoxSizeMode.Zoom:
                    float pic_aspect = pic_wid / (float)pic_hgt;
                    float img_aspect = img_wid / (float)img_wid;
                    if (pic_aspect > img_aspect)
                    {
                        // The PictureBox is wider/shorter than the image.
                        Y0 = (int)(img_hgt * y / (float)pic_hgt);

                        // The image fills the height of the PictureBox.
                        // Get its width.
                        float scaled_width = img_wid * pic_hgt / img_hgt;
                        float dx = (pic_wid - scaled_width) / 2;
                        X0 = (int)((x - dx) * img_hgt / (float)pic_hgt);
                    }
                    else
                    {
                        // The PictureBox is taller/thinner than the image.
                        X0 = (int)(img_wid * x / (float)pic_wid);

                        // The image fills the height of the PictureBox.
                        // Get its height.
                        float scaled_height = img_hgt * pic_wid / img_wid;
                        float dy = (pic_hgt - scaled_height) / 2;
                        Y0 = (int)((y - dy) * img_wid / pic_wid);
                    }
                    break;
            }

            return new Point(X0, Y0);
        }

        private long Mod(long a, long b)
        {
            long result;
            Math.DivRem(a, b, out result);

            return result;
        }

        private void videoPlayer_ImagesGrabbed(object sender, EyeCollection<ImageEye> images)
        {
            if (this.data.CurrentFrameNumber <= this.videoPlayer.LastFrameNumber)
            {
                this.data.CurrentFrameNumber = this.videoPlayer.LastFrameNumber;

                if (this.Mod(this.data.CurrentFrameNumber, this.data.Options.EveryOtherFrame) != 0)
                {
                    var newFrameNumber = Math.Ceiling(this.data.CurrentFrameNumber / (double)this.data.Options.EveryOtherFrame) * this.data.Options.EveryOtherFrame;
                    this.videoPlayer.Scroll((long)newFrameNumber);
                    return;
                }
            }
            else
            {
                this.data.CurrentFrameNumber = this.videoPlayer.LastFrameNumber;

                if (this.Mod(this.data.CurrentFrameNumber, this.data.Options.EveryOtherFrame) != 0)
                {
                    var newFrameNumber = Math.Floor(this.data.CurrentFrameNumber / (double)this.data.Options.EveryOtherFrame) * this.data.Options.EveryOtherFrame;
                    this.videoPlayer.Scroll((long)newFrameNumber);
                    return;
                }
            }

            this.videoPlayerUI1.Update(this.videoPlayer);

            var image = images[0].Image.Resize(3.0, INTER.CV_INTER_CUBIC).Convert<Bgr, byte>();
            image._EqualizeHist();
            image = this.DrawMarkers(image);
            this.imageBox.Image = image;
        }

        private void SubjectiveTracker_Load(object sender, EventArgs e)
        {
            this.Init();
        }
        

        private void imageBox_MouseDown(object sender, MouseEventArgs e)
        {
            var mousePosition = this.ConvertCoordinates(e.Location);
            var action = string.Empty;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (Control.ModifierKeys != Keys.Control)
                    {
                        action = this.data.Options.LeftClickAction;
                    }
                    else
                    {
                        action = this.data.Options.CtrlLeftClickAction;
                    }
                    break;
                case MouseButtons.Right:
                    if (Control.ModifierKeys != Keys.Control)
                    {
                        action = this.data.Options.RightClickAction;
                    }
                    else
                    {
                        action = this.data.Options.CtrlRightClickAction;
                    }
                    break;
                default:
                    break;
            }

            if (action.Length > 0 && !action.ToUpper().Equals("NONE"))
            {
                var marker = new MarkerData();
                marker.Location = mousePosition;
                marker.FrameNumber = this.data.CurrentFrameNumber;
                marker.MarkerName = action;

                this.data.MarkerData.Add(marker);

                this.videoPlayer.Scroll(this.data.CurrentFrameNumber);

            }

            this.data.Save(this.dataFile);
        }


        private void imageBox_MouseUp(object sender, MouseEventArgs e)
        {
            var autoadvance = false;

            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (Control.ModifierKeys != Keys.Control &&  this.radioButtonLeftClick.Checked)
                    {
                        autoadvance = true;
                    }
                    if (Control.ModifierKeys == Keys.Control &&  this.radioButtonCtrlLeftClick.Checked)
                    {
                        autoadvance = true;
                    }
                    break;
                case MouseButtons.Right:
                    if (Control.ModifierKeys != Keys.Control &&  this.radioButtonRightClick.Checked)
                    {
                        autoadvance = true;
                    }
                    if (Control.ModifierKeys == Keys.Control && this.radioButtonCtrlRightClick.Checked)
                    {
                        autoadvance = true;
                    }
                    break;
                default:
                    break;
            }

            if (autoadvance)
            {
                this.Advance();
            }
        }


        private void loadVideoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.dataFile = null;
            this.data = null;

            this.SelectVideoFile();

            if (this.videoFile != null)
            {
                this.LoadVideoFile();
            }

            this.UpdateUI();
        }

        private void loadDataFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SelectDataFile();

            if (this.dataFile != null)
            {
                this.LoadDataFile();
            }

            this.UpdateUI();
        }



        private void comboBoxLeftClick_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.updating && comboBoxLeftClick.SelectedItem != null)
            {
                this.data.Options.LeftClickAction = comboBoxLeftClick.SelectedItem.ToString();
                this.UpdateUI();
            }
        }

        private void comboBoxRightClick_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.updating && comboBoxRightClick.SelectedItem != null)
            {
                this.data.Options.RightClickAction = comboBoxRightClick.SelectedItem.ToString();
                this.UpdateUI();
            }
        }

        private void comboBoxCtrlLeftClick_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.updating && comboBoxCtrlLeftClick.SelectedItem != null)
            {
                this.data.Options.CtrlLeftClickAction = comboBoxCtrlLeftClick.SelectedItem.ToString();
                this.UpdateUI();
            }
        }

        private void comboBoxCtrlRightClick_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.updating && comboBoxCtrlRightClick.SelectedItem != null)
            {
                this.data.Options.CtrlRightClickAction = comboBoxCtrlRightClick.SelectedItem.ToString();
                this.UpdateUI();
            }
        }



        private void buttonAdd_Click(object sender, EventArgs e)
        {
            this.data.MarkerNames.Add(textBoxNewMarkerName.Text);
            this.data.SelectedMarker = textBoxNewMarkerName.Text;
            this.textBoxNewMarkerName.Text = string.Empty;
            this.UpdateUI();
        }

        private void listBoxMarkers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.updating)
            {
                this.data.SelectedMarker = this.listBoxMarkers.SelectedItem.ToString();
                this.UpdateUI();
            }
        }


        private void radioButtonAutoadvance_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.updating)
            {
                this.data.Options.AutoAdvanceLeftClick = radioButtonLeftClick.Checked;
                this.data.Options.AutoAdvanceRightClick = radioButtonRightClick.Checked;
                this.data.Options.AutoAdvanceCtrlLeftClick = radioButtonCtrlLeftClick.Checked;
                this.data.Options.AutoAdvanceCtrlRightClick = radioButtonCtrlRightClick.Checked;
                this.UpdateUI();
            }
        }

        private void SubjectiveTracker_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.data.Save(this.dataFile);
        }

        private void numericUpDownSkipFrames_ValueChanged(object sender, EventArgs e)
        {
            this.data.Options.EveryOtherFrame = (int)numericUpDownSkipFrames.Value;
            this.UpdateUI();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.data.MarkerData.Count > 0)
            {
                this.data.MarkerData.RemoveAt(this.data.MarkerData.Count - 1);
                this.videoPlayer.Scroll(this.data.CurrentFrameNumber);
                this.UpdateUI();
            }
        }
    }
}
