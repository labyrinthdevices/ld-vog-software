﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerDiagnostics.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using OculomotorLab.VOG.Diagnostics;

    /// <summary>
    /// Class with diagnostic information about the eye tracker. Timing, errors, etc.
    /// </summary>
    public class EyeTrackerDiagnostics
    {
        /// <summary>
        /// Gets the timing data from the left eye.
        /// </summary>
        public ProcessingTimer Timer { get; private set; }

        /// <summary>
        /// Gets the manager for debug images.
        /// </summary>
        public DebugImageManager DebugImageManager { get; private set; }

        /// <summary>
        /// Initializes a new instance of the EyeTrackerDiagnostics class.
        /// </summary>
        public EyeTrackerDiagnostics()
        {
            this.Timer = new ProcessingTimer();
            this.DebugImageManager = new DebugImageManager();
        }
    }
}
