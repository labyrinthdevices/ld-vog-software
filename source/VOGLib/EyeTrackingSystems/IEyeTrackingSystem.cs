﻿//-----------------------------------------------------------------------
// <copyright file="IEyeTrackingSystem.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.Composition;

    /// <summary>
    /// Interface for all eye tracking systems.
    /// </summary>
    public interface IEyeTrackingSystem
    {
        ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages);

        EyeTrackerSystemSettings GetSettings();
    }

    /// <summary>
    /// Base class for the settigns of the eye tracking system.
    /// http://stackoverflow.com/questions/20084/xml-serialization-and-inherited-types
    /// </summary>
    [Serializable]
    public class EyeTrackerSystemSettings : Settings
    {
        public double MmPerPixel
        {
            get
            {
                return this.mmPerPixel;
            }
            set
            {
                if (value != this.mmPerPixel)
                {
                    this.mmPerPixel = value;
                    this.OnPropertyChanged("MmPerPixel");
                }
            }
        }
        private double mmPerPixel = 0.15;

        public double DistanceCameraToEyeMm
        {
            get
            {
                return this.distanceCameraToEyeMm;
            }
            set
            {
                if (value != this.distanceCameraToEyeMm)
                {
                    this.distanceCameraToEyeMm = value;
                    this.OnPropertyChanged("DistanceCameraToEyeMm");
                }
            }
        }
        private double distanceCameraToEyeMm = 50;

        public static EyeTrackerSystemSettings Load(string system)
        {
            // Load the current system settings
            var dir = System.IO.Directory.GetCurrentDirectory();

            EyeTrackerSystemSettings eyeTrackerSystemSettings = null;

            var fileName = dir + "\\EyeTrackerSettings-" + system + ".xml";

            var settings = new System.Xml.XmlReaderSettings();
            settings.ConformanceLevel = System.Xml.ConformanceLevel.Auto;

            if (System.IO.File.Exists(fileName))
            {
                using (var reader = System.Xml.XmlReader.Create(fileName, settings))
                {
                    // Cast the Data back from the Abstract Type.
                    reader.ReadStartElement();
                    string typeAttrib = reader.GetAttribute("type");

                    //// Ensure the Type was Specified
                    //if (typeAttrib == null)
                    //    throw new ArgumentNullException("Unable to Read Xml Data for Abstract Type '" + typeof(AbstractType).Name +
                    //        "' because no 'type' attribute was specified in the XML.");

                    if (typeAttrib != null)
                    {

                        Type type = Type.GetType(typeAttrib);

                        if (type != null)
                        {
                            //// Check the Type is Found.
                            //if (type == null)
                            //    throw new InvalidCastException("Unable to Read Xml Data for Abstract Type '" + typeof(EyeTrackerSystemSettings).Name +
                            //        "' because the type specified in the XML was not found.");

                            //// Check the Type is a Subclass of the AbstractType.
                            //if (!(type == typeof(EyeTrackerSystemSettings)) && !type.IsSubclassOf(typeof(EyeTrackerSystemSettings)))
                            //    throw new InvalidCastException("Unable to Read Xml Data for Abstract Type '" + typeof(EyeTrackerSystemSettings).Name +
                            //        "' because the Type specified in the XML differs ('" + type.Name + "').");

                            reader.ReadStartElement();
                            // Read the Data, Deserializing based on the (now known) concrete type.
                            reader.ReadStartElement();
                            eyeTrackerSystemSettings = (EyeTrackerSystemSettings)new System.Xml.Serialization.XmlSerializer(type).Deserialize(reader);
                            reader.ReadEndElement();
                        }
                    }
                }
            }

            if (eyeTrackerSystemSettings == null)
            {
                var eyeTrackingSystem = (IEyeTrackingSystem)EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.Create(system);
                eyeTrackerSystemSettings = eyeTrackingSystem.GetSettings();
            }

            return eyeTrackerSystemSettings;
        }

        public void Save()
        {
            var dir = System.IO.Directory.GetCurrentDirectory();

            // Save the current system settings
            var fileName = dir + "\\EyeTrackerSettings-" + EyeTracker.Settings.EyeTrackerSystem + ".xml";

            var settings = new System.Xml.XmlWriterSettings();
            settings.ConformanceLevel = System.Xml.ConformanceLevel.Auto;

            using (var writer = System.Xml.XmlWriter.Create(fileName, settings))
            {
                // Write the Type Name to the XML Element as an Attrib and Serialize
                Type type = this.GetType();

                // BugFix: Assembly must be FQN since Types can/are external to current.
                writer.WriteStartElement("type1");
                writer.WriteEndElement();
                writer.WriteStartElement("type1");
                writer.WriteAttributeString("type", type.AssemblyQualifiedName);
                writer.WriteEndElement();

                writer.WriteStartElement("settings");
                new System.Xml.Serialization.XmlSerializer(type).Serialize(writer, this);
                writer.WriteEndElement();
            }
        }
    }

    public interface IEyeTrackingSystemMetadata : IEyeTrackerPluginMetadata
    {
        string Name { get; }
        int NumberOfCameras { get; }
        int NumberOfVideos { get; }
    }

    [MetadataAttribute, AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ExportDescriptionEyeTrackingSystemAttribute : ExportAttribute, IEyeTrackingSystemMetadata
    {
        public string Name { get; private set; }
        public int NumberOfCameras { get; private set; }
        public int NumberOfVideos { get; private set; }

        public ExportDescriptionEyeTrackingSystemAttribute(string name, int numberOfCameras, int numberOfVideos)
            : base(typeof(IEyeTrackerPluginMetadata))
        {
            this.Name = name;
            this.NumberOfCameras = numberOfCameras;
            this.NumberOfVideos = numberOfVideos;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
