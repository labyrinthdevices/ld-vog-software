﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemGenericDualCamera.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using System.Collections.Generic;
    using System.Drawing;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Generic system with any two cameras.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("Generic dual Camera", 2, 2)]
    public class EyeTrackingSystemGenericDualCamera : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider
    {
        /// <summary>
        /// Gets the cameras. In this case two, left and right eye. 
        /// </summary>
        /// <returns>The list of cameras.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            var cameraLefteye = new CameraEyeWebCam(Eye.Left, EyeTracker.Settings.FrameRate);
            cameraLefteye.Init();

            var cameraRightEye = new CameraEyeWebCam(Eye.Right, EyeTracker.Settings.FrameRate);
            cameraRightEye.Init();

            return new EyeCollection<ICameraEye>(cameraLefteye, cameraRightEye);
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> fileNames)
        {
            if (fileNames.Count == 2)
            {
                VideoEye videoLeft = null;
                if (fileNames[Eye.Left].Length > 1)
                {
                  videoLeft=  new VideoEye(Eye.Left, fileNames[Eye.Left]);
                }
                VideoEye videoRight = null;
                if (fileNames[Eye.Right].Length > 1)
                {
                    videoRight = new VideoEye(Eye.Right, fileNames[Eye.Right]);
                }

                return new EyeCollection<VideoEye>( videoLeft, videoRight);
            }
            else
            {
                throw new InvalidOperationException("The number of video files should be two.");
            }
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            return images;
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.15;
            settings.DistanceCameraToEyeMm = 30;

            return settings;
        }
    }
}
