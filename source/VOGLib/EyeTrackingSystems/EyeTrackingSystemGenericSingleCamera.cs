﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemGenericSingleCamera.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Generic system with any one camera.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("Generic single camera", 1, 1)]
    public class EyeTrackingSystemGenericSingleCamera : IEyeTrackingSystem, ICameraEyeProvider, IVideoEyeProvider
    {
        /// <summary>
        /// Gets the cameras. In this case two, left and right eye. 
        /// </summary>
        /// <returns>The list of cameras.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            var camera = new CameraEyeWebCam(Eye.Both, EyeTracker.Settings.FrameRate);
            camera.Init();
            return new EyeCollection<ICameraEye>(camera);
        }

        /// <summary>
        /// Gets the image sources.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        public EyeCollection<VideoEye> GetVideos(EyeCollection<string> fileNames)
        {
            if (fileNames.Count == 2)
            {
                return new EyeCollection<VideoEye>(
                        new VideoEye(Eye.Left, fileNames[Eye.Left]),
                        new VideoEye(Eye.Right, fileNames[Eye.Right]));
            }
            if (fileNames.Count == 1)
            {
                return new EyeCollection<VideoEye>(
                        new VideoEye(Eye.Both, fileNames[Eye.Both]));
            }
            else
            {
                throw new InvalidOperationException("The number of video files must be one or two.");
            }
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromVideos(EyeCollection<ImageEye> images)
        {
            if (images != null)
            {
                if (images.Count == 1 && images[0].WhichEye == Eye.Both)
                {
                    var width = images[0].Size.Width;
                    var height = images[0].Size.Height;

                    var imageLeft = images[0].GetCroppedImage(new Rectangle(width / 2, 0, width / 2, height));
                    var imageRight = images[0].GetCroppedImage(new Rectangle(0, 0, width / 2, height));

                    return new EyeCollection<ImageEye>(
                        new ImageEye(imageLeft.Bitmap, Eye.Left, images[0].TimeStamp),
                        new ImageEye(imageRight.Bitmap, Eye.Right, images[0].TimeStamp));
                }
            }
            return images;
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            if (images.Count == 1 && images[0].WhichEye == Eye.Both)
            {
                var width = images[0].Size.Width;
                var height = images[0].Size.Height;

                var imageLeft = images[0].GetCroppedImage(new Rectangle(width / 2, 0, width / 2, height));
                var imageRight = images[0].GetCroppedImage(new Rectangle(0, 0, width / 2, height));

                return new EyeCollection<ImageEye>(
                    new ImageEye(imageLeft, Eye.Left, images[0].TimeStamp),
                    new ImageEye(imageRight, Eye.Right, images[0].TimeStamp));
            }
            return images;
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.15;
            settings.DistanceCameraToEyeMm = 70;

            return settings;
        }
    }
}
