﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingSystemSimulation.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel.Composition;
    using System.Threading;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.HeadTracking;

    /// <summary>
    /// Simulator of eye tracker from videos.
    /// </summary>
    [Export(typeof(IEyeTrackingSystem)), ExportDescriptionEyeTrackingSystem("Simulation", 2, 0)]
    public class EyeTrackingSystemSimulation : IEyeTrackingSystem, ICameraEyeProvider, IHeadSensorProvider
    {
        /// <summary>
        /// Gets the cameras. In this case just one single camera.
        /// </summary>
        /// <returns>List containing the camera object.</returns>
        public EyeCollection<ICameraEye> GetCameras()
        {
            string currentPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var fileNames = new string[] 
                { 
                    currentPath + @"\JorgeLeft.avi", 
                    currentPath + @"\JorgeRight.avi" 
                };

            var cameraLeft = new CameraEyeVideoSimulation(Eye.Left, fileNames[(int)Eye.Left]);
            cameraLeft.IsMirrored = true;

            var cameraRight = new CameraEyeVideoSimulation(Eye.Right, fileNames[(int)Eye.Right]);
            cameraRight.IsMirrored = true;

            long loopAtFrame = Math.Min(cameraLeft.NumberOfFrames-1, cameraRight.NumberOfFrames-1);
            cameraLeft.LoopAtFrame = loopAtFrame;
            cameraRight.LoopAtFrame = loopAtFrame;

            return new EyeCollection<ICameraEye>(cameraLeft, cameraRight);
        }

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        public EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images)
        {
            return images;
        }

        public IHeadSensor GetHeadSensor()
        {
            return null;
        }

        public ExtraData GetExtraData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            return new ExtraData();
        }

        /// <summary>
        /// Gets the settings specific to the eye tracking system.
        /// </summary>
        /// <returns>The settings.</returns>
        public EyeTrackerSystemSettings GetSettings()
        {
            var settings = new EyeTrackerSystemSettings();
            settings.MmPerPixel = 0.15;
            settings.DistanceCameraToEyeMm = 70;

            return settings;
        }
    }
}
