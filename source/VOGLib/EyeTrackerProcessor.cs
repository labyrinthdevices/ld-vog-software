﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerProcessor.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using OculomotorLab.VOG.ImageProcessing;

    /// <summary>
    /// Class in charge of the background thread that processes images. It implements a multiplexed producer consumer.
    /// The processor is at the same time a producer and a consumer of images. It puts then in a concurrent queue and then several
    /// processing threads process the images and place them in another concurrent queue. Then, the output thread will
    /// go through that queue reorder the frames properly (they may have been finished processed out of order) and 
    /// propagates the new data so other entities can use it.
    /// </summary>
    /// <remarks>The processor objects receives the frames from the <see cref="ProcessFrame"/> method and outputs the 
    /// processed frames with the <see cref=" NewProcessedImageAvailable"/> event.</remarks>
    internal class EyeTrackerProcessor : IDisposable
    {
        /// <summary>
        /// Possible states of the eye tracker processor.
        /// </summary>
        private enum State
        {
            /// <summary>
            /// Initializing. No commands should be sent during this time.
            /// </summary>
            Initializing,

            /// <summary>
            /// Collecting images and tracking.
            /// </summary>
            Processing,

            /// <summary>
            /// Clearing the queue.
            /// </summary>
            ClearingBuffer,

            /// <summary>
            /// Getting ready to close. Disposing elements.
            /// </summary>
            Closing,
        }

        /// <summary>
        ///  Maximum number of errors before the processing loop breaks.
        /// </summary>
        private const long maxNumberOfErrors = 20;

        /// <summary>
        /// Current state of the eye tracker processor.
        /// </summary>
        private State state;

        /// <summary>
        /// Thread that runs the processing asynchronously.
        /// </summary>
        private Thread[] processingThreads;

        /// <summary>
        /// Thread that runs reads the output queues and raises events.
        /// </summary>
        private Thread outputThread;

        /// <summary>
        /// Buffer of images to be processed.
        /// </summary>
        private BlockingCollection<ImagesEyeWithNumber> inputQueue;

        /// <summary>
        /// Buffer of images already processed
        /// </summary>
        private BlockingCollection<ProcessedImagesEyeWithNumber> outputQueue;

        /// <summary>
        /// Counter for exceptions during the process image loop. It serves the purpose to allow a few errors and keep going.
        /// </summary>
        private long errorCounter = 0;

        /// <summary>
        /// Calibration object.
        /// </summary>
        private EyeTrackerCalibrationManager calibrationManager;
        
        /// <summary>
        /// Initializes a new instance of the EyeTrackerProcess class.
        /// </summary>
        /// <param name="calibrationManager">Calibration manager object.</param>
        public EyeTrackerProcessor(EyeTrackerCalibrationManager calibrationManager)
        {
            this.state = State.Initializing;
            this.calibrationManager = calibrationManager;

            // By default the number of processing threads will be half of the number of processors.
            // Each processing thread will run two parallel tasks. One for each eye.
            // this.NumberOfProcessingThreads = (int)Math.Floor(System.Environment.ProcessorCount / 2.0);
            this.NumberOfProcessingThreads = EyeTracker.Settings.Tracking.NumberOfProcessingThreads;
        }

        /// <summary>
        /// Notifies listeners that a frame has been processed and new data is available.
        /// </summary>
        public event EventHandler<EyeCollection<ProcessedImageEye>> NewProcessedImageAvailable;

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        public event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        /// <summary>
        /// Gets or sets a value indicating wheter the processor can drop frames or not.
        /// If not it will block in the ProcessFrame method until there is room in the 
        /// buffer.
        /// </summary>
        public bool AvoidDropFrames { get; set; }

        /// <summary>
        /// Gets the number of procesing threads (by default number of processors / 2)
        /// </summary>
        public int NumberOfProcessingThreads { get; private set; }

        /// <summary>
        /// Gets the total number of frames that could not be processed because the buffer was full.
        /// </summary>
        public int NumberFramesDropped { get; private set; }

        /// <summary>
        /// Gets the total number of frames that could not be processed because the buffer was full.
        /// </summary>
        public int NumberFramesMissed { get; private set; }

        /// <summary>
        /// Gets the total number of frames that could be processed.
        /// </summary>
        public int NumberFramesProcessed { get; private set; }

        /// <summary>
        /// Gets the number of frames waiting in the buffer.
        /// </summary>
        public int NumberFramesInInputBuffer
        {
            get
            {
                return this.inputQueue.Count;
            }
        }

        /// <summary>
        /// Starts the processing thread.
        /// </summary>
        public void Start()
        {
            this.NumberFramesDropped = 0;
            this.NumberFramesMissed = 0;
            this.NumberFramesProcessed = 0;

            this.inputQueue = new BlockingCollection<ImagesEyeWithNumber>(
                new ConcurrentQueue<ImagesEyeWithNumber>(),
                EyeTracker.Settings.ProcessingBufferSize);

            this.outputQueue = new BlockingCollection<ProcessedImagesEyeWithNumber>(
                new ConcurrentQueue<ProcessedImagesEyeWithNumber>(),
                EyeTracker.Settings.ProcessingBufferSize);

            // Start the output thread first to minimize the chance of dropping a frame
            this.outputThread = new Thread(new ThreadStart(this.OutputLoop));
            this.outputThread.Name = "EyeTracker:ProcessOutputLoop";
            this.outputThread.Start();

            this.processingThreads = new Thread[this.NumberOfProcessingThreads];
            for (int j = 0; j < this.NumberOfProcessingThreads; j++)
            {
                this.processingThreads[j] = new Thread(new ThreadStart(this.ProcessLoop));
                this.processingThreads[j].Name = "EyeTracker:ProcessLoop" + j;
                this.processingThreads[j].Start();
            }

            this.state = State.Processing;
        }

        /// <summary>
        /// Stops the processing and dispose resources.
        /// </summary>
        public void Stop()
        {
            this.state = State.Closing;

            // Do not add more items to the input queue
            if (this.inputQueue != null)
            {
                this.inputQueue.CompleteAdding();
            }

            // Wait for the processing threads to finish.
            foreach (var thread in this.processingThreads)
            {
                if (thread != null)
                {
                    thread.Join();
                }
            }

            // Now that the processing threads are done. 
            // Do not add more items to the output queue
            if (this.outputQueue != null)
            {
                this.outputQueue.CompleteAdding();
            }

            // Wait for the output thread to finish.
            if (this.outputThread != null)
            {
                this.outputThread.Join();
            }

            // Dispose the objects
            if (inputQueue != null)
            {
                this.inputQueue.Dispose();
                this.inputQueue = null;
            }

            if (outputQueue != null)
            {
                this.outputQueue.Dispose();
                this.outputQueue = null;
            }
        }

        /// <summary>
        /// Adds a frame to the processing queue, if it is full waits.
        /// </summary>
        /// <param name="imagesEye">Images to be processed.</param>
        public bool ProcessFrame(EyeCollection<ImageEye> imagesEye)
        {
            if (this.inputQueue.IsAddingCompleted)
            {
                throw new InvalidOperationException("The processing has already been stopped, cannot process more frames.");
            }

            if (AvoidDropFrames)
            {
                // this option will not drop frames using for offline processing
                // The thread will get blocked here until there is room in the buffer
                this.inputQueue.Add(new ImagesEyeWithNumber(imagesEye, this.NumberFramesProcessed));
                this.NumberFramesProcessed++;
                return true;
            }
            else
            {
                // this option may drop frames used during real time

                // Add the frame images to the input queue for processing
                if (this.inputQueue.TryAdd(new ImagesEyeWithNumber(imagesEye, this.NumberFramesProcessed)))
                {
                    this.NumberFramesProcessed++;
                    return true;
                }
                else
                {
                    // If the buffer is full return false
                    this.NumberFramesDropped++;
                    return false;
                }
            }
        }

        /// <summary>
        /// Empty the queue.
        /// </summary>
        public void ClearBuffer()
        {
            this.state = State.ClearingBuffer;
        }

        /// <summary>
        /// Dispose resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  Dispose resources.
        /// </summary>
        /// <param name="disposing">True if disposing resources.</param>
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Stop();
            }
        }

        /// <summary>
        /// Raises the event NewProcessedImageAvailable.
        /// </summary>
        /// <param name="eventArgs">Data from current frame.</param>
        protected void OnNewProcessedImageAvailable(EyeCollection<ProcessedImageEye> processedImages)
        {
            var handler = this.NewProcessedImageAvailable;
            if (handler != null)
            {
                handler(this, processedImages);
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Process loop that runs on a separate thread processing each frame whenever available in the buffer.
        /// Each image of left and right eye are processed themselves in different threads (Tasks).
        /// </summary>
        private void ProcessLoop()
        {
            var token = new CancellationTokenSource();

            // Create one imageEyeProcess object and one task for each eye (left and right)
            var imageEyeProcess = new EyeCollection<ImageEyeProcess>( new ImageEyeProcess(Eye.Left), new ImageEyeProcess(Eye.Right) );
            var taskArray = new Task[2];

            try
            {
                // Keep processing images until the buffer is marked as complete and empty
                foreach (var imagesEye in this.inputQueue.GetConsumingEnumerable(token.Token))
                {
                    var processedImagesEye = new ProcessedImageEye[2];

                    // Clear the queue without processing
                    if (this.state == State.ClearingBuffer)
                    {
                        if (this.inputQueue.Count == 0)
                        {
                            this.state = State.Processing;
                        }

                        this.outputQueue.Add(new ProcessedImagesEyeWithNumber(new EyeCollection<ProcessedImageEye>(processedImagesEye), imagesEye.OrderNumber));
                        continue;
                    }

                    // Process the left and the right eye images in two parallel tasks
                    foreach (var eye in new Eye[] { Eye.Left, Eye.Right })
                    {
                        var whicheye = eye; // This is necessary each thread gets the proper value in his own copy of the variable

                        taskArray[(int)whicheye] = Task.Run(() =>
                        {
                            try
                            {
                                Thread.CurrentThread.Name = "TASK " + whicheye;

                                EyeTracker.Diagnostics.Timer.TrackTimeBeginingFrame();

                                processedImagesEye[(int)whicheye] = imageEyeProcess[whicheye].ProcessImageEye(
                                    imagesEye.Images[whicheye],
                                    this.calibrationManager.CalibrationParameters.EyeCalibrationParameters[whicheye]);

                                EyeTracker.Diagnostics.Timer.TrackTimeEndFrame();

                            }
                            catch (Exception ex)
                            {
                                this.errorCounter++;
                                System.Diagnostics.Trace.WriteLine("ERROR:" + ex.ToString(), "ImageEyeProcess");
                                processedImagesEye[(int)whicheye] = new ProcessedImageEye(imagesEye.Images[whicheye], ProcessFrameResult.ProcessingError);
                            }
                        });
                    }

                    // Wait for the processing of both eyes to finish
                    Task.WaitAll(taskArray);

                    if (this.errorCounter > EyeTrackerProcessor.maxNumberOfErrors)
                    {
                        this.OnErrorOccurred(new ErrorOccurredEventArgs(new Exception("Too many errors")));
                    }

                    this.outputQueue.Add(new ProcessedImagesEyeWithNumber(new EyeCollection<ProcessedImageEye>(processedImagesEye), imagesEye.OrderNumber));
                }

                System.Diagnostics.Trace.WriteLine("Process loop finished.");
            }
            catch (Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
        }

        /// <summary>
        /// Consumer loop. Gets images from the output queue and reorders them before raising the events
        /// to notify listeners that new data is available.
        /// </summary>
        private void OutputLoop()
        {
            // List of frames that have been processed out of order.
            var waitingList = new List<ProcessedImagesEyeWithNumber>();

            var token = new CancellationTokenSource();
            var maxDelay = EyeTracker.Settings.ProcessingBufferSize;

            var nextExpectedFrame = 0;

            try
            {
                // Keep processing images until the buffer is marked as complete and empty
                foreach (var processedImagesEye in this.outputQueue.GetConsumingEnumerable(token.Token))
                {
                    var currentFrameNumber = processedImagesEye.OrderNumber;

                    // If the image is the one I am waiting for
                    if (nextExpectedFrame == currentFrameNumber)
                    {
                        // Raise an event notifying that a new image was processed
                        this.OnNewProcessedImageAvailable(processedImagesEye.Images);

                        // Increament the current frame number
                        nextExpectedFrame++;
                    }

                    // If it is not the one waiting for add it to the queue
                    if (nextExpectedFrame < currentFrameNumber)
                    {
                        waitingList.Add(processedImagesEye);
                    }

                    // If the frame we are waiting for is too far behind update it
                    if (currentFrameNumber > nextExpectedFrame + maxDelay)
                    {
                        // Next expected frame never arrived or arrived too late.
                        nextExpectedFrame++;
                        this.NumberFramesMissed++;
                    }

                    // Go thru the queue to look for the current frame number
                    int i = 0;
                    while (i < waitingList.Count)
                    {
                        var images = waitingList[i];

                        // If the image we are waiting for is in the list
                        if (images.OrderNumber == nextExpectedFrame)
                        {
                            // Remove the item
                            waitingList.Remove(images);

                            // Raise an event notifying that a new image was processed
                            this.OnNewProcessedImageAvailable(images.Images);

                            // Increament the current frame number
                            nextExpectedFrame++;

                            // Start again from the begining of the list just in case the next
                            // expected frame is also there
                            i = 0;
                            continue;
                        }

                        i++;
                    }
                }

                System.Diagnostics.Trace.WriteLine("Output loop finished.");
            }
            catch (Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
        }

        /// <summary>
        /// Class for wrapping the images with a processing number to mantain order.
        /// </summary>
        private class ImagesEyeWithNumber
        {
            public long OrderNumber { get; private set; }

            public EyeCollection<ImageEye> Images { get; private set; }

            public ImagesEyeWithNumber(EyeCollection<ImageEye> images, long number)
            {
                this.OrderNumber = number;
                this.Images = images;
            }
        }

        /// <summary>
        /// Class for wrapping the images with a processing number to mantain order.
        /// </summary>
        private class ProcessedImagesEyeWithNumber
        {
            public long OrderNumber { get; private set; }

            public EyeCollection<ProcessedImageEye> Images { get; private set; }

            public ProcessedImagesEyeWithNumber(EyeCollection<ProcessedImageEye> images, long number)
            {
                this.OrderNumber = number;
                this.Images = images;
            }
        }
    }
}