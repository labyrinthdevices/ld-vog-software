﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OculomotorLab.VOG
{
    class EyeTrackerPostProcessor
    {
        /// <summary>
        /// Gets a value indicating wether this is a recording corresponding with postprocessing.
        /// </summary>
        public bool Processing { get; private set; }

        /// <summary>
        /// Event that is raised whenever a video finishes playing.
        /// </summary>
        public event EventHandler<EventArgs> ProcessingVideoFinished;
    }
}
