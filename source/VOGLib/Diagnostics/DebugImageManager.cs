﻿//-----------------------------------------------------------------------
// <copyright file="DebugImageManager.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.Diagnostics
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Linq;
    using Emgu.CV;
    using Emgu.CV.Structure;

    public class DebugImageManager
    {
        private static EyeCollection<ConcurrentDictionary<string, Image<Bgr, byte>>> images;

        public string[] GetImageNames()
        {
            var keysLeft = images[Eye.Left].Keys;
            var keysRight = images[Eye.Right].Keys;
            var names = keysLeft.Union(keysRight).ToArray();
            Array.Sort(names);
            return names;
        }

        public Image<Bgr, byte> GetImage(string key, Eye whichEye)
        {
            if (images[whichEye].ContainsKey(key))
            {
                return images[whichEye][key];
            }
            else
            {
                return null;
            }
        }

        public void AddImage(string key, Eye whichEye, Image<Gray, byte> image)
        {
            this.AddImage(key, whichEye, image.Convert<Bgr,byte>());
        }

        public void AddImage(string key, Eye whichEye, Image<Bgr, byte> image)
        {
            if (EyeTracker.Settings.Debug)
            {
                if (images == null)
                {
                    images = new EyeCollection<ConcurrentDictionary<string, Image<Bgr, byte>>>(
                        new ConcurrentDictionary<string, Image<Bgr, byte>>(),
                        new ConcurrentDictionary<string, Image<Bgr, byte>>()
                        );
                }

                if (images[whichEye].ContainsKey(key))
                {
                    images[whichEye][key] = image;
                }
                else
                {
                    images[whichEye].TryAdd(key, image);
                }
            }
        }
    }
}
