﻿//-----------------------------------------------------------------------
// <copyright file="ProcessingTimer.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.Diagnostics
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to keep track of processing time for each frame.
    /// </summary>
    public class ProcessingTimer
    {
        /// <summary>
        /// Dictionary of deltaTimes. Keeps a moving average of a given delta time.
        /// </summary>
        private ConcurrentDictionary<string, double> deltaTimes;

        /// <summary>
        /// For each thread (ThreadID is the key of the dictionary) saves
        /// the last message tracked.
        /// </summary>
        private string[] previousMessages;

        /// <summary>
        /// For each thread (ThreadID is the key of the dictionary) saves
        /// the last time tracked.
        /// </summary>
        private double[] previousTimes;

        /// <summary>
        /// For each thread (ThreadID is the key of the dictionary) saves
        /// the last reset time tracked.
        /// </summary>
        private double[] beginFrameTime;

        /// <summary>
        /// Stopwatch to measure the time it takes to process.
        /// </summary>
        private Stopwatch stopWatch;

        /// <summary>
        /// Initializes a new instance of the TimeTracker class.
        /// </summary>
        internal ProcessingTimer()
        {
            this.stopWatch = new Stopwatch();
            this.stopWatch.Start();

            this.deltaTimes = new ConcurrentDictionary<string, double>();

            this.previousMessages = new string[1000];
            this.previousTimes = new double[1000];
            this.beginFrameTime = new double[1000];
        }

        /// <summary>
        /// Converts the Timer data to a string.
        /// </summary>
        /// <returns>The string with all the timing info.</returns>
        public override string ToString()
        {
            string s = string.Empty;
            foreach (var time in this.deltaTimes)
            {
                s = s + time.Key.PadRight(50, '.') + " : " + string.Format("{0:0.00}", time.Value) + "\r\n";
            }

            return s;
        }

        /// <summary>
        /// Tracks a partial time.
        /// </summary>
        /// <param name="message">Message describing the partial time step.</param>
        internal double TrackTime(string message)
        {
            double deltaTime = 0.0;

            // Get the ID of the current thread to only track delta times within the same thread
            var threadID = System.Threading.Thread.CurrentThread.ManagedThreadId;

            // Check if there is a previos time tracked for the current thread

            var thisTime = this.stopWatch.Elapsed.TotalMilliseconds;

            if (this.previousTimes[threadID] > 0)
            {
                var previousTime = this.previousTimes[threadID];
                var previousMessage = this.previousMessages[threadID];

                // Delta message serves a code for a section of the code
                // the times with the same delta message will be averaged and displayed
                // together
                var deltaMessage = previousMessage + "->" + message;
                deltaTime = thisTime - previousTime;

                if (this.deltaTimes.ContainsKey(deltaMessage))
                {
                    this.deltaTimes[deltaMessage] = (this.deltaTimes[deltaMessage] * 0.99) + (deltaTime * 0.01);
                }
                else
                {
                    this.deltaTimes.TryAdd(deltaMessage, this.stopWatch.Elapsed.TotalMilliseconds);
                }
            }

            this.previousMessages[threadID] = message;
            this.previousTimes[threadID] = thisTime;

            return deltaTime;
        }

        /// <summary>
        /// Resets the partial times for a new frame.
        /// </summary>
        internal void TrackTimeBeginingFrame()
        {
            var threadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
            this.beginFrameTime[threadID] = this.stopWatch.Elapsed.TotalMilliseconds;
            this.TrackTime("FRAME START");
        }

        /// <summary>
        /// Tracks the time at the end of processing a frame.
        /// </summary>
        /// <returns>Total time in the frame.</returns>
        internal double TrackTimeEndFrame()
        {
            var threadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
            var deltaTime = this.stopWatch.Elapsed.TotalMilliseconds - this.beginFrameTime[threadID];

            var deltaMessage = string.Empty;
            if (System.Threading.Thread.CurrentThread.Name != null)
            {
                deltaMessage = System.Threading.Thread.CurrentThread.Name + "-TOTAL";
            }
            else
            {
                deltaMessage = "TOTAL" + "-" + threadID; ;
            }

            if (this.deltaTimes.ContainsKey(deltaMessage))
            {
                this.deltaTimes[deltaMessage] = (this.deltaTimes[deltaMessage] * 0.95) + (deltaTime * 0.05);
            }
            else
            {
                this.deltaTimes.TryAdd(deltaMessage, this.stopWatch.Elapsed.TotalMilliseconds);
            }

            // Remove the previous times for this thread
            this.previousMessages[threadID] = string.Empty;
            this.previousTimes[threadID] = 0;

            return deltaTime;
        }
    }
}
