﻿//-----------------------------------------------------------------------
// <copyright file="CameraEyeVirtualEye.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Emgu.CV;
    using Emgu.CV.Structure;


    public class CameraEyeVirtualEye : ICameraEye
    {
        private DateTime initialTime;
        private float frameRate;
        private DateTime timeLastImage;
        private OculomotorLab.VOG.EyeTrackingSystems.FakeEyeControlUI UI;

        private Image<Gray, byte> baseImage;

        /// <summary>
        /// Initializes an instance of the CameraEyeVirtualEye class.
        /// </summary>
        public CameraEyeVirtualEye(Eye whichEye, OculomotorLab.VOG.EyeTrackingSystems.FakeEyeControlUI controller)
        {
            this.WhichEye = whichEye;
            this.FrameSize = new Size(400, 400);
            this.frameRate = 100f;
            this.queue = new ImageEyeQueue();
            this.UI = controller;

            controller.NewGaze += controller_NewGaze;
        }

        EyeTrackingSystems.GazeOrientation gaze;

        void controller_NewGaze(object sender, EyeTrackingSystems.GazeOrientation e)
        {
            gaze = e;
        }

        #region IImageEyeSource Members

        public Eye WhichEye { get; private set; }

        public ImageEyeQueue queue { get; private set; }

        public long FrameCounter { get; private set; }

        public long DroppedFramesCounter
        {
            get { return 0; }
        }

        public double ReportedFrameRate
        {
            get { return this.frameRate; }
        }

        public bool IsUpsideDown { get; set; }

        public bool IsMirrored { get; set; }

        public Size FrameSize { get; private set; }

        public string Diagnostics
        {
            get { return string.Empty; }
        }

        public ImageEye GrabImageEye()
        {
            var pupilCenter = new PointF(200, 200);

            var pupilR = 20;
            var irisR = 80;
            var eyeGlobe = new EyePhysicalModel(new PointF(200, 200), 160);

            if (this.baseImage == null)
            {
                this.baseImage = new Image<Gray, byte>(this.FrameSize);

                this.baseImage.SetValue(new Gray(128));
                this.baseImage.Draw(new CircleF(eyeGlobe.Center, eyeGlobe.Radius), new Gray(230), 0);
                this.baseImage.Draw(new CircleF(pupilCenter, irisR), new Gray(100), 0);

                var Nsegments = 500;
                for (int i = 0; i < Nsegments; i++)
                {
                    //var theta = 2 * Math.PI / Nsegments * i + gaze.Torsion * Math.PI / 4 + 0*Math.Round(3.0 * Math.Sin(7069 * (double)i / Nsegments * 2.0 * Math.PI));

                    //this.baseImage.Draw(
                    //    new LineSegment2D(
                    //        new Point((int)(Math.Cos(theta) * irisR + pupilCenter.X), (int)(Math.Sin(theta) * irisR + pupilCenter.Y)),
                    //        new Point((int)pupilCenter.X, (int)pupilCenter.Y)),
                    //    new Gray(200), (int)Math.Round(4.0 + 3.0 * Math.Sin(7069 * (double)i / Nsegments * 2.0 * Math.PI)));


                    var theta = 2 * Math.PI / Nsegments * i + gaze.Torsion * Math.PI / 4 + 4 * Math.Round(30.0 * Math.Sin(7069 * (double)i / Nsegments * 2.0 * Math.PI));

                    int offset = 0;
                    Math.DivRem(7069 * i, 10, out offset);

                    var a = 0.2f + offset / 12f;
                    var b = 0f + offset / 12f;

                    this.baseImage.Draw(
                        new LineSegment2D(
                            new Point((int)(Math.Cos(theta) * irisR * a + pupilCenter.X), (int)(Math.Sin(theta) * irisR * a + pupilCenter.Y)),
                            new Point((int)(Math.Cos(theta) * irisR * b + pupilCenter.X), (int)(Math.Sin(theta) * irisR * b + pupilCenter.Y))),
                        new Gray(120 + 50 * Math.Sin(7069 * (double)i / Nsegments * 2.0 * Math.PI)), 4);
                }

                this.baseImage.Draw(new CircleF(pupilCenter, pupilR), new Gray(10), 0);
            }


            // ROTATE THE SPHERE
            var x1 = (int)Math.Floor(eyeGlobe.Center.X - eyeGlobe.Radius);
            var x2 = (int)Math.Ceiling(eyeGlobe.Center.X + eyeGlobe.Radius);
            var y1 = (int)Math.Max(0, Math.Floor(eyeGlobe.Center.Y - eyeGlobe.Radius));
            var y2 = (int)Math.Min(this.FrameSize.Height - 1, Math.Ceiling(eyeGlobe.Center.Y + eyeGlobe.Radius));

            var mapx = new Image<Gray, float>(this.FrameSize);
            var mapy = new Image<Gray, float>(this.FrameSize);



            var dt = (DateTime.Now - this.initialTime).TotalSeconds;
            var angle = gaze.Angle; // axis of rotation
            var ecc = gaze.Eccentricity; // ammount of rotation
            var q = new Quaternions(Math.Cos(ecc / 2), -Math.Sin(angle) * Math.Sin(ecc / 2), Math.Cos(angle) * Math.Sin(ecc / 2), 0);

            for (int x = x1; x <= x2; x++)
            {
                for (int y = y1; y <= y2; y++)
                {
                    double r = Math.Sqrt((x - eyeGlobe.Center.X) * (x - eyeGlobe.Center.X) + (y - eyeGlobe.Center.Y) * (y - eyeGlobe.Center.Y)) / eyeGlobe.Radius;

                    if (r <= 1)
                    {
                        var z = Math.Sqrt(1 - r * r) * eyeGlobe.Radius;

                        var p = new MCvPoint3D64f(x - eyeGlobe.Center.X, y - eyeGlobe.Center.Y, z);

                        var p2 = q.RotatePoint(p);


                        mapx.Data[y, x, 0] = (float)p2.x + eyeGlobe.Center.X;
                        mapy.Data[y, x, 0] = (float)p2.y + eyeGlobe.Center.Y;
                    }
                }
            }


            var image = new Image<Gray, byte>(this.FrameSize);

            CvInvoke.cvRemap(this.baseImage, image, mapx, mapy, (int)Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC | (int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS, new MCvScalar(255));

            image.Draw(new LineSegment2D(new Point(0, (int)pupilCenter.Y + 200), new Point(400, (int)pupilCenter.Y + 200)), new Gray(80), 40);
            image.Draw(new LineSegment2D(new Point(0, (int)pupilCenter.Y - 200), new Point(400, (int)pupilCenter.Y - 200)), new Gray(80), 40);

            ImageEyeTimestamp t = new ImageEyeTimestamp();
            t.DateTimeGrabbed = DateTime.Now;
            t.FrameNumber = FrameCounter;
            t.FrameNumberRaw = FrameCounter;
            t.Seconds = (t.DateTimeGrabbed - this.initialTime).TotalSeconds;

            this.FrameCounter++;
            this.timeLastImage = t.DateTimeGrabbed;

            System.Threading.Thread.Sleep((int)Math.Max(0, 10 - (t.DateTimeGrabbed - this.timeLastImage).TotalMilliseconds));
            return new ImageEye(image, this.WhichEye, t);
        }

        public void Init()
        {
        }

        public void StartCapture()
        {
            initialTime = DateTime.Now;
        }

        public void StopCapture()
        {
            if (!UI.IsDisposed)
            {
                UI.Close();
                UI.Dispose();
            }
        }

        
        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        /// <param name="disposing">Value indicating wether it should disposes resources.</param>
        protected void Dispose(bool disposing)
        {
            if ( disposing)
            {
                this.baseImage.Dispose();
            }
        }

        #endregion
    }
}
