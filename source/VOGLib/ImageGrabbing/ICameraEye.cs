﻿//-----------------------------------------------------------------------
// <copyright file="ICameraEye.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    public interface ICameraEye : IImageEyeSource
    {
    }
}
