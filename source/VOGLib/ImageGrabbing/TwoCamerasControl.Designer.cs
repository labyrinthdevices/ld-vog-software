﻿namespace OculomotorLab.VOG.ImageGrabbing
{
    partial class TwoCamerasControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxCameraPosition = new System.Windows.Forms.GroupBox();
            this.buttonCenterEyes = new System.Windows.Forms.Button();
            this.buttonMoveLeftEyeDown = new System.Windows.Forms.Button();
            this.buttonRightRight = new System.Windows.Forms.Button();
            this.buttonMoveLeftEyeUp = new System.Windows.Forms.Button();
            this.buttonMoveLeftRight = new System.Windows.Forms.Button();
            this.buttonMoveRightEyeUp = new System.Windows.Forms.Button();
            this.buttonMoveLeftLeft = new System.Windows.Forms.Button();
            this.buttonMoveRightEyeDown = new System.Windows.Forms.Button();
            this.buttonMoveRightLeft = new System.Windows.Forms.Button();
            this.groupBoxCameraPosition.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxCameraPosition
            // 
            this.groupBoxCameraPosition.Controls.Add(this.buttonCenterEyes);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveLeftEyeDown);
            this.groupBoxCameraPosition.Controls.Add(this.buttonRightRight);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveLeftEyeUp);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveLeftRight);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveRightEyeUp);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveLeftLeft);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveRightEyeDown);
            this.groupBoxCameraPosition.Controls.Add(this.buttonMoveRightLeft);
            this.groupBoxCameraPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxCameraPosition.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBoxCameraPosition.Location = new System.Drawing.Point(0, 0);
            this.groupBoxCameraPosition.Name = "groupBoxCameraPosition";
            this.groupBoxCameraPosition.Size = new System.Drawing.Size(291, 131);
            this.groupBoxCameraPosition.TabIndex = 31;
            this.groupBoxCameraPosition.TabStop = false;
            this.groupBoxCameraPosition.Text = "Camera position";
            // 
            // buttonCenterEyes
            // 
            this.buttonCenterEyes.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCenterEyes.Location = new System.Drawing.Point(75, 22);
            this.buttonCenterEyes.Name = "buttonCenterEyes";
            this.buttonCenterEyes.Size = new System.Drawing.Size(131, 86);
            this.buttonCenterEyes.TabIndex = 4;
            this.buttonCenterEyes.Text = "Center eyes";
            this.buttonCenterEyes.UseVisualStyleBackColor = true;
            this.buttonCenterEyes.Click += new System.EventHandler(this.buttonCenterEyes_Click);
            // 
            // buttonMoveLeftEyeDown
            // 
            this.buttonMoveLeftEyeDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveLeftEyeDown.Location = new System.Drawing.Point(212, 84);
            this.buttonMoveLeftEyeDown.Name = "buttonMoveLeftEyeDown";
            this.buttonMoveLeftEyeDown.Size = new System.Drawing.Size(60, 24);
            this.buttonMoveLeftEyeDown.TabIndex = 29;
            this.buttonMoveLeftEyeDown.Text = "DOWN";
            this.buttonMoveLeftEyeDown.UseVisualStyleBackColor = true;
            this.buttonMoveLeftEyeDown.Click += new System.EventHandler(this.buttonMoveLeftEyeDown_Click);
            // 
            // buttonRightRight
            // 
            this.buttonRightRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRightRight.Location = new System.Drawing.Point(45, 52);
            this.buttonRightRight.Name = "buttonRightRight";
            this.buttonRightRight.Size = new System.Drawing.Size(24, 24);
            this.buttonRightRight.TabIndex = 27;
            this.buttonRightRight.Text = ">>";
            this.buttonRightRight.UseVisualStyleBackColor = true;
            this.buttonRightRight.Click += new System.EventHandler(this.buttonRightRight_Click); 
            // 
            // buttonMoveLeftEyeUp
            // 
            this.buttonMoveLeftEyeUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveLeftEyeUp.Location = new System.Drawing.Point(212, 22);
            this.buttonMoveLeftEyeUp.Name = "buttonMoveLeftEyeUp";
            this.buttonMoveLeftEyeUp.Size = new System.Drawing.Size(60, 24);
            this.buttonMoveLeftEyeUp.TabIndex = 28;
            this.buttonMoveLeftEyeUp.Text = "UP";
            this.buttonMoveLeftEyeUp.UseVisualStyleBackColor = true;
            this.buttonMoveLeftEyeUp.Click += new System.EventHandler(this.buttonMoveLeftEyeUp_Click); 
            // 
            // buttonMoveLeftRight
            // 
            this.buttonMoveLeftRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveLeftRight.Location = new System.Drawing.Point(248, 52);
            this.buttonMoveLeftRight.Name = "buttonMoveLeftRight";
            this.buttonMoveLeftRight.Size = new System.Drawing.Size(24, 24);
            this.buttonMoveLeftRight.TabIndex = 27;
            this.buttonMoveLeftRight.Text = ">>";
            this.buttonMoveLeftRight.UseVisualStyleBackColor = true;
            this.buttonMoveLeftRight.Click += new System.EventHandler(this.buttonMoveLeftRight_Click);
            // 
            // buttonMoveRightEyeUp
            // 
            this.buttonMoveRightEyeUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveRightEyeUp.Location = new System.Drawing.Point(9, 22);
            this.buttonMoveRightEyeUp.Name = "buttonMoveRightEyeUp";
            this.buttonMoveRightEyeUp.Size = new System.Drawing.Size(60, 24);
            this.buttonMoveRightEyeUp.TabIndex = 25;
            this.buttonMoveRightEyeUp.Text = "UP";
            this.buttonMoveRightEyeUp.UseVisualStyleBackColor = true;
            this.buttonMoveRightEyeUp.Click += new System.EventHandler(this.buttonMoveRightEyeUp_Click);
            // 
            // buttonMoveLeftLeft
            // 
            this.buttonMoveLeftLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveLeftLeft.Location = new System.Drawing.Point(212, 52);
            this.buttonMoveLeftLeft.Name = "buttonMoveLeftLeft";
            this.buttonMoveLeftLeft.Size = new System.Drawing.Size(24, 24);
            this.buttonMoveLeftLeft.TabIndex = 27;
            this.buttonMoveLeftLeft.Text = "<<";
            this.buttonMoveLeftLeft.UseVisualStyleBackColor = true;
            this.buttonMoveLeftLeft.Click += new System.EventHandler(this.buttonMoveLeftLeft_Click); 
            // 
            // buttonMoveRightEyeDown
            // 
            this.buttonMoveRightEyeDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveRightEyeDown.Location = new System.Drawing.Point(9, 84);
            this.buttonMoveRightEyeDown.Name = "buttonMoveRightEyeDown";
            this.buttonMoveRightEyeDown.Size = new System.Drawing.Size(60, 24);
            this.buttonMoveRightEyeDown.TabIndex = 26;
            this.buttonMoveRightEyeDown.Text = "DOWN";
            this.buttonMoveRightEyeDown.UseVisualStyleBackColor = true;
            this.buttonMoveRightEyeDown.Click +=  new System.EventHandler(this.buttonMoveRightEyeDown_Click);
            // 
            // buttonMoveRightLeft
            // 
            this.buttonMoveRightLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMoveRightLeft.Location = new System.Drawing.Point(9, 52);
            this.buttonMoveRightLeft.Name = "buttonMoveRightLeft";
            this.buttonMoveRightLeft.Size = new System.Drawing.Size(24, 24);
            this.buttonMoveRightLeft.TabIndex = 27;
            this.buttonMoveRightLeft.Text = "<<";
            this.buttonMoveRightLeft.UseVisualStyleBackColor = true;
            this.buttonMoveRightLeft.Click += new System.EventHandler(this.buttonMoveRightLeft_Click);
            // 
            // CameraControllerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxCameraPosition);
            this.Name = "CameraControllerControl";
            this.Size = new System.Drawing.Size(291, 131);
            this.groupBoxCameraPosition.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxCameraPosition;
        private System.Windows.Forms.Button buttonCenterEyes;
        private System.Windows.Forms.Button buttonMoveLeftEyeDown;
        private System.Windows.Forms.Button buttonRightRight;
        private System.Windows.Forms.Button buttonMoveLeftEyeUp;
        private System.Windows.Forms.Button buttonMoveLeftRight;
        private System.Windows.Forms.Button buttonMoveRightEyeUp;
        private System.Windows.Forms.Button buttonMoveLeftLeft;
        private System.Windows.Forms.Button buttonMoveRightEyeDown;
        private System.Windows.Forms.Button buttonMoveRightLeft;
    }
}
