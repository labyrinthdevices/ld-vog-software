﻿//-----------------------------------------------------------------------
// <copyright file="NamespaceDoc.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    /// <summary>
    /// The <see cref="OculomotorLab.VOG.ImageGrabbing"/> namespace contains classes for image grabbing.
    /// Image can be grabbed from cameras or video files. 
    /// <see cref="EyeTrackerImageGrabber"/> is the generic class for image grabbing. All grabbers inherit from it. Any image grabber
    /// will also have a number of cameras (or video files) that implement the interface <see cref="IImageEyeSource"/>.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    public class NamespaceDoc
    {
    }
}