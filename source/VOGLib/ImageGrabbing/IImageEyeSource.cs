﻿//-----------------------------------------------------------------------
// <copyright file="IImageEyeSource.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Drawing;
    using OculomotorLab.VOG;

    /// <summary>
    /// Generic interface for a camera in an eye tracker or a video file.
    /// </summary>
    public interface IImageEyeSource : IDisposable
    {
        /// <summary>
        /// Gets or sets which eye the images are from: left eye, right eye, or both.
        /// </summary>
        Eye WhichEye { get; }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        ImageEyeQueue queue { get; }

        /// <summary>
        /// Frame counter starts at zero and increments every time a frame is captured.
        /// </summary>
        long FrameCounter { get; }

        /// <summary>
        /// Dropped frame counter. Countes the number of frames grabbed by the camera but not received.
        /// </summary>
        long DroppedFramesCounter { get; }

        /// <summary>
        /// Gets the frame rate reported by the image source. Specific implementations of cameras should override this property.
        /// </summary>
        double ReportedFrameRate { get; }

        /// <summary>
        /// Gets the frame rate of the video file. Specific implementations of cameras should override this property.
        /// </summary>
        Size FrameSize  { get;}

        /// <summary>
        /// Gets a value indicating whether the camera is upside down. Images will be rotated 180 degrees.
        /// </summary>
        bool IsUpsideDown { get; set; }

        /// <summary>
        /// Gets a value indicating whether looks at the eye trhough a mirror. Images will be horizontally flipped.
        /// </summary>
        bool IsMirrored { get; set; }

        /// <summary>
        /// Gets the diagnostic info.
        /// </summary>
        string Diagnostics { get;}
        
        /// <summary>
        /// Grab new image of the eye. Specific implementations of cameras should override this property.
        /// </summary>
        /// <returns>Image of the eye.</returns>
        ImageEye GrabImageEye();

        /// <summary>
        /// Initialize the image eye soource before it starts capturing.
        /// </summary>
        void Init();

        /// <summary>
        /// Starts capturing images. It is expected that the frame numbers start at 0 and they are always
        /// monotonic. There could be dropped frames though. The <see cref="EyeTrackerImageGrabber"/> object will take care of it.
        /// </summary>
        void StartCapture();

        /// <summary>
        /// Stops capturing images.
        /// </summary>
        void StopCapture();
    }
}
