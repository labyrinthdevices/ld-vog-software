﻿//-----------------------------------------------------------------------
// <copyright file="CameraEyeVideoSimulation.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    
    internal class CameraEyeVideoSimulation : ICameraEye
    {
        /// <summary>
        /// Video capture for the simulation.
        /// </summary>
        private VideoEye videoEye;

        public long NumberOfFrames
        {
            get 
            { 
                if ( this.videoEye != null)
                {
                    return this.videoEye.NumberOfFrames;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Number of frames to play before looping. Necessary to play videos 
        /// where the left and the right eye have different durations. UGLY.
        /// </summary>
        public long LoopAtFrame { get; set; }

        internal CameraEyeVideoSimulation(Eye whichEye, string fileName)
        {
            this.videoEye = new VideoEye(whichEye, fileName);

            this.queue = new ImageEyeQueue();
        }


        #region ICameraEye Members

        public void Init()
        {
            this.videoEye.Init();
        }

        public void StartCapture()
        {
            this.videoEye.StartCapture();
        }

        public ImageEye GrabImageEye()
        {
            // If reached the end of the video loop
            if (this.videoEye.LastFrameNumber >= this.LoopAtFrame)
            {
                this.videoEye.Scroll(0);
            }

            ImageEye image = this.videoEye.GrabImageFromVideo();

            System.Threading.Thread.Sleep((int)(1000.0 / (this.ReportedFrameRate )));

            return image;
        }

        public void StopCapture()
        {
            this.videoEye.StopCapture();
        }

        public void Center(PointF center)
        {
        }

        public void Move(MovementDirection direction)
        {
        }

        public Eye WhichEye
        {
            get
            {
                return this.videoEye.WhichEye;
            }
        }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        public ImageEyeQueue queue { get; private set; }

        public Size FrameSize
        {
            get
            {
                return this.videoEye.FrameSize;
            }
        }

        public double ReportedFrameRate
        {
            get { return this.videoEye.ReportedFrameRate; }
        }

        public double RequestedFrameRate { get; private set; }

        public long FrameCounter
        {
            get
            {
                return this.videoEye.FrameCounter;
            }
        }

        public long DroppedFramesCounter
        {
            get
            {
                return this.videoEye.DroppedFramesCounter;
            }
        }

        public bool IsMirrored
        {
            get
            {
                if (this.videoEye != null)
                {
                    return this.videoEye.IsMirrored;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if ( this.videoEye != null)
                {
                    this.videoEye.IsMirrored = value;
                }
            }
        }

        public bool IsUpsideDown
        {
            get
            {
                if (this.videoEye != null)
                {
                    return this.videoEye.IsUpsideDown;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (this.videoEye != null)
                {
                    this.videoEye.IsUpsideDown = value;
                }
            }
        }

        public string Diagnostics
        {
            get { return this.videoEye.Diagnostics; }
        }

        public void Dispose()
        {
        }

        #endregion
    }
}
