﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OculomotorLab.VOG.ImageGrabbing
{
    public partial class TwoCamerasControl : UserControl
    {
        private bool shouldCenterCamera;
        private EyeCollection<PointF> centers;

        private bool shouldMoveCamera;
        private Eye whichEyeToMove;
        private MovementDirection direction;

        private EyeTracker eyeTracker;

        public EyeTracker EyeTracker
        {
            get
            {
                return this.eyeTracker;
            }
            set
            {
                if (value != null)
                {
                    this.eyeTracker = value;
                    this.eyeTracker.ImageGrabber.ImagesAboutToBeGrabbed += imageGrabber_ImagesToBeGrabbed;
                }
            }
        }

        void imageGrabber_ImagesToBeGrabbed(object sender, EventArgs e)
        {
            // Check if cameras need to be moved. This needs to be done this way
            // to avoid thread problems.
            if (this.shouldCenterCamera)
            {
                this.CenterEyes();
                this.shouldCenterCamera = false;
            }

            if (shouldMoveCamera)
            {
                this.MoveSourceROI();
                this.shouldMoveCamera = false;
            }
        }
        
        public TwoCamerasControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Moves the camera(s) (or ROI) to center the eyes.
        /// </summary>
        /// <param name="centerLeftEye">Center of the left eye.</param>
        /// <param name="centerRightEye">Center of the right eye.</param>
        public void CenterEyes(PointF centerLeftEye, PointF centerRightEye)
        {
            this.shouldCenterCamera = true;
            this.centers = new EyeCollection<PointF>(centerLeftEye, centerRightEye);
        }

        private void CenterEyes()
        {
            foreach (var camera in this.EyeTracker.ImageGrabber.ImageEyeSources)
            {
                var movableCamera = camera as IMovableImageEyeSource;

                if (movableCamera != null)
                {
                    switch (camera.WhichEye)
                    {
                        case Eye.Left:
                            movableCamera.Center(centers[Eye.Left]);
                            break;
                        case Eye.Right:
                            movableCamera.Center(centers[Eye.Right]);
                            break;
                        case Eye.Both:
                            var middle = new PointF(
                                (centers[Eye.Left].X + centers[Eye.Right].X) / 2,
                                (centers[Eye.Left].Y + centers[Eye.Right].Y) / 2);

                            movableCamera.Center(middle);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Moves the camera (or ROI).
        /// </summary>
        /// <param name="whichEyeToMove">Left,right or both.</param>
        /// <param name="direction">Which direction to move.</param>
        public void MoveSourceROI(Eye whichEyeToMove, MovementDirection direction)
        {
            this.shouldMoveCamera = true;
            this.whichEyeToMove = whichEyeToMove;
            this.direction = direction;
        }

        private void MoveSourceROI()
        {
            foreach (var camera in this.EyeTracker.ImageGrabber.ImageEyeSources)
            {
                var movableCamera = camera as IMovableImageEyeSource;

                if (movableCamera != null)
                {
                    if ((camera.WhichEye == whichEyeToMove) || (whichEyeToMove == Eye.Both))
                    {
                        movableCamera.Move(direction);
                    }
                }
            }
        }


        private void buttonMoveRightEyeUp_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Right, MovementDirection.Up);
        }

        private void buttonMoveRightEyeDown_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Right, MovementDirection.Down);
        }

        private void buttonMoveLeftEyeDown_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Left, MovementDirection.Up);
        }

        private void buttonMoveLeftEyeUp_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Left, MovementDirection.Down);
        }

        private void buttonMoveLeftLeft_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Left, MovementDirection.Left);
        }

        private void buttonMoveLeftRight_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Left, MovementDirection.Right);
        }

        private void buttonRightRight_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Right, MovementDirection.Right);
        }

        private void buttonMoveRightLeft_Click(object sender, EventArgs e)
        {
            this.MoveSourceROI(Eye.Right, MovementDirection.Left);
        }

        private void buttonCenterEyes_Click(object sender, EventArgs e)
        {
            var centerLeft = this.EyeTracker.CurrentData.Data.EyeDataRaw[Eye.Left].Pupil.Center;
            var centerRight = this.EyeTracker.CurrentData.Data.EyeDataRaw[Eye.Right].Pupil.Center;
            this.CenterEyes(centerLeft, centerRight);
        }
    }
}
