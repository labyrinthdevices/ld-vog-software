﻿//-----------------------------------------------------------------------
// <copyright file="IMovableImageEyeSource.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IMovableImageEyeSource
    {
        void Center(System.Drawing.PointF center);
        void Move(OculomotorLab.VOG.ImageGrabbing.MovementDirection direction);
    }

    /// <summary>
    /// Possible directions of motion.
    /// </summary>
    public enum MovementDirection
    {
        /// <summary>
        /// Move the camera Up.
        /// </summary>
        Up,

        /// <summary>
        /// Move the camera Down.
        /// </summary>
        Down,

        /// <summary>
        /// Move the camera Left.
        /// </summary>
        Left,

        /// <summary>
        /// Move the camera Right.
        /// </summary>
        Right
    }
}
