﻿//-----------------------------------------------------------------------
// <copyright file="CameraEyeWebCam.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class to control a camera in the context of eye tracking.
    /// </summary>
    /// <remarks>This class could potentially be used to control any camera that can 
    /// be accessed using openCV.</remarks>
    public class CameraEyeWebCam : ICameraEye
    {
        /// <summary>
        /// Capture (camera) object.
        /// </summary>
        private Capture capture;

        /// <summary>
        /// Timer to add timestamps to the images.
        /// </summary>
        private Stopwatch stopwatch = new Stopwatch();

        /// <summary>
        /// Initializes a new instance of the CameraEyeWebCam class.
        /// </summary>
        /// <param name="whichEye">Left or right eye (or both).</param>
        /// <param name="requestedFrameRate">Requested frame rate.</param>
        public CameraEyeWebCam(Eye whichEye, float requestedFrameRate)
        {
            this.WhichEye = whichEye;
            this.RequestedFrameRate = requestedFrameRate;

            this.queue = new ImageEyeQueue();
        }

        /// <summary>
        /// Gets left or right eye (or both).
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        public ImageEyeQueue queue { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the camera is upside down. Images will be rotated 180 degrees.
        /// </summary>
        public bool IsUpsideDown { get; set; }

        /// <summary>
        /// Gets a value indicating whether looks at the eye trhough a mirror. Images will be horizontally flipped.
        /// </summary>
        public bool IsMirrored { get; set; }

        /// <summary>
        /// Gets the frame rate of the camera.
        /// </summary>
        public virtual double ReportedFrameRate
        {
            get
            {
                return this.capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FPS);
            }
        }

        /// <summary>
        /// Gets the frame rate requested to the camera.
        /// </summary>
        public virtual double RequestedFrameRate { get; private set; }

        /// <summary>
        /// Frame size of the camera.
        /// </summary>
        public virtual Size FrameSize
        {
            get
            {
                return new Size(
                    (int)this.capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH),
                    (int)this.capture.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT));
            }
        }

        /// <summary>
        /// Frame counter starts at zero and increments every time a frame is captured.
        /// </summary>
        public long FrameCounter { get; protected set; }

        /// <summary>
        /// Dropped frame counter. Countes the number of frames grabbed by the camera but not received.
        /// </summary>
        public long DroppedFramesCounter { get; protected set; }
        
        /// <summary>
        /// Gets the diagnostics info.
        /// </summary>
        public virtual string Diagnostics
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Initializes the camera.
        /// </summary>
        public virtual void Init()
        {
            this.capture = new Capture();
        }

        /// <summary>
        /// Overrides the StartCapture method.
        /// </summary>
        public virtual void StartCapture()
        {
            if ( this.capture == null )
            {
                throw new InvalidOperationException("Camera must be initialized first;");
            }

            this.capture.Start();
            this.stopwatch.Start();
        }

        /// <summary>
        /// Overrides the StopCapture method.
        /// </summary>
        public virtual void StopCapture()
        {
            if ( this.capture != null )
            {
                this.capture.Stop();
                this.capture.Dispose();
                this.capture = null;
            }
        }

        /// <summary>
        /// Retrieves an image from the camera buffer.
        /// </summary>
        /// <returns>Image grabbed.</returns>
        public virtual ImageEye GrabImageEye()
        {
            // Set up the timestamp for the image
            ImageEyeTimestamp timestamp = new ImageEyeTimestamp();
            timestamp.FrameNumber = this.FrameCounter++;
            timestamp.Seconds = this.stopwatch.ElapsedMilliseconds / 1000;

            // Retrieve the new frame
            Image<Gray, byte> tempImage = this.capture.RetrieveGrayFrame();
            
            var imageEye = new ImageEye(tempImage, this.WhichEye, timestamp);

            imageEye = EyeTrackerImageGrabber.CorrectImageOrientation(imageEye, this.IsMirrored, this.IsUpsideDown);

            return imageEye;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.StopCapture();
            }
        }
    }
}
