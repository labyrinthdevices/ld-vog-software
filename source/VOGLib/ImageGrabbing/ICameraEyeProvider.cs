﻿//-----------------------------------------------------------------------
// <copyright file="ICameraEyeProvider.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;

    public interface ICameraEyeProvider : IEyeTrackingSystem
    {
        /// <summary>
        /// Gets the cameras.
        /// </summary>
        /// <returns>List of image eye source objects.</returns>
        EyeCollection<ICameraEye> GetCameras();

        /// <summary>
        /// Prepares images for processing. Split, rotate, etc. 
        /// </summary>
        /// <remarks>An specific implementation of ImageEyeGrabber can optionally override this 
        /// method to prepare the images. For instance, if a system has only one camera capturing both eyes.
        /// This method would be where the image gets split into two.</remarks>
        /// <param name="images">Images captured from the cameras.</param>
        /// <returns>Images prepared for processing.</returns>
        EyeCollection<ImageEye> PreProcessImagesFromCameras(EyeCollection<ImageEye> images);
    }
}
