﻿//-----------------------------------------------------------------------
// <copyright file="VideoEye.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageGrabbing
{
    using System;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Captures eye images from video files.
    /// </summary>
    /// <remarks>
    /// The main features of the class are to recreate the timestamps from the videos and also rotate or mirror if necessary. 
    /// New classes can inherit from it to implement eye tracking system specific image preparation.
    /// </remarks>
    public class VideoEye : IImageEyeSource
    {
        /// <summary>
        /// Current image.
        /// </summary>
        private ImageEye currentImage;

        /// <summary>
        /// Lock to control the access to the current image.
        /// </summary>
        private object imageLock = new object();

        /// <summary>
        /// Capture video object.
        /// </summary>
        protected Capture video;

        /// <summary>
        /// Initializes a new instance of the VideoEyeGeneric class.
        /// </summary>
        /// <param name="whichEye">Left or right eye.</param>
        /// <param name="fileName">Full path and name of the file.</param>
        public VideoEye(Eye whichEye, string fileName)
        {
            this.WhichEye = whichEye;
            this.IsUpsideDown = false;
            this.IsMirrored = false;
            this.FileName = fileName;

            this.video = new Capture(this.FileName);
            this.ReportedFrameRate = this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FPS);

            this.queue = new ImageEyeQueue();
        }

        /// <summary>
        /// Gets left or right eye (or both).
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets a queue that can be used to save frames while waiting for other cameras.
        /// </summary>
        public ImageEyeQueue queue { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the camera is upside down. Images will be rotated 180 degrees.
        /// </summary>
        public bool IsUpsideDown { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether looks at the eye trhough a mirror. Images will be horizontally flipped.
        /// </summary>
        public bool IsMirrored { get; set; }

        /// <summary>
        /// Gets the filename of the video.
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Gets the frame rate of the video file.
        /// </summary>
        public double ReportedFrameRate{ get; private set; }

        /// <summary>
        /// Gets the frame rate of the video file.
        /// </summary>
        public Size FrameSize
        {
            get
            {
                return new Size(
                    (int)this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_WIDTH),
                    (int)this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_HEIGHT));
            }
        }

        /// <summary>
        /// Frame counter starts at zero and increments every time a frame is captured.
        /// </summary>
        public long FrameCounter { get; protected set; }

        /// <summary>
        /// Last frame number captured.
        /// </summary>
        public long LastFrameNumber { get; private set; }

        /// <summary>
        /// Dropped frame counter. Countes the number of frames grabbed by the camera but not received.
        /// </summary>
        public long DroppedFramesCounter { get; protected set; }

        /// <summary>
        /// Gets the diagnostic info.
        /// </summary>
        public string Diagnostics
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the frame rate of the video file.
        /// </summary>
        public long NumberOfFrames
        {
            get
            {
                return (long)this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_FRAME_COUNT);
            }
        }

        /// <summary>
        /// Retrieves an image. This will be called by image grabber. Part of IImageEyeSource interface.
        /// </summary>
        /// <returns>Image of the eye grabbed.</returns>
        public ImageEye GrabImageEye()
        {
            return null;
        }

        /// <summary>
        /// Retreives an image from the video.
        /// </summary>
        /// <returns>The image.</returns>
        public ImageEye GrabImageFromVideo()
        {
            var image = this.GrabImageEyeBasic();

            this.queue.Enqueue(image);
            this.LastFrameNumber = image.TimeStamp.FrameNumber;
            return image;
        }

        /// <summary>
        /// Retrieves an image from the video file and adds the timestamp.
        /// </summary>
        /// <returns>Image of the eye grabbed.</returns>
        protected virtual ImageEye GrabImageEyeBasic()
        {
            // Set up the timestamp for the image
            ImageEyeTimestamp timestamp = new ImageEyeTimestamp();
            timestamp.FrameNumberRaw = (long)this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES);
            timestamp.FrameNumber = timestamp.FrameNumberRaw;
            timestamp.Seconds = this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_MSEC) / 1000;
            this.FrameCounter++;

            // Retrieve the new frame
            Image<Gray, byte> tempImage = this.video.QueryGrayFrame();

            var imageEye = new ImageEye(tempImage.Bitmap, this.WhichEye, timestamp);

            imageEye = EyeTrackerImageGrabber.CorrectImageOrientation(imageEye, this.IsMirrored, this.IsUpsideDown);

            return imageEye;
        }

        /// <summary>
        /// Initialize the image eye soource before it starts capturing.
        /// </summary>
        public void Init()
        {
        }

        /// <summary>
        /// Starts capturing images it is expected that the frame numbers start at 0 and they are always
        /// monotonic. There could be dropped frames though. The <see cref="EyeTrackerImageGrabber"/> object will take care of it.
        /// </summary>
        public void StartCapture()
        {
            if (this.video != null)
            {
                this.video.Start();
            }
        }

        /// <summary>
        /// Stops capturing images.
        /// </summary>
        public void StopCapture()
        {
            if (this.video != null)
            {
                this.video.Stop();
            }
        }

        public void Scroll(long frameNumber)
        {
            if (frameNumber != (long)this.video.GetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES))
            {
                this.video.SetCaptureProperty(Emgu.CV.CvEnum.CAP_PROP.CV_CAP_PROP_POS_FRAMES, (double)frameNumber);
            }
            this.queue.ShouldReset = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.StopCapture();
                this.video.Dispose();
            }
        }
    }
}
