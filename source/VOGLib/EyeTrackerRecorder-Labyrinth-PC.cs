﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerRecorder.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using System.IO;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class that controls data and video recording.
    /// The object has to take care of syncronizing all the recordings. There are 5 main threads interacting.
    /// 1) Thread recording raw frames to disk
    /// 2) Thread recording processed frames to disk
    /// 3) Grabbing thread sending the raw frames through the eye tracker event
    /// 4) Processing thread sending the frames through the eye tracker event
    /// 5) Main eye tracker thread sending the start and stop recording commands
    /// The raw video recording and the data recording should have the same number of frames. At least, start 
    /// and stop with the same frame. There can be frames dropped in the data that were not dropped in the video.
    /// </summary>
    public class EyeTrackerRecorder : IDisposable
    {
        /// <summary>
        /// Possible states of the recording.
        /// </summary>
        private enum State
        {
            /// <summary>
            /// Not doing anything.
            /// </summary>
            Idle,

            /// <summary>
            /// Recording.
            /// </summary>
            Recording,
        }

        /// <summary>
        /// Current state of the recordings.
        /// </summary>
        private State state;

        /// <summary>
        /// Background thread and buffer for raw frames.
        /// </summary>
        private Consumer<EyeCollection<ImageEye>> rawFramesConsumer;

        /// <summary>
        /// Background thread and buffer for processed frames.
        /// </summary>
        private Consumer<EyeTrackerDataAndImages> processedFramesConsumer;

        /// <summary>
        /// Background thread and buffer for events.
        /// </summary>
        private Consumer<EyeTrackerEvent> eventConsumer;

        /// <summary>
        /// Video writers for the raw video files.
        /// </summary>
        private EyeCollection<VideoWriter> rawVideoWriters;

        /// <summary>
        /// Data file.
        /// </summary>
        private StreamWriter dataFile = null;

        /// <summary>
        /// Event text file.
        /// </summary>
        private StreamWriter eventFile = null;

        /// <summary>
        /// Video writer for the processed video file.
        /// </summary>
        private VideoWriter processedVideoWriter = null;

        /// <summary>
        /// Sound recorder.
        /// </summary>
        private SoundRecorder soundRecorder;

        /// <summary>
        /// Range of frames to record.
        /// </summary>
        private Range RangeToRecord;

        /// <summary>
        /// Options on how to save the processed video.
        /// </summary>
        private OptionsSaveProcessedVideo OptionsProcessedVideo;

        /// <summary>
        /// Event raised when the recording has finished emptying its buffers.
        /// </summary>
        public event EventHandler RecordingCompleted;

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        public event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        /// <summary>
        /// Initializes a new instance of the EyeTrackerRecorder class.
        /// </summary>
        public EyeTrackerRecorder()
        {
            this.state = State.Idle;
        }

        /// <summary>
        /// Gets a value indiating whether there is an ongoing recording.
        /// </summary>
        public bool Recording
        {
            get
            {
                return this.state != State.Idle;
            }
        }

        /// <summary>
        /// Gets a string with information about video recording.
        /// </summary>
        public string StatusMessage
        {
            get
            {
                if (this.rawFramesConsumer != null && this.processedFramesConsumer != null)
                {
                    var diagnostics = new System.Text.StringBuilder();
                    diagnostics.Append("V : [ ");
                    diagnostics.AppendFormat(
                        "{0}/{1}/{2} ",
                        this.rawFramesConsumer.AddedCount,
                        this.rawFramesConsumer.DroppedCount,
                        this.rawFramesConsumer.BufferCount);

                    diagnostics.Append("] T : [");

                    diagnostics.AppendFormat(
                        "{0}/{1}/{2} ",
                        this.processedFramesConsumer.AddedCount,
                        this.processedFramesConsumer.DroppedCount,
                        this.processedFramesConsumer.BufferCount);

                    diagnostics.Append("]");

                    return diagnostics.ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Frame size of the images.
        /// </summary>
        public Size FrameSize { get; private set; }

        /// <summary>
        /// Frame rate of the videos.
        /// </summary>
        public double FrameRate { get; private set; }

        /// <summary>
        /// Starts the recording.
        /// </summary>
        /// <param name="frameRate">Frame rate of the image source.</param>
        /// <param name="frameSize">Frame size of the image source</param>
        public void StartRecording(double frameRate, Size frameSize, Range rangeOfFramestoRecord, bool saveRawVideo, bool saveData, bool saveEvents, bool saveProcessedVideo, OptionsSaveProcessedVideo optionsSaveProcessedVideo, bool recordAudio)
        {
            switch (this.state)
            {
                case State.Idle:

                    this.rawFramesConsumer = new Consumer<EyeCollection<ImageEye>>(this.SaveRawFrames, EyeTracker.Settings.VideoRecordingBufferSize);
                    this.processedFramesConsumer = new Consumer<EyeTrackerDataAndImages>(this.SaveProcessedFrames, EyeTracker.Settings.VideoRecordingBufferSize);
                    this.eventConsumer = new Consumer<EyeTrackerEvent>(this.SaveEvent, EyeTracker.Settings.VideoRecordingBufferSize);

                    this.FrameRate = frameRate;
                    this.FrameSize = frameSize;
                    this.RangeToRecord = rangeOfFramestoRecord;

                    // Prepare file names
                    var time = DateTime.Now.ToString("yyyyMMMdd-HHmmss");
                    var folder = EyeTracker.Settings.DataFolder;
                    var sessionName = EyeTracker.Settings.SessionName;

                    var dataFileName = string.Format(folder + "\\" + sessionName + "-{0}.txt", time);
                    var eventFileName = dataFileName.Replace(".txt", "-events.txt");
                    var audioFileName = dataFileName.Replace(".txt", ".wav");
                    var rawVideoFileNameLeft = dataFileName.Replace(".txt", "-" + Eye.Left + ".avi");
                    var rawVideoFileNameRight = dataFileName.Replace(".txt", "-" + Eye.Right + ".avi");
                    var processedVideoFileNAme = dataFileName.Replace(".txt", ".avi");

                    // Check that the recording folder is valid
                    if (!Directory.Exists(folder))
                    {
                        System.Diagnostics.Trace.WriteLine("Error starting the recording: Directory not found.");
                        return;
                    }

                    // Set up file objects
                    if (saveData)
                    {
                        this.dataFile = new StreamWriter(dataFileName);
                    }

                    if (saveEvents)
                    {
                        this.eventFile = new StreamWriter(eventFileName);
                    }

                    if (saveRawVideo)
                    {
                        this.rawVideoWriters = new EyeCollection<VideoWriter>(
                            new VideoWriter(rawVideoFileNameLeft, 0, (int)this.FrameRate, this.FrameSize.Width, this.FrameSize.Height, false),
                            new VideoWriter(rawVideoFileNameRight, 0, (int)this.FrameRate, this.FrameSize.Width, this.FrameSize.Height, false)
                            );
                    }

                    if (saveProcessedVideo)
                    {
                        this.OptionsProcessedVideo = optionsSaveProcessedVideo;

                        var width = (int)(this.FrameSize.Width * 2.0);
                        if (optionsSaveProcessedVideo.WhichEye != Eye.Both)
                        {
                            width = this.FrameSize.Width;
                        }
                        this.processedVideoWriter = new VideoWriter(processedVideoFileNAme, 0, (int)this.FrameRate, width, this.FrameSize.Height, true);
                    }

                    if (recordAudio)
                    {
                        this.soundRecorder = new SoundRecorder(audioFileName);
                        this.soundRecorder.StartRecording();
                    }

                    // Start the consumers
                    this.rawFramesConsumer.Start();
                    this.processedFramesConsumer.Start();
                    this.eventConsumer.Start();

                    // As soon as the buffers are initialized it is possible to start recording frames.
                    // They will come from different threads.
                    // It is better to do it here because opening files may take up some time.
                    this.state = State.Recording;
                    break;

                default:
                    throw new InvalidOperationException("Cannot start a new recording. Another recording is in process.");
            }
        }

        /// <summary>
        /// Stop the recording inmidiately.
        /// </summary>
        public void StopRecording()
        {
            switch (this.state)
            {
                case State.Recording:

                    if (this.rawFramesConsumer != null)
                    {
                        this.rawFramesConsumer.Stop();
                    }

                    if (rawVideoWriters != null)
                    {
                        if (rawVideoWriters[Eye.Left] != null)
                        {
                            rawVideoWriters[Eye.Left].Dispose();
                        }

                        if (rawVideoWriters[Eye.Right] != null)
                        {
                            rawVideoWriters[Eye.Right].Dispose();
                        }
                    }

                    if (this.eventConsumer != null)
                    {
                        this.eventConsumer.Stop();
                    }

                    if (eventFile != null)
                    {
                        eventFile.Close();
                    }

                    if (this.processedFramesConsumer != null)
                    {
                        this.processedFramesConsumer.WaitFinish(5000);
                    }

                    if (dataFile != null)
                    {
                        this.dataFile.Dispose();
                    }

                    if (processedVideoWriter != null)
                    {
                        this.processedVideoWriter.Dispose();
                    }

                    if (soundRecorder != null)
                    {
                        soundRecorder.StopRecording();
                    }

                    this.state = State.Idle;

                    // Signal that the recording is complete
                    this.OnRecordingCompleted(this, new EventArgs());
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Records new raw frames. If buffer is full it will drop the frame.
        /// </summary>
        /// <param name="images">Frames to be recorded</param>
        /// <returns>True if the frames were recording, false if dropped.</returns>
        internal bool RecordRawImages(EyeCollection<ImageEye> images)
        {
            if (this.Recording)
            {
                var frameNumber = this.GetFrameNumber(images);

                return this.rawFramesConsumer.TryAdd(images, frameNumber);
            }

            return false;
        }

        /// <summary>
        /// Records new processed images.
        /// </summary>
        /// <param name="newDataAndImages">Processed images to be saved.</param>
        /// <returns>True if the frames were recording, false if dropped.</returns>
        internal bool RecordNewDataAndImages(EyeTrackerDataAndImages newDataAndImages)
        {
            if (this.Recording)
            {
                var frameNumber = this.GetFrameNumber(newDataAndImages.Images);

                if (this.rawFramesConsumer.firstItemNumberAdded < 0 ||
                    frameNumber < this.rawFramesConsumer.firstItemNumberAdded)
                {
                    return false;
                }

                var result = this.processedFramesConsumer.TryAdd(newDataAndImages, frameNumber);

                // Check if the processed frame consumer has also finished and stop it.
                if (this.rawFramesConsumer.State == ConsumerState.Finished &&
                    frameNumber >= this.rawFramesConsumer.lastItemConsumed)
                {
                    this.processedFramesConsumer.Stop();
                }

                return result;
            }

            return false;
        }

        /// <summary>
        /// Records new event.
        /// </summary>
        /// <param name="eyeTrackerEvent">Info about the event.</param>
        /// <returns>True if it could be added to the buffer.</returns>
        internal bool RecordNewEvent(EyeTrackerEvent eyeTrackerEvent)
        {
            if (this.Recording)
            {
                return this.eventConsumer.TryAdd(eyeTrackerEvent, eyeTrackerEvent.FrameNumber);
            }

            return false;
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Frees resources.
        /// </summary>
        /// <param name="disposing">Disposing now.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.StopRecording();
            }
        }

        /// <summary>
        /// Raises the event video finished. Useful to know when to finish processing.
        /// </summary>
        /// <param name="sender">Object that generates the event.</param>
        /// <param name="eventArgs">Event arguments.</param>
        protected void OnRecordingCompleted(object sender, EventArgs eventArgs)
        {
            var handler = this.RecordingCompleted;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Loop that records the frames to video files in a different thread.
        /// </summary>
        private void SaveRawFrames(EyeCollection<ImageEye> imagesEye)
        {
            try
            {
                var frameNumber = this.GetFrameNumber(imagesEye);
                if (!this.RangeToRecord.IsEmpty && !this.RangeToRecord.Contains(frameNumber))
                {
                    return;
                }

                if (this.rawVideoWriters != null)
                {
                    foreach (var image in imagesEye)
                    {
                        this.rawVideoWriters[(int)image.WhichEye].WriteFrame(image.Image);
                    }
                }

                // Check if the processed frame consumer has also finished and stop it.
                if (this.processedFramesConsumer.lastItemConsumed >= this.rawFramesConsumer.lastItemConsumed)
                {
                    this.processedFramesConsumer.Stop();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("ERROR recording: " + ex.ToString());
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
        }

        /// <summary>
        /// Loop that records the processed frames to video files in a different thread.
        /// </summary>
        private void SaveProcessedFrames(EyeTrackerDataAndImages data)
        {
            try
            {
                var frameNumber = this.GetFrameNumber(data.Images);

                if (!this.RangeToRecord.IsEmpty && !this.RangeToRecord.Contains(frameNumber) ||
                    this.rawFramesConsumer.firstItemConsumed < 0 ||
                    frameNumber < this.rawFramesConsumer.firstItemConsumed ||
                    frameNumber > this.rawFramesConsumer.lastItemConsumed)
                {
                    return;
                }

                if (this.dataFile != null)
                {
                    // Save the data
                    this.dataFile.WriteLine(data.Data.GetStringLine());
                }

                if (this.processedVideoWriter != null)
                {
                    Image<Bgr, byte> imageForVideo = null;

                    Image<Bgr, byte> imageLeft = null;
                    if (data.Images[Eye.Left] != null)
                    {
                        imageLeft = data.Images[Eye.Left].ImageRaw.Convert<Bgr, byte>();
                        if (this.OptionsProcessedVideo.IncreaseContrast)
                        {
                            imageLeft._EqualizeHist();
                        }
                        if (this.OptionsProcessedVideo.AddCross)
                        {
                            ProcessedImageEye.DrawCross(imageLeft, data.Images[Eye.Left].EyeData);
                        }
                    }
                    Image<Bgr, byte> imageRight = null;
                    if (data.Images[Eye.Right] != null)
                    {
                        imageRight = data.Images[Eye.Right].ImageRaw.Convert<Bgr, byte>();
                        if (this.OptionsProcessedVideo.IncreaseContrast)
                        {
                            imageRight._EqualizeHist();
                        }
                        if (this.OptionsProcessedVideo.AddCross)
                        {
                            ProcessedImageEye.DrawCross(imageRight, data.Images[Eye.Right].EyeData);
                        }
                    }
                    switch (OptionsProcessedVideo.WhichEye)
                    {
                        case Eye.Left:
                            imageForVideo = imageLeft;
                            break;
                        case Eye.Right:
                            imageForVideo = imageRight;
                            break;
                        case Eye.Both:
                            var imgBoth = new Image<Bgr, byte>(new Size(imageLeft.Size.Width * 2, imageLeft.Size.Height));

                            imgBoth.ROI = new Rectangle(imageLeft.Width, 0, imageLeft.Size.Width, imageLeft.Size.Height);
                            imageLeft.CopyTo(imgBoth);

                            imgBoth.ROI = imageRight.ROI;
                            imageRight.CopyTo(imgBoth);
                            imgBoth.ROI = new Rectangle();
                            break;
                    }

                    this.processedVideoWriter.WriteFrame(imageForVideo);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("ERROR recording: " + ex.ToString());
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
        }

        /// <summary>
        /// Loop that records the events to a text file.
        /// </summary>
        private void SaveEvent(EyeTrackerEvent eyeTrackerEvent)
        {
            try
            {
                if (this.eventFile != null)
                {
                    // Save the data
                    this.eventFile.WriteLine(eyeTrackerEvent.GetStringLine());
                }
            }
            catch (Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
        }

        /// <summary>
        /// Gets the common frame number of the images in the collection. It takes care of null images.
        /// </summary>
        /// <param name="images">Collection of images from the same frame.</param>
        /// <returns>Common frame number.</returns>
        private long GetFrameNumber(EyeCollection<ImageEye> images)
        {
            long frameNumber = 0;

            foreach (var image in images)
            {
                if (image != null)
                {
                    frameNumber = image.TimeStamp.FrameNumber;
                }
            }

            return frameNumber;
        }

        /// <summary>
        /// Gets the common frame number of the images in the collection. It takes care of null images.
        /// </summary>
        /// <param name="images">Collection of images from the same frame.</param>
        /// <returns>Common frame number.</returns>
        private long GetFrameNumber(EyeCollection<ProcessedImageEye> images)
        {
            long frameNumber = 0;

            foreach (var image in images)
            {
                if (image != null)
                {
                    frameNumber = image.EyeData.Timestamp.FrameNumber;
                }
            }

            return frameNumber;
        }
    }

    public class OptionsSaveProcessedVideo
    {
        public Eye WhichEye;
        public bool IncreaseContrast;
        public bool AddCross;
    }
}
