﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackingPluginLoader.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.ImageProcessing;

    /// <summary>
    /// http://stackoverflow.com/questions/11488297/how-do-you-use-exportfactoryt
    /// </summary>
    public class EyeTrackingPluginLoader
    {
        /// <summary>
        /// Static variable used to load plugins.
        /// </summary>
        public static EyeTrackingPluginLoader Loader;

        /// <summary>
        /// Init the singleton.
        /// </summary>
        public static void Init()
        {
            if (Loader == null)
            {
                Loader = new EyeTrackingPluginLoader();
            }
        }

        public EyeTrackingPluginLoader()
        {
            //An aggregate catalog that combines multiple catalogs
            var catalog = new AggregateCatalog();
            //Adds all the parts found in all assemblies in 
            //the same directory as the executing program
            catalog.Catalogs.Add(new DirectoryCatalog(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)));

            this.eyeTrackingsystemFactory = new Loader<IEyeTrackingSystem, IEyeTrackingSystemMetadata>(catalog);
            this.eyeCalibrationImplementationFactory = new Loader<IEyeCalibrationImplementation, IEyeTrackerPluginMetadata>(catalog);

            this.pupilTrackerFactory = new Loader<IPupilTracker, IEyeTrackerPluginMetadata>(catalog);
            this.positionTrackerFactory = new Loader<IPositionTracker, IEyeTrackerPluginMetadata>(catalog);
            this.eyelidTrackerFactory = new Loader<IEyelidTracker, IEyeTrackerPluginMetadata>(catalog);
            this.torsionTrackerFactory = new Loader<ITorsionTracker, IEyeTrackerPluginMetadata>(catalog);
        }

        /// This are the possible objects that can get plugins

        public Loader<IEyeTrackingSystem, IEyeTrackingSystemMetadata> eyeTrackingsystemFactory { get; private set; }
        internal Loader<IEyeCalibrationImplementation, IEyeTrackerPluginMetadata> eyeCalibrationImplementationFactory { get; private set; }

        internal Loader<IPupilTracker, IEyeTrackerPluginMetadata> pupilTrackerFactory { get; private set; }
        internal Loader<IPositionTracker, IEyeTrackerPluginMetadata> positionTrackerFactory { get; private set; }
        internal Loader<IEyelidTracker, IEyeTrackerPluginMetadata> eyelidTrackerFactory { get; private set; }
        internal Loader<ITorsionTracker, IEyeTrackerPluginMetadata> torsionTrackerFactory { get; private set; }

        public TypeConverter.StandardValuesCollection GetStandardValues<iT>()
            where iT : class
        {
            // TODO: This is not very clean

            if (typeof(iT) == typeof(IEyeTrackingSystem))
            {
                return new TypeConverter.StandardValuesCollection(EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.classesAvaiable.Select(x => x.Name).ToArray());
            }

            if (typeof(iT) == typeof(IEyeCalibrationImplementation))
            {
                return new TypeConverter.StandardValuesCollection(EyeTrackingPluginLoader.Loader.eyeCalibrationImplementationFactory.classesAvaiable.Select(x => x.Name).ToArray());
            }

            if (typeof(iT) == typeof(IPupilTracker))
            {
                return new TypeConverter.StandardValuesCollection(EyeTrackingPluginLoader.Loader.pupilTrackerFactory.classesAvaiable.Select(x => x.Name).ToArray());
            }

            if (typeof(iT) == typeof(IPositionTracker))
            {
                return new TypeConverter.StandardValuesCollection(EyeTrackingPluginLoader.Loader.positionTrackerFactory.classesAvaiable.Select(x => x.Name).ToArray());
            }

            if (typeof(iT) == typeof(IEyelidTracker))
            {
                return new TypeConverter.StandardValuesCollection(EyeTrackingPluginLoader.Loader.eyelidTrackerFactory.classesAvaiable.Select(x => x.Name).ToArray());
            }

            if (typeof(iT) == typeof(ITorsionTracker))
            {
                return new TypeConverter.StandardValuesCollection(EyeTrackingPluginLoader.Loader.torsionTrackerFactory.classesAvaiable.Select(x => x.Name).ToArray());
            }

            return null;
        }
    }

    public class Loader<I, Imeta>
        where I : class
        where Imeta : IEyeTrackerPluginMetadata
    {
        object locking = new object();

        public Loader(AggregateCatalog catalog)
        {
            lock (locking)
            {
                //Create the CompositionContainer with the parts in the catalog
                CompositionContainer container = new CompositionContainer(catalog);

                try
                {
                    //Fill the imports of this object
                    container.ComposeParts(this);
                }
                catch (ReflectionTypeLoadException ex)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (Exception exSub in ex.LoaderExceptions)
                    {
                        sb.AppendLine(exSub.Message);
                        FileNotFoundException exFileNotFound = exSub as FileNotFoundException;
                        if (exFileNotFound != null)
                        {
                            if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                            {
                                sb.AppendLine("Fusion Log:");
                                sb.AppendLine(exFileNotFound.FusionLog);
                            }
                        }
                        sb.AppendLine();
                    }
                    string errorMessage = sb.ToString();

                    throw new Exception("Error loading plugins: " + errorMessage);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine("Error loading plugins." + ex.Message);
                }
            }
        }

        [ImportMany]
        private IEnumerable<ExportFactory<I, Imeta>> classFactories { get; set; }

        /// <summary>
        /// Gets a list of camera systems available.
        /// </summary>
        public Imeta[] classesAvaiable
        {
            get
            {
                var list = classFactories.Select(x => x.Metadata).ToArray();

                return list;
            }
        }

        /// <summary>
        /// Factory of IVideoEyeProvider objects.
        /// </summary>
        /// <param name="name">Name of the plugin to load. From it's metadata.</param>
        /// <returns>The new image grabber.</returns>
        public I Create(string name)
        {
            lock (locking)
            {
                var matchingFactory = classFactories.FirstOrDefault(
                    x => x.Metadata.Name == name);

                if (matchingFactory == null)
                {
                    // If the selected factory is not available change to the first one

                    System.Diagnostics.Trace.WriteLine(
                        string.Format("'{0}' is not a known compType", name), "compType");

                    matchingFactory = classFactories.First();
                }

                I foo = matchingFactory.CreateExport().Value;
                return foo;
            }
        }
    }

    public interface IEyeTrackerPluginMetadata
    {
        string Name { get; }
    }

    [MetadataAttribute, AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ExportDescriptionAttribute : ExportAttribute, IEyeTrackerPluginMetadata
    {
        public string Name { get; private set; }

        public ExportDescriptionAttribute(string name)
            : base(typeof(IEyeTrackerPluginMetadata))
        {
            this.Name = name;
        }
    }

}
