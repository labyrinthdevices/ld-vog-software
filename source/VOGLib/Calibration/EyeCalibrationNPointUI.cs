﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel.Composition;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace OculomotorLab.VOG.Calibration
{
    [Export(typeof(IEyeCalibrationImplementation)), ExportDescription("NPointUI")]
    public partial class EyeCalibrationNPointUI : UserControl, ICalibrationUI, IEyeCalibrationImplementation
    {
        public EyeCalibrationImplementationState state { get; protected set; }
        // private PointF[] points;

        public int framesPerCalibrationPoint { get; set; }

        private PointF[] pointsLeft;
        private PointF[] pointsRight;
        private int currentPointLeft;
        private int currentPointRight;
        private const int NUMCALIBPOINTS = 9;

        private PointF[] calibrationMeansLeft;
        private PointF[] calibrationMeansRight;

        private PointF[] calibrationVariancesLeft;
        private PointF[] calibrationVariancesRight;

        private ProcessedImageEye[] tempTorsionReferencesLeft;
        private ProcessedImageEye[] tempTorsionReferencesRight;

        int lastPoint = 0;
        long currentIndex = 0;
        private PointF chartproportions;
        long lastIndex = -1;

        bool sizeset = false;
        bool sizeupdated = false;

        object lockobj = new object();

        StreamWriter sw;

        public ICalibrationUI UserInterface
        {
            get
            {
                return this as ICalibrationUI;
            }
        }

        public EyeCalibrationNPointUI()
        {
            InitializeComponent();
            // Create series

            this.chartLeftEye.ChartAreas.Add(new System.Windows.Forms.DataVisualization.Charting.ChartArea());
            this.chartRightEye.ChartAreas.Add(new System.Windows.Forms.DataVisualization.Charting.ChartArea());

            this.chartLeftEye.Series.Add(new System.Windows.Forms.DataVisualization.Charting.Series());
            this.chartLeftEye.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            this.chartLeftEye.Series[0].Color = Color.LightGray;
            this.chartRightEye.Series.Add(new System.Windows.Forms.DataVisualization.Charting.Series());
            this.chartRightEye.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            this.chartRightEye.Series[0].Color = Color.LightGray;

            this.chartLeftEye.Series.Add(new System.Windows.Forms.DataVisualization.Charting.Series());
            this.chartLeftEye.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            this.chartLeftEye.Series[1].Color = Color.Blue;
            this.chartRightEye.Series.Add(new System.Windows.Forms.DataVisualization.Charting.Series());
            this.chartRightEye.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastPoint;
            this.chartRightEye.Series[1].Color = Color.Blue;

            this.chartLeftEye.Visible = true;
            this.chartRightEye.Visible = true;

            this.state = EyeCalibrationImplementationState.Idle;
            this.EyeCalibrationParameters = new EyeCollection<EyeCalibrationParameters>(new EyeCalibrationParameters(Eye.Left), new EyeCalibrationParameters(Eye.Right));

            
        }
        public void UpdateUI()
        {
           switch(this.state)
           {
               case EyeCalibrationImplementationState.Idle:

                   break;
               case EyeCalibrationImplementationState.Waiting:
               case EyeCalibrationImplementationState.Finalizing:
                   if (!sizeupdated)
                   {
                       sizeupdated = true;

                       if(currentIndex == 1)
                       {
                           foreach (var ser in this.chartLeftEye.Series)
                           {
                               ser.Points.Clear();
                           }
                           foreach (var ser in this.chartRightEye.Series)
                           {
                               ser.Points.Clear();
                           }
                       }
                       this.chartLeftEye.SuspendLayout();
                       this.chartRightEye.SuspendLayout();

                       this.chartLeftEye.ChartAreas[0].AxisX.Minimum = 0;
                       this.chartRightEye.ChartAreas[0].AxisX.Minimum = 0;
                       this.chartLeftEye.ChartAreas[0].AxisX.Maximum = chartproportions.X;
                       this.chartRightEye.ChartAreas[0].AxisX.Maximum = chartproportions.X;
                       this.chartLeftEye.ChartAreas[0].AxisY.Minimum = 0;
                       this.chartRightEye.ChartAreas[0].AxisY.Minimum = 0;
                       this.chartLeftEye.ChartAreas[0].AxisY.Maximum = chartproportions.Y;
                       this.chartRightEye.ChartAreas[0].AxisY.Maximum = chartproportions.Y;
                       // sizeupdated = true;

                           for (int i = 0; i < pointsLeft.Length; i++)
                           {
                               this.chartLeftEye.Series[0].Points.AddXY(pointsLeft[i].X, pointsLeft[i].Y);
                               this.chartRightEye.Series[0].Points.AddXY(pointsRight[i].X, pointsRight[i].Y);
                               lock (lockobj)
                               {
                                   sw.WriteLine((currentIndex) + "," + pointsLeft[i].X + "," + pointsLeft[i].Y + "," + pointsRight[i].X + "," + pointsRight[i].Y);
                               }
                               
                           }

                           
                          
                           this.chartLeftEye.Series[1].Points.AddXY(calibrationMeansLeft[currentIndex-1].X, calibrationMeansLeft[currentIndex-1].Y);
                           this.chartRightEye.Series[1].Points.AddXY(calibrationMeansRight[currentIndex-1].X, calibrationMeansRight[currentIndex-1].Y);

                           var tempstr = "";
                           for (int i = 0; i < NUMCALIBPOINTS; i++)
                           {
                               tempstr += calibrationMeansLeft[i].X + " ";
                               lock (lockobj)
                               {
                                   sw.WriteLine((currentIndex) + " Mean," + calibrationMeansLeft[i].X + "," + calibrationMeansLeft[i].Y + "," + calibrationMeansRight[i].X + "," + calibrationMeansRight[i].Y);
                               }
                           }
                           System.Diagnostics.Trace.WriteLine(tempstr);

                           if (currentIndex == NUMCALIBPOINTS)
                           {
                               lock (lockobj)
                               {
                                   sw.Flush();
                                   sw.Close();
                               }
                           }

                               this.chartLeftEye.ResumeLayout();
                       this.chartRightEye.ResumeLayout();
                   }
                   
                   break;
               case EyeCalibrationImplementationState.ResettingReference:
                   if (lastIndex == -1)
                   {

                   }
                   else
                   {

                   }
                   break;
           }
        }
        public Button btnStartCalibration { get { return this.buttonStart; } }
        public Button btnAbortCalibration { get { return this.buttonAbort; } }

        public bool PrepareCalibration()
        {
            try
            {
                this.framesPerCalibrationPoint = (int)Math.Round(EyeTracker.Settings.FrameRate * Double.Parse(this.txtSecondsPerPoint.Text));
            }
            catch (FormatException fex)
            {
                System.Diagnostics.Trace.WriteLine("Calibration error: non-numeric value entered for seconds per calibration point.");
                return false;
            }

            // this.framesPerCalibrationPoint = 1;
            if(this.framesPerCalibrationPoint < 0 || Double.IsInfinity(this.framesPerCalibrationPoint) || Double.IsNaN(this.framesPerCalibrationPoint))
            {
                System.Diagnostics.Trace.WriteLine("Calibration error: numeric value for seconds per calibration point is invalid");
                return false;
            }

            this.EyeCalibrationParameters = new EyeCollection<VOG.EyeCalibrationParameters>(new EyeCalibrationParameters(Eye.Left), new EyeCalibrationParameters(Eye.Right));

            currentIndex = 0;
            this.CleanCalibrationVariables();

            try
            {
                var time = DateTime.Now.ToString("yyyyMMMdd-HHmmss");
                var folder = EyeTracker.Settings.DataFolder;
                var sessionName = EyeTracker.Settings.SessionName;
                var dataFileName = string.Format(folder + "\\Calibration\\" + sessionName + "-{0}.csv", time);
                sw = new StreamWriter(dataFileName,false);
            } catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }


            this.state = EyeCalibrationImplementationState.CalibratingEyeModel;
            return true;
        }
        public void ContinueCalibration()
        {
            currentPointLeft = 0;
            currentPointRight = 0;
            this.state = EyeCalibrationImplementationState.CalibratingEyeModel;
            return;
        }
        public void FinalizeCalibration(bool abort)
        {
            if (abort)
            {
                this.state = EyeCalibrationImplementationState.Stopping;
            }
            
        }

        private void CleanCalibrationVariables()
        {
            currentIndex = 0;
            pointsLeft = new PointF[framesPerCalibrationPoint];
            pointsRight = new PointF[framesPerCalibrationPoint];

            tempTorsionReferencesLeft = new ProcessedImageEye[NUMCALIBPOINTS];
            tempTorsionReferencesRight = new ProcessedImageEye[NUMCALIBPOINTS];

            calibrationMeansLeft = new PointF[NUMCALIBPOINTS];
            calibrationMeansRight = new PointF[NUMCALIBPOINTS];
            calibrationVariancesLeft = new PointF[NUMCALIBPOINTS];
            calibrationVariancesRight = new PointF[NUMCALIBPOINTS];
            
            currentPointLeft = 0;
            currentPointRight = 0;
        }
        private void CalibrationPointCompleted()
        {
            
            // Calculate Center Points
            //calibrationMeansLeft[currentIndex] = CalculateMean(ref pointsLeft);
            
            //calibrationMeansRight[currentIndex] = CalculateMean(ref pointsRight);
            //calibrationVariancesLeft[currentIndex] = CalculateVariance(ref pointsLeft, calibrationMeansLeft[currentIndex]);
            //calibrationVariancesRight[currentIndex] = CalculateVariance(ref pointsRight, calibrationMeansRight[currentIndex]);

            this.clusterCenters(3, ref pointsLeft, out calibrationMeansLeft[currentIndex], out calibrationVariancesLeft[currentIndex]);
            this.clusterCenters(3, ref pointsRight, out calibrationMeansRight[currentIndex], out calibrationVariancesRight[currentIndex]);

            System.Diagnostics.Trace.WriteLine(String.Format("{8}: L ({0:F2},{1:F2}) +/- ({2:F2},{3:F2}), R ({4:F2},{5:F2}) +/- ({6:F2},{7:F2}))",calibrationMeansLeft[currentIndex].X,
                calibrationMeansLeft[currentIndex].Y, calibrationVariancesLeft[currentIndex].X, calibrationVariancesLeft[currentIndex].Y,
                calibrationMeansRight[currentIndex].X, calibrationMeansRight[currentIndex].Y, calibrationVariancesRight[currentIndex].X, calibrationVariancesRight[currentIndex].Y, currentIndex));


            System.Media.SystemSounds.Beep.Play();
            sizeupdated = false;
            currentIndex++;
        }

        private PointF CalculateMean(ref PointF[] dataArray)
        {
            PointF avg = new PointF(0, 0);
            for (int i = 0; i < dataArray.Length; i++ )
            {
                avg.X += dataArray[i].X;
                avg.Y += dataArray[i].Y;
            }
            avg.X /= dataArray.Length;
            avg.Y /= dataArray.Length;

            return avg;
        }
        private PointF CalculateVariance(ref PointF[] dataArray, PointF mean)
        {
            PointF var = new PointF(0, 0);
            for (int i = 0; i < dataArray.Length; i++ )
            {
                var.X += (dataArray[i].X - mean.X) * (dataArray[i].X - mean.X);
                var.Y += (dataArray[i].Y - mean.Y) * (dataArray[i].Y - mean.Y);
            }
            var.X /= dataArray.Length;
            var.Y /= dataArray.Length;

            return var;
        }


        // 6-13-2014 JB
        /// <summary>
        /// Determine the mean and variance of a given calibration point, removing outliers according to specified threshold
        /// </summary>
        /// <param name="pointidx">Index of the calibration point for which to calculate centers</param>
        /// <param name="threshold_std">Number of standard deviations outside which to exclude data</param>
        /// <param name="mean">Pointer to variable to store mean</param>
        /// <param name="variance">Pointer to variable to store variance</param>
        private void clusterCenters(double threshold_std, ref PointF[] data, out PointF mean, out PointF variance)
        {
            // Calculate initial mean and variance
            PointF tempmean = this.CalculateMean(ref data);
            PointF tempvariance = this.CalculateVariance(ref data, tempmean);

            // Initialize array of data points and copy necessary values for this pointidx into the tempbuffer array
            PointF[] tempbuffer = new PointF[this.framesPerCalibrationPoint];
            Array.Copy(data, 0, tempbuffer, 0, this.framesPerCalibrationPoint);

            // This array indicates whether the point at index j is an outlier (0) or not (1)
            int[] indices = new int[this.framesPerCalibrationPoint];
            for (int j = 0; j < this.framesPerCalibrationPoint; ++j)
            {
                indices[j] = 1;
            }

            int prevassigned = this.framesPerCalibrationPoint;
            int assigned = 0;

            // Begin iterating
            while (true)
            {
                // Remove outliers
                for (int i = 0; i < tempbuffer.Length; ++i)
                {
                    if (indices[i] == 0) continue; // skip points that have been removed
                    // Otherwise, calculate the squared distance
                    // If squared distance > (fraction of std_deviation * std_deviation)^2 == (fraction of std_deviation)^2*variance in either X or Y direction, then the point is an outlier, remove it
                    if ((tempbuffer[i].X - tempmean.X) * (tempbuffer[i].X - tempmean.X) > threshold_std * threshold_std * tempvariance.X || (tempbuffer[i].Y - tempmean.Y) * (tempbuffer[i].Y - tempmean.Y) > threshold_std * threshold_std * tempvariance.Y)
                    {
                        indices[i] = 0;
                    }
                    else
                        ++assigned;
                }

                // If we did not remove any outliers, then we are done
                if (assigned == prevassigned)
                    break;

                // Otherwise, recalculate mean and standard deviation
                tempmean.X = 0;
                tempmean.Y = 0;
                for (int i = 0; i < tempbuffer.Length; ++i)
                {
                    if (indices[i] == 0) continue;
                    tempmean.X += tempbuffer[i].X;
                    tempmean.Y += tempbuffer[i].Y;
                }
                tempmean.X /= assigned;
                tempmean.Y /= assigned;

                tempvariance.X = 0;
                tempvariance.Y = 0;
                for (int i = 0; i < tempbuffer.Length; ++i)
                {
                    if (indices[i] == 0) continue;
                    tempvariance.X += (tempbuffer[i].X - tempmean.X) * (tempbuffer[i].X - tempmean.X);
                    tempvariance.Y += (tempbuffer[i].Y - tempmean.Y) * (tempbuffer[i].Y - tempmean.Y);
                }
                tempvariance.X /= assigned;
                tempvariance.Y /= assigned;

                // Keep track of how many points we assigned this time
                prevassigned = assigned;
                assigned = 0;
            }

            Console.WriteLine("Removed " + (this.framesPerCalibrationPoint - prevassigned) + " outliers.");

            mean = tempmean;
            variance = tempvariance;

        }

        /// <summary>
        /// Compare two PointF objects based on the X coordinate
        /// </summary>
        public class XComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                PointF pt1 = (PointF)x;
                PointF pt2 = (PointF)y;
                if (pt1.X > pt2.X)
                    return 1;
                else if (pt1.X < pt2.X)
                    return -1;
                else
                    return 0;
            }
        }
        /// <summary>
        /// Compare two PointF objects based on the Y coordinate
        /// </summary>
        public class YComparer : IComparer
        {
            int IComparer.Compare(object x, object y)
            {
                PointF pt1 = (PointF)x;
                PointF pt2 = (PointF)y;
                if (pt1.Y > pt2.Y)
                    return 1;
                else if (pt1.Y < pt2.Y)
                    return -1;
                else
                    return 0;
            }
        }
        private EyeCalibrationParameters generateEyeModel(PointF[] referencePoints, PointF[] variances, ref ProcessedImageEye[] referenceImages, Eye whichEye)
        {
            EyeCalibrationParameters param = new EyeCalibrationParameters(whichEye);
            // 6-12-2014 JB Changed sort so that it sorts keys (points using XComparer and YCompare classes) and values (indices) so that we can assign reference images as reference data
            int[] indices = new int[] { 0, 1, 2, 3, 4 };
            PointF[] refPoints = new PointF[referencePoints.Length];
            referencePoints.CopyTo(refPoints, 0);

            MLApp.MLApp matlab = new MLApp.MLApp();

            double[] H_Avg = new double[NUMCALIBPOINTS];
            double[] H_Std = new double[NUMCALIBPOINTS];
            double[] V_Avg = new double[NUMCALIBPOINTS];
            double[] V_Std = new double[NUMCALIBPOINTS];

            for (int i = 0; i < NUMCALIBPOINTS; i++)
            {
                H_Avg[i] = refPoints[i].X;
                H_Std[i] = variances[i].X;
                V_Avg[i] = refPoints[i].Y;
                V_Std[i] = variances[i].X;
            }

            var time = DateTime.Now.ToString("yyyyMMMdd-HHmmss");
            var datafolder = EyeTracker.Settings.DataFolder;
            var scriptsfolder = EyeTracker.Settings.DataFolder + "\\Scripts";
            var sessionName = EyeTracker.Settings.SessionName;
            var dataFileName = string.Format(datafolder + "\\Calibration\\" + sessionName + "-{0}", time);

            int EyeVal = 0;
            if (whichEye == Eye.Left)
            {
                EyeVal = 0;
            }
            else if (whichEye == Eye.Right)
            {
                EyeVal = 1;
            }

            double DistToWall = Convert.ToDouble(numDistanceWallToEyes.Value);

            object result = null;
            matlab.Execute("clear all");
            matlab.Execute("close all");
            matlab.Execute("clc all");
            matlab.Execute(@"cd " + scriptsfolder);
            matlab.Feval("VOG_Calibration_9_Points_Physical", 1, out result, H_Avg, H_Std, V_Avg, V_Std, EyeVal, DistToWall, dataFileName);
            
            Thread.Sleep(1000);

            double[] PolyH = new double[6];
            double[] PolyV = new double[6];

            String Calibration_File;

            if (whichEye == Eye.Left) {
                Calibration_File = scriptsfolder + "\\Calibration_Left_Eye.txt";

                string wholefile;
                try
                {
                    using (StreamReader reader = new StreamReader(Calibration_File))
                    {
                        wholefile = reader.ReadToEnd();
                        String[] line = wholefile.Split('\n');
                        String[][] coeff = new string[line.Length][];
                        for (int i = 0; i < line.Length; i++)
                        {
                            line[i] = line[i].TrimEnd('\n', '\r');
                            coeff[i] = line[i].Split('\t');
                        }

                        for (int i = 0; i < PolyH.Length; i++)
                        {
                            PolyH[i] = Convert.ToDouble(coeff[i][0]);
                            PolyV[i] = Convert.ToDouble(coeff[i][1]);
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error reading calibration file!");
                }

            }
            else if (whichEye == Eye.Right) 
            {
                Calibration_File = scriptsfolder + "\\Calibration_Right_Eye.txt";

                string wholefile;
                try
                {
                    using (StreamReader reader = new StreamReader(Calibration_File))
                    {
                        wholefile = reader.ReadToEnd();
                        String[] line = wholefile.Split('\n');
                        String[][] coeff = new string[line.Length][];
                        for (int i = 0; i < line.Length; i++)
                        {
                            line[i] = line[i].TrimEnd('\n', '\r');
                            coeff[i] = line[i].Split('\t');
                        }

                        for (int i = 0; i < PolyH.Length; i++)
                        {
                            PolyH[i] = Convert.ToDouble(coeff[i][0]);
                            PolyV[i] = Convert.ToDouble(coeff[i][1]);
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error reading calibration file!");
                }
            }

            param.HPolynomials = PolyH;
            param.VPolynomials = PolyV;

            //Array.Sort(refPoints, indices, new XComparer());
            //Array.Sort(refPoints, indices, 1, 3, new YComparer());

            /*// Calculate X radius
            Console.WriteLine(String.Format("{0} Raw X: {1:F2}, {2:F2}", whichEye.ToString(), Math.Abs(refPoints[0].X - refPoints[2].X) / (float)Math.Sin(7.6 * Math.PI / 180), Math.Abs(refPoints[4].X - refPoints[2].X) / (float)Math.Sin(7.6 * Math.PI / 180)));
            var xradius = ((Math.Abs(refPoints[0].X - refPoints[2].X) + Math.Abs(refPoints[4].X - refPoints[2].X)) / 2) / (float)Math.Sin(7.6 * Math.PI / 180);
            // Calculate Y radius
            Console.WriteLine(String.Format("{0} Raw Y: {1:F2}, {2:F2}", whichEye.ToString(), Math.Abs(refPoints[1].Y - refPoints[2].Y) / (float)Math.Sin(7.6 * Math.PI / 180), Math.Abs(refPoints[3].Y - refPoints[2].Y) / (float)Math.Sin(7.6 * Math.PI / 180)));
            var yradius = ((Math.Abs(refPoints[1].Y - refPoints[2].Y) + Math.Abs(refPoints[3].Y - refPoints[2].Y)) / 2) / (float)Math.Sin(7.6 * Math.PI / 180);*/

            // Calculate X radius
            //Console.WriteLine(String.Format("{0} Raw X: {1:F2}, {2:F2}", whichEye.ToString(), Math.Abs(refPoints[0].X - refPoints[2].X) / (float)Math.Sin(7.6 * Math.PI / 180), Math.Abs(refPoints[4].X - refPoints[2].X) / (float)Math.Sin(7.6 * Math.PI / 180)));
            var xradius = ((Math.Abs(refPoints[4].X - refPoints[5].X) + Math.Abs(refPoints[4].X - refPoints[6].X)) / 2) / (float)Math.Sin(7.805 * Math.PI / 180);
            // Calculate Y radius
            //Console.WriteLine(String.Format("{0} Raw Y: {1:F2}, {2:F2}", whichEye.ToString(), Math.Abs(refPoints[1].Y - refPoints[2].Y) / (float)Math.Sin(7.6 * Math.PI / 180), Math.Abs(refPoints[3].Y - refPoints[2].Y) / (float)Math.Sin(7.6 * Math.PI / 180)));
            var yradius = ((Math.Abs(refPoints[4].Y - refPoints[1].Y) + Math.Abs(refPoints[4].Y - refPoints[7].Y)) / 2) / (float)Math.Sin(7.805 * Math.PI / 180);
            
            
            // Set reference point
            // 6-14-2014 JB Added assignment of ReferenceData from temporary data based on sort
            PointF center;
            //if (whichEye == Eye.Left)
            //{
            //    center = refPoints[0];
            //    param.SetReference(referenceImages[indices[0]]);
            //}
            //else
            //{
            //    center = refPoints[4];
            //    param.SetReference(referenceImages[indices[4]]);
            //}
            center = refPoints[4];
            param.SetReference(referenceImages[indices[4]]);
            var zeroHorizontal = -Math.Asin((double)Math.Min(1, (refPoints[4].X - center.X) / (xradius * Math.Cos(Math.Asin((refPoints[4].Y - center.Y) / yradius))))) / Math.PI * 180;
            int skewfactor;
            if (whichEye == Eye.Left)
                skewfactor = -1;
            else
                skewfactor = 1;
            var zeroVertical = -Math.Asin((double)Math.Min(1, (refPoints[4].Y - center.Y) / yradius)) / Math.PI * 180;// -0.05 * skewfactor * zeroHorizontal;
            Array.Sort((int[])indices.Clone(), variances);

            param.SetEyeModel(new EyePhysicalModel(center, 0.5f * xradius + 0.5f * yradius, new PointF((float)zeroHorizontal, (float)zeroVertical)));
            Console.WriteLine(String.Format("{0}: XRadius {1:F2} YRadius {2:F2}, Center {3:F2}, {4:F2}", whichEye.ToString(), xradius, yradius, center.X, center.Y));

            return param;
        }

        public void UpdateReference(ProcessedImageEye imageEye)
        {
            this.EyeCalibrationParameters[imageEye.WhichEye].SetReference(imageEye);

        }

        public void UpdateEyeModel(ProcessedImageEye imageEye)
        {
            if (!sizeset)
            {
                chartproportions = new PointF(imageEye.ImageRaw.Size.Width,imageEye.ImageRaw.Size.Height );
                sizeset = true;
            }

            if(this.state != EyeCalibrationImplementationState.Waiting)
            {
                if(imageEye.WhichEye == Eye.Left && currentPointLeft < framesPerCalibrationPoint)
                {
                    pointsLeft[currentPointLeft] = imageEye.EyeData.Pupil.Center;
                    ++currentPointLeft;
                } else if (imageEye.WhichEye == Eye.Right && currentPointRight < framesPerCalibrationPoint) {
                    pointsRight[currentPointRight] = imageEye.EyeData.Pupil.Center;
                    ++currentPointRight;
                }

                if (imageEye.WhichEye == Eye.Left && currentPointLeft == framesPerCalibrationPoint)
                {
                    this.tempTorsionReferencesLeft[currentIndex] = imageEye;
                }
                if (imageEye.WhichEye == Eye.Right && currentPointLeft == framesPerCalibrationPoint)
                {
                    this.tempTorsionReferencesRight[currentIndex] = imageEye;
                }

                if(currentPointRight == framesPerCalibrationPoint && currentPointLeft == framesPerCalibrationPoint)
                {
                    this.CalibrationPointCompleted();
                    if (currentIndex == NUMCALIBPOINTS)
                    {
                        this.state = EyeCalibrationImplementationState.Finalizing;
                        var eyeparamleft = this.generateEyeModel(calibrationMeansLeft, calibrationVariancesLeft, ref tempTorsionReferencesLeft, Eye.Left);
                        var eyeparamright = this.generateEyeModel(calibrationMeansRight, calibrationVariancesRight, ref tempTorsionReferencesRight, Eye.Right);
                        this.EyeCalibrationParameters = new EyeCollection<EyeCalibrationParameters>(eyeparamleft, eyeparamright);
                        // this.CleanCalibrationVariables();
                    }
                    else
                    {
                        this.state = EyeCalibrationImplementationState.Waiting;
                        // sizeupdated = false;
                    }
                    currentPointLeft = 0;
                    currentPointRight = 0;
                    
                }
            }
        }

        public EyeCollection<EyeCalibrationParameters> EyeCalibrationParameters
        {
            get;
            private set;
        }

    }
}
