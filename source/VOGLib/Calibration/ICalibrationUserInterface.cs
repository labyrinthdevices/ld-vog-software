﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OculomotorLab.VOG
{
    public interface ICalibrationUI
    {
        Button btnStartCalibration { get; }
        Button btnAbortCalibration { get; }

        void UpdateUI();
    }
}
