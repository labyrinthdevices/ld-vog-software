﻿//-----------------------------------------------------------------------
// <copyright file="EyeCalibrationManualUI.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace OculomotorLab.VOG.Calibration
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using System.Windows.Forms;
    using Emgu.CV;
    using Emgu.CV.Structure;

    [Export(typeof(IEyeCalibrationImplementation)), ExportDescription("Manual")]
    public partial class EyeCalibrationManual : UserControl, IEyeCalibrationImplementation, ICalibrationUI
    {
        public ICalibrationUI UserInterface
        {
            get
            {
                return this as ICalibrationUI;
            }
        }

        public void UpdateUI()
        {
            if (this.lastImageLeftEye != null)
            {
                var imageLeft = this.lastImageLeftEye.ImageRaw.Convert<Bgr,byte>();

                var globeLeft = this.GetEyeGlobe(Eye.Left);
                ProcessedImageEye.DrawEyeGlobe(imageLeft, globeLeft, true);

                this.imageBoxLeftEye.Image = imageLeft;
            }

            if (this.lastImageRightEye != null)
            {
                var imageRight = this.lastImageRightEye.ImageRaw.Convert<Bgr, byte>();

                var globeRight = this.GetEyeGlobe(Eye.Right);
                ProcessedImageEye.DrawEyeGlobe(imageRight, globeRight, true);

                this.imageBoxRightEye.Image = imageRight;
            }

            // Enable or disable
            if (!this.accepted)
            {
                this.Enabled = true;
                this.buttonAccept.Enabled = true;
            }
            else
            {
                this.Enabled = false;
                this.buttonAccept.Enabled = false;
            }
        }

        public Button btnStartCalibration { get { return null;  } }
        public Button btnAbortCalibration { get { return null;  } }

        public EyeCalibrationImplementationState state { get; protected set; }

        private bool accepted;

        private ProcessedImageEye lastImageLeftEye;
        private ProcessedImageEye lastImageRightEye;

        public EyeCollection<EyeCalibrationParameters> EyeCalibrationParameters { get; private set; }

        public EyeCalibrationManual()
        {

            InitializeComponent();
            this.state = EyeCalibrationImplementationState.Idle;

            this.EyeCalibrationParameters = new EyeCollection<EyeCalibrationParameters>(
                new EyeCalibrationParameters(Eye.Left),
                new EyeCalibrationParameters(Eye.Right));

            this.sliderTextControlLeftEyeGlobeH.Text = "Left Eye globe H";
            this.sliderTextControlLeftEyeGlobeH.Range = new Range(0, 1000);
            this.sliderTextControlLeftEyeGlobeH.Value = EyeTracker.Settings.Tracking.LeftEyeGlobe.Center.X;

            this.sliderTextControlLeftEyeGlobeV.Text = "Left Eye globe V";
            this.sliderTextControlLeftEyeGlobeV.Range = new Range(0, 1000);
            this.sliderTextControlLeftEyeGlobeV.Value = EyeTracker.Settings.Tracking.LeftEyeGlobe.Center.Y;

            this.sliderTextControlLeftEyeGlobeR.Text = "Left Eye globe R";
            this.sliderTextControlLeftEyeGlobeR.Range = new Range(0, 500);
            this.sliderTextControlLeftEyeGlobeR.Value = EyeTracker.Settings.Tracking.LeftEyeGlobe.Radius;

            this.sliderTextControlRightEyeGlobeH.Text = "Right Eye globe H";
            this.sliderTextControlRightEyeGlobeH.Range = new Range(0, 1000);
            this.sliderTextControlRightEyeGlobeH.Value = EyeTracker.Settings.Tracking.RightEyeGlobe.Center.X;

            this.sliderTextControlRightEyeGlobeV.Text = "Right Eye globe V";
            this.sliderTextControlRightEyeGlobeV.Range = new Range(0, 1000);
            this.sliderTextControlRightEyeGlobeV.Value = EyeTracker.Settings.Tracking.RightEyeGlobe.Center.Y;

            this.sliderTextControlRightEyeGlobeR.Text = "Right Eye globe R";
            this.sliderTextControlRightEyeGlobeR.Range = new Range(0, 500);
            this.sliderTextControlRightEyeGlobeR.Value = EyeTracker.Settings.Tracking.RightEyeGlobe.Radius;
        }

        public bool PrepareCalibration()
        {
            this.state = EyeCalibrationImplementationState.CalibratingEyeModel;
            return true;
        }
        public void ContinueCalibration()
        {
            this.state = EyeCalibrationImplementationState.CalibratingEyeModel;
            return;
        }
        public void FinalizeCalibration(bool abort)
        {
            this.state = EyeCalibrationImplementationState.Idle;
            return;
        }

        public void UpdateEyeModel(ProcessedImageEye imageEye)
        {
            if (imageEye.WhichEye == Eye.Left)
            {
                this.lastImageLeftEye = imageEye;
            }

            if (imageEye.WhichEye == Eye.Right)
            {
                this.lastImageRightEye = imageEye;
            }

            if (imageEye.EyeData.ProcessFrameResult == ProcessFrameResult.Good)
            {
                if (this.accepted )
                {
                    this.EyeCalibrationParameters[imageEye.WhichEye].SetEyeModel(this.GetEyeGlobe(imageEye.WhichEye));
                    this.state = EyeCalibrationImplementationState.ResettingReference;
                }
            }
        }

        public void UpdateReference(ProcessedImageEye imageEye)
        {
            if (imageEye.EyeData.ProcessFrameResult == ProcessFrameResult.Good)
            {
                this.EyeCalibrationParameters[imageEye.WhichEye].SetReference(imageEye);
            }
        }

        public EyePhysicalModel GetEyeGlobe(Eye whichEye)
        {
            var eyeGlobe = new EyePhysicalModel();

            switch (whichEye)
            {
                case Eye.Left:
                    eyeGlobe = new EyePhysicalModel(
                                    new PointF((float)this.sliderTextControlLeftEyeGlobeH.Value, (float)this.sliderTextControlLeftEyeGlobeV.Value),
                                    (float)this.sliderTextControlLeftEyeGlobeR.Value);
                    return eyeGlobe;
                    break;
                case Eye.Right:
                    eyeGlobe = new EyePhysicalModel(
                                    new PointF((float)this.sliderTextControlRightEyeGlobeH.Value, (float)this.sliderTextControlRightEyeGlobeV.Value),
                                    (float)this.sliderTextControlRightEyeGlobeR.Value);
                    return eyeGlobe;
                    break;
                default:
                    break;
            }

            return eyeGlobe;
        }

        private void buttonAccept_Click(object sender, EventArgs e)
        {
            this.accepted = true;
        }

        private void EyeCalibrationManualUI_Load(object sender, EventArgs e)
        {
        }

        private void buttonAuto_Click(object sender, EventArgs e)
        {
            if (this.lastImageLeftEye != null)
            {
                this.sliderTextControlLeftEyeGlobeH.Value = this.lastImageLeftEye.EyeData.Pupil.Center.X;
                this.sliderTextControlLeftEyeGlobeV.Value = this.lastImageLeftEye.EyeData.Pupil.Center.Y;
                this.sliderTextControlLeftEyeGlobeR.Value = this.lastImageLeftEye.EyeData.Iris.Radius * 2.0;
            }

            if (this.lastImageRightEye != null)
            {
                this.sliderTextControlRightEyeGlobeH.Value = this.lastImageRightEye.EyeData.Pupil.Center.X;
                this.sliderTextControlRightEyeGlobeV.Value = this.lastImageRightEye.EyeData.Pupil.Center.Y;
                this.sliderTextControlRightEyeGlobeR.Value = this.lastImageRightEye.EyeData.Iris.Radius * 2.0;
            }
        }

    }
}
