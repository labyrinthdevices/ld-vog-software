﻿//-----------------------------------------------------------------------
// <copyright file="IEyeCalibrationImplementation.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Windows.Forms;

    public enum EyeCalibrationImplementationState
    {
        /// <summary>
        /// Idle, not doing anything.
        /// </summary>
        Idle,

        /// <summary>
        /// Calibrating the physical properties of the eye. 
        /// </summary>
        CalibratingEyeModel,

        Waiting,

        /// <summary>
        /// Calibrating the reference position. This is independent of the physical properties
        /// of the eye. But it is necessary to first get the physical properties and then the reference.
        /// </summary>
        ResettingReference,

        Finalizing,

        /// <summary>
        /// Stopping the calibration.
        /// </summary>
        Stopping
    }

    /// <summary>
    /// Interface for different calibration implementations. Including the logic and a reference to the interface.
    /// The object will be created when the calibration starts. Then, it receives every frame of data
    /// with the <see cref="ProcessEyeData"/> method. Each implementation can do with this data whatever it wants.
    /// But at some point the <see cref="Compelted"/> property must be seat to true and the EyeCalibrationParameters
    /// will contain all the information necessary.
    /// </summary>
    public interface IEyeCalibrationImplementation
    {
        /// <summary>
        /// Gets the user interface associated with this calibration implementation.
        /// </summary>
        ICalibrationUI UserInterface { get; }

        /// <summary>
        /// Possible states of the calibration.
        /// </summary>

        EyeCalibrationImplementationState state { get; }

        bool PrepareCalibration();
        void ContinueCalibration();
        void FinalizeCalibration(bool abort);

        /// <summary>
        /// Update the reference information with a new image and data. Reference position, reference torsion image. Etc.
        /// </summary>
        /// <param name="imageEye">Current processed image.</param>
        void UpdateReference(ProcessedImageEye imageEye);

        /// <summary>
        /// Updates the eye model with new image and data. Physical properties of the eyeglobe and the position relative
        /// to the camera.
        /// </summary>
        /// <param name="imageEye">Current processed image.</param>
        void UpdateEyeModel(ProcessedImageEye imageEye);

        /// <summary>
        /// Gets the calibration parameters for the two eyes.
        /// </summary>
        EyeCollection<EyeCalibrationParameters> EyeCalibrationParameters {get;}

    }
}
