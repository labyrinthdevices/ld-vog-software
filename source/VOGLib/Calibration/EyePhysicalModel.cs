﻿//-----------------------------------------------------------------------
// <copyright file="EyePhysicalModel.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------                            
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel;
    using System.Collections;
    using System.Drawing;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Xml;
    using System.Xml.Serialization;
    using Emgu.CV.Structure;

    /// <summary>
    /// Data structure containing the geometric properties of the measured iris.
    /// </summary>
    [Serializable, TypeConverter(typeof(StructConverter<EyePhysicalModel>))]
    public struct EyePhysicalModel
    {
        /// <summary>
        /// Gets or sets the center of the circle.
        /// </summary>
        [TypeConverter(typeof(StructConverter<PointF>))]
        public PointF Center { get;  set; }
        public PointF Zero { get; set; }

        float r;
        private double p;
        private PointF pointF;
        /// <summary>
        /// Gets or sets the radius of the circle.
        /// </summary>
        public float Radius {
            get { return r; }
            set { r = value; }
        }

        /// <summary>
        /// Initializes a new instance of the CircleFSerializable structure.
        /// </summary>
        /// <param name="center">Center of the circle.</param>
        /// <param name="radius">Radius of the circle.</param>
        public EyePhysicalModel(PointF center, float radius)
            : this()
        {
            this.Center = center;
            this.Radius = radius;
            this.Zero = new PointF(0, 0);
        }
        public EyePhysicalModel(PointF center, float radius, PointF zero)
            : this()
        {
            this.Center = center;
            this.Radius = radius;
            this.Zero = zero;
        }



        public bool IsEmpty
        {
            get
            {
                return this.Radius == 0;
            }
        }

        public CircleF CircleF
        {
            get
            {
                return new CircleF(this.Center, this.Radius);
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is EyePhysicalModel))
            {
                return false;
            }

            return base.Equals(obj);
        }

        public bool Equals(EyePhysicalModel obj)
        {
            return obj.Center == this.Center && obj.Radius == this.Radius;
        }

        public static bool operator ==(EyePhysicalModel x, EyePhysicalModel y)
        {
            return x.Equals(y);
        }
        public static bool operator !=(EyePhysicalModel x, EyePhysicalModel y)
        {
            return !(x == y);
        }

        public override string ToString()
        {
            return "Center: (" + this.Center.X + ", " + this.Center.Y + ") - Radius : " + this.Radius;
        }
    }

    /// <summary>
    /// http://stackoverflow.com/questions/15746897/modifying-structure-property-in-a-propertygrid
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StructConverter<T> : ExpandableObjectConverter where T : struct
    {
        public override bool GetCreateInstanceSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override object CreateInstance(ITypeDescriptorContext context, System.Collections.IDictionary propertyValues)
        {
            if (propertyValues == null)
                throw new ArgumentNullException("propertyValues");

            T ret = default(T);
            object boxed = ret;
            foreach (DictionaryEntry entry in propertyValues)
            {
                PropertyInfo pi = ret.GetType().GetProperty(entry.Key.ToString());
                if ((pi != null) && (pi.CanWrite))
                {
                    pi.SetValue(boxed, Convert.ChangeType(entry.Value, pi.PropertyType), null);
                }
            }
            return (T)boxed;
        }
    }
}
