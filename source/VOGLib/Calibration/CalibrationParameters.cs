﻿//-----------------------------------------------------------------------
// <copyright file="CalibrationParameters.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class containing all the info regarding the calibration of one eye.
    /// </summary>
    [Serializable]
    public class CalibrationParameters
    {
        /// <summary>
        /// Initializes a new instance of the CalibrationParameters class;
        /// </summary>
        public CalibrationParameters()
        {
            this.EyeCalibrationParameters = new EyeCollection<EyeCalibrationParameters>(
                new EyeCalibrationParameters(Eye.Left),
                new EyeCalibrationParameters(Eye.Right));

            this.HeadCalibrationParameters = new HeadCalibration();
        }

        public EyeCollection<EyeCalibrationParameters> EyeCalibrationParameters { get; set; }

        /// <summary>
        /// Gets calibration info for the head.
        /// </summary>
        public HeadCalibration HeadCalibrationParameters { get; set; }

        /// <summary>
        /// Settings that affect the calibration. They will be saved and load with the calibration.
        /// For instant the pupil thresholds.
        /// </summary>
        public TrackingSettings TrackingSettings { get; set; }

        public static CalibrationParameters Load(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    using (var fileStream = File.OpenRead(fileName))
                    {
                        BinaryFormatter deserializer = new BinaryFormatter();
                        return (CalibrationParameters)deserializer.Deserialize(fileStream);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error loading the calibration file." + fileName + ex.Message, ex);
                }
            }
            else
            {
                throw new FileNotFoundException(fileName);
            }
        }
    }

    /// <summary>
    /// Class containing all the info regarding the calibration of one eye.
    /// </summary>
    [Serializable]
    public class EyeCalibrationParameters
    {
        public bool HasEyeModel
        {
            get
            {
                return !this.EyePhysicalModel.IsEmpty;
            }
        }

        public bool HasReference
        {
            get
            {
                return (!this.ReferenceData.Pupil.IsEmpty) && (this.ImageTorsionReference != null);
            }
        }

        /// <summary>
        /// Initializes a new instance of the EyeTrackerCalibrationInfo class.
        /// </summary>
        /// <param name="whichEye">Left or right eye.</param>
        internal EyeCalibrationParameters(Eye whichEye)
        {
            this.WhichEye = whichEye;
            this.ReferenceData = new EyeData();
            this.HPolynomials = new double[6];
            this.VPolynomials = new double[6];
        }

        ///// <summary>
        ///// Gets the size in pixels of the raw images.
        ///// </summary>
        public Size ImageSize { get; set; }

        /// <summary>
        /// Left or right eye.
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// HPolynomials
        /// </summary>
        public double[] HPolynomials { get; set; }

        /// <summary>
        /// VPolynomials
        /// </summary>
        public double[] VPolynomials { get; set; }

        /// <summary>
        /// Gets the radius and the center of the eye globe.
        /// </summary>
        public EyePhysicalModel EyePhysicalModel { get; set; }

        /// <summary>
        /// Gets the reference data.
        /// </summary>
        public EyeData ReferenceData { get; set; }

        /// <summary> 
        /// Gets the reference image for the torsion.
        /// </summary>
        public Image<Gray,byte> ImageTorsionReference { get; set; }

        public void SetEyeModel(EyePhysicalModel eyePhysicalModel)
        {
            this.EyePhysicalModel = eyePhysicalModel;
        }

        public void SetReference(ProcessedImageEye processedImageEye)
        {
            if (processedImageEye != null)
            {
                this.ReferenceData = processedImageEye.EyeData;
                if (processedImageEye.ImageTorsion != null)
                {
                    this.ImageTorsionReference = processedImageEye.ImageTorsion.Copy();
                }
                
            }
            else
            {
                this.ReferenceData = new EyeData();
                this.ImageTorsionReference = null;
            }
        }
    }
}
