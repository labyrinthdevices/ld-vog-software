﻿namespace OculomotorLab.VOG.Calibration
{
    partial class EyeCalibrationNPointUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonAbort = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxLeftEye = new System.Windows.Forms.GroupBox();
            this.chartLeftEye = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBoxRightEye = new System.Windows.Forms.GroupBox();
            this.chartRightEye = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonStart = new System.Windows.Forms.Button();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.numDistanceWallToEyes = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSecondsPerPoint = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBoxLeftEye.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartLeftEye)).BeginInit();
            this.groupBoxRightEye.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartRightEye)).BeginInit();
            this.panelSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDistanceWallToEyes)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.buttonAbort, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonStart, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panelSettings, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.79668F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.20332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(863, 587);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // buttonAbort
            // 
            this.buttonAbort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonAbort.Location = new System.Drawing.Point(3, 532);
            this.buttonAbort.Name = "buttonAbort";
            this.buttonAbort.Size = new System.Drawing.Size(857, 39);
            this.buttonAbort.TabIndex = 3;
            this.buttonAbort.Text = "button2";
            this.buttonAbort.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBoxLeftEye, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBoxRightEye, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(857, 422);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBoxLeftEye
            // 
            this.groupBoxLeftEye.Controls.Add(this.chartLeftEye);
            this.groupBoxLeftEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxLeftEye.Location = new System.Drawing.Point(431, 3);
            this.groupBoxLeftEye.Name = "groupBoxLeftEye";
            this.groupBoxLeftEye.Size = new System.Drawing.Size(423, 416);
            this.groupBoxLeftEye.TabIndex = 1;
            this.groupBoxLeftEye.TabStop = false;
            this.groupBoxLeftEye.Text = "Left Eye";
            // 
            // chartLeftEye
            // 
            this.chartLeftEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartLeftEye.Location = new System.Drawing.Point(3, 16);
            this.chartLeftEye.Name = "chartLeftEye";
            this.chartLeftEye.Size = new System.Drawing.Size(417, 397);
            this.chartLeftEye.TabIndex = 1;
            this.chartLeftEye.Text = "chartLeftEye";
            // 
            // groupBoxRightEye
            // 
            this.groupBoxRightEye.Controls.Add(this.chartRightEye);
            this.groupBoxRightEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxRightEye.Location = new System.Drawing.Point(3, 3);
            this.groupBoxRightEye.Name = "groupBoxRightEye";
            this.groupBoxRightEye.Size = new System.Drawing.Size(422, 416);
            this.groupBoxRightEye.TabIndex = 0;
            this.groupBoxRightEye.TabStop = false;
            this.groupBoxRightEye.Text = "Right Eye";
            // 
            // chartRightEye
            // 
            this.chartRightEye.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartRightEye.Location = new System.Drawing.Point(3, 16);
            this.chartRightEye.Name = "chartRightEye";
            this.chartRightEye.Size = new System.Drawing.Size(416, 397);
            this.chartRightEye.TabIndex = 2;
            // 
            // buttonStart
            // 
            this.buttonStart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStart.Location = new System.Drawing.Point(3, 485);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(857, 41);
            this.buttonStart.TabIndex = 4;
            this.buttonStart.Text = "button1";
            this.buttonStart.UseVisualStyleBackColor = true;
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.numDistanceWallToEyes);
            this.panelSettings.Controls.Add(this.label2);
            this.panelSettings.Controls.Add(this.txtSecondsPerPoint);
            this.panelSettings.Controls.Add(this.label1);
            this.panelSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSettings.Location = new System.Drawing.Point(3, 431);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(857, 48);
            this.panelSettings.TabIndex = 5;
            // 
            // numDistanceWallToEyes
            // 
            this.numDistanceWallToEyes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numDistanceWallToEyes.Location = new System.Drawing.Point(622, 10);
            this.numDistanceWallToEyes.Name = "numDistanceWallToEyes";
            this.numDistanceWallToEyes.Size = new System.Drawing.Size(143, 26);
            this.numDistanceWallToEyes.TabIndex = 3;
            this.numDistanceWallToEyes.Value = new decimal(new int[] {
            75,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(457, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Target Distance to Eyes (in)";
            // 
            // txtSecondsPerPoint
            // 
            this.txtSecondsPerPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSecondsPerPoint.Location = new System.Drawing.Point(117, 9);
            this.txtSecondsPerPoint.Name = "txtSecondsPerPoint";
            this.txtSecondsPerPoint.Size = new System.Drawing.Size(181, 26);
            this.txtSecondsPerPoint.TabIndex = 1;
            this.txtSecondsPerPoint.Text = "2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Seconds per point:";
            // 
            // EyeCalibrationNPointUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "EyeCalibrationNPointUI";
            this.Size = new System.Drawing.Size(863, 587);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBoxLeftEye.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartLeftEye)).EndInit();
            this.groupBoxRightEye.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartRightEye)).EndInit();
            this.panelSettings.ResumeLayout(false);
            this.panelSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDistanceWallToEyes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonAbort;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBoxLeftEye;
        private System.Windows.Forms.GroupBox groupBoxRightEye;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartLeftEye;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSecondsPerPoint;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartRightEye;
        private System.Windows.Forms.NumericUpDown numDistanceWallToEyes;
        private System.Windows.Forms.Label label2;



    }
}
