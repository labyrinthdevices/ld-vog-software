﻿//-----------------------------------------------------------------------
// <copyright file="EyeCalibrationNPoint.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace OculomotorLab.VOG.Calibration
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using System.Windows.Forms;
    using Emgu.CV;
    using Emgu.CV.Structure;

    [Export(typeof(IEyeCalibrationImplementation)), ExportDescription("NPoint")]
    class EyeCalibrationNPoint : IEyeCalibrationImplementation, ICalibrationUI
    {
        private EyeCollection<Image<Gray, byte>> gazeMaps;

        public EyeCalibrationNPoint()
        {

            this.EyeCalibrationParameters = new EyeCollection<EyeCalibrationParameters>(
                new EyeCalibrationParameters(Eye.Left),
                new EyeCalibrationParameters(Eye.Right));

            this.gazeMaps = new EyeCollection<Image<Gray, byte>>(
                new Image<Gray, byte>(1000, 1000),
                new Image<Gray, byte>(1000, 1000));
        }

        #region ICalibrationUI Members

        public void UpdateUI()
        {
        }
        public Button btnStartCalibration { get { return null;  } }
        public Button btnAbortCalibration { get { return null; } }

        #endregion

        #region IEyeCalibrationImplementation Members

        public ICalibrationUI UserInterface
        {
            get { return null; }
        }

        public EyeCalibrationImplementationState state { get { return EyeCalibrationImplementationState.Idle;  } }

        public bool PrepareCalibration()
        {
            return true;
        }
        public void ContinueCalibration()
        {
            return;
        }
        public void FinalizeCalibration(bool abort)
        {
            return;
        }

        /// <summary>
        /// Update the reference information. Reference position, reference torsion image. Etc.
        /// </summary>
        /// <param name="imageEye">Current processed image.</param>
        public void UpdateReference(ProcessedImageEye imageEye)
        {
            if (imageEye.EyeData.ProcessFrameResult == ProcessFrameResult.Good)
            {
                var whichEye = imageEye.WhichEye;

                var p = new Point(
                    (int)Math.Round(imageEye.EyeData.Pupil.Center.X),
                    (int)Math.Round(imageEye.EyeData.Pupil.Center.Y)
                    );

                this.gazeMaps[whichEye].Data[p.X, p.Y, 0] = (byte)Math.Min(255,this.gazeMaps[whichEye].Data[p.X, p.Y, 0] + 1);
            }
        }

        public void UpdateEyeModel(ProcessedImageEye imageEye)
        {
            if (imageEye.EyeData.ProcessFrameResult == ProcessFrameResult.Good)
            {
                var eyeGlobe = new EyePhysicalModel( imageEye.EyeData.Pupil.Center, (float)(imageEye.EyeData.Iris.Radius*2.0));
                this.EyeCalibrationParameters[imageEye.WhichEye].SetEyeModel(eyeGlobe);
            }
        }

        public EyeCollection<EyeCalibrationParameters> EyeCalibrationParameters
        {
            get;
            private set;
        }

        #endregion
    }
}
