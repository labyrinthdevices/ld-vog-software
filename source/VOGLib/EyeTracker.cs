﻿//-----------------------------------------------------------------------
// <copyright file="EyeTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using OculomotorLab.VOG.HeadTracking;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.Remote;

    /// <summary>
    /// Main class that offers the public interface of the library for eye tracking.
    /// </summary>
    public class EyeTracker : IDisposable
    {
        /// <summary>
        /// Possible states of the eye tracker.
        /// </summary>
        private enum EyeTrackerState
        {
            /// <summary>
            /// Initializing. No commands should be sent during this time.
            /// </summary>
            Initializing,

            /// <summary>
            /// Idle. The eye tracker is ready to start.
            /// </summary>
            Idle,

            /// <summary>
            /// Collecting images and tracking.
            /// </summary>
            Tracking,

            /// <summary>
            /// Post processing a video file.
            /// </summary>
            PostProcessing,

            /// <summary>
            /// Getting ready to close. Disposing elements.
            /// </summary>
            Closing,
        }

        /// <summary>
        /// Current state of the eye tracker.
        /// </summary>
        private EyeTrackerState state;

        /// <summary>
        /// Singleton object to control there is only one eye tracker object at any time.
        /// </summary>
        private static EyeTracker eyeTrackerSingleton;

        /// <summary>
        /// SynchronizationContext of the thread that creates the EyeTracker object.
        /// </summary>
        /// <remarks>
        /// This object is necessary to save the information of the thread that started the eye tracker. So
        /// events can be raised within that thread.
        /// <example>
        /// <code>
        /// var handler = this.EventHappened;
        /// if (handler != null)
        /// {
        ///     // The event is raised in the thread that has initiated the tracker
        ///     this.syncrhoznizationContext.Post(o => handler.DynamicInvoke(sender, e), null);
        /// }
        /// </code></example></remarks>
        private SynchronizationContext syncrhoznizationContext;

        /// <summary>
        /// Eye tracker system corresponding with the current set up.
        /// </summary>
        private IEyeTrackingSystem eyeTrackingSystem;

        /// <summary>
        /// Head tracker associated with the eye tracker.
        /// </summary>
        private EyeTrackerHeadTracker headTracker;

        /// <summary>
        /// Eye tracker process.
        /// </summary>
        private EyeTrackerProcessor processor;

        /// <summary>
        /// Gets the settings object. Useful to get status messages, timing info and debug images.
        /// </summary>
        private EyeTrackerSettings settings;

        /// <summary>
        /// Sets / Gets software trigger
        /// </summary>
        public bool SoftwareTrigger { get; set; }

        /// <summary>
        /// Event raised when there is new data available.
        /// </summary>
        public event EventHandler<NewDataAvailableEventArgs> NewDataAvailable;

        /// <summary>
        /// New event happened.
        /// </summary>
        public event EventHandler<EyeTrackerEvent> NewEyeTrackerEvent;

        /// <summary>
        /// Event that is raised whenever a property is changed.
        /// </summary>
        public event EventHandler<EyeTrackerSettingChangedEventArgs> SettingChanged;

        /// <summary>
        /// Event that is raised whenever a video finishes playing.
        /// </summary>
        public event EventHandler<EventArgs> ProcessingVideoFinished;

        /// <summary>
        /// Gets a string with information about image processing.
        /// </summary>
        public string ProcessingStatusMessage
        {
            get
            {
                return string.Format(
                      "Dropped: {0}({1:0.0}%); Missed: {2} Buffer: {3}",
                      this.processor.NumberFramesDropped,
                      this.processor.NumberFramesDropped / (this.processor.NumberFramesProcessed + 1.0) * 100.0,
                      this.processor.NumberFramesMissed,
                      this.processor.NumberFramesInInputBuffer);
            }
        }

        /// <summary>
        /// Log tracer.
        /// </summary>
        public LogTraceListener LogTraceListener { get; private set; }

        /// <summary>
        /// Gets the diagnostics object. Useful to get status messages, timing info and debug images.
        /// </summary>
        public static EyeTrackerDiagnostics Diagnostics { get; private set; }

        /// <summary>
        /// Gets the settings object. 
        /// </summary>
        public static EyeTrackerSettings Settings
        {
            get
            {
                if (EyeTracker.eyeTrackerSingleton == null)
                {
                    return null;
                }

                return EyeTracker.eyeTrackerSingleton.settings;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the eye tracker is currently idle.
        /// </summary>
        public bool Idle
        {
            get
            {
                return this.state == EyeTrackerState.Idle;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the eye tracker is currently initializing.
        /// </summary>
        public bool Initializing
        {
            get
            {
                return this.state == EyeTrackerState.Initializing;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the eye tracker is currently tracking.
        /// </summary>
        public bool Tracking
        {
            get
            {
                return this.state == EyeTrackerState.Tracking;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the eye tracker is currently post processing a file.
        /// </summary>
        public bool PostProcessing
        {
            get
            {
                return this.state == EyeTrackerState.PostProcessing;
            }
        }

        /// <summary>
        /// Image grabber from cameras or videos.
        /// </summary>
        public EyeTrackerImageGrabber ImageGrabber { get; private set; }

        /// <summary>
        /// Eye tracker recorder.
        /// </summary>
        public EyeTrackerRecorder RecordingManager { get; private set; }

        /// <summary>
        /// Gets the calibration object.
        /// </summary>
        public EyeTrackerCalibrationManager CalibrationManager { get; private set; }

        /// <summary>
        /// Gets the video player (if playing video or processing).  
        /// </summary>
        public VideoPlayer VideoPlayer { get; private set; }

        /// <summary>
        /// Gets the current processed images and data. Alternative to listen to the event (new data available).
        /// </summary>
        public EyeTrackerDataAndImages CurrentData { get; private set; }

        /// <summary>
        /// Static constructor for singleton. The initializations runs on a separate thread to not block
        /// the execution of the thread calling the initialization. The state will change to idle when
        /// the initialization is complete
        /// </summary>
        /// <returns>The eye tracker singleton object.</returns>
        public static EyeTracker Initialize()
        {
            if (EyeTracker.eyeTrackerSingleton == null)
            {
                // Initialize the singleton
                var eyeTracker = new EyeTracker();

                // Set the sigleton
                EyeTracker.eyeTrackerSingleton = eyeTracker;

                // Save the current syncrhonization context to be able to fire events on it
                eyeTracker.syncrhoznizationContext = System.Threading.SynchronizationContext.Current;

                eyeTracker.state = EyeTrackerState.Initializing;

                // Initialize the object that loads the different eye tracking system objects. 
                // It can load objects from new classes present in new dlls in the application folder.
                EyeTrackingPluginLoader.Init();

                // Load settings and set the event handler for event 
                eyeTracker.settings = EyeTrackerSettings.Load();
                eyeTracker.settings.PropertyChanged += (o, e) =>
                {
                    // Raise the event in whichever thread started the eye tracker to avoid cross thread problems.
                    // Settings can be changed in any thread.
                    eyeTracker.RaiseSyncrhonizedEvent(eyeTracker.SettingChanged, e as EyeTrackerSettingChangedEventArgs);
                };

                // Initialize event logging
                eyeTracker.LogTraceListener = new LogTraceListener();
                Trace.Listeners.Add(eyeTracker.LogTraceListener);
                Trace.AutoFlush = true;

                // Initialize Software Trigger
                eyeTracker.SoftwareTrigger = false;

                // Start the server to accept remote requests
                // For some reason I don't understand this cannot be done in a separate thread.
                Trace.WriteLine("Initializing network service ...");
                EyeTrackerService.StartService(eyeTracker);
                Trace.WriteLine("Initializing network service ... completed.");

                // Run the initialization in the background so it does not hung the caller thread
                Task initializationTask = Task.Run(() =>
                {
                    // Initialize diagnostics
                    EyeTracker.Diagnostics = new EyeTrackerDiagnostics();

                    // Initalize the recorder
                    //
                    // If there is an error in the recording don't stop the eye tracker, just the recording.
                    eyeTracker.RecordingManager = new EyeTrackerRecorder();
                    eyeTracker.RecordingManager.ErrorOccurred += (o, e) => eyeTracker.RecordingManager.StopRecording();

                    // Initialize the image grabber
                    //
                    // When tehre are new images pass them to the processor and the recorder
                    // If there is anerror grabbing stop everything
                    eyeTracker.ImageGrabber = new EyeTrackerImageGrabber();
                    eyeTracker.ImageGrabber.ImagesGrabbed += (o, images) => eyeTracker.RecordingManager.RecordRawImages(images);
                    eyeTracker.ImageGrabber.ImagesGrabbed += (o, images) => eyeTracker.processor.ProcessFrame(images);
                    eyeTracker.ImageGrabber.ErrorOccurred += (o, e) => eyeTracker.OnError(e.Exception);
                    eyeTracker.ImageGrabber.Start();

                    // Initialize the calibration
                    //
                    // It needs to keep track of the image grabber to know the physical paramters of the image sources.
                    eyeTracker.CalibrationManager = new EyeTrackerCalibrationManager(eyeTracker.ImageGrabber);
                    eyeTracker.CalibrationManager.Start();

                    // Initialize the processing thread
                    //
                    // It needs the calibration for some steps in the processing like the geometric correction for torsion.
                    eyeTracker.processor = new EyeTrackerProcessor(eyeTracker.CalibrationManager);
                    eyeTracker.processor.NewProcessedImageAvailable += (o, processedImages) => eyeTracker.HandleNewProcessedImages(processedImages);
                    eyeTracker.processor.ErrorOccurred += (o, e) => eyeTracker.OnError(e.Exception);
                    eyeTracker.processor.Start();

                    // Finally what to do with the new Data and images
                    eyeTracker.NewDataAvailable += (o, e) => eyeTracker.RecordingManager.RecordNewDataAndImages(e.EyeTrackerDataAndImages);
                    eyeTracker.NewDataAvailable += (o, e) => eyeTracker.CalibrationManager.ProcessNewDataAndImages(e.EyeTrackerDataAndImages);

                    Trace.WriteLine("Initializing complete.");
                });

                // Go to idle state
                eyeTracker.state = EyeTrackerState.Idle;
            }


            return EyeTracker.eyeTrackerSingleton;
        }

        /// <summary>
        /// Starts tracking, using the eye tracking system selected in the configuration settings.
        /// </summary>
        public void Start()
        {
            try
            {
                // Initialize the eye tracking system
                this.eyeTrackingSystem = EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.Create(EyeTracker.Settings.EyeTrackerSystem);
                var cameraProvider = this.eyeTrackingSystem as ICameraEyeProvider;

                this.ImageGrabber.SetImageEyeSources(
                    cameraProvider.GetCameras().ConvertElements<IImageEyeSource>(),
                    cameraProvider.PreProcessImagesFromCameras);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error initializating the system, going into simulation mode.");
                System.Diagnostics.Trace.WriteLine(ex.Message);

                // If there was a problem go into simulation mode
                this.eyeTrackingSystem = EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.Create("Simulation");
                var cameraProvider = this.eyeTrackingSystem as ICameraEyeProvider;

                this.ImageGrabber.SetImageEyeSources(
                    cameraProvider.GetCameras().ConvertElements<IImageEyeSource>(),
                    cameraProvider.PreProcessImagesFromCameras);
            }

            this.processor.AvoidDropFrames = false;

            // If the eye tracker system has also a head sensor
            if (this.eyeTrackingSystem is IHeadSensorProvider)
            {
                this.headTracker = new EyeTrackerHeadTracker(this.eyeTrackingSystem as IHeadSensorProvider);
                this.headTracker.ErrorOccurred += (o, e) => this.OnError(e.Exception);
                this.headTracker.StartTracking();
            }

            // Start tracking and calibration
            this.state = EyeTrackerState.Tracking;
        }

        /// <summary>
        /// Stops tracking.
        /// </summary>
        public void Stop()
        {
            if (this.VideoPlayer != null)
            {
                this.VideoPlayer.Stop();
                this.VideoPlayer = null;
            }

            if (this.headTracker != null)
            {
                this.headTracker.StopTracking();
            }

            this.ImageGrabber.StopGrabbing();

            if (this.RecordingManager.Recording)
            {
                this.RecordingManager.StopRecording();
            }

            this.processor.ClearBuffer();

            this.state = EyeTrackerState.Idle;
        }

        /// <summary>
        /// Starts playing a video.
        /// </summary>
        /// <param name="options">Select video dialog options.</param>
        public void PlayVideo(PlayVideoOptions options)
        {
            // Initialize the eye tracking system
            this.eyeTrackingSystem = EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.Create(options.EyeTrackingSystem);
            var videoProvider = this.eyeTrackingSystem as IVideoEyeProvider;

            // Load calibration if necessary
            if (options.CalibrationFileName != null && options.CalibrationFileName.Length > 0)
            {
                this.CalibrationManager.Load(options.CalibrationFileName);
            }

            // Initialize the video grabbing
            this.VideoPlayer = new VideoPlayer(videoProvider, options.VideoFileNames, options.CustomRange);

            this.ImageGrabber.SetImageEyeSources(
                this.VideoPlayer.VideoFileSources.ConvertElements<IImageEyeSource>(),
               videoProvider.PreProcessImagesFromVideos);

            this.processor.AvoidDropFrames = false;

            System.Diagnostics.Trace.WriteLine("Playing videos: " + options.VideoFileNames);

            this.state = EyeTrackerState.Tracking;

            this.VideoPlayer.Play();
        }

        /// <summary>
        /// Starts processing a video.
        /// </summary>
        /// <param name="options">Select video dialog options.</param>
        public void ProcessVideo(ProcessVideoOptions options)
        {
            // Initialize the eye tracking system
            this.eyeTrackingSystem = EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.Create(options.EyeTrackingSystem);
            var videoProvider = this.eyeTrackingSystem as IVideoEyeProvider;

            // Load calibration if necessary
            if (options.CalibrationFileName != null && options.CalibrationFileName.Length > 0)
            {
                this.CalibrationManager.Load(options.CalibrationFileName);
            }

            // Initialize the video player
            this.VideoPlayer = new VideoPlayer(videoProvider, options.VideoFileNames, options.CustomRange);
            this.VideoPlayer.VideoFinished += (o, e) => this.RecordingManager.StopRecording();

            this.ImageGrabber.SetImageEyeSources(
                this.VideoPlayer.VideoFileSources.ConvertElements<IImageEyeSource>(),
                videoProvider.PreProcessImagesFromVideos);

            // Important tell the processor to not drop frames
            this.processor.AvoidDropFrames = true;

            // If processing a file notify that the recording has finished. This also implies that the 
            // processing has also finished. So another file can be processed.
            EventHandler recordingCompletedEventHandler = null;
            recordingCompletedEventHandler = delegate(object o, EventArgs e)
            {
                if (this.VideoPlayer != null)
                {
                    this.RaiseSyncrhonizedEvent(this.ProcessingVideoFinished, new EventArgs());
                    this.VideoPlayer.Stop();
                    this.VideoPlayer = null;
                }
                this.state = EyeTrackerState.Idle;

                // Unsuscribe from the event handler.
                this.RecordingManager.RecordingCompleted -= recordingCompletedEventHandler;
            };

            // Start recording (important to initialize before the playing, so all frames are recorded)
            this.RecordingManager.RecordingCompleted += recordingCompletedEventHandler;
            var optionsProcessedVideo = new OptionsSaveProcessedVideo();
            optionsProcessedVideo.AddCross = true;
            if (this.ImageGrabber.ImageEyeSources.Count == 2)
            {
                if (this.ImageGrabber.ImageEyeSources[Eye.Left] == null && this.ImageGrabber.ImageEyeSources[Eye.Left] != null)
                {
                    optionsProcessedVideo.WhichEye = Eye.Right;
                }
                if (this.ImageGrabber.ImageEyeSources[Eye.Left] != null && this.ImageGrabber.ImageEyeSources[Eye.Left] == null)
                {
                    optionsProcessedVideo.WhichEye = Eye.Left;
                }
                if (this.ImageGrabber.ImageEyeSources[Eye.Left] != null && this.ImageGrabber.ImageEyeSources[Eye.Left] != null)
                {
                    optionsProcessedVideo.WhichEye = Eye.Both;
                }
            }
            else
            {
                // TODO: this is not correct
                optionsProcessedVideo.WhichEye = Eye.Both;
            }

            optionsProcessedVideo.IncreaseContrast = true;
            this.RecordingManager.StartRecording(this.ImageGrabber.FrameRate, this.ImageGrabber.FrameSize, this.VideoPlayer.FrameRange, false, true, false, options.SaveProcessedVideo,optionsProcessedVideo, false);

            System.Diagnostics.Trace.WriteLine("Processing video: " + options.VideoFileNames + " Number of frames: " + (this.VideoPlayer.FrameRange.End - this.VideoPlayer.FrameRange.Begin));

            this.state = EyeTrackerState.PostProcessing;

            this.VideoPlayer.Play();
        }

        /// <summary>
        /// Records a new event
        /// </summary>
        public void RecordEvent(string eventMessage, object data)
        {
            var eyeTrackerEvent = new EyeTrackerEvent(eventMessage, data);
            if (this.ImageGrabber != null)
            {
                eyeTrackerEvent.FrameNumber = this.ImageGrabber.CurrentFrameNumber;
            }

            System.Diagnostics.Trace.WriteLine(eyeTrackerEvent.GetStringLine());

            if (this.RecordingManager != null)
            {
                this.RecordingManager.RecordNewEvent(eyeTrackerEvent);
            }

            this.RaiseSyncrhonizedEvent(NewEyeTrackerEvent, eyeTrackerEvent);
        }

        #region IDisposable Members

        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose resources.
        /// </summary>
        /// <param name="disposing">True if disposing.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {

                if (this.headTracker != null)
                {
                    this.headTracker.StopTracking();
                }

                if (this.CalibrationManager != null)
                {
                    if (this.CalibrationManager.Calibrating)
                    {
                        this.CalibrationManager.StopCalibration();
                    }
                }

                if (this.RecordingManager != null)
                {
                    if (this.RecordingManager.Recording)
                    {
                        this.RecordingManager.StopRecording();
                    }
                }

                if (this.ImageGrabber != null)
                {
                    this.ImageGrabber.StopGrabbing();
                }

                this.state = EyeTrackerState.Idle;

                if (this.ImageGrabber != null)
                {
                    this.ImageGrabber.Dispose();
                    this.ImageGrabber = null;
                }

                if (this.processor != null)
                {
                    this.processor.Dispose();
                    this.processor = null;
                }

                if (this.RecordingManager != null)
                {
                    this.RecordingManager.Dispose();
                    this.RecordingManager = null;
                }

                if (this.headTracker != null)
                {
                    this.headTracker.Dispose();
                    this.headTracker = null;
                }

                if (this.CalibrationManager != null)
                {
                    this.CalibrationManager.Dispose();
                    this.CalibrationManager = null;
                }

                EyeTrackerService.StopService();
            }
        }

        #endregion

        /// <summary>
        /// Raises the error event.
        /// </summary>
        /// <param name="ex">Exception that caused the error.</param>
        private void OnError(Exception ex)
        {
            System.Diagnostics.Trace.WriteLine("FATAL ERROR: " + ex.ToString());
            this.Stop();
        }

        /// <summary>
        /// Handles the event of new data processed.
        /// </summary>
        /// <param name="images">New data and corresponding images.</param>
        private void HandleNewProcessedImages(EyeCollection<ProcessedImageEye> images)
        {
            try
            {
                var data = new EyeTrackerData();

                ///////////////////////////////////////////////////
                // Get eye movement data
                ///////////////////////////////////////////////////
                if (images != null)
                {
                    var rawDataLeftEye = new EyeData();
                    var rawDataRightEye = new EyeData();
                    var calibratedDataLeftEye = new CalibratedEyeData();
                    var calibratedDataRightEye = new CalibratedEyeData();

                    if (images[Eye.Left] != null)
                    {
                        calibratedDataLeftEye = this.CalibrationManager.GetCalibratedEyeData(images[Eye.Left].EyeData);
                        rawDataLeftEye = images[Eye.Left].EyeData;
                    }

                    if (images[Eye.Right] != null)
                    {
                        calibratedDataRightEye = this.CalibrationManager.GetCalibratedEyeData(images[Eye.Right].EyeData);
                        rawDataRightEye = images[Eye.Right].EyeData;
                    }

                    data.EyeDataRaw = new EyeCollection<EyeData>(rawDataLeftEye, rawDataRightEye);
                    data.EyeDataCalibrated = new EyeCollection<CalibratedEyeData>(calibratedDataLeftEye, calibratedDataRightEye);
                }

                ///////////////////////////////////////////////////
                // Add data from other sources
                ///////////////////////////////////////////////////

                this.headTracker.SoftwareTrigger = this.SoftwareTrigger;

                // Atach head data
                if (this.headTracker != null)
                {
                    // Find the head data that correspond with the current images.
                    data.HeadDataRaw = this.headTracker.GetHeadData(images);
                    data.HeadDataCalibrated = this.CalibrationManager.GetCalibratedHeadData(data.HeadDataRaw);

                }

                // Add Extra data specific to the eye tracker system
                if (this.eyeTrackingSystem != null)
                {
                    // Find the extra data tha correspond with the current images.
                    data.ExtraData = this.eyeTrackingSystem.GetExtraData(images);
                }

                ///////////////////////////////////////////////////
                // Propagate the processed images to other objects 
                // that may need them
                // TODO: think if it would be better that the other
                // objects also suscribed to the process event.
                // I think it would more ofuscated but clener...
                ///////////////////////////////////////////////////

                var dataAndimages = new EyeTrackerDataAndImages(data, images);

                // Save it in the current data for other classes to access without listening to the event
                this.CurrentData = dataAndimages;

                // Raise the newFrameAvailable event so listeneners (GUI, etc) can act on it
                this.OnNewDataAvailable(new NewDataAvailableEventArgs(dataAndimages));
            }
            catch (Exception ex)
            {
                this.OnError(ex);
            }
        }

        /// <summary>
        /// Raises the event New Data Available.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        private void OnNewDataAvailable(NewDataAvailableEventArgs e)
        {
            var handler = this.NewDataAvailable;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises an event in the thread that initialized the eye tracker.
        /// </summary>
        /// <typeparam name="T">Event delegate type.</typeparam>
        /// <param name="eventHandler">Event to be raised.</param>
        /// <param name="eventArgs">Event arguments.</param>
        private void RaiseSyncrhonizedEvent<T>(EventHandler<T> eventHandler, T eventArgs)
        {
            if (eventHandler != null)
            {
                this.syncrhoznizationContext.Post(o => eventHandler.DynamicInvoke(this, eventArgs), null);
            }
        }
    }

    /// <summary>
    /// Arguments to be sent when the event NewDataAvailable is raised.
    /// </summary>
    public class NewDataAvailableEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the NewDataAvailableEventArgs class.
        /// </summary>
        /// <param name="processedImages">Processed images from the current frame.</param>
        /// <param name="eyeTrackerData">New data available.</param>
        internal NewDataAvailableEventArgs(EyeTrackerDataAndImages eyeTrackerDataAndImages)
        {
            this.EyeTrackerDataAndImages = eyeTrackerDataAndImages;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        public EyeTrackerDataAndImages EyeTrackerDataAndImages { get; private set; }
    }

    /// <summary>
    /// Class to encapsulate any kind of event
    /// </summary>
    public class EyeTrackerEvent
    {
        public EyeTrackerEvent(string EventMessage)
        {

            this.EventMessage = EventMessage;
            this.ComputerTime = DateTime.Now;
        }

        public EyeTrackerEvent(string EventMessage, object data)
        {
            this.EventMessage = EventMessage;
            this.ComputerTime = DateTime.Now;
            this.Data = data;
        }

        public DateTime ComputerTime;
        public long FrameNumber;
        public string EventMessage;
        public object Data;

        public string GetStringLine()
        {
            if (Data != null)
            {
                return this.ComputerTime.ToString("yyyy-MM-dd-HH:mm:ss.fff") + " " + FrameNumber + " M: " + EventMessage + " D: " + Data.ToString();
            }
            else
            {
                return this.ComputerTime.ToString("yyyy-MM-dd-HH:mm:ss.fff") + " " + FrameNumber + " M: " + EventMessage + " D: ";
            }
        }
    }


    /// <summary>
    /// Arguments for ErrorOccurred event.
    /// </summary>
    public class ErrorOccurredEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the NewImageEyeAvailableEventArgs class.
        /// </summary>
        /// <param name="exception">Exception that occurred.</param>
        internal ErrorOccurredEventArgs(Exception exception)
        {
            this.Exception = exception;
        }

        /// <summary>
        /// Gets the exception that occurred
        /// </summary>
        public Exception Exception { get; private set; }
    }

    /// <summary>
    /// Structure to represent a range of values from a minimum to a maximum.
    /// </summary>
    [TypeConverter(typeof(StructConverter<Range>))]
    public struct Range
    {
        /// <summary>
        /// Initializes a new instance of the Range structure.
        /// </summary>
        /// <param name="begin">Begining of the range.</param>
        /// <param name="end">End of the range.</param>
        public Range(double begin, double end)
            : this()
        {
            this.Begin = begin;
            this.End = end;
        }

        /// <summary>
        /// Gets or sets the begining of the range.
        /// </summary>
        public double Begin { get; set; }

        /// <summary>
        /// Gets or sets the end of the range.
        /// </summary>
        public double End { get; set; }

        /// <summary>
        /// Gets a value indicating wether the range is empty.
        /// </summary>
        [Browsable(false)]
        public bool IsEmpty
        {
            get
            {
                return this.Begin == this.End;
            }
        }

        /// <summary>
        /// Check if the range contains a certain value.
        /// </summary>
        /// <param name="value">Number to check.</param>
        /// <returns>Value indicating if the value is within the range.</returns>
        public bool Contains(double value)
        {
            return value >= this.Begin && value <= this.End;
        }

        public override string ToString()
        {
            return "[" + this.Begin + "-" + this.End + "]";
        }
    }
}