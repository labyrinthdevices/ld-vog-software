﻿//-----------------------------------------------------------------------
// <copyright file="NamespaceDoc.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    /// <summary>
    /// The <see cref="OculomotorLab.VOG"/> namespace contains the main classes of the eye tracker logic.
    /// <see cref=" EyeTracker"/> is the main class that starts all the background services (grabbing, processing, recording, etc.) and also
    /// receives the commands from the user interface or the remote interface.
    /// <see cref="EyeTrackerProcessor"/> corresponds with a background thread that processes images to extract the data.
    /// <see cref=" EyeTrackerGrabber"/> corresponds with a background thread that processes grabs images.
    /// <see cref=" EyeTrackerRecorder"/> corresponds with several background threads that record data, sound and video.
    /// <see cref=" EyeTrackerCalibrationManager"/> manages the calibration.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    public class NamespaceDoc
    {
    }
}