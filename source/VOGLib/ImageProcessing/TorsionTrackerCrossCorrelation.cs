﻿//-----------------------------------------------------------------------
// <copyright file="TorsionTrackerCrossCorrelation.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.ComponentModel.Composition;
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Calculates the torsion angle using the cross correlation of the iris pattern.
    /// </summary>
    [Export(typeof(ITorsionTracker)), ExportDescriptionAttribute("CrossCorrelation")]
    internal class TorsionTrackerCrossCorrelation : ITorsionTracker
    {
        /// <summary>
        /// Pixel with of the iris images used for the cross correlation. The iris
        /// pattern is resized to a fixed size for two reasons. First, to keep processing time 
        /// constant. Second, to account for pupil size changes. 
        /// Better make it a multiple of 4.
        /// </summary>
        private static int irisWidth = 60;

        /// <summary>
        /// Interpolation ratio of the crosscorrelation (how many samples per degree)
        /// for subpixel resolution. The cross correlation is calculated with 1 pixel per degree.
        /// A value of 50 would mean the subpixel resolution will be 0.02 degrees.
        /// </summary>
        private static int interpolation = 50;

        /// <summary>
        /// Magnification of the iris, in pixels per degree. Larger values will provide better 
        /// torsion resolution but also more time to process. Default is 1 pixel per degree.
        /// </summary>
        private static double resolution = 1;

        /// <summary>
        /// Size of the highpass filter for the iris pattern in degrees.
        /// </summary>
        private static int sobelFilterSize = 4;

        /// <summary>
        /// Calculate the angle between the current iris and the reference.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyeModel">Physical model of the eye.</param>
        /// <param name="imageTorsionReference">Iris image reference.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupil">Pupil ellipse.</param>
        /// <param name="iris">Iris circle.</param>
        /// <param name="imageTorsion">The image used to calculate the torsion angle.</param>
        /// <param name=param name="dataQuality">Index of data quality [0-100]</param>
        /// <returns>The torsion angle.</returns>
        public double CalculateTorsionAngle(ImageEye imageEye, EyePhysicalModel eyeModel, Image<Gray, byte> imageTorsionReference, EyeTrackerMask mask, PupilData pupil, IrisData iris, out Image<Gray, byte> imageTorsion, out double dataQuality)
        {
            imageTorsion = null;
            dataQuality = 0;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Iris pattern segmentation and polar transformation --
            // Get the iris pattern in polar coordinates and optimize the signature and the contrast
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            // Define the ROI containing the iris, leaving some margin to avoid edge effects
            var roiIris = new Rectangle(
                (int)Math.Round(iris.Center.X - (iris.Radius + 5)),
                (int)Math.Round(iris.Center.Y - (iris.Radius + 5)),
                (int)Math.Round((iris.Radius + 5) * 2),
                (int)Math.Round((iris.Radius + 5) * 2));

            // Limit the ROI by the size of the image
            roiIris.Intersect(new Rectangle(0, 0, imageEye.Size.Width, imageEye.Size.Height));

            // Check if the ROI is not empty
            if (roiIris.Width * roiIris.Height == 0)
            {
                return double.NaN;
            }

            // Mask the image, setting to zero the masked pixels
            var imageIris = imageEye.GetCroppedImage(roiIris);
            mask.CurrentMask.ROI = roiIris;
            imageIris.SetValue(new Gray(0), mask.CurrentMask.Not());
            mask.CurrentMask.ROI = new Rectangle();
            EyeTracker.Diagnostics.DebugImageManager.AddImage("torsion0", imageEye.WhichEye, imageIris);

            // Transform the eye to polar coordinates (unwrap iris)
            var imageEyePolar = this.GetImageIrisPolar(imageIris, eyeModel, pupil, iris, roiIris);
            //EyeTracker.Diagnostics.DebugImageManager.AddImage("torsion1", imageEye.WhichEye, imageEyePolar.Convert<Gray, byte>());

            // Extract the polar mask (pixels that were set to zero)
            var imageIrisPolarMask = imageEyePolar.ThresholdBinary(new Gray(25), new Gray(255));

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Iris pattern optimization --
            // Sobel filtering, smoothing and masking
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            // Angular high pass filter (columns in the iris pattern)
            imageEyePolar = imageEyePolar.SmoothBlur(10, 1);
            var imageIrisPolarFloat = imageEyePolar.Sobel(0, 1, (int)Math.Min(31, (sobelFilterSize * resolution) + 1));
            EyeTracker.Diagnostics.DebugImageManager.AddImage("torsion2", imageEye.WhichEye, imageIrisPolarFloat.Convert<Gray, byte>());

            // Apply the zero mask and add saturations to keep the range symmetric around zero
            imageTorsion = imageIrisPolarFloat.Max(-100).Min(100).Convert<Gray, byte>();
            //EyeTracker.Diagnostics.DebugImageManager.AddImage("torsion3", imageEye.WhichEye, imageTorsion);
            
            // Apply the noise mask
            imageTorsion = this.ApplyMaskNoise(imageTorsion, imageIrisPolarMask, imageEye.WhichEye);
            EyeTracker.Diagnostics.DebugImageManager.AddImage("torsion4", imageEye.WhichEye, imageTorsion);

            if (this.lastImageIrisReference != imageTorsionReference)
            {
                // This is a bit ugly. To make sure the noise mask is different after the reference has been 
                // changed
                this.UpdateIrisNoiseMask();
                this.lastImageIrisReference = imageTorsionReference;
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Torsion calculation --
            // Calculate the cross correlation between the reference image and the current image
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (imageTorsion == null || imageTorsionReference == null || imageTorsionReference.Width != imageTorsion.Width)
            {
                return double.NaN;
            }

            var maxTorsion = EyeTracker.Settings.Tracking.MaxTorsion;

            // Calculate the cross correlation between the reference and the current iris
            imageTorsion.ROI = new Rectangle(0, (int)(maxTorsion * resolution), imageTorsion.Width, (int)(360*resolution));
            Image<Gray, float> imageCorr = imageTorsionReference.MatchTemplate(imageTorsion, Emgu.CV.CvEnum.TM_TYPE.CV_TM_CCOEFF_NORMED);
            imageTorsion.ROI = new Rectangle();

            // Check the shape of the correlation. We want a correlation with a very sharp peak
            // so the mean should be low
            dataQuality = (255 - imageCorr.Convert<Gray, byte>().GetAverage().Intensity) / 255.0 * 100;

            // Interpolate the correlation to get subpixel resolution
            imageCorr = imageCorr.Resize(1, imageCorr.Height * interpolation, Emgu.CV.CvEnum.INTER.CV_INTER_LANCZOS4);

            // Get the maximum of the correlation
            double[] minVals, maxVals;
            Point[] minLocs, maxLocs;
            imageCorr.MinMax(out minVals, out maxVals, out minLocs, out maxLocs);

            // Save stuff for debugging
            EyeTracker.Diagnostics.DebugImageManager.AddImage("crosscorrelation", imageEye.WhichEye, imageCorr.Convert<Gray, byte>());
            EyeTracker.Diagnostics.DebugImageManager.AddImage("torsion", imageEye.WhichEye, imageTorsion);

            return (maxLocs[0].Y - (maxTorsion * resolution * interpolation) - (interpolation / 2)) / resolution / interpolation;
        }

        /// <summary>
        /// Gets the polar image of the eye around the pupil center.
        /// </summary>
        /// <param name="imageIris">Current image of the eye.</param>
        /// <param name="eyeModel">Physical model of the eye.</param>
        /// <param name="iris">Iris circle.</param>
        /// <param name="roiIris">ROI of the iris image in the original image.</param>
        /// <returns>Image of the eye in polar coordinates.</returns>
        internal Image<Gray, byte> GetImageIrisPolar(Image<Gray, byte> imageIris, EyePhysicalModel eyeModel, PupilData pupil, IrisData iris, Rectangle roiIris)
        {
            if (imageIris == null)
            {
                return null;
            }

            try
            {
                var pupilRadius = Math.Max(pupil.Size.Width / 2, pupil.Size.Height / 2) + 2;

                // Get the portion of the image containing the iris.
                var imageEyePolarTemp = new Image<Gray, byte>(
                    irisWidth,
                    (int)Math.Round(360 * resolution));

                // Do the polar transformation
                var polarCenter = iris.Center - new Size(roiIris.Location);

                var src = imageIris;
                var dest = new Image<Gray, byte>(imageEyePolarTemp.Size);

                var mapx = new Image<Gray, float>(dest.Size);
                var mapy = new Image<Gray, float>(dest.Size);

                unsafe
                {
                    double cp = 0;
                    double sp = 0;
                    float r = 0;
                    var cx = polarCenter.X;
                    var cy = polarCenter.Y;
                    var w = dest.Width;
                    var h = dest.Height;
                    var rad = iris.Radius;
                    var pi = Math.PI;

                    float ex = eyeModel.Center.X;
                    float ey = eyeModel.Center.Y;
                    float er = eyeModel.Radius;
                    float ix = iris.Center.X;
                    float iy = iris.Center.Y;

                    float exMinusRoirisX = ex - roiIris.X;
                    float eyMinusRoirisY = ey - roiIris.Y;

                    float ixMinusRoirisX = ix - roiIris.X;
                    float iyMinusRoirisY = iy - roiIris.Y;

                    // quaternion defining the rotation of the eyeball (ignoring torsion) just the center of the pupil
                    var angle = Math.Atan2((iris.Center.Y - eyeModel.Center.Y), (iris.Center.X - eyeModel.Center.X));
                    var yC = iris.Center.Y - eyeModel.Center.Y;
                    var xC = iris.Center.X - eyeModel.Center.X;
                    var ecc = Math.Asin(Math.Sqrt(yC * yC + xC * xC) / eyeModel.Radius);
                    var q = new Quaternions(Math.Cos(ecc / 2), -Math.Sin(angle) * Math.Sin(ecc / 2), Math.Cos(angle) * Math.Sin(ecc / 2), 0);


                    // Intermideate variables neccesary for the quaternion rotation
                    double
                       t2 = q.W * q.X,
                       t3 = q.W * q.Y,
                       t4 = q.W * q.Z,
                       t5 = -q.X * q.X,
                       t6 = q.X * q.Y,
                       t7 = q.X * q.Z,
                       t8 = -q.Y * q.Y,
                       t9 = q.Y * q.Z,
                       t10 = -q.Z * q.Z;

                    var rowsize = sizeof(float) * w;

                    fixed (float* px = mapx.Data)
                    {
                        fixed (float* py = mapy.Data)
                        {
                            IntPtr ptrx = (IntPtr)px;
                            IntPtr ptry = (IntPtr)py;
                            // do you stuff here

                            for (int phi = 0; phi < h; phi++)
                            {
                                cp = Math.Cos(phi * 2 * pi / h);
                                sp = Math.Sin(phi * 2 * pi / h);

                                float* mx = (float*)(ptrx + phi * rowsize);
                                float* my = (float*)(ptry + phi * rowsize);

                                for (int rho = 0; rho < w; rho++)
                                {
                                    r = (float)rho / w * (rad - pupilRadius) + pupilRadius;

                                    // x and y are the coordinates relative to the center of the pupil that correspond with rho and phi
                                    var x = r * cp;
                                    var y = r * sp;

                                    if (EyeTracker.Settings.Tracking.UseGeometricTransformation)
                                    {
                                        // now we need to rotate those to the actual position

                                        double rr = Math.Sqrt(x * x + y * y) / er;

                                        if (rr <= 1)
                                        {
                                            var z = Math.Sqrt(1 - rr * rr) * er;

                                            // var p2 = q.RotatePoint(p);
                                            double xx = 2.0 * ((t8 + t10) * x + (t6 - t4) * y + (t3 + t7) * z) + x;
                                            double yy = 2.0 * ((t4 + t6) * x + (t5 + t10) * y + (t9 - t2) * z) + y;
                                            double zz = 2.0 * ((t7 - t3) * x + (t2 + t9) * y + (t5 + t8) * z) + z;

                                            mx[rho] = (float)xx + exMinusRoirisX;
                                            my[rho] = (float)yy + eyMinusRoirisY;
                                        }
                                    }
                                    else
                                    {
                                        mx[rho] = (float)x + ixMinusRoirisX;
                                        my[rho] = (float)y + iyMinusRoirisY;
                                    }
                                }
                            }
                        }
                        EyeTracker.Diagnostics.DebugImageManager.AddImage("MAPX", Eye.Left, mapx.Convert<Gray, byte>());
                        EyeTracker.Diagnostics.DebugImageManager.AddImage("MAPY", Eye.Left, mapx.Convert<Gray, byte>());

                        CvInvoke.cvRemap(imageIris, imageEyePolarTemp, mapx, mapy, (int)Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR | (int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS, new MCvScalar(0));
                    }
                }

                //Emgu.CV.CvInvoke.cvLinearPolar(
                //imageIris,
                //imageEyePolarTemp,
                //polarCenter,
                //iris.Radius,
                //(int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS + (int)Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                // Shift the iris pattern by 270 degrees so the better part is not cut at the bottom and the top.
                // This will reduce edge effects.
                // At the same time extended maxTorsion degrees in both directions to do the cross correlations

                var maxTorsion = EyeTracker.Settings.Tracking.MaxTorsion;

                int totalHeight = (int)Math.Round(360 * resolution);
                int totalWidth = imageEyePolarTemp.Width;
                int totalHeightWithExtra = (int)Math.Round((360 + maxTorsion * 2) * resolution);
                int topSectionHeight = (int)((90 + maxTorsion) * resolution);
                int bottomSectionHeight = (int)((270 + maxTorsion) * resolution);

                var imageEyePolar = new Image<Gray, byte>(totalWidth, totalHeightWithExtra);

                // Copy the top section
                imageEyePolar.ROI = new Rectangle(0, 0, totalWidth, topSectionHeight);
                imageEyePolarTemp.ROI = new Rectangle(0, totalHeight - topSectionHeight, totalWidth, topSectionHeight);
                imageEyePolarTemp.CopyTo(imageEyePolar);

                // Copy the bottom section
                imageEyePolar.ROI = new Rectangle(0, topSectionHeight, totalWidth, bottomSectionHeight);
                imageEyePolarTemp.ROI = new Rectangle(0, 0, totalWidth, bottomSectionHeight);
                imageEyePolarTemp.CopyTo(imageEyePolar);

                imageEyePolar.ROI = new Rectangle();

                return imageEyePolar;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("ERROR in cross correlation : " + ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Last reference image used. Necessary to know if it must reset the noise mask.
        /// </summary>
        public Image<Gray, byte> lastImageIrisReference;

        /// <summary>
        /// Image full of noise. Used to mask the iris. Left Eye.
        /// </summary>
        private Image<Gray, float> imageNoiseMask;

        /// <summary>
        /// Applies the noise mask to the image of the iris.
        /// </summary>
        /// <typeparam name="TDeph">Type of the pixels of the image.</typeparam>
        /// <param name="image">Current image.</param>
        /// <param name="mask">Current mask.</param>
        /// <param name="whichEye">Left or right eye.</param>
        /// <returns>Masked image.</returns>
        internal Image<Gray, byte> ApplyMaskNoise<TDeph>(Image<Gray, TDeph> image, Image<Gray, byte> mask, Eye whichEye)
            where TDeph : new()
        {
            // Increase the mask by eroding the mask image. This will reduce the edge effects of the filter.
            IntPtr se21 = CvInvoke.cvCreateStructuringElementEx(15, 15, 8, 8, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE, IntPtr.Zero);
            CvInvoke.cvErode(mask, mask, se21, 1);

            if (mask == null)
            {
                return null;
            }

            // Wait just in case another thread is prepearing the mask
            if (this.imageNoiseMask == null)
            {
                UpdateIrisNoiseMask();
            }

            // Set to zero the pixels in the noise mask that do not belong to the mask
            this.imageNoiseMask.ROI = image.ROI;
            var noise = 128 + (imageNoiseMask * 60);
            this.imageNoiseMask.ROI = new Rectangle();

            // Set the masked pixels in the image to zero and then add the noise to them
            image.SetValue(new Gray(0), mask.Not());
            noise.SetValue(new Gray(0), mask);
            image = image + noise.Convert<Gray, TDeph>();

            return image.Convert<Gray, byte>();
        }


        /// <summary>
        /// Applies the zero mask to the image of the iris.
        /// </summary>
        /// <typeparam name="TDeph">Type of the pixels of the image so as to keep the same type on the output.</typeparam>
        /// <param name="image">Current image.</param>
        /// <param name="mask">Current mask.</param>
        /// <param name="whichEye">Left or right eye.</param>
        /// <returns>Masked image.</returns>
        internal Image<Gray, TDeph> ApplyMaskZero<TDeph>(Image<Gray, TDeph> image, Image<Gray, byte> mask, Eye whichEye)
            where TDeph : new()
        {
            IntPtr se21 = CvInvoke.cvCreateStructuringElementEx(5, 5, 2, 2, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE, IntPtr.Zero);
            CvInvoke.cvErode(mask, mask, se21, 1);

            if (mask == null)
            {
                return null;
            }

            // Set the masked pixels in the image to zero and then add the noise to them
            image.SetValue(new Gray(0), mask.Not());

            return image;
        }

        /// <summary>
        /// Updates the noise mask (actually it will be updated the next time Apply mask is called).
        /// </summary>
        internal void UpdateIrisNoiseMask()
        {
            imageNoiseMask = null;

            // Random generator for noise mask
            Random r = new Random();

            Image<Gray, float> imageNoiseMaskTemp = null;
            try
            {
                //imageNoiseMaskTemp = new Image<Gray, float>(
                //    (int)((360 + EyeTracker.Settings.Tracking.MaxTorsion * 2) * TorsionTrackerCrossCorrelation.resolution), 
                //    (int)((360 + EyeTracker.Settings.Tracking.MaxTorsion * 2) * TorsionTrackerCrossCorrelation.resolution));

                imageNoiseMaskTemp = new Image<Gray, float>(100, 100);

                // Fill the pixels with uniform noise (it is easier tha gaussian and it seems to work)
                for (int j = 0; j < imageNoiseMaskTemp.Width; j++)
                {
                    for (int i = 0; i < imageNoiseMaskTemp.Height; i++)
                    {
                        imageNoiseMaskTemp.Data[i, j, 0] = (float)((r.NextDouble() * 2) - 1);
                    }
                }

                imageNoiseMask = imageNoiseMaskTemp.Resize(
                        (int)((360 + EyeTracker.Settings.Tracking.MaxTorsion * 2) * TorsionTrackerCrossCorrelation.resolution),
                    (int)((360 + EyeTracker.Settings.Tracking.MaxTorsion * 2) * TorsionTrackerCrossCorrelation.resolution), Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);


                imageNoiseMaskTemp = null;
            }
            finally
            {
                if (imageNoiseMaskTemp != null)
                {
                    imageNoiseMaskTemp.Dispose();
                }
            }
        }
    }
}
