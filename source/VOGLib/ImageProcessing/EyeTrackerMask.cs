﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerMask.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class that creates the mask of the non-important parts of the eye: eyelids, reflections, etc.
    /// </summary>
    public class EyeTrackerMask
    {
        /// <summary>
        /// Last mask for the last image of the eye that has been processed.
        /// </summary>
        public Image<Gray, byte> CurrentMask { get; private set; }

        /// <summary>
        /// Gets the mask of the non-important parts of the eye: eyelids, reflections, etc.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyelid">Eyelid points.</param>
        /// <param name="eyeGlobe">Eye globe position and size.</param>
        /// <returns>The mask, ones in the good pixels zeros in masked ones.</returns>
        internal Image<Gray, byte> GetMask(ImageEye imageEye, EyelidData eyelid, EyePhysicalModel eyeGlobe)
        {
            var imageMask = GetEyelidMask(imageEye.Size, eyelid, eyeGlobe);

            var reflectionMask = GetReflectionsMask(imageEye);

            imageMask.SetValue(new Gray(0), reflectionMask);

            // Debug stuff
            this.CurrentMask = imageMask;

            EyeTracker.Diagnostics.DebugImageManager.AddImage("mask", imageEye.WhichEye, this.CurrentMask);

            return imageMask;
        }

        /// <summary>
        /// Gets the mask corresponding to the bright parts of the image.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <returns>The image mask.</returns>
        internal Image<Gray, byte> GetReflectionsMask(ImageEye imageEye)
        {
            var thresholdBright = (imageEye.WhichEye == Eye.Left) ?
                EyeTracker.Settings.Tracking.BrightThresholdLeftEye : EyeTracker.Settings.Tracking.BrightThresholdRightEye;

            // Find the brightest pixels (reflections)
            return imageEye.ThresholdBright(thresholdBright);
        }

        /// <summary>
        /// Gets the eyelid mask.
        /// </summary>
        /// <param name="size">Size of the image.</param>
        /// <param name="eyeLids">Eyelid position.</param>
        /// <param name="eyeGlobe">Eye globe position and size.</param>
        /// <returns>The eyelid mask.</returns>
        internal Image<Gray, byte> GetEyelidMask(Size size, EyelidData eyeLids, EyePhysicalModel eyeGlobe)
        {
            // It is necessary to clone the arrays because the points will be modified later to add the offset
            // and we want to keep the raw eyelid data.
            var upper = (Point[])Array.ConvertAll(eyeLids.Upper, point => new Point((int)Math.Round(point.X), (int)Math.Round(point.Y)));
            var lower = (Point[])Array.ConvertAll(eyeLids.Lower, point => new Point((int)Math.Round(point.X), (int)Math.Round(point.Y)));

            var backgroundColor = new Gray(255);
            var maskColor = new Gray(0);

            Image<Gray, byte> imageMaskTemp = null;
            Image<Gray, byte> imageMask = null;
            try
            {
                imageMaskTemp = new Image<Gray, byte>(size.Width, size.Height, maskColor);

                var c1 = new Point(
                    Math.Max(5, (int)(eyeGlobe.Center.X - eyeGlobe.Radius)),
                    (int)((eyeLids.Upper[0].Y + eyeLids.Upper[1].Y +
                    1.5 * eyeLids.Lower[0].Y + 1.5 * eyeLids.Lower[1].Y) / 5.0));

                var c2 = new Point(
                    (int)Math.Min(size.Width - 5,
                    (int)(eyeGlobe.Center.X + eyeGlobe.Radius)),
                    (int)((eyeLids.Upper[3].Y + 1.5 * eyeLids.Lower[3].Y + eyeLids.Upper[2].Y + 1.5 * eyeLids.Lower[2].Y) / 5.0));

                var eyelidOffsetTop = (int)Math.Round(EyeTracker.Settings.Tracking.TopEyelid * 100);
                var eyelidOffsetBottom = (int)Math.Round(EyeTracker.Settings.Tracking.BottomEyelid * 100);

                for (int i = 0; i < upper.Length; i++)
                {
                    upper[i].Y = upper[i].Y + (int)eyelidOffsetTop;
                }

                var upper2 = ProcessedImageEye.FitParabola(upper, eyeGlobe);

                for (int i = 0; i < lower.Length; i++)
                {
                    lower[i].Y = lower[i].Y - (int)eyelidOffsetBottom;
                }

                var lower2 = ProcessedImageEye.FitParabola(lower, eyeGlobe);


                //imageMaskTemp.FillConvexPoly(new Point[] { c1, upper[0], upper[1], upper[2], upper[3], c2, lower[3], lower[2], lower[1], lower[0] }, backgroundColor);

                var points = new Point[upper2.Length + lower2.Length];
                Array.Copy(upper2, points, upper2.Length);
                Array.Reverse(lower2);
                Array.Copy(lower2,0, points, upper2.Length, lower2.Length);
                imageMaskTemp.FillConvexPoly(points, backgroundColor);

                imageMask = imageMaskTemp;
                imageMaskTemp = null;
            }
            finally
            {
                if (imageMaskTemp != null)
                {
                    imageMaskTemp.Dispose();
                }
            }

            return imageMask;
        }
    }
}
