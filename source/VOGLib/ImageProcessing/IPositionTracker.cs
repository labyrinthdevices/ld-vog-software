﻿//-----------------------------------------------------------------------
// <copyright file="IPositionTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for eye position tracking. Follows Template Method Pattern.
    /// </summary>
    interface IPositionTracker
    {   
        /// <summary>
        /// Calculates the position of the eye (vertical and horizontal).
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupilAprox">Pupil ellipse, rough aproximation.</param>
        /// <param name="pupilReference">Pupil ellipse reference from calibration.</param>
        /// <returns>The pupil ellipse.</returns>
        PupilData CalculatePosition(ImageEye imageEye, Image<Gray, byte> mask, PupilData pupilAprox, PupilData pupilReference);
    }
}
