﻿//-----------------------------------------------------------------------
// <copyright file="PositionTrackerCentroid.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Drawing;
    using System.ComponentModel.Composition;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for eye position tracking using the center of mass of the dark pixels within a region of interest.
    /// </summary>
    [Export(typeof(IPositionTracker)), ExportDescriptionAttribute("Centroid")]
    internal class PositionTrackerCentroid : IPositionTracker
    {
        /// <summary>
        /// Memory storage for countours.
        /// </summary>
        MemStorage storage = new MemStorage();

        /// <summary>
        /// Calculates the position of the eye (vertical and horizontal).
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupilAprox">Pupil ellipse, rough aproximation.</param>
        /// <param name="pupilReference">Pupil ellipse reference from calibration.</param>
        /// <returns>The pupil ellipse.</returns>
        public PupilData CalculatePosition(ImageEye imageEye, Image<Gray, byte> mask, PupilData pupilAprox, PupilData pupilReference)
        {
            Image<Gray, byte> imgPupilBinary = null;

            var roiPupil = new Rectangle(
                 (int)(pupilAprox.Center.X - (pupilAprox.Size.Width / 2.0) - 20),
                 (int)(pupilAprox.Center.Y - (pupilAprox.Size.Height / 2.0) - 20),
                 (int)pupilAprox.Size.Width + 40,
                 (int)pupilAprox.Size.Height + 40);
            roiPupil.Intersect(new Rectangle(new Point(0,0), imageEye.Size));

            if (roiPupil.IsEmpty)
            {
                return pupilAprox;
            }

            float scaleUp = 4;

            var thresholdDark = (imageEye.WhichEye == Eye.Left) ?
                EyeTracker.Settings.Tracking.DarkThresholdLeftEye :
                EyeTracker.Settings.Tracking.DarkThresholdRightEye;

            var imgTemp = imageEye.GetCroppedImage(roiPupil).Convert<Gray, float>().Resize(scaleUp, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
            imgPupilBinary = imgTemp.ThresholdBinaryInv(new Gray(thresholdDark), new Gray(255)).Convert<Gray, byte>();

            // Image closing to remove little dots over threshold (eyelashes, for instance)
            int size = 3;
            if (size > 1)
            {
                int center = (int)Math.Floor(size / 2.0);
                StructuringElementEx kernel = new StructuringElementEx(size, size, center, center, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
                CvInvoke.cvErode(imgPupilBinary, imgPupilBinary, kernel, 1);
                CvInvoke.cvDilate(imgPupilBinary, imgPupilBinary, kernel, 1);
            }

            var centerMass = imgPupilBinary.GetMoments(true).GravityCenter;

            pupilAprox = new PupilData(
                new PointF( (float)(centerMass.x / scaleUp) + (float)roiPupil.X, (float)(centerMass.y / scaleUp) + (float)roiPupil.Y),
                new SizeF( pupilAprox.Size.Width, pupilAprox.Size.Height),
                90.0f);

            // Save stuff for debugging
            EyeTracker.Diagnostics.DebugImageManager.AddImage("position", imageEye.WhichEye, imgPupilBinary);

            return pupilAprox;
        }
    }
}
