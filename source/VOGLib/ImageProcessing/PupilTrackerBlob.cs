﻿// <copyright file="PupilTrackerBlob.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Cvb;
    using Emgu.CV.Structure;

    /// <summary>
    /// Finds the pupil using blob detection. The blobs are scored according to size and compacity.
    /// </summary>
    [Export(typeof(IPupilTracker)), ExportDescriptionAttribute("Blob")]
    internal class PupilTrackerBlob : IPupilTracker
    {
        MemStorage storage = new MemStorage();
        CvBlobDetector detector = new CvBlobDetector();
        CvBlobs blobs = new CvBlobs();

        /// <summary>
        /// Finds the pupil.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyeRoi">Roi containing the pupil.</param>
        /// <returns>Pupil ellipse.</returns>
        public PupilData FindPupil(ImageEye imageEye, Rectangle eyeRoi)
        {
            if (imageEye == null)
            {
                throw new ArgumentNullException("imageEye");
            }

            // Reduce the image to incrase processing speed because precision at this point is not
            // very important. 
            var imageSize = 200.0f;
            var scaleDown = imageSize / Math.Max(imageEye.Size.Width, imageEye.Size.Height);

            var maxPupRad = (imageEye.WhichEye == Eye.Left) ? EyeTracker.Settings.Tracking.IrisRadiusPixLeft : EyeTracker.Settings.Tracking.IrisRadiusPixRight;
            var minPupArea = Math.PI * Math.Pow(EyeTracker.Settings.Tracking.MinPupRadPix, 2);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Thresholding --
            // Get the binary image with the dark pixels
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            var thresholdDark = (imageEye.WhichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;
            var imageThreshold = imageEye.ThresholdDarkResized(thresholdDark, 1 / scaleDown, eyeRoi);

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // --  Morphological erosion and dilation --
            // Optimize the blobs by removing small white or black spots
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            int kernelSize = 1;
            if (kernelSize > 1)
            {
                int kernelCenter = (int)Math.Floor(kernelSize / 2.0);
                var kernel = new StructuringElementEx(kernelSize, kernelSize, kernelCenter, kernelCenter, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);

                // Image opening. Morpholigical operation to remove small spots that are above treshold.
                // First it erodes the image. That is reduce the size of the white blobs. Next it dilates the
                // remaning blobs. This will eliminate the blobs that are smaller than the kernel size.
                CvInvoke.cvErode(imageThreshold, imageThreshold, kernel, 1);
                CvInvoke.cvDilate(imageThreshold, imageThreshold, kernel, 1);

                // Image closing. Morpholigical operation to remove small spots that are below treshold.
                // First it dilates the image. That is increase the size of the white blobs. Next it erods the
                // remaning blobs. This will eliminate the spots within the blobs that are smaller than the kernel size.
                CvInvoke.cvDilate(imageThreshold, imageThreshold, kernel, 1);
                CvInvoke.cvErode(imageThreshold, imageThreshold, kernel, 1);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // --  Blob selection --
            // Find the blob that is most likely to be the pupil.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            var pupil = new PupilData();

            // Score of the blob with the maximum score.
            var maxScore = 0.0;

            detector.Detect(imageThreshold, blobs);

            foreach (var item in blobs)
            {
                storage.Clear();
                CvBlob blob = item.Value;

                // If area is too small
                if (blob.Area < minPupArea * scaleDown * scaleDown)
                {
                    continue;
                }

                // If the bounding box is too large
                if ((blob.BoundingBox.Width > (maxPupRad * 2 * scaleDown)) || (blob.BoundingBox.Height > (maxPupRad * 2 * scaleDown)))
                {
                    continue;
                }

                var contour = blob.GetContour(storage);
                
                // the score is the ration between area and perimeter
                var score = contour.Area / contour.Perimeter / Math.Sqrt(contour.Perimeter);

                // If touching the edges of the image lower the score
                if (blob.BoundingBox.X < 2 ||
                    blob.BoundingBox.Y < 2 ||
                    blob.BoundingBox.X + blob.BoundingBox.Width > imageThreshold.Width - 2 ||
                    blob.BoundingBox.Y + blob.BoundingBox.Height > imageThreshold.Height - 2)
                {
                    score = score / 10;
                }

                if (score < maxScore)
                {
                    continue;
                }

                if (score > 0.1)
                {
                    maxScore = score;

                    pupil = new PupilData(
                        new PointF(blob.Centroid.X, blob.Centroid.Y),
                        blob.BoundingBox.Size,
                        (float)90.0);
                }
            }

            var imgDebug = imageThreshold.Convert<Bgr, byte>();
            imgDebug.Draw( new CircleF(pupil.Center, pupil.Size.Width / 2), new Bgr(Color.Red), 2);
            EyeTracker.Diagnostics.DebugImageManager.AddImage("pupil", imageEye.WhichEye, imgDebug);

            pupil = new PupilData(
                new PointF((pupil.Center.X / scaleDown) + eyeRoi.X, (pupil.Center.Y / scaleDown) + eyeRoi.Y),
                new SizeF(pupil.Size.Width / scaleDown, pupil.Size.Height / scaleDown),
                (float)90.0);



            return pupil;
        }
    }
}
