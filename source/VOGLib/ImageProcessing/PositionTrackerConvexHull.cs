﻿//-----------------------------------------------------------------------
// <copyright file="PositionTrackerConvexHull.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Drawing;
    using System.ComponentModel.Composition;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for eye position tracking using the convex hull surrounding the pupil.
    /// </summary>
    [Export(typeof(IPositionTracker)), ExportDescriptionAttribute("ConvexHull")]
    internal class PositionTrackerConvexHull : IPositionTracker
    {

        /// <summary>
        /// Memory storage for countours.
        /// </summary>
        MemStorage storage = new MemStorage();

        /// <summary>
        /// Calculates the position of the eye (vertical and horizontal).
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupilAprox">Pupil ellipse, rough aproximation.</param>
        /// <param name="pupilReference">Pupil ellipse reference from calibration.</param>
        /// <returns>The pupil ellipse.</returns>
        public PupilData CalculatePosition(ImageEye imageEye, Image<Gray, byte> mask, PupilData pupilAprox, PupilData pupilReference)
        {
            // Get the necessary settings
            var minPupArea = Math.PI * Math.Pow(EyeTracker.Settings.Tracking.MinPupRadPix, 2);

            Image<Gray, byte> imgPupilBinary = null;

            var roiPupil = new Rectangle(
                 (int)(pupilAprox.Center.X - (pupilAprox.Size.Width / 2.0) - 20),
                 (int)(pupilAprox.Center.Y - (pupilAprox.Size.Height / 2.0) - 20),
                 (int)pupilAprox.Size.Width + 40,
                 (int)pupilAprox.Size.Height + 40);
            roiPupil.Intersect(new Rectangle(new Point(0,0), imageEye.Size));

            if (roiPupil.IsEmpty)
            {
                return pupilAprox;
            }

            float scaleUp = 2;

            var thresholdDark = (imageEye.WhichEye == Eye.Left) ?
                EyeTracker.Settings.Tracking.DarkThresholdLeftEye :
                EyeTracker.Settings.Tracking.DarkThresholdRightEye;

            var imgTemp = imageEye.GetCroppedImage(roiPupil).Convert<Gray, float>().Resize(scaleUp, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).SmoothBlur((int)(2 * scaleUp), (int)(2 * scaleUp));
            // imgTemp._EqualizeHist(); This could be an interesting idea for dynamic thresholding, 
            // instead of doing calculations on the histogram just equalize it and apply a constant threshold
            imgPupilBinary = imgTemp.ThresholdBinaryInv(new Gray(thresholdDark), new Gray(255)).Convert<Gray, byte>();

            // Image closing to remove little dots over threshold (eyelashes, for instance)
            int size = 3;
            int center = (int)Math.Floor(size / 2.0);
            StructuringElementEx kernel = new StructuringElementEx(size, size, center, center, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
            CvInvoke.cvErode(imgPupilBinary, imgPupilBinary, kernel, 1);
            CvInvoke.cvDilate(imgPupilBinary, imgPupilBinary, kernel, 1);

            // Find the contour with the largest area
            Seq<Point> c = null;
            double area = 0;

            for (Contour<Point> contours = imgPupilBinary.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL); contours != null; contours = contours.HNext)
            {
                if (contours.Area < area)
                {
                    continue;
                }

                area = contours.Area;
                c = contours.GetConvexHull(Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);
            }
            
            if ( c== null)
            {
                return pupilAprox;
            }

            var centerMass = c.GetMoments().GravityCenter;

            if (double.IsNaN(centerMass.x))
            {
                return pupilAprox;
            }

            pupilAprox = new PupilData(
                new PointF(
                (float)(centerMass.x / scaleUp) + (float)roiPupil.X,
                (float)(centerMass.y / scaleUp) + (float)roiPupil.Y),
                new SizeF(
                    c.BoundingRectangle.Width / scaleUp,
                    c.BoundingRectangle.Height / scaleUp),
                (float)90.0);

            // Save stuff for debugging
            EyeTracker.Diagnostics.DebugImageManager.AddImage("position", imageEye.WhichEye, imgPupilBinary);

            return pupilAprox;
        }
    }
}
