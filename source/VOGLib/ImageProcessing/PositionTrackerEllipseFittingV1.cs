﻿//-----------------------------------------------------------------------
// <copyright file="PositionTrackerEllipseFittingV1.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for eye position tracking using the convex hull surrounding the pupil and then fitting an ellipse to it.
    /// </summary>
    [Export(typeof(IPositionTracker)), ExportDescriptionAttribute("EllipseFittingV1")]
    internal class PositionTrackerEllipseFittingV1 : IPositionTracker
    {
        /// <summary>
        /// Dynamic threshold. If dynamic trheshold is activated.
        /// </summary>
        private double dynamicThresdhold;

        /// <summary>
        /// Last threshold used.
        /// </summary>
        private int lastThreshold;

        MemStorage storage = new MemStorage();

        /// <summary>
        /// Calculates the position of the eye (vertical and horizontal).
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupilAprox">Pupil ellipse, rough aproximation.</param>
        /// <param name="pupilReference">Pupil ellipse reference from calibration.</param>
        /// <returns>The pupil ellipse.</returns>
        public PupilData CalculatePosition(ImageEye imageEye, Image<Gray, byte> mask, PupilData pupilAprox, PupilData pupilReference)
        {
            // Set up options and parameters

            // Number of pixels added to the pupil roi.
            var roiPupilPadding = 20;

            // Size of the image of the pupil that will be used to run the ellipse detection.
            // To avoid bigger pupils taking longer to process than small pupils
            // resize the image to a fixed size that works well. Since the ROI is not necessarily square
            // the new image will keep the proportions but the larger dimension will be fixed.
            var desiredMaxSize = 200;

            var DO_DYNAMIC_THRESHOLDING = false;
            var DO_IMAGE_CLOSING = false;
            var DO_CANNY = false;
            var DO_CURVATURE_FILTER = false;
            var DO_HEURISTIC_INTERPOLATION = true;

            // Add some padding to the pupil ROI just in case
            var roiPupil = new Rectangle(
                 (int)(pupilAprox.Center.X - (pupilAprox.Size.Width / 2.0) - roiPupilPadding),
                 (int)(pupilAprox.Center.Y - (pupilAprox.Size.Height / 2.0) - roiPupilPadding),
                 (int)pupilAprox.Size.Width + roiPupilPadding * 2,
                 (int)pupilAprox.Size.Height + roiPupilPadding * 2);
            roiPupil.Intersect(new Rectangle(new Point(0, 0), imageEye.Size));

            if (roiPupil.IsEmpty)
            {
                return new PupilData();
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Resizing --
            // To avoid bigger pupils taking longer to process than small pupils resize the image to a fixed size 
            // that works well. Since the ROI is not necessarily square the new image will keep the proportions but 
            // the larger dimension will be fixed.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            float scaleFactor = (float)desiredMaxSize / Math.Max(roiPupil.Width, roiPupil.Height);
            int resizeWidth = (int)Math.Round(scaleFactor * (float)roiPupil.Width);
            int resizeHeight = (int)Math.Round(scaleFactor * (float)roiPupil.Height);
            float scaleFactorX = resizeWidth / (float)roiPupil.Width;
            float scaleFactorY = resizeHeight / (float)roiPupil.Height;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Thresholding --
            // Get the binary image with the dark pixels
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            Image<Gray, byte> imgPupilBinary = null;
            if (!DO_CANNY)
            {
                int blurSize = (int)Math.Round(4 * scaleFactor);
                var imgTemp = imageEye.GetCroppedImage(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).SmoothBlur(blurSize, blurSize);
                var threshold = (imageEye.WhichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;

                if (DO_DYNAMIC_THRESHOLDING)
                {
                    // dynamic thresholding
                    threshold = this.UpdateDynamicThreshold(imgTemp, imageEye.WhichEye);
                }
                imgPupilBinary = imgTemp.ThresholdBinaryInv(new Gray(threshold), new Gray(255));

                // TODO: Rethink this
                //imgPupilBinary = imgPupilBinary.Mul(mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN));

                if (DO_IMAGE_CLOSING)
                {
                    // Image closing to remove little dots over threshold (eyelashes, for instance)
                    int size = 2 * (int)Math.Round(2 * scaleFactor) + 1;
                    int center = (int)Math.Floor(size / 2.0);
                    StructuringElementEx kernel = new StructuringElementEx(size, size, center, center, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
                    CvInvoke.cvErode(imgPupilBinary, imgPupilBinary, kernel, 1);
                    CvInvoke.cvDilate(imgPupilBinary, imgPupilBinary, kernel, 1);
                }
            }
            else
            {
                int blurSize = (int)(5 * scaleFactor);
                //// It could be a good idea to use canny somehow. This will reduce dependency on the threshold
                // Mask pixels in the eyelids and reflections
                // imgPupilBinary = imgPupilBinary.Mul(mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN));
                imgPupilBinary = imageEye.GetCroppedImage(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).SmoothBlur(blurSize, blurSize).Canny(30, 80);
                imgPupilBinary = imgPupilBinary.Mul(mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN));

                // For canny to work I need to connect the contours somehow
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Contour analysis --
            // Find the contour with the largest area and select the points that are more likely to be part of the 
            // pupil and not artifacts like eyelashes or reflections.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            Seq<Point> pupilCountour = null;
            double maxArea = 0;
            Contour<Point> contours = imgPupilBinary.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);
            for (var contour = contours; contour != null; contour = contour.HNext)
            {
                if (contour.Perimeter < maxArea)
                {
                    continue;
                }

                maxArea = contour.Perimeter;
                pupilCountour = contour;
            }
            //maxArea = 0;
            //Seq<Point> pupilCountour2 = null;
            //for (var contour = contours; contour != null; contour = contour.HNext)
            //{
            //    if (contour.Perimeter < maxArea || contour.BoundingRectangle.IntersectsWith(pupilCountour.BoundingRectangle))
            //    {
            //        continue;
            //    }

            //    maxArea = contour.Perimeter;
            //    pupilCountour2 = contour;
            //}
            //if (pupilCountour2 != null)
            //{
            //    foreach (var point in pupilCountour2)
            //    {
            //        pupilCountour.Insert(0, point);
            //    }
            //}

            // If there are too few points do not try to fit the ellipse, just exit
            if (pupilCountour == null || pupilCountour.Total < 10)
            {
                return new PupilData();
            }


            // Simplify the contour
            pupilCountour = pupilCountour.ApproxPoly(0.5);


            // Curvature analysis
            if (DO_CURVATURE_FILTER)
            {
                var curvature = new double[pupilCountour.Total];
                var curvature2 = new double[pupilCountour.Total];
                for (int i = 0; i < pupilCountour.Total; i++)
                {
                    // var center = pupilCountour.GetMoments().GravityCenter;
                    var center =
                        new PointF(pupilCountour.BoundingRectangle.Location.X, pupilCountour.BoundingRectangle.Location.Y)
                        +
                        new SizeF((float)(pupilCountour.BoundingRectangle.Size.Width / 2f), (float)(pupilCountour.BoundingRectangle.Size.Height / 2f));
                    var p1 = pupilCountour[i];
                    var p2 = pupilCountour[i + 1];


                    var a = Math.Sqrt(Math.Pow((p1.Y - p2.Y), 2) + Math.Pow((p1.X - p2.X), 2));
                    var b = Math.Sqrt(Math.Pow((p1.Y - center.Y), 2) + Math.Pow((p1.X - center.X), 2));
                    var c = Math.Sqrt(Math.Pow((p2.Y - center.Y), 2) + Math.Pow((p2.X - center.X), 2));

                    curvature[i] = Math.Acos((Math.Pow(a, 2) + Math.Pow(b, 2) - Math.Pow(c, 2)) / (2 * a * b)) * 180 / Math.PI;
                    var inext = i + 1;
                    if (inext >= pupilCountour.Total)
                    {
                        inext = 0;
                    }
                    curvature2[inext] = Math.Acos((Math.Pow(a, 2) + Math.Pow(c, 2) - Math.Pow(b, 2)) / (2 * a * c)) * 180 / Math.PI;

                }

                // Go trhough the points in the countour in reverese order to avoid problems when deleting
                var nPoints = pupilCountour.Total;
                for (int i = nPoints - 1; i >= 0; i--)
                {
                    if (curvature[i] < 50 || curvature[i] > 130 || curvature2[i] < 50 || curvature2[i] > 130)
                    {
                        pupilCountour.RemoveAt(i);
                        continue;
                    }
                }

                // If there are too few points do not try to fit the ellipse, just exit
                if (pupilCountour.Total < 8)
                {
                    return new PupilData();
                }
            }

            // Get the convex hull to remove the reflection and simplify the contour again
            pupilCountour = pupilCountour.GetConvexHull(Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);

            // Remove points that are too close to the mask because they are probably not edges of the pupil
            var maskROIpupil = mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN).Erode(5);

            // Go trhough the points in the countour in reverese order to avoid problems when deleting
            for (int i = pupilCountour.Total - 1; i >= 0; i--)
            {
                if (maskROIpupil.Data[pupilCountour[i].Y, pupilCountour[i].X, 0] == 0)
                {
                    pupilCountour.RemoveAt(i);
                }
            }

            // If there are too few points do not try to fit the ellipse, just exit
            if (pupilCountour.Total < 8)
            {
                return new PupilData();
            }

            // Get the convex hull to remove the reflection and simplify the contour again
            pupilCountour = pupilCountour.GetConvexHull(Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);


            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Heuristic interpolation -- 
            // Fill in points to interpolate the top part of the pupil.
            // The top part of the pupil is usually cover by the eyelid. In that case it helps to 
            // add a few points so the ellipse fitting is more accurate and less noise. One good
            // guess of the position of the top of the pupil is to keep a memory of the aspect ratio 
            // of the pupil and then taking into account the current position of the bottom and the
            // current width of the pupil calculate the corresponding top.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            bool contourComplete = true;

            if (DO_HEURISTIC_INTERPOLATION)
            {
                var boundingBox = GetCountourBoundingBox(pupilCountour);

                // Check if the counter is complete
                for (int i = 0; i < pupilCountour.Total; i++)
                {
                    // Middle of the bounding box
                    var middleX = pupilCountour.BoundingRectangle.X + boundingBox.Width / 2.0;

                    // If there is a gap and the gap includes the middle of the box
                    var gapIncludesMiddle = ((pupilCountour[i].X - middleX) * (pupilCountour[i + 1].X - middleX) < 0);
                    var gapTooLarge = pupilCountour[i].X - pupilCountour[i + 1].X > desiredMaxSize / 5;
                    if (gapTooLarge && gapIncludesMiddle)
                    {
                        contourComplete = false;
                        break;
                    }
                } 

                if (!contourComplete && !pupilReference.IsEmpty)
                {
                    // Add the extra points to interpolate the top part of the countour
                    var currentWidth = boundingBox.Width;
                    var currentHeight = boundingBox.Height;
                    var pupilSizeRatio = pupilReference.Size.Width / pupilReference.Size.Height;
                    var pupilHeight = currentWidth / pupilSizeRatio;

                    pupilCountour.Insert(0, new Point(
                        (int)(boundingBox.X + currentWidth / 2.0),
                        (int)(boundingBox.Y + currentHeight - pupilHeight)));
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Ellipse fitting --
            // Fit an ellipse to the points of the contour.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            // If there are too few points do not try to fit the ellipse, just exit
            if (pupilCountour.Total < 8)
            {
                return new PupilData();
            }

            var ellipse = CvInvoke.cvFitEllipse2(pupilCountour);

            // Update the aspect ratio
            var rect = ellipse.MinAreaRect();

            // Debug stuff
            //// If the pupil is full and the aspect ration makes sense
            //// (that is, it is similar to the averaged one) update the average
            //var currentRation = (rect.Width / (double)rect.Height);
            //if (Math.Abs(currentRation - pupilSizeRatio) < 0.1 && currentRation < 1.5 && currentRation > 1 / 1.5)
            //{
            //    pupilSizeRatio = pupilSizeRatio * 0.9 + currentRation * 0.1;
            //}

            // Change the ellipse information to the coordinates of the original image
            var pupil = new PupilData(
                new PointF((ellipse.center.X / scaleFactorX) + (float)roiPupil.X, (ellipse.center.Y / scaleFactorY) + (float)roiPupil.Y),
                new SizeF(ellipse.size.Width / scaleFactorX, ellipse.size.Height / scaleFactorY),
                ellipse.angle - 90);

            // Save stuff for debugging
            imgPupilBinary.SetValue(new Gray(50), maskROIpupil.Not());
            imgPupilBinary.Draw(new Ellipse(ellipse.center, ellipse.size, ellipse.angle - 90), new Gray(200), 1);
            if (pupilCountour.Total > 2)
            {
                foreach (var point in pupilCountour)
                {
                    imgPupilBinary.Draw(new CircleF(new PointF(point.X, point.Y), 1f), new Gray(100), 2);
                }
            }
            //this.lastImageEye.Draw(pupilCountour.BoundingRectangle, new Gray(100), 2);
            EyeTracker.Diagnostics.DebugImageManager.AddImage("position", imageEye.WhichEye, imgPupilBinary);

            return pupil;
        }

        /// <summary>
        /// Finds the bounding box of the countour. It should not be necessary but I don't understand how
        /// countours deal with removing points. I suspect that the bounding box does not get recalculated
        /// automatically
        /// </summary>
        /// <param name="countour">Contour.</param>
        /// <returns></returns>
        private static Rectangle GetCountourBoundingBox(Seq<Point> countour)
        {
            var minx = 500;
            var maxx = 0;
            var miny = 500;
            var maxy = 0;

            // Go trhough the points in the countour in reverese order to avoid problems when deleting
            for (int i = countour.Total - 1; i >= 0; i--)
            {
                if (countour[i].X > maxx)
                {
                    maxx = countour[i].X;
                }
                if (countour[i].X < minx)
                {
                    minx = countour[i].X;
                }
                if (countour[i].Y > maxy)
                {
                    maxy = countour[i].Y;
                }
                if (countour[i].Y < miny)
                {
                    miny = countour[i].Y;
                }
            }
            return new Rectangle(minx, miny, maxx - minx, maxy - miny);
        }

        /// <summary>
        /// Inspired from: https://www.assembla.com/code/EyeGaze/subversion/nodes/90/EyeGaze/FeatureDetection/PupilHistogram.cs
        /// </summary>
        /// <param name="image"></param>
        /// <param name="whichEye"></param>
        /// <returns></returns>
        private int UpdateDynamicThreshold(Image<Gray, byte> image, Eye whichEye)
        {
            DenseHistogram hist = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            // Histogram Computing
            hist.Calculate<Byte>(new Image<Gray, byte>[] { image }, true, null);


            // Array to hold the intensity count
            float[] histogram = new float[256];

            hist.MatND.ManagedArray.CopyTo(histogram, 0);

            //EyeTrackerDiagnostics.TrackTime("hist11");
            //for (int i = 1; i < image.Size.Height; i++)
            //{
            //    for (int j = 1; j < image.Size.Width; j++)
            //        histogram[(int)image[i, j].Intensity]++;
            //} EyeTrackerDiagnostics.TrackTime("hist12");

            // Find maximums
            List<int> maxs = new List<int>();
            for (int i = 20; i < histogram.Length; i++)
            {
                bool max = true;
                for (int j = 1; (i - j) > 0 && (i + j) < histogram.Length && j < 20; j++)
                {
                    if (histogram[i] <= histogram[i - j] || histogram[i] <= histogram[i + j])
                    {
                        max = false;
                        break;
                    }
                }
                if (max)
                    maxs.Add(i);
            }

            if (maxs.Count < 2)
            {
                return (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;
            }

            // Place threshold between the two first maximums
            int threshold = (maxs[1] - maxs[0]) / 2 + maxs[0];


            var currentThresdhold = (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;
            if (this.lastThreshold != currentThresdhold)
            {
                this.dynamicThresdhold = currentThresdhold;
            }

            if (this.dynamicThresdhold == 0)
            {
                if (whichEye == Eye.Left)
                {
                    this.dynamicThresdhold = EyeTracker.Settings.Tracking.DarkThresholdLeftEye;
                }
                else
                {
                    this.dynamicThresdhold = EyeTracker.Settings.Tracking.DarkThresholdRightEye;
                }
            }
            else
            {
                if (Math.Abs(dynamicThresdhold - threshold) < 20)
                {
                    this.dynamicThresdhold = this.dynamicThresdhold * 0.8 + threshold * 0.2;
                }

                if (whichEye == Eye.Left)
                {
                    if (Math.Abs(this.dynamicThresdhold - EyeTracker.Settings.Tracking.DarkThresholdLeftEye) > 5)
                    {
                        EyeTracker.Settings.Tracking.DarkThresholdLeftEye = (int)Math.Round(this.dynamicThresdhold);
                    }
                }
                else
                {
                    if (Math.Abs(this.dynamicThresdhold - EyeTracker.Settings.Tracking.DarkThresholdRightEye) > 5)
                    {
                        EyeTracker.Settings.Tracking.DarkThresholdRightEye = (int)Math.Round(this.dynamicThresdhold);
                    }
                }
            }

            this.lastThreshold = (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;

            return (int)Math.Round(dynamicThresdhold);
        }
    }
}
