﻿//-----------------------------------------------------------------------
// <copyright file="EyelidTrackerNone.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.ComponentModel.Composition;

    /// <summary>
    /// Does not find eyelids
    /// </summary>
    [Export(typeof(IEyelidTracker)), ExportDescriptionAttribute("None")]
    internal class EyelidTrackerNone : IEyelidTracker
    {
        public EyelidData UpdateFilteredEyelids(EyelidData eyeLids, EyePhysicalModel eyeGlobe)
        {
            return eyeLids;
        }

        /// <summary>
        /// Finds the eyelids.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="iris">Iris position and radius.</param>
        /// <returns>Eyelids as an array of 8 points. First 4 points are the top eyelid from left to right,
        /// the next 4 are the bottom eyelid from left to right.</returns>
        public EyelidData FindEyelids(ImageEye imageEye, IrisData iris, EyePhysicalModel eyeGlobe)
        {
            return new EyelidData();
        }
    }
}
