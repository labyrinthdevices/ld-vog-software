﻿//-----------------------------------------------------------------------
// <copyright file="EyeLidTrackerHoughLinesV1.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Drawing;
    using System.ComponentModel.Composition;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Tracks the eyelids using the Hough transform. It finds four segments of the eyelids independently around the pupil.
    /// </summary>
    [Export(typeof(IEyelidTracker)), ExportDescriptionAttribute("HoughLinesV1")]
    internal class EyeLidTrackerHoughLinesV1 : IEyelidTracker
    {
        /// <summary>
        /// Four positions of the eyelid segments.
        /// </summary>
        internal enum EyeLidCorners
        {
            /// <summary>
            /// Top left corner.
            /// </summary>
            TopLeft,

            /// <summary>
            /// Top right corner.
            /// </summary>
            TopRight,

            /// <summary>
            /// Bottom left corner.
            /// </summary>
            BottomLeft,

            /// <summary>
            /// Bottom right corner.
            /// </summary>
            BottomRight
        }

        /// <summary>
        /// Accumulator threshold for the Hough Lines function.
        /// </summary>
        private static int houghThreshold = 20;

        /// <summary>
        /// Resolution of rho in pixels for the Hough Lines function.
        /// </summary>
        private static double houghRhoResolution = 2;

        /// <summary>
        /// Resolution of theta in degrees for the Hough Lines function.
        /// </summary>
        private static double houghThetaResolution = 5;

        /// <summary>
        /// Resolution of the image used for the eyelid detection. Number of pixels
        /// that correspond to the eyeglobe radius.
        /// </summary>
        private static int pixelsPerEyeRadius = 80;

        /// <summary>
        /// Initializes a new instance of the EyeLidTrackerHoughLines class.
        /// </summary>
        public EyeLidTrackerHoughLinesV1()
        {
            this.filteredEyelids = new EyelidData();
        }

        /// <summary>
        /// Running average of the position of the eyelids.
        /// </summary>
        private EyelidData filteredEyelids;

        /// <summary>
        /// Variable to keep track of the eyeGlobe used for the eyelids.
        /// </summary>
        private EyePhysicalModel latEyeGlobe;

        MemStorage storage = new MemStorage();

        /// <summary>
        /// Finds the eyelids.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="iris">Iris position and radius.</param>
        /// <param name="eyeGlobe">Eye globe position and size.</param>
        /// <returns>Eyelids as an array of 8 points. First 4 points are the top eyelid from left to right,
        /// the next 4 are the bottom eyelid from left to right.</returns>
        public EyelidData FindEyelids(ImageEye imageEye, IrisData iris, EyePhysicalModel eyeGlobe)
        {
            float scale = (float)(eyeGlobe.Radius / (float)pixelsPerEyeRadius);
            if (scale == 0)
            {
                scale = 1;
            }

            // Resize the image to a fixed size. That way processing time is independent on the original
            // image size and the parameters don't need to change.
            var irisResize = new CircleF(new PointF(iris.Center.X / scale, iris.Center.Y / scale), iris.Radius / scale);
            var eyeGlobeResize = new CircleF(new PointF(eyeGlobe.Center.X / scale, eyeGlobe.Center.Y / scale), eyeGlobe.Radius / scale);

            var imageResize = imageEye.GetResizedImage(1 / scale);
            this.debugImage = imageResize.Copy();

            var lineLeftTop = this.FindEyelidSegment(imageResize, EyeLidCorners.TopLeft, irisResize, eyeGlobeResize);
            var lineLeftBottom = this.FindEyelidSegment(imageResize, EyeLidCorners.BottomLeft, irisResize, eyeGlobeResize);
            var lineRightTop = this.FindEyelidSegment(imageResize, EyeLidCorners.TopRight, irisResize, eyeGlobeResize);
            var lineRightBottom = this.FindEyelidSegment(imageResize, EyeLidCorners.BottomRight, irisResize, eyeGlobeResize);

            var pointsUpper = new PointF[] 
            {
                lineLeftTop.P1,
                lineLeftTop.P2,
                lineRightTop.P1,
                lineRightTop.P2,
            };

            var pointsLower = new PointF[] 
            {
                lineLeftBottom.P1,
                lineLeftBottom.P2,
                lineRightBottom.P1,
                lineRightBottom.P2,
            };

            var eyelidData = new EyelidData();
            eyelidData.Upper = Array.ConvertAll(pointsUpper, point => new PointF( point.X * scale,  point.Y * scale));
            eyelidData.Lower = Array.ConvertAll(pointsLower, point => new PointF( point.X * scale,  point.Y * scale));

            EyeTracker.Diagnostics.DebugImageManager.AddImage("blink", imageEye.WhichEye, this.debugImage);

            return eyelidData;
        }

        public EyelidData UpdateFilteredEyelids(EyelidData eyeLids, EyePhysicalModel eyeGlobe)
        {
            // If the eyeGlobe position has changed reset the eyelids
            if (eyeGlobe.IsEmpty || !eyeGlobe.Equals(this.latEyeGlobe))
            {
                this.filteredEyelids = new EyelidData();
            }

            this.latEyeGlobe = eyeGlobe;

            double alfa = 0.8;

            for (int i = 0; i < 4; i++)
            {
                if (this.filteredEyelids.Upper[i].IsEmpty)
                {
                    this.filteredEyelids.Upper[i] = eyeLids.Upper[i];
                }
                else
                {
                    this.filteredEyelids.Upper[i].X = (int)Math.Round(this.filteredEyelids.Upper[i].X * alfa + eyeLids.Upper[i].X * (1 - alfa));
                    this.filteredEyelids.Upper[i].Y = (int)Math.Round(this.filteredEyelids.Upper[i].Y * alfa + eyeLids.Upper[i].Y * (1 - alfa));
                }
            }

            for (int i = 0; i < 4; i++)
            {
                if (this.filteredEyelids.Lower[i].IsEmpty)
                {
                    this.filteredEyelids.Lower[i] = eyeLids.Lower[i];
                }
                else
                {
                    this.filteredEyelids.Lower[i].X = (int)Math.Round(this.filteredEyelids.Lower[i].X * alfa + eyeLids.Lower[i].X * (1 - alfa));
                    this.filteredEyelids.Lower[i].Y = (int)Math.Round(this.filteredEyelids.Lower[i].Y * alfa + eyeLids.Lower[i].Y * (1 - alfa));
                }
            }

            return this.filteredEyelids;
        }

        /// <summary>
        /// Finds one of the segments of the eyelid.
        /// </summary>
        /// <param name="image">Image of the eye.</param>
        /// <param name="corner">Which corner.</param>
        /// <param name="iris">Center and radius of the iris.</param>
        /// <param name="eyeGlobe">Eye globe position and radius.</param>
        /// <returns>The eyelid line segment.</returns>
        private LineSegment2DF FindEyelidSegment(Image<Gray, byte> image, EyeLidCorners corner, CircleF iris, CircleF eyeGlobe)
        {
            var roi = new Rectangle();

            //////////////////////////////////////////////////////////////////////////////////////
            // Define the search parameters depending on the corner
            //////////////////////////////////////////////////////////////////////////////////////

            var areaWidth = (int)Math.Round(iris.Radius * 0.6);
            var centerGap = iris.Radius * 0.5;

            var topY = (int)(iris.Center.Y - (iris.Radius * 1.1f));
            var topHeight = (int)(iris.Radius * 1.3f);
            var bottomY = (int)Math.Max(iris.Center.Y + iris.Radius / 6, eyeGlobe.Center.Y + iris.Radius / 6);
            var bottomHeight = (int)(iris.Radius * 1.3f);

            var eyeGlobeRadius = eyeGlobe.Radius / 1.5;

            // Get the ROI that corresponds with the corner
            switch (corner)
            {
                case EyeLidCorners.TopLeft:
                    roi = new Rectangle(
                        (int)Math.Max(eyeGlobe.Center.X - eyeGlobeRadius, Math.Min(eyeGlobe.Center.X - iris.Radius / 2, iris.Center.X - areaWidth - centerGap)),
                        topY,
                        areaWidth,
                        topHeight);
                    break;
                case EyeLidCorners.TopRight:
                    roi = new Rectangle(
                        (int)Math.Min(eyeGlobe.Center.X + eyeGlobeRadius, Math.Max(eyeGlobe.Center.X + iris.Radius / 2, iris.Center.X + centerGap)),
                        topY,
                        areaWidth,
                        topHeight);
                    break;

                // The bottom eyelid does not move up and down much
                case EyeLidCorners.BottomLeft:
                    roi = new Rectangle(
                        (int)Math.Max(eyeGlobe.Center.X - eyeGlobeRadius, Math.Min(eyeGlobe.Center.X - iris.Radius / 2, iris.Center.X - areaWidth - centerGap)),
                        bottomY,
                        areaWidth,
                        bottomHeight);
                    break;
                case EyeLidCorners.BottomRight:
                    roi = new Rectangle(
                        (int)Math.Min(eyeGlobe.Center.X + eyeGlobeRadius, Math.Max(eyeGlobe.Center.X + iris.Radius / 2, iris.Center.X + centerGap)),
                        bottomY,
                        areaWidth,
                        bottomHeight);
                    break;
            }

            roi.Intersect(image.ROI);

            // Get the range of angles of the segment for each piece of eyelid
            var mincos = 0.7;
            var maxcos = 0.7;
            switch (corner)
            {
                case EyeLidCorners.TopLeft:
                    mincos = -0.2;
                    maxcos = 0.7;
                    break;
                case EyeLidCorners.TopRight:
                    mincos = -0.7;
                    maxcos = 0.2;
                    break;

                // The bottom eyelid does not move up and down much
                case EyeLidCorners.BottomLeft:
                    mincos = -0.4;
                    maxcos = 0;
                    break;
                case EyeLidCorners.BottomRight:
                    mincos = 0;
                    maxcos = 0.4;
                    break;
            }

            // Set up the default line in case a line is not found
            var eyeLidLine = new LineSegment2DF();
            switch (corner)
            {
                case EyeLidCorners.TopLeft:
                case EyeLidCorners.TopRight:
                    eyeLidLine = new LineSegment2DF(
                        roi.Location,
                        new PointF(roi.X + roi.Width, roi.Y + roi.Height / 2f));
                    break;

                case EyeLidCorners.BottomLeft:
                case EyeLidCorners.BottomRight:
                    eyeLidLine = new LineSegment2DF(
                        new PointF(roi.X, roi.Y + roi.Height / 2f),
                        roi.Location + roi.Size);
                    break;
            }

            // If the ROI is too small don't try to find a line
            if (roi.Size.Height < 2 || roi.Size.Width < 2)
            {
                return eyeLidLine;
            }

            //////////////////////////////////////////////////////////////////////////////////////
            // Optimize the image for line segment search
            //////////////////////////////////////////////////////////////////////////////////////

            image = image.Copy(roi);

            // Low pass filter in the horizontal direction and high pass second order filter in the 
            // vertical direction
            image = image.SmoothBlur(7, 3).Sobel(0, 2, 9).Convert<Gray, byte>();

            // Equalize the contrast and threshold to find the brightest spots
            image._EqualizeHist();
            image = image.ThresholdBinary(new Gray(230), new Gray(255));

            //////////////////////////////////////////////////////////////////////////////////////
            // Find lines in the segment
            //////////////////////////////////////////////////////////////////////////////////////

            // Keep track of the highest (lowest) line
            var maxy2 = (corner == EyeLidCorners.TopLeft || corner == EyeLidCorners.TopRight) ?
                0f : image.Height * 2;

            // Find the lines
            IntPtr lines = CvInvoke.cvHoughLines2(
                image.Ptr,
                storage.Ptr,
                Emgu.CV.CvEnum.HOUGH_TYPE.CV_HOUGH_STANDARD,
                houghRhoResolution,
                (houghThetaResolution * Math.PI) / 180,
                houghThreshold,
                0,
                0);

            // Loop troough the lines to find the highest (lowest) line
            int maxLines = 30;
            for (int i = 0; i < maxLines; i++)
            {
                // Get the next line
                IntPtr line = CvInvoke.cvGetSeqElem(lines, i);
                if (line == IntPtr.Zero)
                {
                    // No more lines
                    break;
                }

                PolarCoordinates coords = (PolarCoordinates)System.Runtime.InteropServices.Marshal.PtrToStructure(line, typeof(PolarCoordinates));

                // Check if the angle of the line is within the range for that corner
                if (Math.Abs(coords.Theta) > 0 && Math.Cos(coords.Theta) > mincos && Math.Cos(coords.Theta) < maxcos)
                {
                    // The lines are given in kind of polar coordinates. Rho and theta define a vector that touches the
                    // line and is perpendicular to it.
                    var y1 = coords.Rho / (float)Math.Sin(coords.Theta);
                    var y2 = -(float)(Math.Cos(coords.Theta) / (float)Math.Sin(coords.Theta) * image.Width) + (coords.Rho / (float)Math.Sin(coords.Theta));

                    switch (corner)
                    {
                        case EyeLidCorners.TopLeft:
                        case EyeLidCorners.TopRight:
                            // For top eyelids find the lowest line (highest y)
                            if (y2 + y1 > maxy2)
                            {
                                maxy2 = y2 + y1;
                                eyeLidLine = new LineSegment2DF(
                                    new PointF(roi.X + 0, roi.Y + y1),
                                    new PointF(roi.X + image.Width, roi.Y + y2));
                            }
                            break;
                        case EyeLidCorners.BottomLeft:
                        case EyeLidCorners.BottomRight:
                            // For top eyelids find the highest line (lowest y)
                            if (y2 + y1 < maxy2)
                            {
                                maxy2 = y2 + y1;
                                eyeLidLine = new LineSegment2DF(
                                    new PointF(roi.X + 0, roi.Y + y1),
                                    new PointF(roi.X + image.Width, roi.Y + y2));
                            }
                            break;
                    }
                }
            }

            // Debug stuff
            this.debugImage.ROI = roi;
            image.CopyTo(this.debugImage);
            this.debugImage.ROI = new Rectangle();

            return eyeLidLine;
        }

        /// <summary>
        /// Polar coordinates structure.
        /// </summary>
        internal struct PolarCoordinates
        {
            /// <summary>
            /// Magnitude of the vector.
            /// </summary>
            internal float Rho;

            /// <summary>
            /// Angle of the vector.
            /// </summary>
            internal float Theta;

            /// <summary>
            /// Initializes a new instance of the PolarCoordinates struct.
            /// </summary>
            /// <param name="rho">Magnitude of the vector.</param>
            /// <param name="theta">Angle of the vector.</param>
            internal PolarCoordinates(float rho, float theta)
            {
                this.Rho = rho;
                this.Theta = theta;
            }
        }

        Image<Gray, byte> debugImage = null;

    }
}
