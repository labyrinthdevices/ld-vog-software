﻿//-----------------------------------------------------------------------
// <copyright file="IPupilTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for fast pupil tracking. Follows Template Method Pattern so multiple different methods for 
    /// pupil tracking can be implemented by inheriting from this class. 
    /// </summary>
    /// <remarks>
    /// This method are supposed to be rough quick methods to find the pupil. Not necessarily the 
    /// exact eye position.  The fine eye position will be calculated by the family of classes inheriting 
    /// from <see cref="PositionTracker"/>.
    /// </remarks>
    /// <seealso cref="PositionTracker"/>
    public interface IPupilTracker
    {
        /// <summary>
        /// Finds the pupil.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyeRoi">Roi containing the pupil.</param>
        /// <returns>Pupil ellipse.</returns>
        PupilData FindPupil(ImageEye imageEye, Rectangle eyeRoi);
    }
}
