﻿//-----------------------------------------------------------------------
// <copyright file="IEyelidTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Eyelid tracking generic interface.
    /// </summary>
    public interface IEyelidTracker
    {
        EyelidData UpdateFilteredEyelids(EyelidData eyeLids, EyePhysicalModel eyeGlobe);

        /// <summary>
        /// Finds the eyelids.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="iris">Iris position and radius.</param>
        /// <param name="eyeGlobe">Eye globe position and size.</param>
        /// <returns>Eyelids as an array of 8 points. First 4 points are the top eyelid from left to right,
        /// the next 4 are the bottom eyelid from left to right.</returns>
        EyelidData FindEyelids(ImageEye imageEye, IrisData iris, EyePhysicalModel eyeGlobe);
    }
}
