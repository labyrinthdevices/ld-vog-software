﻿//-----------------------------------------------------------------------
// <copyright file="IrisTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Methods to detect the limits of the iris.
    /// </summary>
    public enum IrisDetectionMethod
    {
        /// <summary>
        /// Automatic method detecting the outer edge.
        /// </summary>
        Automatic,

        /// <summary>
        /// Manual method using the UI.
        /// </summary>
        Manual,
    }

    /// <summary>
    /// Class for pupil tracking. Follows Template Method Pattern.
    /// </summary>
    internal class IrisTracker
    {
        /// <summary>
        /// Initializes a new instance of the IrisTracker class.
        /// </summary>
        internal IrisTracker()
        {
        }

        /// <summary>
        /// Crop the polar image of the eye to get the iris band.
        /// </summary>
        /// <typeparam name="TDeph">Type of the pixels of the image.</typeparam>
        /// <param name="imagePolar">Image of the eye in polar coordinates.</param>
        /// <param name="irisRadius">Radius of the iris.</param>
        /// <param name="pupilRadius">Radius of the pupil.</param>
        /// <returns>Image of the iris band.</returns>
        internal static Image<Gray, TDeph> CropIrisImage<TDeph>(Image<Gray, TDeph> imagePolar, double irisRadius, double pupilRadius)
            where TDeph : new()
        {
            int innerLimit = (int)Math.Min(Math.Ceiling(pupilRadius), imagePolar.Width - 1);
            int irisWidth = (int)Math.Max(irisRadius - pupilRadius, 1);

            // Extract the iris band
            imagePolar.ROI = new Rectangle(
                innerLimit,
                0,
                irisWidth,
                imagePolar.Height);

            return imagePolar;
        }

        /// <summary>
        /// Updates the iris information.
        /// </summary>
        /// <param name="imageEye">Current image f the eye..</param>
        /// <param name="pupil">Current pupil position.</param>
        /// <returns>Iris position.</returns>
        internal IrisData FindIris(ImageEye imageEye, PupilData pupil)
        {
            Debug.Assert(imageEye != null, "The imageEye cannot be null");
            Debug.Assert(!pupil.Size.IsEmpty, "The pupil box cannot be empty");

            var iris = new IrisData();

            // Find the iris radius
            iris = this.FindIris(imageEye, pupil, imageEye.WhichEye);

            return iris;
        }

        /// <summary>
        /// Finds the limit of the iris.
        /// </summary>                                            
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="pupil">Pupil information.</param>
        /// <param name="whichEye">Left or right eye.</param>
        /// <returns>The number of pixels of the iris radius.</returns>
        private IrisData FindIris(ImageEye imageEye, PupilData pupil, Eye whichEye)
        {
            Debug.Assert(imageEye != null, "The imageEye cannot be null");

            ////TODO: this should return more properties of the iris. Probably an ellipse or a custom object

            // Get the necessary settings
            var irisDetectionMethod = EyeTracker.Settings.Tracking.IrisDetectionMethod;

            switch (irisDetectionMethod)
            {
                case IrisDetectionMethod.Manual:

                    // Use the value of the radius in the settings (coming from the UI)
                    var irisRadius = (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.IrisRadiusPixLeft : EyeTracker.Settings.Tracking.IrisRadiusPixRight;
                    return new IrisData(pupil.Center, (float)irisRadius);

                case IrisDetectionMethod.Automatic:
                    throw new NotImplementedException("This option is not implemented.");

                ////// Return the same radius as the reference unless it is being resetted
                ////if (!this.EyeTrackerProcess.GetCalibration(whichEye).Calibrating)
                ////{
                ////    return new CircleF(pupil.MCvBox2D.center, this.EyeTrackerProcess.GetCalibration(whichEye).IrisReference.Radius);
                ////}

                ////// Find limit of the iris by finding the column with a maximum average value
                ////// after vertical edge detection
                ////Image<Gray, float> imageEdgePolar = imagePolar.Copy();
                ////float[] colsum = new float[imageEdgePolar.Width];

                ////for (int i = 0; i < imageEdgePolar.Height; i++)
                ////{
                ////    for (int j = (int)pupil.MCvBox2D.size.Width / 2 + 10; j < imageEdgePolar.Width; j++)
                ////    {
                ////        float temppix = 0;
                ////        for (int k = Math.Max(j - 3, 0); k < Math.Min(j + 3, imageEdgePolar.Width); k++)
                ////        {
                ////            if (k >= j)
                ////            {
                ////                temppix += imagePolar.Data[i, k, 0];
                ////            }
                ////            else
                ////            {
                ////                temppix -= imagePolar.Data[i, k, 0];
                ////            }
                ////        }

                ////        imageEdgePolar.Data[i, j, 0] = 128 + (temppix / 6);

                ////        colsum[j] += imageEdgePolar.Data[i, j, 0];
                ////    }
                ////}

                ////double maxval = 0;
                ////int colmax2 = -1;
                ////for (int j = (int)pupil.MCvBox2D.size.Width / 2 + 10; j < imageEdgePolar.Width; j++)
                ////{
                ////    if (maxval < colsum[j])
                ////    {
                ////        colmax2 = j;
                ////        maxval = colsum[j];
                ////    }
                ////}

                ////return new CircleF(pupil.MCvBox2D.center, (float)colmax2);
                default:
                    return new IrisData(pupil.Center, 0.0f);
            }
        }
    }
}