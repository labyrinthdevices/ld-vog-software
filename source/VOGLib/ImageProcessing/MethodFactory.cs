﻿//-----------------------------------------------------------------------
// <copyright file="TrackingMethodFactory.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    /// <summary>
    /// Generic abstract factory to initialize objects that implement different tracking methods for the same problem. For example, pupil tracking
    /// or torsion tracking.
    /// Inspired in : http://dotnetcodr.com/2013/05/02/design-patterns-and-practices-in-net-the-factory-patterns-concrete-static-abstract/
    /// </summary>
    class TrackingMethodFactory<IMethod> where IMethod : class
    {
        /// <summary>
        /// Current method.
        /// </summary>
        private IMethod currentMethod;

        Loader<IMethod, IEyeTrackerPluginMetadata> loader;

        /// <summary>
        /// Las description requested. 
        /// </summary>
        string lastDescription = string.Empty;

        /// <summary>
        /// Initializes a new instance of the TrackingMethodFactory class.
        /// </summary>
        public TrackingMethodFactory(Loader<IMethod, IEyeTrackerPluginMetadata> loader)
        {
            this.loader = loader;
        }

        /// <summary>
        /// Gets the current method corresponding the the description provided. It will only 
        /// initialize a new method if the description is different than the last one requested.
        /// </summary>
        /// <param name="methodDescription">Description of the method. It should correspond to the name of the class
        /// that implements it or part of it.</param>
        /// <returns>The object that implements the proper method.</returns>
        public IMethod GetCurrentMethod(string methodDescription)
        {
            if ( this.lastDescription == methodDescription && this.currentMethod != null)
            {
                return this.currentMethod;
            }

            this.lastDescription = methodDescription;
            this.currentMethod = loader.Create(methodDescription);

            return this.currentMethod;
        }
    }
}
