﻿//-----------------------------------------------------------------------
// <copyright file="ImageEyeProcess.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Collections.Concurrent;
    using System.Drawing;
    using System.Threading;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using OculomotorLab.VOG.ImageProcessing;

    /// <summary>
    /// Class in charge of processing images and tracking the pupil and iris to obtain 
    /// the eye position and the torsion angle.
    /// </summary>
    internal class ImageEyeProcess
    {
        /// <summary>
        /// Gets the pupil tracker factory.
        /// </summary>
        private TrackingMethodFactory<IPupilTracker> pupilTrackerFactory;

        /// <summary>
        /// Gets the position tracker factory.
        /// </summary>
        private TrackingMethodFactory<IPositionTracker> positionTrackerFactory;

        /// <summary>
        /// Gets the eyelid tracker factory.
        /// </summary>
        private TrackingMethodFactory<IEyelidTracker> eyeLidTrackerFactory;

        /// <summary>
        /// Gets the torsion Tracker factory.
        /// </summary>
        private TrackingMethodFactory<ITorsionTracker> torsionTrackerFactory;

        /// <summary>
        /// Gets the iris Tracker.
        /// </summary>
        private IrisTracker irisTracker;

        /// <summary>
        /// Gets the EyeTrackerMask.
        /// </summary>
        private EyeTrackerMask eyeTrackerMask;
        
        /// <summary>
        /// Initializes a new instance of the ImageEyeProcess class.
        /// </summary>
        /// <param name="whichEye">Left or right eye.</param>
        public ImageEyeProcess(Eye whichEye)
        {
            this.irisTracker = new IrisTracker();
            this.eyeTrackerMask = new EyeTrackerMask();

            this.torsionTrackerFactory = new TrackingMethodFactory<ITorsionTracker>(EyeTrackingPluginLoader.Loader.torsionTrackerFactory);
            this.eyeLidTrackerFactory = new TrackingMethodFactory<IEyelidTracker>(EyeTrackingPluginLoader.Loader.eyelidTrackerFactory);
            this.positionTrackerFactory = new TrackingMethodFactory<IPositionTracker>(EyeTrackingPluginLoader.Loader.positionTrackerFactory);
            this.pupilTrackerFactory = new TrackingMethodFactory<IPupilTracker>(EyeTrackingPluginLoader.Loader.pupilTrackerFactory);
        }
        
        /// <summary>
        /// Processes the current image to get the eye movement data.
        /// </summary>
        /// <param name="imageEye">Image from current frame.</param>
        /// <param name="eyeCalibrationParameters">Calibration info.</param>
        /// <returns>The data obtained from processing the image.</returns>
        public ProcessedImageEye ProcessImageEye(ImageEye imageEye, EyeCalibrationParameters eyeCalibrationParameters)
        {
            // Check the settings to see if it is necessary to change some of the processing objects
            // This enables to change the methods online without having to restart
            var pupilTracker = pupilTrackerFactory.GetCurrentMethod(EyeTracker.Settings.Tracking.PupilTrackingMethod);
            var positionTracker = positionTrackerFactory.GetCurrentMethod(EyeTracker.Settings.Tracking.PositionTrackingMethod);
            var eyeLidTracker = eyeLidTrackerFactory.GetCurrentMethod(EyeTracker.Settings.Tracking.EyelidTrackingMethod);
            var torsionTracker = torsionTrackerFactory.GetCurrentMethod(EyeTracker.Settings.Tracking.TorsionMethod);

            // Dropped frame or missing image
            if (imageEye == null)
            {
                return null;
            }

            // Copy the calibration variables just in case they change during the processing to avoid inconsistencies
            // The next frame will use the updated calibration
            var eyeModel = eyeCalibrationParameters.EyePhysicalModel;
            var referencePupil = eyeCalibrationParameters.ReferenceData.Pupil;
            var imageTorsionReference = eyeCalibrationParameters.ImageTorsionReference;

            // Cropping rectangle
            var eyeROI = new Rectangle(
                EyeTracker.Settings.Tracking.Cropping.Left,
                EyeTracker.Settings.Tracking.Cropping.Top,
                imageEye.Size.Width - EyeTracker.Settings.Tracking.Cropping.Left - EyeTracker.Settings.Tracking.Cropping.Width,
                imageEye.Size.Height - EyeTracker.Settings.Tracking.Cropping.Top - EyeTracker.Settings.Tracking.Cropping.Height);

            // Quick search of the pupil
            var pupilAprox = pupilTracker.FindPupil(imageEye, eyeROI);
            EyeTracker.Diagnostics.Timer.TrackTime("FindPupil");
            if (pupilAprox.Size.IsEmpty)
            {
                return new ProcessedImageEye(imageEye, ProcessFrameResult.MissingPupil);
            }

            // Iris limits
            var irisTemp = this.irisTracker.FindIris(imageEye, pupilAprox);
            if (eyeModel.IsEmpty)
            {
                eyeModel = new EyePhysicalModel(irisTemp.Center, irisTemp.Radius * 2);
            }

            // Eyelid tracking
            var eyelids = eyeLidTracker.FindEyelids(imageEye, irisTemp, eyeModel);
            EyeTracker.Diagnostics.Timer.TrackTime("FindEyeLids");
            var filteredEyelids = eyeLidTracker.UpdateFilteredEyelids(eyelids, eyeModel);

            // Mask of reflections and eyelids
            var eyeMask = this.eyeTrackerMask.GetMask(imageEye, filteredEyelids, eyeModel);
            EyeTracker.Diagnostics.Timer.TrackTime("GetMask");

            // Refine the pupil position
            var pupil = positionTracker.CalculatePosition(imageEye, eyeMask, pupilAprox, referencePupil);
            if (pupil.Size.IsEmpty)
            {
                // If the fine pupil tracker did not find a proper pupil revert to the first aproximation
                pupil = pupilAprox;
            }
            EyeTracker.Diagnostics.Timer.TrackTime("CalculatePosition");

            // Find the iris
            var iris = this.irisTracker.FindIris(imageEye, pupil);
            EyeTracker.Diagnostics.Timer.TrackTime("UpdateIris");

            // Calculate torsion angle
            Image<Gray, byte> imageTorsion = null;
            double dataQuality = 0.0;
            double torsionAngle = torsionTracker.CalculateTorsionAngle(imageEye, eyeModel, imageTorsionReference, this.eyeTrackerMask, pupil, iris, out imageTorsion, out dataQuality);
            EyeTracker.Diagnostics.Timer.TrackTime("CalculateTorsionAngle");

            // Create the data structure
            var eyeData = new EyeData(
                imageEye.WhichEye,
                imageEye.TimeStamp,
                pupil,
                iris,
                torsionAngle,
                eyelids,
                dataQuality,
                ProcessFrameResult.Good);

            return new ProcessedImageEye(imageEye, imageTorsion, eyeData);
        }
    }
}