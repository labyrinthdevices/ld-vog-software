﻿// <copyright file="PupilTrackerCentroid.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Finds the pupil as the centroid of the dark pixels.
    /// </summary>
    [Export(typeof(IPupilTracker)), ExportDescriptionAttribute("Centroid")]
    internal class PupilTrackerCentroid : IPupilTracker
    {
        /// <summary>
        /// Finds the pupil.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyeRoi">Roi containing the pupil.</param>
        /// <returns>Pupil ellipse.</returns>
        public PupilData FindPupil(ImageEye imageEye, Rectangle eyeRoi)
        {
            if (imageEye == null)
            {
                throw new ArgumentNullException("imageEye");
            }

            var thresholdDark = (imageEye.WhichEye == Eye.Left) ?
                EyeTracker.Settings.Tracking.DarkThresholdLeftEye :
                EyeTracker.Settings.Tracking.DarkThresholdRightEye;

            //// Threshold the image to find the dark parts (pupil)
            var imageThreshold = imageEye.ThresholdDark(thresholdDark);

            imageThreshold.ROI = eyeRoi;
            var moments = imageThreshold.GetMoments(true);
            imageThreshold.ROI = new Rectangle();

            var centerPupilThreshold = new PointF(
                (int)Math.Round(moments.GravityCenter.x + eyeRoi.X),
                (int)Math.Round(moments.GravityCenter.y + eyeRoi.Y));
            var radius = (float)Math.Sqrt(imageThreshold.GetAverage().Intensity / 255.0 * imageThreshold.Width * imageThreshold.Height / Math.PI);

            // Save stuff for debugging
            EyeTracker.Diagnostics.DebugImageManager.AddImage("pupil", imageEye.WhichEye, imageThreshold);

            return new PupilData(centerPupilThreshold, new SizeF(radius * 2, radius * 2), (float)0.0);
        }
    }
}
