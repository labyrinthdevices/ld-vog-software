﻿//-----------------------------------------------------------------------
// <copyright file="ITorsionTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Interfaces for classes implementing a torsion tracker.
    /// </summary>
    public interface ITorsionTracker
    {
        /// <summary>
        /// Calculate the angle between the current iris and the reference.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyeModel">Physical model of the eye.</param>
        /// <param name="imageTorsionReference">Iris image reference.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupil">Pupil ellipse.</param>
        /// <param name="iris">Iris circle.</param>
        /// <param name="imageTorsion">The image used to calculate the torsion angle.</param>
        /// <param name=param name="dataQuality">Index of data quality [0-100]</param>
        /// <returns>The torsion angle.</returns>
        double CalculateTorsionAngle(ImageEye imageEye, EyePhysicalModel eyeModel, Image<Gray, byte> imageTorsionReference, EyeTrackerMask mask, PupilData pupil, IrisData iris, out Image<Gray, byte> imageTorsion, out double dataQuality);
    }
}
