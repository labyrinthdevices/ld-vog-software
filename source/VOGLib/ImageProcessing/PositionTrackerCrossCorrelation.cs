﻿//-----------------------------------------------------------------------
// <copyright file="PositionTrackerCrossCorrelation.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Tracks the position of the eye using the iris.
    /// </summary>
    [Export(typeof(IPositionTracker)), ExportDescriptionAttribute("CrossCorrelation ")]
    internal class PositionTrackerCrossCorrelation : IPositionTracker
    {
        /// <summary>
        /// Reference image.
        /// </summary>
        private Image<Gray, float> imageRef = null;

        /// <summary>
        /// Center of the pupil in the reference image.
        /// </summary>
        private PointF pupilCenterRef = new PointF();

        /// <summary>
        /// Last image of the eye.
        /// </summary>
        private Image<Gray, byte> lastImageEye;

        /// <summary>
        /// Polar radial edge detector filter.
        /// </summary>
        /// <param name="imageCur">Image to be filtered.</param>
        /// <param name="polarCenter">Center of the radial pattern.</param>
        /// <returns>Filtered image.</returns>
        internal static Image<Gray, float> PolarSobel(Image<Gray, float> imageCur, PointF polarCenter)
        {
            Image<Gray, float> imageEyePolar = new Image<Gray, float>(
                (int)300, 360);

            Emgu.CV.CvInvoke.cvLinearPolar(
                imageCur,
                imageEyePolar,
                polarCenter,
                150,
                (int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS + (int)Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

            var imagePolarInf = imageCur.Copy();

            var i1 = imageEyePolar.SmoothBlur(10, 1).Sobel(0, 1, 7);
            ////var i2 = imageEyePolar.Sobel(1, 0, 7);
            ////var imagePolarInfsob = i1 + i2;
            var imagePolarInfsob = i1 * 20.0;

            Emgu.CV.CvInvoke.cvLinearPolar(
                imagePolarInfsob,
                imagePolarInf,
                polarCenter,
                imagePolarInfsob.Size.Width / 2,
                (int)Emgu.CV.CvEnum.WARP.CV_WARP_INVERSE_MAP + (int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS + (int)Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

            return imagePolarInf;
        }

        /////// <summary>
        /////// Polar tangent edge detector filter.
        /////// </summary>
        /////// <param name="imageCur">Image to be filtered.</param>
        /////// <param name="polarCenter">Center of the radial pattern.</param>
        /////// <returns>Filtered image.</returns>
        ////internal static Image<Gray, float> PolarSobelTangent(Image<Gray, float> imageCur, PointF polarCenter)
        ////{
        ////    Image<Gray, float> imageEyePolar = new Image<Gray, float>(
        ////        (int)300, 360);

        ////    Emgu.CV.CvInvoke.cvLinearPolar(
        ////        imageCur,
        ////        imageEyePolar,
        ////        polarCenter,
        ////        150,
        ////        (int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS + (int)Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

        ////    var imagePolarInf = imageCur.Copy();

        ////    var i1 = imageEyePolar.SmoothBlur(1, 10).Sobel(1, 0, 7);
        ////    ////var i2 = imageEyePolar.Sobel(1, 0, 7);
        ////    ////var imagePolarInfsob = i1 + i2;
        ////    var imagePolarInfsob = i1 * 20.0;

        ////    Emgu.CV.CvInvoke.cvLinearPolar(
        ////        imagePolarInfsob,
        ////        imagePolarInf,
        ////        polarCenter,
        ////        imagePolarInfsob.Size.Width / 2,
        ////        (int)Emgu.CV.CvEnum.WARP.CV_WARP_INVERSE_MAP + (int)Emgu.CV.CvEnum.WARP.CV_WARP_FILL_OUTLIERS + (int)Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);

        ////    return imagePolarInf;
        ////}

        /// <summary>
        /// Calculates the position of the eye (vertical and horizontal).
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupilAprox">Pupil ellipse, rough aproximation.</param>
        /// <param name="pupilReference">Pupil ellipse reference from calibration.</param>
        /// <returns>The pupil ellipse.</returns>
        public PupilData CalculatePosition(ImageEye imageEye, Image<Gray, byte> mask, PupilData pupilAprox, PupilData pupilReference)
        {
            ////// Get the necessary settings
            ////var minPupArea = Math.PI * Math.Pow(EyeTracker.Settings.MinPupRadPix, 2);
            ////var thresholdDark = (imageEye.WhichEye == Eye.Left) ?
            ////    EyeTracker.Settings.DarkThresholdLeftEye :
            ////    EyeTracker.Settings.DarkThresholdRightEye;

            ////// Second pass increase the size of the image but only analyze the blob

            ////// Region of interest contining the pupil plus some margin
            ////var roiPupil = new Rectangle(
            ////    (int)(pupilAprox.center.X - (pupilAprox.size.Width / 2) - 10),
            ////    (int)(pupilAprox.center.Y - (pupilAprox.size.Height / 2) - 10),
            ////    (int)pupilAprox.size.Width + 20,
            ////    (int)pupilAprox.size.Height + 20);
            ////roiPupil.Intersect(imageEye.Image.ROI);

            ////// Exact bounding box of the pupil
            ////var boundingBox = new Rectangle(
            ////    (int)(pupilAprox.center.X - (pupilAprox.size.Width / 2)),
            ////    (int)(pupilAprox.center.Y - (pupilAprox.size.Height / 2)),
            ////    (int)pupilAprox.size.Width,
            ////    (int)pupilAprox.size.Height);

            ////if (!boundingBox.IsEmpty)
            ////{
            ////    var irisRadius = (imageEye.WhichEye == Eye.Left) ? EyeTracker.Settings.IrisRadiusPixLeft : EyeTracker.Settings.IrisRadiusPixRight;
            ////    var eyeMask = mask.Copy();
            ////    eyeMask.SetValue(new Gray(0), imageEye.Image.ThresholdBinaryInv(new Gray(thresholdDark), new Gray(255)).Convert<Gray, byte>());

            ////    var roiImageRef = new Rectangle();

            ////    EyeTrackerDiagnostics.TrackTime("StartCrossCorr");

            ////    if (this.imageRef == null || calibration.Calibrating)
            ////    {
            ////        if (calibration.Calibrating)
            ////        {
            ////            // Reference
            ////            this.imageRef = imageEye.Image.Convert<Gray, float>();

            ////            this.pupilCenterRef = pupilAprox.center;
            ////        }

            ////        if (this.imageRef == null)
            ////        {
            ////            this.imageRef = calibration.ImageEyeReference.Convert<Gray, float>();

            ////            this.pupilCenterRef = calibration.ReferenceData.Pupil.center;
            ////            ////roiImageRef = new Rectangle(
            ////            ////    (int)(calibration.ImageEyeReference.EyeData.Pupil.MCvBox2D.center.X - irisRadius),
            ////            ////    (int)(calibration.ImageEyeReference.EyeData.Pupil.MCvBox2D.center.Y - irisRadius * 1.5), 
            ////            ////    (int)(irisRadius * 2.0), 
            ////            ////    (int)(irisRadius * 3));
            ////        }

            ////        ////imageCur = PolarSobel(imageCur, pupil.MCvBox2D.center);
            ////        var i1 = this.imageRef.Sobel(0, 2, 7);

            ////        ////var i2 = imageCur.Sobel(0, 1, 7);
            ////        this.imageRef = i1 * 30;
            ////        EyeTrackerDiagnostics.TrackTime("SobelPositionRef");

            ////        this.imageRef = EyeTrackerMask.ApplyMaskNoise(this.imageRef, eyeMask).Convert<Gray,float>();
            ////    }
            ////    else
            ////    {
            ////        EyeTrackerDiagnostics.TrackTime("ApplyMaskRef");

            ////        var pupilCenter = pupilAprox.center;
            ////        if (Math.Abs(this.torsion - calibration.LastGoodEyeData.TorsionAngle) > 5)
            ////        {
            ////            this.torsion = 0.0;
            ////        }
            ////        else
            ////        {
            ////            this.torsion = calibration.LastGoodEyeData.TorsionAngle;
            ////            if (double.IsNaN(this.torsion))
            ////            {
            ////                this.torsion = 0.0;
            ////            }
            ////            ////pupilCenter.X = calibration.lastGoodEyeData.Pupil.MCvBox2D.center.X;
            ////            ////pupilCenter.Y = (pupilCenter.Y * 0.5f + calibration.lastGoodEyeData.Pupil.MCvBox2D.center.Y * 1.5f) / 2;
            ////        }

            ////        var roiRef = new Rectangle(
            ////            (int)(this.pupilCenterRef.X - irisRadius),
            ////            (int)(this.pupilCenterRef.Y - (irisRadius * 1.3)),
            ////            (int)(irisRadius * 2.0),
            ////            (int)(irisRadius * 2.6));
            ////        roiRef.Intersect(imageEye.Image.ROI);

            ////        var roiCur = new Rectangle(
            ////            (int)(pupilCenter.X - irisRadius + (int)(this.pupilCenterRef.X - irisRadius) - roiRef.X),
            ////            (int)(pupilCenter.Y - irisRadius),
            ////            (int)roiRef.Width,
            ////           (int)Math.Min(roiRef.Height, (int)(irisRadius * 2.0)));

            ////        roiCur.Intersect(imageEye.Image.ROI);

            ////        roiRef.X = (int)(roiRef.X + (int)(pupilCenter.X - irisRadius) - roiCur.X);
            ////        roiRef.Width = roiCur.Width;

            ////        var centerRotation = pupilCenter;
            ////        ////centerRotation.X -= roiCur.X;
            ////        ////centerRotation.Y -= roiCur.Y;

            ////        var imageCur = imageEye.Image.Convert<Gray, float>();

            ////        // Rotate
            ////        imageCur = imageCur.Rotate(this.torsion, centerRotation, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC, new Gray(128), true).Copy(roiCur);

            ////        // Enhance
            ////        ////imageCur = PolarSobel(imageCur, pupil.MCvBox2D.center);
            ////        var i1 = imageCur.Sobel(0, 2, 7);
            ////        ////var i2 = imageCur.Sobel(0, 1, 7);
            ////        imageCur = i1 * 30;

            ////        // Mask
            ////        imageCur = EyeTrackerMask.ApplyMaskNoise(imageCur, eyeMask.Copy(roiCur)).Convert<Gray,float>();

            ////        EyeTrackerDiagnostics.TrackTime("BeforeMatchTemplatePupil");
            ////        this.imageRef.ROI = roiRef;
            ////        Image<Gray, float> imageCorr = this.imageRef.MatchTemplate(imageCur, Emgu.CV.CvEnum.TM_TYPE.CV_TM_CCOEFF_NORMED);
            ////        this.imageRef.ROI = new Rectangle();
            ////        EyeTrackerDiagnostics.TrackTime("MatchTemplatePupil");

            ////        double[] minVals, maxVals;
            ////        Point[] minLocs, maxLocs;

            ////        imageCorr = imageCorr.Resize(imageCorr.Width, imageCorr.Height * 50, Emgu.CV.CvEnum.INTER.CV_INTER_LANCZOS4);
            ////        imageCorr.MinMax(out minVals, out maxVals, out minLocs, out maxLocs);

            ////        ////center.X = (float)(calibration.ReferencePosition.X - this.roiImageRef.X + roiIris.X - (roi.X + maxLocs[0].X));
            ////        var centerPupil = new PointF();
            ////        centerPupil.X = pupilAprox.center.X;
            ////        centerPupil.Y = (float)(calibration.ReferenceData.Pupil.center.Y - roiImageRef.Y + roiCur.Y - (maxLocs[0].Y / 50.0));
            ////        pupilAprox = new PupilData(centerPupil, pupilAprox.size, (float)90.0);

            ////        // Save stuff for debugging
            ////        this.lastImageEye = this.imageRef.Copy(roiRef).ConcateVertical(imageCur).Convert<Gray, byte>();
            ////        EyeTrackerDiagnostics.TrackTime("FindMax");
            ////    }
            ////}

            return pupilAprox;
        }
    }
}