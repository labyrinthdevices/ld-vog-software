﻿//-----------------------------------------------------------------------
// <copyright file="PositionTrackerEllipseFitting.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Class for eye position tracking using the convex hull surrounding the pupil and then fitting an ellipse to it.
    /// </summary>
    [Export(typeof(IPositionTracker)), ExportDescriptionAttribute("EllipseFitting")]
    internal class PositionTrackerEllipseFitting : IPositionTracker
    {
        /// <summary>
        /// Dynamic threshold. If dynamic trheshold is activated.
        /// </summary>
        private double dynamicThresdhold;

        /// <summary>
        /// Last threshold used.
        /// </summary>
        private int lastThreshold;

        MemStorage storage = new MemStorage();

        /// <summary>
        /// Calculates the position of the eye (vertical and horizontal).
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="mask">Mask of the eyelids, reflections, etc.</param>
        /// <param name="pupilAprox">Pupil ellipse, rough aproximation.</param>
        /// <param name="pupilReference">Pupil ellipse reference from calibration.</param>
        /// <returns>The pupil ellipse.</returns>
        public PupilData CalculatePosition(ImageEye imageEye, Image<Gray, byte> mask, PupilData pupilAprox, PupilData pupilReference)
        {
            // Set up options and parameters

            // Size of the image of the pupil that will be used to run the ellipse detection.
            // To avoid bigger pupils taking longer to process than small pupils
            // resize the image to a fixed size that works well. Since the ROI is not necessarily square
            // the new image will keep the proportions but the larger dimension will be fixed.
            var desiredMaxSize = 200;

            // Factor for the polynomial aproximation of the countour. 
            // The larger the number the smoother the counter will be.
            var aproxPolyFactor = 1;

            var DO_DYNAMIC_THRESHOLDING = false;
            var DO_IMAGE_CLOSING = false;
            var DO_CANNY = false;
            var DO_CURVATURE_FILTER = true;
            var DO_CONVEX_HULL = false;
            var DO_MASK = true;
            var DO_APROX_POLY = true;
            var DO_HEURISTIC_INTERPOLATION = false;

            // Add some padding to the pupil ROI just in case
            // make the ROI always square. This may help in some occasions.
            var squareSize = Math.Max(pupilAprox.Size.Width, pupilAprox.Size.Height);
            var roiPupil = new Rectangle(
                 (int)Math.Round(pupilAprox.Center.X - (squareSize / 2.0) - squareSize / 4),
                 (int)Math.Round(pupilAprox.Center.Y - (squareSize / 2.0) - squareSize / 4),
                 (int)Math.Round(squareSize + squareSize / 4 * 2),
                 (int)Math.Round(squareSize + squareSize / 4 * 2));
            roiPupil.Intersect(new Rectangle(new Point(0, 0), imageEye.Size));

            if (roiPupil.IsEmpty)
            {
                return new PupilData();
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Resizing --
            // To avoid bigger pupils taking longer to process than small pupils resize the image to a fixed size 
            // that works well. Since the ROI is not necessarily square the new image will keep the proportions but 
            // the larger dimension will be fixed.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            float scaleFactor = (float)desiredMaxSize / Math.Max(roiPupil.Width, roiPupil.Height);
            int resizeWidth = (int)Math.Round(scaleFactor * (float)roiPupil.Width);
            int resizeHeight = (int)Math.Round(scaleFactor * (float)roiPupil.Height);
            float scaleFactorX = resizeWidth / (float)roiPupil.Width;
            float scaleFactorY = resizeHeight / (float)roiPupil.Height;

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Thresholding --
            // Get the binary image with the dark pixels
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            Image<Gray, byte> imgPupilBinary = null;

            int blurSize = (int)Math.Round(4 * scaleFactor);
            var imgTemp = imageEye.GetCroppedImage(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).SmoothBlur(blurSize, blurSize);
            var threshold = (imageEye.WhichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;
            
            // USE CANNY TO FIND THRESHOLD?
            if (DO_CANNY)
            {
                int blurSize2 = (int)(5 * scaleFactor);
                //// It could be a good idea to use canny somehow. This will reduce dependency on the threshold
                // Mask pixels in the eyelids and reflections
                // imgPupilBinary = imgPupilBinary.Mul(mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN));
                var imgPupilBinary2 = imageEye.GetCroppedImage(roiPupil).Resize(50,50, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC).Canny(40, 60);
                var imgPupilBinary3 = imageEye.GetCroppedImage(roiPupil).Resize(50, 50, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                var maskcanny = mask.Copy(roiPupil).Resize(imgPupilBinary2.Width, imgPupilBinary2.Height, Emgu.CV.CvEnum.INTER.CV_INTER_NN);

                imgPupilBinary3.SetValue(0, imgPupilBinary2.Not() / 255);
                imgPupilBinary3.SetValue(0, maskcanny.Not());

                DenseHistogram hist = new DenseHistogram(50, new RangeF(2f, 101.0f));
                // Histogram Computing
                hist.Calculate<Byte>(new Image<Gray, byte>[] { imgPupilBinary3 }, true, null);

                float minValue;
                float maxValue;
                int[] minLocations;
                int[] maxLocations;
                hist.MinMax(out minValue, out maxValue, out minLocations, out maxLocations);

                // imgPupilBinary2 = imgPupilBinary2.Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
                // imgPupilBinary2 = imgPupilBinary2.Mul(mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN));

                EyeTracker.Diagnostics.DebugImageManager.AddImage("positionCanny", imageEye.WhichEye, imgPupilBinary3);
                // For canny to work I need to connect the contours somehow
                threshold = (int)((maxLocations[0]*2 + 2 + threshold )/2.0);
            }

            if (DO_DYNAMIC_THRESHOLDING)
            {
                // dynamic thresholding
                threshold = this.UpdateDynamicThreshold(imgTemp, imageEye.WhichEye);
            }
            imgPupilBinary = imgTemp.ThresholdBinaryInv(new Gray(threshold), new Gray(255));

            // TODO: Rethink this
            //imgPupilBinary = imgPupilBinary.Mul(mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN));

            if (DO_IMAGE_CLOSING)
            {
                // Image closing to remove little dots over threshold
                int size = 2 * (int)Math.Round(2 * (desiredMaxSize/50.0)) + 1;
                int center = (int)Math.Floor(size / 2.0);
                StructuringElementEx kernel = new StructuringElementEx(size, size, center, center, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
                CvInvoke.cvErode(imgPupilBinary, imgPupilBinary, kernel, 1);
                CvInvoke.cvDilate(imgPupilBinary, imgPupilBinary, kernel, 1);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Contour analysis --
            // Find the contour with the largest area and select the points that are more likely to be part of the 
            // pupil and not artifacts like eyelashes or reflections.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            Seq<Point> pupilCountour = null;
            double maxArea = 0;
            Contour<Point> contours = imgPupilBinary.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_NONE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL);
            for (var contour = contours; contour != null; contour = contour.HNext)
            {
                if (contour.Perimeter < maxArea)
                {
                    continue;
                }

                maxArea = contour.Perimeter;
                pupilCountour = contour;
            }

            // If there are too few points do not try to fit the ellipse, just exit
            if (pupilCountour == null || pupilCountour.Total < 10)
            {
                return new PupilData();
            }

            if (DO_APROX_POLY)
            {
                // Simplify the contour
                pupilCountour = pupilCountour.ApproxPoly(aproxPolyFactor);
            }

            if (EyeTracker.Settings.Debug)
            {
                var imgPupilBinary1 = imgPupilBinary.Copy();
                // Save stuff for debugging
                if (pupilCountour.Total > 2)
                {
                    foreach (var point in pupilCountour)
                    {
                        imgPupilBinary1.Draw(new CircleF(new PointF(point.X, point.Y), 1f), new Gray(100), 2);
                    }
                }
                EyeTracker.Diagnostics.DebugImageManager.AddImage("position1", imageEye.WhichEye, imgPupilBinary1);
            }

            // Curvature analysis
            if (DO_CURVATURE_FILTER)
            {
                // Get an approxiamte center of the ellipse
                var gravCenter = pupilCountour.GetMoments().GravityCenter;
                var center = new PointF((float)gravCenter.x, (float)gravCenter.y);

                Image<Bgr, byte> imgPupilBinary1 = null;
                if (EyeTracker.Settings.Debug)
                {
                    imgPupilBinary1 = imgPupilBinary.Convert<Bgr, byte>();

                    // Save stuff for debugging
                    if (pupilCountour.Total > 2)
                    {
                        foreach (var point in pupilCountour)
                        {
                            imgPupilBinary1.Draw(new CircleF(new PointF(point.X, point.Y), 1f), new Bgr(Color.Green), 2);
                        }
                    }
                    imgPupilBinary1.Draw(new Cross2DF(center, 10, 10), new Bgr(Color.Red), 2);
                }


                var curvature = new double[pupilCountour.Total];
                var curvature2 = new double[pupilCountour.Total];
                var distanceToCenter = new double[pupilCountour.Total];
                var centerAngle = new double[pupilCountour.Total];
                var goodPoint = new bool[pupilCountour.Total];


                for (int i = 0; i < pupilCountour.Total; i++)
                {
                    // Circular index
                    var inext = i + 1;
                    if (inext >= pupilCountour.Total)
                    {
                        inext = 0;
                    }

                    // Get two points of the countour
                    var p1 = pupilCountour[i];
                    var p2 = pupilCountour[inext];

                    // Define the triangle formed by those two points and the center
                    var a = Math.Sqrt(Math.Pow((p1.Y - p2.Y), 2) + Math.Pow((p1.X - p2.X), 2));
                    var b = Math.Sqrt(Math.Pow((p1.Y - center.Y), 2) + Math.Pow((p1.X - center.X), 2));
                    var c = Math.Sqrt(Math.Pow((p2.Y - center.Y), 2) + Math.Pow((p2.X - center.X), 2));

                    // The two curvatures are the angles of the triangle not including the one at the center
                    curvature[i] = Math.Acos((Math.Pow(a, 2) + Math.Pow(b, 2) - Math.Pow(c, 2)) / (2 * a * b)) * 180 / Math.PI;
                    curvature2[inext] = Math.Acos((Math.Pow(a, 2) + Math.Pow(c, 2) - Math.Pow(b, 2)) / (2 * a * c)) * 180 / Math.PI;
                    centerAngle[i] = Math.Acos((Math.Pow(b, 2) + Math.Pow(c, 2) - Math.Pow(a, 2)) / (2 * b * c)) * 180 / Math.PI;

                    distanceToCenter[i] = b;
                }

                var radiousEquivCircle = Math.Sqrt(pupilCountour.Area/Math.PI);

                var nPoints = pupilCountour.Total;
                for (int i = 0; i < pupilCountour.Total; i++)
                {
                    // Make it circular
                    var inext = i + 1;
                    if (inext >= pupilCountour.Total)
                    {
                        inext = 0;
                    }
                    var iprev = i - 1;
                    if (iprev < 0)
                    {
                        inext = pupilCountour.Total-1;
                    }

                    goodPoint[i] = true;

                    if (curvature[i] < 50 || curvature[i] > 140 || curvature2[i] < 50 || curvature2[i] > 140)
                    {
                        goodPoint[i] = false;
                        continue;
                    }
                    
                    if ( Math.Abs(curvature[i] - curvature2[i]) / Math.Abs(curvature[i] + curvature2[i]) > 0.2)
                    {
                        goodPoint[i] = false;
                        continue;
                    }
                    if  ( Math.Abs(distanceToCenter[i] - radiousEquivCircle) > radiousEquivCircle*0.5  )
                    {
                        goodPoint[i] = false;
                        continue;
                    }
                }

                for (int i = 0; i < nPoints; i++)
                {
                    var firstNextBad = i;
                    var firstPreviousBad = i;
                    if (goodPoint[i])
                    {
                        for (int j = i + 1; j != i; j++)
                        {
                            if (j >= nPoints)
                            {
                                j = -1;
                                continue;
                            };

                            if (!goodPoint[j])
                            {
                                firstNextBad = j;
                                break;
                            }

                        }
                        for (int j = i - 1; j != i; j--)
                        {
                            if (j < 0)
                            {
                                j = nPoints - 1;
                            };

                            if (!goodPoint[j])
                            {
                                firstPreviousBad = j;
                                break;
                            }
                        }

                        if (firstPreviousBad > firstNextBad)
                        {
                            firstNextBad = firstNextBad + nPoints;
                        }

                        // Remove all groups of points that have a neighborhood of less than 4 good points.
                        if (firstNextBad - firstPreviousBad < 4)
                        {
                            goodPoint[i] = false;
                        }
                    }
                }

                for (int i = nPoints - 1; i >= 0; i--)
                {
                    if (!goodPoint[i])
                    {
                        pupilCountour.RemoveAt(i);
                        continue;
                    }
                }

                // If there are too few points do not try to fit the ellipse, just exit
                if (pupilCountour.Total < 8)
                {
                    return new PupilData();
                }


                if (EyeTracker.Settings.Debug)
                {
                    // Save stuff for debugging
                    if (pupilCountour.Total > 4)
                    {
                        foreach (var point in pupilCountour)
                        {
                            imgPupilBinary1.Draw(new CircleF(new PointF(point.X, point.Y), 1f), new Bgr(Color.Orange), 2);
                        }
                    }
                    EyeTracker.Diagnostics.DebugImageManager.AddImage("position2", imageEye.WhichEye, imgPupilBinary1);
                }
            }

            if (DO_CONVEX_HULL)
            {
                // Get the convex hull to remove the reflection and simplify the contour again
                pupilCountour = pupilCountour.GetConvexHull(Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);
            }

            // Remove points that are too close to the mask because they are probably not edges of the pupil
            var maskROIpupil = mask.Copy(roiPupil).Resize(resizeWidth, resizeHeight, Emgu.CV.CvEnum.INTER.CV_INTER_NN);
            int maskErodeSize = resizeWidth / 10;
            int maskErodeCenter = (int)Math.Floor(maskErodeSize / 2.0);
            StructuringElementEx maskErodeKernel = new StructuringElementEx(maskErodeSize, maskErodeSize, maskErodeCenter, maskErodeCenter, Emgu.CV.CvEnum.CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE);
            CvInvoke.cvErode(maskROIpupil, maskROIpupil, maskErodeKernel, 1);

            if (DO_MASK)
            {
                // Go trhough the points in the countour in reverese order to avoid problems when deleting
                for (int i = pupilCountour.Total - 1; i >= 0; i--)
                {
                    if (maskROIpupil.Data[pupilCountour[i].Y, pupilCountour[i].X, 0] == 0)
                    {
                        pupilCountour.RemoveAt(i);
                    }
                }
            }

            // If there are too few points do not try to fit the ellipse, just exit
            if (pupilCountour.Total < 8)
            {
                return new PupilData();
            }

            if (DO_CONVEX_HULL)
            {
                // Get the convex hull to remove the reflection and simplify the contour again
                pupilCountour = pupilCountour.GetConvexHull(Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);
            }

            if (pupilCountour.BoundingRectangle.Height < pupilCountour.BoundingRectangle.Width / 2)
            {
                return new PupilData();
            }


            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Heuristic interpolation -- 
            // Fill in points to interpolate the top part of the pupil.
            // The top part of the pupil is usually cover by the eyelid. In that case it helps to 
            // add a few points so the ellipse fitting is more accurate and less noise. One good
            // guess of the position of the top of the pupil is to keep a memory of the aspect ratio 
            // of the pupil and then taking into account the current position of the bottom and the
            // current width of the pupil calculate the corresponding top.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            bool contourComplete = true;

            if (DO_HEURISTIC_INTERPOLATION)
            {
                // TODO: If the current position is too far from the reference position don't do this
                // unless there is a geometric correction

                var boundingBox = GetCountourBoundingBox(pupilCountour);

                // Check if the counter is complete
                for (int i = 0; i < pupilCountour.Total; i++)
                {
                    // Middle of the bounding box
                    var middleX = pupilCountour.BoundingRectangle.X + boundingBox.Width / 2.0;

                    // If there is a gap and the gap includes the middle of the box
                    var gapIncludesMiddle = ((pupilCountour[i].X - middleX) * (pupilCountour[i + 1].X - middleX) < 0);
                    var gapTooLarge = Math.Abs(pupilCountour[i].X - pupilCountour[i + 1].X) > desiredMaxSize / 5;
                    if (gapTooLarge && gapIncludesMiddle)
                    {
                        contourComplete = false;
                        break;
                    }
                }

                if (!contourComplete && !pupilReference.IsEmpty)
                {
                    // Add the extra points to interpolate the top part of the countour
                    var currentWidth = boundingBox.Width;
                    var currentHeight = boundingBox.Height;

                    var box = new MCvBox2D(pupilReference.Center, pupilReference.Size, pupilReference.Angle + 90);
                    var r = GetEllipseBoundingBox(box);

                    var pupilSizeRatio = r.Width / r.Height;


                    var pupilHeight = currentWidth / pupilSizeRatio;

                    pupilCountour.Insert(0, new Point(
                        (int)(boundingBox.X + currentWidth / 2.0),
                        (int)(boundingBox.Y + currentHeight - pupilHeight)));
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            // -- Ellipse fitting --
            // Fit an ellipse to the points of the contour.
            ////////////////////////////////////////////////////////////////////////////////////////////////////////

            // If there are too few points do not try to fit the ellipse, just exit
            if (pupilCountour.Total < 8)
            {
                return new PupilData();
            }

            var ellipse = CvInvoke.cvFitEllipse2(pupilCountour);

            // Update the aspect ratio
            var rect = ellipse.MinAreaRect();

            // Change the ellipse information to the coordinates of the original image
            var pupil = new PupilData(
                new PointF((ellipse.center.X / scaleFactorX) + (float)roiPupil.X, (ellipse.center.Y / scaleFactorY) + (float)roiPupil.Y),
                new SizeF(ellipse.size.Width / scaleFactorX, ellipse.size.Height / scaleFactorY),
                ellipse.angle - 90);


            if (EyeTracker.Settings.Debug)
            {
                var imageDebug = imgPupilBinary.Convert<Bgr, byte>();
                imageDebug.SetValue(new Bgr(Color.Yellow), maskROIpupil.Not());
                imageDebug.Draw(new Ellipse(ellipse.center, ellipse.size, ellipse.angle - 90), new Bgr(Color.Red), 1);
                if (pupilCountour.Total > 2)
                {
                    foreach (var point in pupilCountour)
                    {
                        imageDebug.Draw(new CircleF(new PointF(point.X, point.Y), 1f), new Bgr(Color.Pink), 2);
                    }
                }
                EyeTracker.Diagnostics.DebugImageManager.AddImage("position", imageEye.WhichEye, imageDebug);
            }

            return pupil;
        }

        /// <summary>
        /// Gets the smallest rectangle that fits the ellipse within.
        /// </summary>
        /// <param name="ellipse">Ellipse to be fit.</param>
        /// <returns>Rectangle around the pupil.</returns>
        private static RectangleF GetEllipseBoundingBox(MCvBox2D ellipse)
        {
            var A = ellipse.size.Width;
            var B = ellipse.size.Height;

            var angle = (double)ellipse.angle / 180 * Math.PI;
            if (angle > Math.PI)
            {
                angle = angle - Math.PI;
            }
            if (angle > Math.PI / 2)
            {
                angle = Math.PI - angle;
            }
            if (angle < 0)
            {
                angle = angle + Math.PI;
            }
            var t = 2.0 * Math.Atan((1.0 / Math.Sin(angle)) * (Math.Sqrt(Math.Pow(A * Math.Cos(angle), 2) + Math.Pow(B * Math.Sign(angle), 2)) + A * Math.Cos(angle)) / B);
            var w = A * Math.Cos(t) * Math.Cos(angle) - B * Math.Sin(t) * Math.Sin(angle);

            angle = angle + Math.PI / 2;
            if (angle > Math.PI)
            {
                angle = angle - Math.PI;
            }
            if (angle > Math.PI / 2)
            {
                angle = Math.PI - angle;
            }
            t = 2.0 * Math.Atan((1.0 / Math.Sin(angle)) * (Math.Sqrt(Math.Pow(A * Math.Cos(angle), 2) + Math.Pow(B * Math.Sign(angle), 2)) + A * Math.Cos(angle)) / B);
            var h = A * Math.Cos(t) * Math.Cos(angle) - B * Math.Sin(t) * Math.Sin(angle);

            //var w = A * (1 + Math.Sin(2 * ellipse.angle / 180 * Math.PI) / 2) / 2 + B * (1 + Math.Sin(2 * ellipse.angle / 180 * Math.PI + Math.PI) / 2) / 2;
            //var h = A * (1 + Math.Sin(2 * ellipse.angle / 180 * Math.PI + Math.PI) / 2) / 2 + B * (1 + Math.Sin(2 * ellipse.angle / 180 * Math.PI) / 2) / 2;
            var sizeRect = new SizeF((float)w, (float)h);
            var rect2 = new RectangleF(
                (float)Math.Round(ellipse.center.X - sizeRect.Width / 2f),
                (float)Math.Round(ellipse.center.Y - sizeRect.Height / 2f),
                (float)Math.Round(sizeRect.Width),
                (float)Math.Round(sizeRect.Height));

            return rect2;
        }

        /// <summary>
        /// Finds the bounding box of the countour. It should not be necessary but I don't understand how
        /// countours deal with removing points. I suspect that the bounding box does not get recalculated
        /// automatically
        /// </summary>
        /// <param name="countour">Contour.</param>
        /// <returns></returns>
        private static Rectangle GetCountourBoundingBox(Seq<Point> countour)
        {
            var minx = 500;
            var maxx = 0;
            var miny = 500;
            var maxy = 0;

            // Go trhough the points in the countour in reverese order to avoid problems when deleting
            for (int i = countour.Total - 1; i >= 0; i--)
            {
                if (countour[i].X > maxx)
                {
                    maxx = countour[i].X;
                }
                if (countour[i].X < minx)
                {
                    minx = countour[i].X;
                }
                if (countour[i].Y > maxy)
                {
                    maxy = countour[i].Y;
                }
                if (countour[i].Y < miny)
                {
                    miny = countour[i].Y;
                }
            }
            return new Rectangle(minx, miny, maxx - minx, maxy - miny);
        }

        /// <summary>
        /// Inspired from: https://www.assembla.com/code/EyeGaze/subversion/nodes/90/EyeGaze/FeatureDetection/PupilHistogram.cs
        /// </summary>
        /// <param name="image"></param>
        /// <param name="whichEye"></param>
        /// <returns></returns>
        private int UpdateDynamicThreshold(Image<Gray, byte> image, Eye whichEye)
        {
            DenseHistogram hist = new DenseHistogram(256, new RangeF(0.0f, 255.0f));
            // Histogram Computing
            hist.Calculate<Byte>(new Image<Gray, byte>[] { image }, true, null);

            // Array to hold the intensity count
            float[] histogram = new float[256];

            hist.MatND.ManagedArray.CopyTo(histogram, 0);

            //EyeTrackerDiagnostics.TrackTime("hist11");
            //for (int i = 1; i < image.Size.Height; i++)
            //{
            //    for (int j = 1; j < image.Size.Width; j++)
            //        histogram[(int)image[i, j].Intensity]++;
            //} EyeTrackerDiagnostics.TrackTime("hist12");

            // Find maximums
            List<int> maxs = new List<int>();
            for (int i = 20; i < histogram.Length; i++)
            {
                bool max = true;
                for (int j = 1; (i - j) > 0 && (i + j) < histogram.Length && j < 20; j++)
                {
                    if (histogram[i] <= histogram[i - j] || histogram[i] <= histogram[i + j])
                    {
                        max = false;
                        break;
                    }
                }
                if (max)
                    maxs.Add(i);
            }

            if (maxs.Count < 2)
            {
                return (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;
            }

            // Place threshold between the two first maximums
            int threshold = (maxs[1] - maxs[0]) / 2 + maxs[0];


            var currentThresdhold = (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;
            if (this.lastThreshold != currentThresdhold)
            {
                this.dynamicThresdhold = currentThresdhold;
            }

            if (this.dynamicThresdhold == 0)
            {
                if (whichEye == Eye.Left)
                {
                    this.dynamicThresdhold = EyeTracker.Settings.Tracking.DarkThresholdLeftEye;
                }
                else
                {
                    this.dynamicThresdhold = EyeTracker.Settings.Tracking.DarkThresholdRightEye;
                }
            }
            else
            {
                if (Math.Abs(dynamicThresdhold - threshold) < 20)
                {
                    this.dynamicThresdhold = this.dynamicThresdhold * 0.8 + threshold * 0.2;
                }

                if (whichEye == Eye.Left)
                {
                    if (Math.Abs(this.dynamicThresdhold - EyeTracker.Settings.Tracking.DarkThresholdLeftEye) > 5)
                    {
                        EyeTracker.Settings.Tracking.DarkThresholdLeftEye = (int)Math.Round(this.dynamicThresdhold);
                    }
                }
                else
                {
                    if (Math.Abs(this.dynamicThresdhold - EyeTracker.Settings.Tracking.DarkThresholdRightEye) > 5)
                    {
                        EyeTracker.Settings.Tracking.DarkThresholdRightEye = (int)Math.Round(this.dynamicThresdhold);
                    }
                }
            }

            this.lastThreshold = (whichEye == Eye.Left) ? EyeTracker.Settings.Tracking.DarkThresholdLeftEye : EyeTracker.Settings.Tracking.DarkThresholdRightEye;

            return (int)Math.Round(dynamicThresdhold);
        }
    }
}
