﻿//-----------------------------------------------------------------------
// <copyright file="EyeLidTrackerFixed.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using System.ComponentModel.Composition;

    /// <summary>
    /// Finds the eyelids without using any algorithm. It just uses fixed settings. Eyelid may be relative to pupil position.
    /// </summary>
    [Export(typeof(IEyelidTracker)), ExportDescriptionAttribute("Fixed")]
    internal class EyelidTrackerFixed : IEyelidTracker
    {
        public EyelidData UpdateFilteredEyelids(EyelidData eyeLids, EyePhysicalModel eyeGlobe)
        {
            return eyeLids;
        }

        /// <summary>
        /// Finds the eyelids.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="iris">Iris position and radius.</param>
        /// <returns>Eyelids as an array of 8 points. First 4 points are the top eyelid from left to right,
        /// the next 4 are the bottom eyelid from left to right.</returns>
        public EyelidData FindEyelids(ImageEye imageEye, IrisData iris, EyePhysicalModel eyeGlobe)
        {
            var ytop = (int)(iris.Center.Y - iris.Radius + (iris.Radius * EyeTracker.Settings.Tracking.TopEyelid));
            var ybottom = (int)(iris.Center.Y + iris.Radius - (iris.Radius * EyeTracker.Settings.Tracking.BottomEyelid));

            var eyelidData = new EyelidData();

            eyelidData.Upper = new PointF[] 
            {
                // left top to right top
                new PointF(iris.Center.X - (iris.Radius * 1.5f), ytop), 
                new PointF(iris.Center.X - (iris.Radius * 1f), ytop),  
                new PointF(iris.Center.X + (iris.Radius * 1f), ytop),   
                new PointF(iris.Center.X + (iris.Radius * 1.5f), ytop), 
            };

            eyelidData.Lower = new PointF[] 
            {
                // left bottom to right bottom
                new PointF(iris.Center.X - (iris.Radius * 1.5f), ybottom), 
                new PointF(iris.Center.X - (iris.Radius * 1f), ybottom), 
                new PointF(iris.Center.X + (iris.Radius * 1f), ybottom), 
                new PointF(iris.Center.X + (iris.Radius * 1.5f), ybottom), 
            };

            return eyelidData;
        }
    }
}
