﻿//-----------------------------------------------------------------------
// <copyright file="NamespaceDoc.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    /// <summary>
    /// The <see cref="OculomotorLab.VOG.ImageProcessing"/> namespace contains classes for image processes and analysis: pupil tracking,
    /// eyelid masking, torsion tracking, etc.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    public class NamespaceDoc
    {
    }
}