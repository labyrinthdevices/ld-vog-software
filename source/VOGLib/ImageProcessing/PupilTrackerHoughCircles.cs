﻿// <copyright file="PupilTrackerHoughCircles.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.ImageProcessing
{
    using System;
    using System.ComponentModel.Composition;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Find the pupil using the Hough transform for circles.
    /// </summary>
    [Export(typeof(IPupilTracker)), ExportDescriptionAttribute("HoughCircles")]
    internal class PupilTrackerHoughCircles : IPupilTracker
    {
        /// <summary>
        /// Finds the pupil.
        /// </summary>
        /// <param name="imageEye">Image of the eye.</param>
        /// <param name="eyeRoi">Roi containing the pupil.</param>
        /// <returns>Pupil ellipse.</returns>
        public PupilData FindPupil(ImageEye imageEye, Rectangle eyeRoi)
        {
            if (imageEye == null)
            {
                throw new ArgumentNullException("imageEye");
            }

            var minPupRad = EyeTracker.Settings.Tracking.MinPupRadPix;
            var maxPupRad = (imageEye.WhichEye == Eye.Left) ? EyeTracker.Settings.Tracking.IrisRadiusPixLeft : EyeTracker.Settings.Tracking.IrisRadiusPixRight;
            
            var thresholdDark = (imageEye.WhichEye == Eye.Left) ?
                EyeTracker.Settings.Tracking.DarkThresholdLeftEye :
                EyeTracker.Settings.Tracking.DarkThresholdRightEye;

            var imageThreshold = imageEye.ThresholdDarkResized(thresholdDark, 1/(float)2.0, eyeRoi);
            
            //var pupil = PupilTrackerBlob.FindPupilBlob(imageThreshold, eyeRoi, 1 / (float)2.0, maxPupRad);

            //eyeRoi = new Rectangle(
            //    (int)pupil.Center.X - 10,
            //    (int)pupil.Center.Y - 10,
            //    (int)pupil.Size.Width + 20,
            //    (int)pupil.Size.Height + 20);
            //eyeRoi.Intersect(imageEye.Image.ROI);

            //var center1 = pupil.Center;
            //center1.X -= eyeRoi.X;
            //center1.Y -= eyeRoi.Y;
            ////var imgBinarTemp = PupilTrackerCrossCorrelation.PolarSobelTangent(imageEye.Image.Copy(roiPupil).Convert<Gray, float>(), center1).Convert<Gray, byte>();

            Image<Gray, byte> imgBinarTemp = imageEye.GetCroppedImage(eyeRoi).PyrDown().PyrUp();
            CircleF[] pupilHoughCircles = imgBinarTemp.HoughCircles(new Gray(100), new Gray(20), 1, 50, (int)minPupRad, (int)maxPupRad)[0];

            ////CircleF[] pupilHoughCircles = m.HoughCircles(new Gray(40), new Gray(10), 2, 50, (int)minPupRad, (int)maxPupRad)[0];

            PupilData pupil = new PupilData();
            if (pupilHoughCircles.Length > 0)
            {
                PointF center = pupilHoughCircles[0].Center;
                center.X += eyeRoi.X;
                center.Y += eyeRoi.Y;

                pupil = new PupilData(center, new SizeF(pupilHoughCircles[0].Radius * 2, pupilHoughCircles[0].Radius * 2), 0.0f);
            }

            // Save stuff for debugging
            for (int i = 0; i < pupilHoughCircles.Length; i++)
            {
                imgBinarTemp.Draw(pupilHoughCircles[i], new Gray(200), 2);
            }

            EyeTracker.Diagnostics.DebugImageManager.AddImage("pupil", imageEye.WhichEye, imgBinarTemp);

            return pupil;
        }
    }
}
