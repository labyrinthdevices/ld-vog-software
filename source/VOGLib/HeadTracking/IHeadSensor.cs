﻿//-----------------------------------------------------------------------
// <copyright file="IHeadSensor.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.HeadTracking
{
    using System;

    /// <summary>
    /// Interface for head tracking sensors
    /// </summary>
    public interface IHeadSensor : IDisposable
    {
        /// <summary>
        /// Starts tracking.
        /// </summary>
        void StartTracking();

        /// <summary>
        /// Stops tracking.
        /// </summary>
        void StopTracking();

        /// <summary>
        /// Grabs the current head tracking data from the sensor.
        /// </summary>
        /// <returns>The head tracking data structure.</returns>
        HeadData GrabHeadData(bool SoftwareTriggerOn);
    }
}
