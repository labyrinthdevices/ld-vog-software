﻿//-----------------------------------------------------------------------
// <copyright file="IHeadSensorProvider.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.HeadTracking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for objects that can provide a head tracking sensor.
    /// </summary>
    public interface IHeadSensorProvider
    {
        /// <summary>
        /// Gets the head tracking sensor.
        /// </summary>
        /// <returns>The head tracking sensor.</returns>
        IHeadSensor GetHeadSensor();
    }
}
