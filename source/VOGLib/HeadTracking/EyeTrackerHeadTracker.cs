﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerHeadTracker.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading;
    using OculomotorLab.VOG.HeadTracking;

    internal class EyeTrackerHeadTracker : IDisposable
    {
        /// <summary>
        /// Possible states of the processor.
        /// </summary>
        public enum State
        {
            /// <summary>
            /// Initializing before starting the processing loop.
            /// </summary>
            Initializing,

            /// <summary>
            /// Processing images.
            /// </summary>
            Tracking,

            /// <summary>
            /// Signal to stop received waiting to empty the buffer.
            /// </summary>
            Stopping
        }

        /// <summary>
        /// Current state of the head tracker.
        /// </summary>
        private State state;

        /// <summary>
        /// Thread that runs the 
        /// </summary>
        private Thread trackingThread;

        /// <summary>
        /// Head sensor.
        /// </summary>
        private IHeadSensor sensor;

        /// <summary>
        /// Buffer for data grabbed by the sensor.
        /// </summary>
        private ConcurrentQueue<HeadData> headDataBuffer;

        /// <summary>
        /// Sets / Gets software trigger
        /// </summary>
        public bool SoftwareTrigger { get; set; }

        /// <summary>
        /// Initializes a new instance of the EyeTrackerHeadTracker class.
        /// </summary>
        /// <param name="headSensorProvider">Object that will provide the head sensor.</param>
        internal EyeTrackerHeadTracker(IHeadSensorProvider headSensorProvider)
        {
            this.state = State.Initializing;
            this.sensor = headSensorProvider.GetHeadSensor();
            this.headDataBuffer = new ConcurrentQueue<HeadData>();
        }

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        public event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        /// <summary>
        /// Starts tracking the head movements.
        /// </summary>
        internal void StartTracking()
        {
            this.trackingThread = new Thread(new ThreadStart(this.TrackingLoop));
            this.trackingThread.Name = "EyeTracker:TrackingLoop";
            this.trackingThread.Priority = ThreadPriority.Highest;
            this.trackingThread.Start();

            if (this.sensor != null)
            {
                this.sensor.StartTracking();
            }
        }

        /// <summary>
        /// Stops tracking the head movements.
        /// </summary>
        internal void StopTracking()
        {
            // Mark the buffer as completed.
            this.state = State.Stopping;
            
            if (this.trackingThread != null)
            {
                // Wait for the thread to finish.
                this.trackingThread.Join();
            }

            if ( this.sensor!= null)
            {
                this.sensor.StopTracking();
                this.sensor.Dispose();
                this.sensor = null;
            }
        }

        /// <summary>
        /// Get the head data corresponding to a given time stamp.
        /// </summary>
        /// <param name="procesedImages">Images that the head data must correspond with.</param>
        /// <returns>Head data.</returns>
        public HeadData GetHeadData(EyeCollection<ProcessedImageEye> procesedImages)
        {
            while (this.headDataBuffer.Count == 0)
            {
                // System.Threading.Thread.Sleep(1);
                HeadData dat = new HeadData();
                dat.DataResult = HeadData.Result.Missed;
                return dat;
            }

            // Get one current timestamp
            var timeStamp = new ImageEyeTimestamp();
            foreach (var image in procesedImages)
            {
                if (image != null)
                {
                    timeStamp = image.EyeData.Timestamp;
                    break;
                }
            }

            HeadData data = new HeadData();
            data.DataResult = HeadData.Result.Missed;
            HeadData nextdata;
            if (this.state != State.Tracking)
            {
                return data;
            }
            bool success;
            bool found = false;
            while (this.headDataBuffer.Count > 0)
            {
                success = this.headDataBuffer.TryPeek(out nextdata);
                if (!success)
                {
                    data = new HeadData();
                    data.DataResult = HeadData.Result.Missed;
                    found = true;
                    break;
                }

                // If the next element is the proper one for this frame
                if (nextdata.TimeStamp.FrameNumber == timeStamp.FrameNumber)
                {
                    success = this.headDataBuffer.TryDequeue(out data);
                    if (!success)
                    {
                        data = new HeadData();
                        data.DataResult = HeadData.Result.Missed;
                    }
                    found = true;
                    break;
                }
                else if (nextdata.TimeStamp.FrameNumber > timeStamp.FrameNumber)
                {
                    data = new HeadData();
                    data.DataResult = HeadData.Result.Missed;
                    found = true;
                    break;
                }
                else
                {
                    this.headDataBuffer.TryDequeue(out data); // otherwise we need to dequeue and throw the result away
                }
            }
            if(!found)
            {
                data = new HeadData();
                data.DataResult = HeadData.Result.Missed;
            }
            return data;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.StopTracking();
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void TrackingLoop()
        {
            this.state = State.Tracking;

            // Keep processing images until the buffer is marked as complete and empty
            while (this.state == State.Tracking)
            {
                try
                {
                    if ( sensor == null)
                    {
                        return;
                    }
                    
                    var headData = this.sensor.GrabHeadData(SoftwareTrigger);

                    if (headData.DataResult != HeadData.Result.Missed && headData.DataResult != HeadData.Result.Empty)
                    {
                        this.headDataBuffer.Enqueue(headData);
                        //if (headData.MagnetometerX == 0)
                        //    Thread.Sleep(1);
                        // Thread.Sleep((int)(1000 / EyeTracker.Settings.FrameRate));
                    }
                    else
                    {
                        //Thread.Sleep(1);
                    }
                    //Thread.Sleep(1);
                    
                }
                catch (Exception ex)
                {
                    this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
                }
            }
            System.Diagnostics.Trace.WriteLine("Finished head tracker loop.");
        }


    }
}
