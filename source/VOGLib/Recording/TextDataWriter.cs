﻿//-----------------------------------------------------------------------
// <copyright file="TextDataWriter.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Concurrent;
    using System.ComponentModel;
    using System.Threading;

    /// <summary>
    /// Class that saves the eye movement data into a file.
    /// </summary>
    public class TextDataWriter : IDisposable
    {
        /// <summary>
        /// Buffer for data.
        /// </summary>
        private BlockingCollection<EyeTrackerData> dataBuffer;

        /// <summary>
        /// Thread that runs the loop writing the data to the file.
        /// </summary>
        private Thread writingThread;

        /// <summary>
        /// Initializes a new instance of the EyeTrackerDataWriter class.
        /// </summary>
        /// <param name="fileName">File name where to save the data.</param>
        public TextDataWriter(string fileName)
        {
            this.FileName = fileName;

            this.dataBuffer = new BlockingCollection<EyeTrackerData>(new ConcurrentQueue<EyeTrackerData>(), 20);
        }

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        public event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        /// <summary>
        /// Gets the name of the file (including complete path).
        /// </summary>
        public string FileName { get; private set; }

        /// <summary>
        /// Gets the number of frames that could not be saved because the buffer was full.
        /// </summary>
        public int NumberFramesDropped { get; private set; }

        /// <summary>    
        /// Gets the number of frames that could be saved.
        /// </summary>
        public int NumberFramesSaved { get; private set; }

        /// <summary>
        /// Gets the number of frames waiting in the buffer.
        /// </summary>
        internal int NumberFramesInBuffer
        {
            get
            {
                if (this.dataBuffer != null)
                {
                    return this.dataBuffer.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Starts the recording thread.
        /// </summary>
        public void StartRecording()
        {
            this.writingThread = new Thread(new ThreadStart(this.WritingLoop));
            this.writingThread.Name = "EyeTracker:SaveDataLoop";
            this.writingThread.Start();
        }

        /// <summary>
        /// Stops the recording thread.
        /// </summary>
        public void StopRecording()
        {
            if ( this.dataBuffer != null)
            {
                this.dataBuffer.CompleteAdding();
            }

            if (this.writingThread != null)
            {
                // Wait for the thread to finish.
                // this will make sure all data are saved before disposing objects.
                this.writingThread.Join();
            }

            if ( this.dataBuffer != null)
            {
                this.dataBuffer.Dispose();
                this.dataBuffer = null;
            }
        }

        /// <summary>
        /// Tries to add more data to the file.
        /// </summary>
        /// <param name="eyeTrackerData">Data from the current frames.</param>
        /// <returns>True if data could be added to the buffer. False if buffer was full, in that case the data is lost.</returns>
        public bool TryAddNewData(EyeTrackerData eyeTrackerData)
        {
            if (!this.dataBuffer.IsAddingCompleted)
            {
                if (this.dataBuffer.TryAdd(eyeTrackerData))
                {
                    return true;
                }
            }

            this.NumberFramesDropped++;
            return false;
        }

        /// <summary>
        /// Add more data to the file, if buffer is full waits.
        /// </summary>
        /// <param name="eyeTrackerData">Data from the current frame.</param>
        public void AddNewData(EyeTrackerData eyeTrackerData)
        {
            if (!this.dataBuffer.IsAddingCompleted)
            {
                try
                {
                    this.dataBuffer.Add(eyeTrackerData);
                }
                catch (InvalidOperationException ex)
                {
                    if (!this.dataBuffer.IsAddingCompleted)
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Dispose resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose resources.
        /// </summary>
        /// <param name="disposing">Disposing now.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.StopRecording();
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Writing data loop.
        /// </summary>
        private void WritingLoop()
        {
            this.NumberFramesSaved = 0;
            long lastFrameRecorded = 0;
            long firstFrameRecorded = -1;

            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(this.FileName))
                {
                    var token = new CancellationTokenSource();

                    // Read data from buffer until CompleteAdding is called.
                    // The thread will block here when there is no data unless CompleteAdding is called.
                    foreach (var data in this.dataBuffer.GetConsumingEnumerable(token.Token))
                    {
                        // Save the data
                        file.WriteLine(this.GetDataLine(data));
                        this.NumberFramesSaved++;
                        lastFrameRecorded = data.EyeDataRaw[Eye.Left].Timestamp.FrameNumber;
                        if ( firstFrameRecorded < 0 )
                        {
                            firstFrameRecorded = data.EyeDataRaw[Eye.Left].Timestamp.FrameNumber;
                        }
                    }

                    System.Diagnostics.Trace.WriteLine(string.Format("Finished data recording loop: {0} frames saved; Frames: {1}-{2}", this.NumberFramesSaved, firstFrameRecorded, lastFrameRecorded));
                }
            }
            catch(Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
        }

        /// <summary>
        /// Converts the data to a text line to add to the file.
        /// </summary>
        /// <param name="data">Data to be converted.</param>
        /// <returns>The string.</returns>
        private string GetDataLine(EyeTrackerData data)
        {
            var leftUpperEyelid = (data.EyeDataRaw[Eye.Left].Eyelids.Upper == null) ? 0 : data.EyeDataRaw[Eye.Left].Eyelids.Upper[0].Y + data.EyeDataRaw[Eye.Left].Eyelids.Upper[1].Y + data.EyeDataRaw[Eye.Left].Eyelids.Upper[2].Y + data.EyeDataRaw[Eye.Left].Eyelids.Upper[3].Y;

            var leftLowerEyelid = (data.EyeDataRaw[Eye.Left].Eyelids.Lower == null) ? 0 : data.EyeDataRaw[Eye.Left].Eyelids.Lower[0].Y + data.EyeDataRaw[Eye.Left].Eyelids.Lower[1].Y + data.EyeDataRaw[Eye.Left].Eyelids.Lower[2].Y + data.EyeDataRaw[Eye.Left].Eyelids.Lower[3].Y;

            var rightUpperEyelid = (data.EyeDataRaw[Eye.Right].Eyelids.Upper == null) ? 0 : data.EyeDataRaw[Eye.Right].Eyelids.Upper[0].Y + data.EyeDataRaw[Eye.Right].Eyelids.Upper[1].Y + data.EyeDataRaw[Eye.Right].Eyelids.Upper[2].Y + data.EyeDataRaw[Eye.Right].Eyelids.Upper[3].Y;

            var rightLowerEyelid = (data.EyeDataRaw[Eye.Right].Eyelids.Lower == null) ? 0 : data.EyeDataRaw[Eye.Right].Eyelids.Lower[0].Y + data.EyeDataRaw[Eye.Right].Eyelids.Lower[1].Y + data.EyeDataRaw[Eye.Right].Eyelids.Lower[2].Y + data.EyeDataRaw[Eye.Right].Eyelids.Lower[3].Y;

            string line =
                string.Format("{0}", data.EyeDataRaw[Eye.Left].Timestamp.FrameNumberRaw) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Timestamp.Seconds) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Center.X) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Center.Y) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Size.Width) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Size.Height) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Angle) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Iris.Radius) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].TorsionAngle) + " " +
                string.Format("{0:0.0000}", leftUpperEyelid) + " " +
                string.Format("{0:0.0000}", leftLowerEyelid) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].DataQuality) + " " +

                string.Format("{0}", data.EyeDataRaw[Eye.Right].Timestamp.FrameNumberRaw) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Timestamp.Seconds) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Center.X) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Center.Y) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Size.Width) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Size.Height) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Angle) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Iris.Radius) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].TorsionAngle) + " " +
                string.Format("{0:0.0000}", rightUpperEyelid) + " " +
                string.Format("{0:0.0000}", rightLowerEyelid) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].DataQuality) + " " +

                string.Format("{0:0.0000}", data.HeadDataRaw.AccelerometerX) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.AccelerometerY) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.AccelerometerZ) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GyroX) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GyroY) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GyroZ) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.MagnetometerX) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.MagnetometerY) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.MagnetometerZ) + " " +

                string.Format("{0:0}", data.Events[0]) + " " +

                string.Format("{0}", data.ExtraData.Int0) + " " +
                string.Format("{0}", data.ExtraData.Int1) + " " +
                string.Format("{0}", data.ExtraData.Int2) + " " +
                string.Format("{0}", data.ExtraData.Int3) + " " +
                string.Format("{0}", data.ExtraData.Int4) + " " +
                string.Format("{0}", data.ExtraData.Int5) + " " +
                string.Format("{0}", data.ExtraData.Int6) + " " +
                string.Format("{0}", data.ExtraData.Int7) + " " +

                string.Format("{0:0.0000}", data.ExtraData.Double0) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double1) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double2) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double3) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double4) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double5) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double6) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double7) + " " +

                string.Format("{0}", data.EyeDataRaw[Eye.Left].Timestamp.FrameNumber) + " " +
                string.Format("{0}", data.HeadDataRaw.TimeStamp.FrameNumber);

            return line;
        }
    }
}
