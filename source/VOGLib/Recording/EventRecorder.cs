﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

namespace OculomotorLab.VOG
{
    internal class EventRecorder : IDisposable
    {
        /// <summary>
        /// Buffer of events.
        /// </summary>
        private BlockingCollection<EyeTrackerEvent> eventBuffer;

        /// <summary>
        /// Thread that runs the loop writing events to file
        /// </summary>
        private Thread eventsRecordingThread;

        private string fileName;

        public void Start(string fileName)
        {
            this.fileName = fileName;

            this.eventsRecordingThread = new Thread(new ThreadStart(this.EventsRecordingLoop));
            this.eventsRecordingThread.Name = "EyeTracker:EventsRecordingLoop";
            this.eventsRecordingThread.Start();
        }

        public void Stop()
        {
            if (this.eventBuffer != null)
            {
                this.eventBuffer.CompleteAdding();
            }

            if (this.eventsRecordingThread != null)
            {
                this.eventsRecordingThread.Join();
            }
        }

        public bool RecordEvent(EyeTrackerEvent eyeTrackerEvent)
        {
            if ((this.eventBuffer != null) && !this.eventBuffer.IsAddingCompleted)
            {
                // Add the frame images to the input queue for processing
                if (this.eventBuffer.TryAdd(eyeTrackerEvent))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        /// <summary>
        /// Loop that records the events to a text file.
        /// </summary>
        private void EventsRecordingLoop()
        {
            var file = new System.IO.StreamWriter(this.fileName);

            var token = new CancellationTokenSource();

            try
            {
                // Read data from buffer until CompleteAdding is called.
                // The thread will block here when there is no data unless CompleteAdding is called.
                foreach (var eyeTrackerEvent in this.eventBuffer.GetConsumingEnumerable(token.Token))
                {
                    // Save the data
                    file.WriteLine(eyeTrackerEvent.GetStringLine());
                }
            }
            catch (Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
            finally
            {
                if (file != null)
                {
                    file.Dispose();
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}
