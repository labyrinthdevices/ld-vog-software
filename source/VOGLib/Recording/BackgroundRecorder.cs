﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

namespace OculomotorLab.VOG
{
    internal class BackGroundrecorder<T> : IDisposable
        where T:class
    {
        /// <summary>
        /// Data buffer.
        /// </summary>
        private BlockingCollection<T> buffer;

        /// <summary>
        /// Thread that runs the loop writing the data
        /// </summary>
        private Thread thread;

        public string ThreadName = "Recorder";

        private string fileName;

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        public event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        public void Start(string fileName)
        {
            this.fileName = fileName;

            this.thread = new Thread(new ThreadStart(this.RecordingLoop));
            this.thread.Name = "EyeTracker:EventsRecordingLoop";
            this.thread.Start();
        }

        public void Stop()
        {
            if (this.buffer != null)
            {
                this.buffer.CompleteAdding();
            }

            if (this.thread != null)
            {
                this.thread.Join();
            }
        }

        public bool RecordNewData(T data)
        {
            if ((this.buffer != null) && !this.buffer.IsAddingCompleted)
            {
                // Add the frame images to the input queue for processing
                if (this.buffer.TryAdd(data))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        protected virtual void InitFiles()
        {

        }

        protected virtual void RecordDataToFile(T data)
        {

        }

        protected virtual void CloseFiles()
        {

        }

        /// <summary>
        /// Loop that records the events to a text file.
        /// </summary>
        private void RecordingLoop()
        {
            this.InitFiles();

            var token = new CancellationTokenSource();

            try
            {
                // Read data from buffer until CompleteAdding is called.
                // The thread will block here when there is no data unless CompleteAdding is called.
                foreach (var data in this.buffer.GetConsumingEnumerable(token.Token))
                {
                    // Save the data
                    this.RecordDataToFile(data);
                }
            }
            catch (Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }
            finally
            {
                this.CloseFiles();
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion
    }
}
