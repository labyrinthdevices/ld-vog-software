﻿//-----------------------------------------------------------------------
// <copyright file="VideoPlayer.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Class that handles reading frames from a video file.
    /// </summary>
    public class VideoPlayer
    {
        /// <summary>
        /// Possible states of the video player
        /// </summary>
        enum VideoPlayerState
        {
            /// <summary>
            /// Initial state.
            /// </summary>
            Idle,

            /// <summary>
            /// Playing the videos.
            /// </summary>
            Playing,

            /// <summary>
            /// Paused.
            /// </summary>
            Paused,

            /// <summary>
            /// Waiting to stop.
            /// </summary>
            Stopping
        }

        /// <summary>
        /// Current state of the player.
        /// </summary>
        private VideoPlayerState state;

        /// <summary>
        /// Background thread of the player.
        /// </summary>
        private Thread playerThread;

        /// <summary>
        /// Timer to control the frame rate of the playback.
        /// </summary>
        private Stopwatch timer;
        
        /// <summary>
        /// Last timestamp of the time the last frame was displayed.
        /// </summary>
        private long lastMiliseconds;

        /// <summary>
        /// Value indicating wheter scrolling was requested.
        /// </summary>
        private bool scrolling;

        /// <summary>
        /// Frame that needs to scroll to.
        /// </summary>
        private long nextFrameScroll;

        /// <summary>
        /// Initializes a new instance of the ImageEyeVideoPlayer class.
        /// </summary>
        /// <param name="videoEyeProvider">Image eye sources.</param>
        /// <param name="fileNames">Names of the video files.</param>
        /// <param name="frameRange">Range of frames to be played</param>
        /// <returns>New ImageGrabberVideoFile object.</returns>
        public VideoPlayer(IVideoEyeProvider videoEyeProvider, EyeCollection<string> fileNames, Range frameRange)
        {
            // Initialize the video files
            if (videoEyeProvider != null)
            {
                this.VideoFileSources = videoEyeProvider.GetVideos(fileNames);
            }
            else
            {
                if (fileNames.Count == 2)
                {
                    VideoEye videoLeft = null;
                    if (fileNames[Eye.Left].Length > 1)
                    {
                        videoLeft = new VideoEye(Eye.Left, fileNames[Eye.Left]);
                    }
                    VideoEye videoRight = null;
                    if (fileNames[Eye.Right].Length > 1)
                    {
                        videoRight = new VideoEye(Eye.Right, fileNames[Eye.Right]);
                    }

                    this.VideoFileSources = new EyeCollection<VideoEye>(videoLeft, videoRight);
                }
                if (fileNames.Count == 1)
                {
                    VideoEye video = null;
                    if (fileNames[Eye.Both].Length > 1)
                    {
                        video = new VideoEye(Eye.Both, fileNames[Eye.Both]);
                    }

                    this.VideoFileSources = new EyeCollection<VideoEye>(video);
                }
            }


            // Get the number of frames (get the shortest in the case the videos have
            // different durations
            this.FrameCount = long.MaxValue;
            foreach (var video in VideoFileSources)
            {
                if (video != null)
                {
                    this.FrameCount = Math.Min(this.FrameCount, video.NumberOfFrames);
                }
            }

            // Get the frame rate from the videos
            this.FrameRate = -1;
            foreach (var video in VideoFileSources)
            {
                if (video != null)
                {
                    if (this.FrameRate > 0 && this.FrameRate != video.ReportedFrameRate)
                    {
                        throw new InvalidOperationException("Videos have different frame rates.");
                    }

                    this.FrameRate = video.ReportedFrameRate;
                }
            }

            if (frameRange.IsEmpty)
            {
                this.FrameRange = new Range(0, this.FrameCount - 1);
            }
            else
            {
                this.FrameRange = frameRange;
            }

            // Scroll to the begining
            foreach (var video in this.VideoFileSources)
            {
                if (video != null)
                {
                    if (this.FrameRange.Begin > 0)
                    {
                        video.Scroll((int)Math.Round(this.FrameRange.Begin));
                    }
                }
            }
            
            // Set the state as paused waiting for play to start
            this.state = VideoPlayerState.Paused;

            // Start the playback thread
            this.playerThread = new Thread(new ThreadStart(this.PlayerLoop));
            this.playerThread.Name = "EyeTracker:PlayerLoop";
            this.playerThread.Priority = ThreadPriority.Highest;
            this.playerThread.Start();
        }

        public VideoPlayer(EyeCollection<string> fileNames)
            : this(null, fileNames, new Range())
        {

        }

        public VideoPlayer(string fileName)
            : this(null, new EyeCollection<string>(fileName), new Range())
        {

        }

        /// <summary>
        /// List of sources of images of the eyes.
        /// </summary>
        public EyeCollection<VideoEye> VideoFileSources { get; private set; }

        /// <summary>
        /// Value indicating whether the video is currently playing.
        /// </summary>
        public bool Playing
        {
            get
            {
                return this.state == VideoPlayerState.Playing;
            }
        }

        /// <summary>
        /// Event to notify that new images have just been grabbed.
        /// </summary>
        public event EventHandler<EyeCollection<ImageEye>> ImagesGrabbed;

        /// <summary>
        /// Event raised when the video playing has finished.
        /// </summary>
        public event EventHandler VideoFinished;

        /// <summary>
        /// Gets or sets Ranges of frames to be played.
        /// </summary>
        public Range FrameRange { get; set; }

        /// <summary>
        /// Last images grabbed.
        /// </summary>
        public EyeCollection<ImageEye> LastImagesGrabbed;

        /// <summary>
        /// Gets or sets the last frame number.
        /// </summary>
        public long LastFrameNumber { get; private set; }

        /// <summary>
        /// Frame rate of the video.
        /// </summary>
        public double FrameRate { get; private set; }

        /// <summary>
        /// Gets or sets the total number of frames in the video file.
        /// </summary>
        public long FrameCount { get; private set; }

        /// <summary>
        /// Start the video playback.
        /// </summary>
        public void Play()
        {
            switch (this.state)
            {
                case VideoPlayerState.Paused:
                    this.state = VideoPlayerState.Playing;
                    break;
                default:
                    throw new Exception("Invalid state of the video player.");
            }

        }

        /// <summary>
        /// Pauses playing.
        /// </summary>
        public void Pause()
        {
            switch (this.state)
            {
                case VideoPlayerState.Playing:
                    this.state = VideoPlayerState.Paused;
                    break;
                default:
                    throw new Exception("Invalid state of the video player.");
            }
        }

        /// <summary>
        /// Stops the video player.
        /// </summary>
        public void Stop()
        {
            switch (this.state)
            {
                case VideoPlayerState.Idle:
                case VideoPlayerState.Playing:
                case VideoPlayerState.Paused:
                    this.state = VideoPlayerState.Stopping;
                    break;
                case VideoPlayerState.Stopping:
                default:
                    throw new Exception("Invalid state of the video player.");
            }
        }

        /// <summary>
        /// Scrolls the playback to the specified frame number.
        /// </summary>
        /// <param name="frameNumber">Frame number to play next.</param>
        public void Scroll(long frameNumber)
        {
            switch (this.state)
            {
                case VideoPlayerState.Playing:
                case VideoPlayerState.Paused:
                    this.scrolling = true;
                    this.nextFrameScroll = frameNumber;
                    break;
                default:
                    throw new Exception("Invalid state of the video player.");
            }
        }

        /// <summary>
        /// Raises the event video finished. Useful to know when to finish processing.
        /// </summary>
        /// <param name="sender">Object that generates the event.</param>
        /// <param name="eventArgs">Event arguments.</param>
        protected void OnVideoFinished(object sender, EventArgs eventArgs)
        {
            var handler = this.VideoFinished;
            if (handler != null)
            {
                handler(this, eventArgs);
            }
        }

        /// <summary>
        /// Raises the event Images Grabbed;
        /// </summary>
        /// <param name="sender">Object that generates the event.</param>
        /// <param name="images">Event arguments.</param>
        protected void OnImagesGrabbed(object sender, EyeCollection<ImageEye> images)
        {
            var handler = this.ImagesGrabbed;
            if (handler != null)
            {
                handler(this, images);
            }
        }

        /// <summary>
        /// Loop that runs in the background thread grabbing the images.
        /// </summary>
        private void PlayerLoop()
        {
            this.timer = new Stopwatch();
            timer.Start();

            while (this.state != VideoPlayerState.Stopping)
            {
                // If paused wait a bit and check later.
                // If scrolling and paused it is necessary to grab the frames at the new position.
                if (this.state == VideoPlayerState.Paused && !this.scrolling)
                {
                    Thread.Sleep(1);
                    continue;
                }

                // If scrolling move all the videos to the next frame.
                if (scrolling)
                {
                    foreach (var video in this.VideoFileSources)
                    {
                        if (video != null)
                        {
                            video.Scroll(this.nextFrameScroll);
                        }
                    }

                    scrolling = false;
                }
                else
                {
                    // Check if all the videos are finished.
                    var lastFrameToPlay = this.FrameCount - 1;
                    if (!this.FrameRange.IsEmpty)
                    {
                        lastFrameToPlay = (long)this.FrameRange.End;
                    }
                    if (this.LastFrameNumber >= lastFrameToPlay)
                    {
                        this.state = VideoPlayerState.Paused;
                        this.OnVideoFinished(this, new EventArgs());
                        continue;
                    }
                }

                // Grab the images from the videos.
                var images = new ImageEye[this.VideoFileSources.Count];
                int i = 0;
                foreach (var video in this.VideoFileSources)
                {
                    if (video != null)
                    {
                        images[i] = video.GrabImageFromVideo();
                        this.LastFrameNumber = images[i].TimeStamp.FrameNumber;
                    }
                    i = i + 1;
                }

                this.LastImagesGrabbed = new EyeCollection<ImageEye>(images);
                this.OnImagesGrabbed(this, this.LastImagesGrabbed);

                // Sleep to ensure proper frame rate playback
                var currentMiliseconds = this.timer.ElapsedMilliseconds;
                if (currentMiliseconds - lastMiliseconds < 1000.0 / this.FrameRate)
                {
                    var ms = 1000.0 / this.FrameRate - (currentMiliseconds - lastMiliseconds);
                    System.Threading.Thread.Sleep((int)Math.Floor(ms));
                }
                this.lastMiliseconds = currentMiliseconds;
            }
        }
    }
}
