﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerSettings.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Reflection;
    using System.Linq;
    using OculomotorLab.VOG.ImageGrabbing;
    using OculomotorLab.VOG.ImageProcessing;

    [Serializable]
    public class Settings : INotifyPropertyChanged
    {
        protected bool AllowNotify = true;

        #region Events

        /// <summary>
        /// Event that is raised whenever a property is changed
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the PropertyChange event
        /// </summary>
        /// <param name="e">Event parameters</param>
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            // Save thesettings everytime something changes
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the PropertyChange event
        /// </summary>
        /// <param name="propertyName">Name of the property that has changed</param>
        protected void OnPropertyChanged(string propertyName)
        {
            if (this.AllowNotify)
            {
                this.OnPropertyChanged(new EyeTrackerSettingChangedEventArgs(propertyName));
            }
        }

        #endregion
    }

    /// <summary>
    /// Class containing all the settings of the eye tracker. It can be linked to a property grid to offer a nice gui
    /// </summary>
    [Serializable]
    public class EyeTrackerSettings : Settings
    {
        #region Main settings

        /// <summary>
        /// Separate structure of settings for image processing.
        /// </summary>
        private TrackingSettings trackingSettings;

        /// <summary>
        /// Separate structure of settings for each eye tracking system. This is saved to a different file.
        /// </summary>
        private EyeTrackerSystemSettings systemSettings;

        /// <summary>
        /// Initializes a new instance of the EyeTrackerSettings class.
        /// </summary>
        public EyeTrackerSettings()
        {
            this.Tracking = new TrackingSettings();
        }

        [Browsable(false)]
        public TrackingSettings Tracking
        {
            get
            {
                return this.trackingSettings;
            }
            set
            {
                if (value != this.trackingSettings)
                {
                    this.trackingSettings = value;
                    this.trackingSettings.PropertyChanged += (o, e) =>
                    {
                        this.OnPropertyChanged("Tracking." + e.PropertyName);
                    };
                    this.trackingSettings = value;
                    this.OnPropertyChanged("TrackingSettings");
                }
            }
        }

        [Browsable(false)]
        public EyeTrackerSystemSettings EyeTrackingSystemSettings
        {
            get
            {
                return this.systemSettings;
            }
            set
            {
                if (value != this.systemSettings)
                {
                    this.systemSettings = value;
                    if (value != null)
                    {
                        this.systemSettings.PropertyChanged += (o, e) =>
                        {
                            this.OnPropertyChanged("System." + e.PropertyName);
                        };
                    }
                    this.OnPropertyChanged("EyeTrackerSystemSettings");
                }
            }
        }


        /// <summary>
        /// Gets or sets the camera system
        /// </summary>       
        [Category("General settings"), Description("EyeTracker system")]
        [NeedsRestarting]
        [TypeConverter(typeof(ExportSelectorConverter<IEyeTrackingSystem>))]
        public string EyeTrackerSystem
        {
            get
            {
                return this.eyeTrackerSystem;
            }
            set
            {
                if (value != this.eyeTrackerSystem)
                {
                    this.eyeTrackerSystem = value;

                    // Change also the settings variable
                    this.EyeTrackingSystemSettings = EyeTrackerSystemSettings.Load(this.EyeTrackerSystem);

                    this.OnPropertyChanged("EyeTrackerSystem");
                }
            }
        }
        private string eyeTrackerSystem = "Micromedical";

        /// <summary>
        /// Gets or sets the frame rate of the cameras
        /// </summary>       
        [Category("General settings"), Description("Frame rate")]
        [NeedsRestarting]
        public float FrameRate
        {
            get
            {
                return this.frameRate;
            }
            set
            {
                if (value != this.frameRate)
                {
                    this.frameRate = value;
                    this.OnPropertyChanged("FrameRate");
                }
            }
        }
        private float frameRate = 100.0f;

        /// <summary>
        /// Gets or sets the shutter duration
        /// </summary>       
        [Category("General settings"), Description("Shutter duration - 0 = Automatic")]
        [NeedsRestarting]
        public float ShutterDuration
        {
            get
            {
                return this.shutterDuration;
            }
            set
            {
                if (value != this.shutterDuration)
                {
                    this.shutterDuration = value;
                    this.OnPropertyChanged("ShutterDuration");
                }
            }
        }
        private float shutterDuration = 0.0f;

        /// <summary>
        /// Gets or sets a Value indicating where debuging images should be shown
        /// </summary>       
        [Category("General settings"), Description("Value indicating where debuging images should be shown")]
        public bool Debug
        {
            get
            {
                return this.debug;
            }
            set
            {
                if (value != this.debug)
                {
                    this.debug = value;
                    this.OnPropertyChanged("Debug");
                }
            }
        }
        private bool debug = false;

        /// <summary>
        /// Gets or sets the tracking mode
        /// </summary>    
        [Category("General settings"), Description("Gets or sets the tracking mode")]
        [NeedsRestarting]
        public Eye TrackingMode
        {
            get
            {
                return this.trackingMode;
            }

            set
            {
                if (value != this.trackingMode)
                {
                    this.trackingMode = value;
                    this.OnPropertyChanged("TrackingMode");
                }
            }
        }
        private Eye trackingMode = Eye.Both;

        /// <summary>
        /// Gets or sets a value indicating wether audio should be recorded.
        /// </summary>    
        [Category("General settings"), Description(" Gets or sets a value indicating whether video should be recorded")]
        [NeedsRestarting]
        public bool RecordVideo
        {
            get
            {
                return this.recordVideo;
            }

            set
            {
                if (value != this.recordVideo)
                {
                    this.recordVideo = value;
                    this.OnPropertyChanged("RecordVideo");
                }
            }
        }
        private bool recordVideo = true;

        /// <summary>
        /// Gets or sets a value indicating wether audio should be recorded.
        /// </summary>    
        [Category("General settings"), Description(" Gets or sets a value indicating wether audio should be recorded")]
        [NeedsRestarting]
        public bool RecordAudio
        {
            get
            {
                return this.recordAudio;
            }

            set
            {
                if (value != this.recordAudio)
                {
                    this.recordAudio = value;
                    this.OnPropertyChanged("RecordAudio");
                }
            }
        }
        private bool recordAudio = true;

        /// <summary>
        /// Gets or sets the name of the data files
        /// </summary>
        [Category("General settings"), Description("Name of the data files")]
        public string SessionName
        {
            get
            {
                return this.sessionName;
            }

            set
            {
                if (value != this.sessionName)
                {
                    this.sessionName = value;
                    this.OnPropertyChanged("DataFileName");
                }
            }
        }
        private string sessionName = "SESSION";

        /// <summary>
        /// Gets or sets the folder where data is saved
        /// </summary>
        [Category("General settings"), Description("Folder where data is saved")]
        public string DataFolder
        {
            get
            {
                return this.dataFolder;
            }

            set
            {
                if (value != this.dataFolder)
                {
                    this.dataFolder = value;
                    this.OnPropertyChanged("DataFolder");
                }
            }
        }
        private string dataFolder = @"C:\secure\Data\Raw\Torsion\DataTest";

        /// <summary>
        /// Gets or sets the size of the video recording buffer (number of frames)
        /// </summary>
        [Category("General settings"), Description("Size of the video recording buffer (number of frames)")]
        [NeedsRestarting]
        public int VideoRecordingBufferSize
        {
            get
            {
                return this.videoRecordingBufferSize;
            }

            set
            {
                if (value != this.videoRecordingBufferSize)
                {
                    this.videoRecordingBufferSize = value;
                    this.OnPropertyChanged("VideoRecordingBufferSize");
                }
            }
        }
        private int videoRecordingBufferSize = 100;

        /// <summary>
        /// Gets or sets the size of the processing buffer (number of frames)
        /// </summary>
        [Category("General settings"), Description("Size of the processing buffer (number of frames)")]
        [NeedsRestarting]
        public int ProcessingBufferSize
        {
            get
            {
                return this.processingBufferSize;
            }

            set
            {
                if (value != this.processingBufferSize)
                {
                    this.processingBufferSize = value;
                    this.OnPropertyChanged("ProcessingBufferSize");
                }
            }
        }
        private int processingBufferSize = 100;

        /// <summary>
        /// Gets or sets the TCP port where the host is listening
        /// </summary>
        [Category("General settings"), Description("TCP port where the host is listening")]
        [NeedsRestarting]
        public int ServiceListeningPort
        {
            get
            {
                return this.serviceListeningPort;
            }

            set
            {
                if (value != this.serviceListeningPort)
                {
                    this.serviceListeningPort = value;
                    this.OnPropertyChanged("ServiceListeningPort");
                }
            }
        }
        private int serviceListeningPort = 9000;


        /// <summary>
        /// Gets or sets the camera system
        /// </summary>       
        [Category("General settings"), Description("Calibration method")]
        [TypeConverter(typeof(ExportSelectorConverter<IEyeCalibrationImplementation>))]
        public string CalibrationMethod
        {
            get
            {
                return this.calibrationMethod;
            }
            set
            {
                if (value != this.calibrationMethod)
                {
                    this.calibrationMethod = value;
                    this.OnPropertyChanged("CalibrationMethod");
                }
            }
        }
        private string calibrationMethod = ((ExportDescriptionAttribute)Attribute.GetCustomAttribute(typeof(OculomotorLab.VOG.Calibration.EyeCalibrationManual), typeof(ExportDescriptionAttribute))).Name;
        #endregion

        #region Display settings

        /// <summary>
        /// Number of seconds in trace
        /// </summary>
        [Category("Display settings"), Description("Number of samples in trace")]
        public double TraceSpan
        {
            get
            {
                return this.traceSpan;
            }

            set
            {
                if (value != this.traceSpan)
                {
                    this.traceSpan = value;
                    this.OnPropertyChanged("TraceSpan");
                }
            }
        }
        private double traceSpan = 500;

        /// <summary>
        /// Value indicating wether the set up images should show the data.
        /// </summary>
        [Category("Display settings"), Description("Value indicating wether the set up images should show the data.")]
        public bool OverlayData
        {
            get
            {
                return this.overlayData;
            }

            set
            {
                if (value != this.overlayData)
                {
                    this.overlayData = value;
                    this.OnPropertyChanged("OverlayData");
                }
            }
        }
        private bool overlayData = true;

        #endregion

        #region Methods

        /// <summary>
        /// Tries to load the settings from disk. If not possible loads the default settings
        /// </summary>
        internal static EyeTrackerSettings Load()
        {
            try
            {
                System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(typeof(EyeTrackerSettings));

                var dir = System.IO.Directory.GetCurrentDirectory();

                EyeTrackerSettings settings = null;

                using (System.IO.StreamReader file = new System.IO.StreamReader(dir + "\\EyeTrackerSettings.xml"))
                {
                    settings = (EyeTrackerSettings)reader.Deserialize(file);
                }

                if (settings == null)
                {
                    System.Diagnostics.Trace.WriteLine("Error loading settings going to defaults.");
                    return EyeTrackerSettings.LoadDefaultSettings();
                }

                settings.EyeTrackingSystemSettings = EyeTrackerSystemSettings.Load(settings.EyeTrackerSystem);

                settings.PropertyChanged += (sender, e) => { settings.Save(); };

                return settings;
            }
            catch (System.IO.FileNotFoundException)
            {
                System.Diagnostics.Trace.WriteLine("Error loading settings going to defaults.");
                return EyeTrackerSettings.LoadDefaultSettings();
            }
            catch (InvalidOperationException)
            {
                System.Diagnostics.Trace.WriteLine("Error loading settings going to defaults.");
                return EyeTrackerSettings.LoadDefaultSettings();
            }
        }

        /// <summary>
        /// Loads the default settings.
        /// </summary>
        internal static EyeTrackerSettings LoadDefaultSettings()
        {
            var settings = new EyeTrackerSettings();
            settings.PropertyChanged += (sender, e) => { settings.Save(); };
            settings.Save();

            return settings;
        }

        private object lockSave = new object();

        /// <summary>
        /// Saves the current settings to disk
        /// </summary>
        internal void Save()
        {
            lock (lockSave)
            {
                // Save the system settings
                if (this.EyeTrackingSystemSettings != null)
                {
                    this.EyeTrackingSystemSettings.Save();
                }

                // Do not save the system settings in the main file
                var tempSettings = this.EyeTrackingSystemSettings;
                this.AllowNotify = false;
                this.EyeTrackingSystemSettings = null;

                // Save the main file
                System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(EyeTrackerSettings));

                var dir = System.IO.Directory.GetCurrentDirectory();

                using (var file = new System.IO.StreamWriter(dir + "\\EyeTrackerSettings.xml"))
                {
                    writer.Serialize(file, EyeTracker.Settings);
                }
                this.EyeTrackingSystemSettings = tempSettings;
                this.AllowNotify = true;
            }
        }

        #endregion
    }

    [Serializable]
    public class TrackingSettings : Settings
    {
        #region tracking settings

        /// <summary>
        /// Gets or sets the number of processing threads to use
        /// </summary>
        [Category("Processing settings"), Description("Value indicating how many processing threads to use. Default value is half of number of available processors.")]
        [NeedsRestarting]
        public int NumberOfProcessingThreads
        {
            get
            {
                return this.numberOfProcessingThreads;
            }
            set
            {
                if (value != this.numberOfProcessingThreads)
                {
                    this.numberOfProcessingThreads = value;
                    this.OnPropertyChanged("NumberOfProcessingThreads");
                }
            }
        }
        private int numberOfProcessingThreads = (int)Math.Floor(System.Environment.ProcessorCount/2.0);

        /// <summary>
        /// Gets or sets a value indicating whether to use Geometric transformation or not
        /// </summary>
        [Category("Processing settings"), Description("Value indicating whether to use Geometric transformation or not")]
        public bool UseGeometricTransformation
        {
            get
            {
                return this.useGeometricTransformation;
            }

            set
            {
                if (value != this.useGeometricTransformation)
                {
                    this.useGeometricTransformation = value;
                    this.OnPropertyChanged("UseGeometricTransformation");
                }
            }
        }
        private bool useGeometricTransformation = false;

        /// <summary>
        /// Gets or sets threshold to find the dark pixels that should belong to the pupil (left eye)
        /// </summary>
        [Category("Processing settings"), Description("Threshold to find the dark pixels that should belong to the pupil (left eye)")]
        public int DarkThresholdLeftEye
        {
            get
            {
                return this.thresholdDarkLeftEye;
            }

            set
            {
                if (value != this.thresholdDarkLeftEye)
                {
                    this.thresholdDarkLeftEye = value;
                    this.OnPropertyChanged("ThresholdLeftEye");
                }
            }
        }
        private int thresholdDarkLeftEye = 60;

        /// <summary>
        /// Gets or sets threshold to find the dark pixels that should belong to the pupil (right eye)
        /// </summary>
        [Category("Processing settings"), Description("Threshold to find the dark pixels that should belong to the pupil (right eye)")]
        public int DarkThresholdRightEye
        {
            get
            {
                return this.thresholdDarkRightEye;
            }

            set
            {
                if (value != this.thresholdDarkRightEye)
                {
                    this.thresholdDarkRightEye = value;
                    this.OnPropertyChanged("ThresholdRightEye");
                }
            }
        }
        private int thresholdDarkRightEye = 60;

        /// <summary>
        /// Gets or sets threshold to find the bright pixels that should belong to the reflexions (left eye)
        /// </summary>
        [Category("Processing settings"), Description("Threshold to find the bright pixels that should belong to the reflexions (left eye)")]
        public int BrightThresholdLeftEye
        {
            get
            {
                return this.thresholdBrightLeftEye;
            }

            set
            {
                if (value != this.thresholdBrightLeftEye)
                {
                    this.thresholdBrightLeftEye = value;
                    this.OnPropertyChanged("BrightThresholdLeftEye");
                }
            }
        }
        private int thresholdBrightLeftEye = 200;

        /// <summary>
        /// Gets or sets threshold to find the bright pixels that should belong to the reflexions (right eye)
        /// </summary>
        [Category("Processing settings"), Description("Threshold to find the bright pixels that should belong to the reflexions (right eye)")]
        public int BrightThresholdRightEye
        {
            get
            {
                return this.thresholdBrightRightEye;
            }

            set
            {
                if (value != this.thresholdBrightRightEye)
                {
                    this.thresholdBrightRightEye = value;
                    this.OnPropertyChanged("BrightThresholdRightEye");
                }
            }
        }
        private int thresholdBrightRightEye = 200;

        /// <summary>
        /// Gets or sets the left part to the frame that is not processed. Right, top, left, bottom.
        /// </summary>   
        [Category("Processing settings"), Description("Part to the frame that is not processed. Right, top, left, bottom.")]
        public Rectangle Cropping
        {
            get
            {
                return this.cropping;
            }

            set
            {
                if (value != this.cropping)
                {
                    this.cropping = value;
                    this.OnPropertyChanged("Cropping");
                }
            }
        }
        private Rectangle cropping = new Rectangle(1, 1, 1, 1);

        /// <summary>
        /// Gets or sets the X coordinate of the Left Eye Globe, relative to the center of the image.
        /// </summary>    
        [Category("Processing settings"), Description("Coordinate of the Left Eye Globe, relative to the center of the image")]
        public EyePhysicalModel LeftEyeGlobe
        {
            get
            {
                return this.leftEyeGlobe;
            }

            set
            {
                if (value != this.leftEyeGlobe)
                {
                    this.leftEyeGlobe = value;
                    this.OnPropertyChanged("LeftEyeGlobe");
                }
            }
        }
        private EyePhysicalModel leftEyeGlobe = new EyePhysicalModel(new Point(0, 0), 0f);

        /// <summary>
        /// Gets or sets the X coordinate of the right Eye Globe, relative to the center of the image.
        /// </summary>    
        [Category("Processing settings"), Description("Coordinate of the right Eye Globe, relative to the center of the image")]
        public EyePhysicalModel RightEyeGlobe
        {
            get
            {
                return this.rightEyeGlobe;
            }

            set
            {
                if (value != this.rightEyeGlobe)
                {
                    this.rightEyeGlobe = value;
                    this.OnPropertyChanged("RightEyeGlobe");
                }
            }
        }
        private EyePhysicalModel rightEyeGlobe = new EyePhysicalModel(new Point(0, 0), 0f);

        #endregion

        #region Pupil tracking settings

        /// <summary>
        /// Gets or sets the method to track the pupil
        /// </summary>
        [Category("Pupil tracking settings"), Description("Method to track the pupil")]
        [TypeConverter(typeof(ExportSelectorConverter<IPupilTracker>))]
        public string PupilTrackingMethod
        {
            get
            {
                return this.pupilTrackingMethod;
            }

            set
            {
                if (value != this.pupilTrackingMethod)
                {
                    this.pupilTrackingMethod = value;
                    this.OnPropertyChanged("PupilTrackingMethod");
                }
            }
        }
        private string pupilTrackingMethod = "Blob";

        /// <summary>
        /// Gets or sets the resolution of the camera in mm per pixel. This should be set up automatically
        /// after a camera system is selected.
        /// </summary>       
        [Category("General settings"), Description("Camera resolution (aprox mm per pixels)")]
        public double MmPerPix
        {
            get
            {
                return this.mmPerPix;
            }
            set
            {
                if (value != this.mmPerPix)
                {
                    this.mmPerPix = value;
                    this.OnPropertyChanged("MmPerPix");
                }
            }
        }
        private double mmPerPix = 0.15;

        /// <summary>
        /// Gets or sets the minimum radius of the pupil
        /// </summary>
        [Category("Pupil tracking settings"), Description("Minimum radius of the pupil in pixels")]
        [ReadOnly(true)]
        public double MinPupRadPix
        {
            get
            {
                return this.minPupRadmm / this.mmPerPix;
            }

        }

        /// <summary>
        /// Gets or sets the minimum radius of the pupil in mm
        /// </summary>
        [Category("Pupil tracking settings"), Description("Minimum radius of the pupil in mms")]
        public double MinPupRadmm
        {
            get
            {
                return this.minPupRadmm;
            }

            set
            {
                if (value != this.minPupRadmm)
                {
                    this.minPupRadmm = value;
                    this.OnPropertyChanged("MinPupRadmm");
                }
            }
        }
        private double minPupRadmm = 1;

        #endregion

        #region EyeLid tracking settings

        /// <summary>
        /// Gets or sets the method to track the eyelid
        /// </summary>
        [Category("EyeLid tracking settings"), Description("Method to track the EyeLid")]
        [TypeConverter(typeof(ExportSelectorConverter<IEyelidTracker>))]
        public string EyelidTrackingMethod
        {
            get
            {
                return this.eyelidTrackingMethod;
            }

            set
            {
                if (value != this.eyelidTrackingMethod)
                {
                    this.eyelidTrackingMethod = value;
                    this.OnPropertyChanged("EyeLidTrackingMethod");
                }
            }
        }
        private string eyelidTrackingMethod = "HoughLines";

        /// <summary>
        /// Gets the top eyelid, relative to pupil center
        /// </summary>
        [Category("EyeLid tracking settings"), Description("Top eyelid, relative to pupil center")]
        public double TopEyelid
        {
            get
            {
                return this.topEyelid;
            }

            set
            {
                if (value != this.topEyelid)
                {
                    this.topEyelid = value;
                    this.OnPropertyChanged("TopEyelid");
                }
            }
        }
        private double topEyelid = 0;

        /// <summary>
        /// Gets or sets the maximum torsion in one direction (degrees)
        /// </summary>
        [Category("EyeLid tracking settings"), Description("Maximum torsion in one direction (degrees)")]
        public double BottomEyelid
        {
            get
            {
                return this.bottomEyelid;
            }

            set
            {
                if (value != this.bottomEyelid)
                {
                    this.bottomEyelid = value;
                    this.OnPropertyChanged("BottomEyelid");
                }
            }
        }
        private double bottomEyelid = 0;

        #endregion

        #region Position tracking settings

        /// <summary>
        /// Gets or sets the method to track the position
        /// </summary>
        [Category("Position tracking settings"), Description("Method to track the position")]
        [TypeConverter(typeof(ExportSelectorConverter<IPositionTracker>))]
        public string PositionTrackingMethod
        {
            get
            {
                return this.positionTrackingMethod;
            }

            set
            {
                if (value != this.positionTrackingMethod)
                {
                    this.positionTrackingMethod = value;
                    this.OnPropertyChanged("PupilTrackingMethod");
                }
            }
        }
        private string positionTrackingMethod = "EllipseFitting";

        #endregion

        #region Torsion tracking settings

        /// <summary>
        /// Gets or sets the method to measure torsion
        /// </summary>
        [Category("Torsion settings"), Description("Method to measure torsion")]
        [TypeConverter(typeof(ExportSelectorConverter<ITorsionTracker>))]
        public string TorsionMethod
        {
            get
            {
                return this.torsionMethod;
            }

            set
            {
                if (value != this.torsionMethod)
                {
                    this.torsionMethod = value;
                    this.OnPropertyChanged("TorsionMethod");
                }
            }
        }
        private string torsionMethod = "CrossCorrelation";

        /// <summary>
        /// Gets or sets the method to detect the iris limits
        /// </summary>
        [Category("Torsion settings"), Description("Method to detect the iris limits")]
        [NeedsRestarting]
        public IrisDetectionMethod IrisDetectionMethod
        {
            get
            {
                return this.irisDetectionMethod;
            }

            set
            {
                if (value != this.irisDetectionMethod)
                {
                    this.irisDetectionMethod = value;
                    this.OnPropertyChanged("IrisDetectionMethod");
                }
            }
        }
        private IrisDetectionMethod irisDetectionMethod = IrisDetectionMethod.Manual;

        /// <summary>
        /// Gets or sets the current radius of the left iris
        /// </summary>
        [Category("Torsion settings"), Description("Current radius of the left iris")]
        [ReadOnly(true)]
        public double IrisRadiusPixLeft
        {
            get
            {
                return this.irisRadiusPixLeft;
            }

            set
            {
                if (value > this.MaxIrisPixRad)
                {
                    value = this.MaxIrisPixRad;
                }
                if (value != this.irisRadiusPixLeft)
                {
                    this.irisRadiusPixLeft = Math.Min(value, this.MaxIrisPixRad);
                    this.OnPropertyChanged("IrisRadiusLeft");
                }
            }
        }
        private double irisRadiusPixLeft = 80;

        /// <summary>
        /// Gets or sets the current radius of the right iris
        /// </summary>
        [Category("Torsion settings"), Description("Current radius of the right iris")]
        [ReadOnly(true)]
        public double IrisRadiusPixRight
        {
            get
            {
                return this.irisRadiusPixRight;
            }

            set
            {
                if (value != this.irisRadiusPixRight)
                {
                    this.irisRadiusPixRight = Math.Min(value, this.MaxIrisPixRad);
                    this.OnPropertyChanged("IrisRadiusRight");
                }
            }
        }
        private double irisRadiusPixRight = 80;

        /// <summary>
        /// Gets or sets the maximum radius of the iris
        /// </summary>
        [Category("Torsion settings"), Description("Maximum radius of the iris in pixels")]
        [ReadOnly(true)]
        public double MaxIrisPixRad
        {
            get
            {
                return this.maxIrisRadmm / this.mmPerPix;
            }
        }

        /// <summary>
        /// Gets or sets the maximum radius of the iris
        /// </summary>
        [Category("Torsion settings"), Description("Maximum radius of the iris in milimiters")]
        public double MaxIrisRadmm
        {
            get
            {
                return this.maxIrisRadmm;
            }

            set
            {
                if (value != this.maxIrisRadmm)
                {
                    this.maxIrisRadmm = value;
                    this.OnPropertyChanged("MaxIrisRadmm");
                }
            }
        }
        private double maxIrisRadmm = 15;

        /// <summary>
        /// Gets or sets the maximum torsion in one direction (degrees)
        /// </summary>
        [Category("Torsion settings"), Description("Maximum torsion in one direction (degrees)")]
        public double MaxTorsion
        {
            get
            {
                return this.maxTorsion;
            }

            set
            {
                if (value != this.maxTorsion)
                {
                    this.maxTorsion = value;
                    this.OnPropertyChanged("MaxTorsion");
                }
            }
        }
        private double maxTorsion = 25;

        #endregion
    }

    /// <summary>
    /// Attribute used to indicate if a given settings requires the eye tracker
    /// to restart to take effect.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property)]
    internal class NeedsRestarting : System.Attribute
    {
        /// <summary>
        /// Gets a value indicating whether the changed property requires restarting to take effect.
        /// </summary>
        public bool Value { get; private set; }

        /// <summary>
        /// Initializes a new instance of the NeedsRestarting class.
        /// </summary>
        /// <param name="needsRestarting">Value indicating whether the changed property requires restarting to take effect.</param>
        public NeedsRestarting(bool needsRestarting)
        {
            this.Value = needsRestarting;
        }

        /// <summary>
        /// Initializes a new instance of the NeedsRestarting class.
        /// </summary>
        public NeedsRestarting()
        {
            this.Value = true;
        }
    }

    /// <summary>
    /// Arguments for the EyeTrackerSettingChanged event.
    /// </summary>
    public class EyeTrackerSettingChangedEventArgs : PropertyChangedEventArgs
    {
        /// <summary>
        /// Gets a value indicating whether the changed property requires restarting to take effect.
        /// </summary>
        public bool NeedsRestarting { get; private set; }

        /// <summary>
        /// Initializes a new instance of the EyeTrackerSettingChangedEventArgs class.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        public EyeTrackerSettingChangedEventArgs(string propertyName)
            : base(propertyName)
        {
            var needsRestarting = false;

            var prop = typeof(EyeTrackerSettings).GetProperty(propertyName);
            if (prop != null)
            {
                var needsRestartingAttributes = prop.GetCustomAttributes(
                    typeof(NeedsRestarting),
                     false) as NeedsRestarting[];

                if (needsRestartingAttributes.Length > 0)
                {
                    needsRestarting = needsRestartingAttributes[0].Value;
                }
            }

            this.NeedsRestarting = needsRestarting;
        }
    }

    /// <summary>
    /// http://stackoverflow.com/questions/14593364/propertygrid-control-and-drop-down-lists
    /// </summary>
    /// <typeparam name="iT">Interface that the list elements must implement.</typeparam>
    class ExportSelectorConverter<iT> : TypeConverter
        where iT : class
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return EyeTrackingPluginLoader.Loader.GetStandardValues<iT>();
        }
    }


}
