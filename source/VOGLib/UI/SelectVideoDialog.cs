﻿//-----------------------------------------------------------------------
// <copyright file="SelectVideoDialog.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Linq;
    using OculomotorLab.VOG;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Dialog used to select a video to play or process.
    /// </summary>
    public partial class SelectVideoDialog : Form
    {
        /// <summary>
        /// Initializes a new instance of the SelectVideoDialog class.
        /// </summary>
        public SelectVideoDialog()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Shows a dialog to select a video.
        /// </summary>
        /// <param name="processing">True if processing the video, false if playing it.</param>
        /// <returns>The options selected in the dialog.</returns>
        public static PlayVideoOptions SelectVideo(bool processing)
        {
            SelectVideoDialog dialog = new SelectVideoDialog();
            dialog.checkBoxSaveProcessedVideo.Enabled = processing;

            var result = dialog.ShowDialog();

            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return null;
            }

            var options = processing ?
                new ProcessVideoOptions() :
                new PlayVideoOptions();

            options.EyeTrackingSystem = dialog.GetCameraSystem().Name;
            options.CalibrationFileName = dialog.GetCalibrationFileName();
            options.VideoFileNames = dialog.GetVideoFileNames();
            options.CustomRange = dialog.GetCustomRange();

            if (processing)
            {
                ((ProcessVideoOptions)options).SaveProcessedVideo = dialog.checkBoxSaveProcessedVideo.Checked;
            }

            return options;
        }

        /// <summary>
        /// Gets the names of the video files.
        /// </summary>
        /// <returns>The names of the video files.</returns>
        private EyeCollection<string> GetVideoFileNames()
        {
            switch (this.GetCameraSystem().NumberOfVideos)
            {
                case 2:
                    return new EyeCollection<string>(file1TextBox.Text, file2TextBox.Text);

                case 1:
                    return new EyeCollection<string>(file1TextBox.Text);
            }

            return null;
        }

        /// <summary>
        /// Gets the names of the calibration files.
        /// </summary>
        /// <returns>The names of the calibration files.</returns>
        private string GetCalibrationFileName()
        {
            return textBoxCalibration.Text;
        }

        /// <summary>
        /// Gets the camera system.
        /// </summary>
        /// <returns>Camera system.</returns>
        private IEyeTrackingSystemMetadata GetCameraSystem()
        {
            var metadata = from provider in EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.classesAvaiable
                           where provider.Name == (string)this.systemComboBox.SelectedValue
                           select provider;

            return metadata.ElementAt(0);
        }

        /// <summary>
        /// Gets the custom range of frames.
        /// </summary>
        /// <returns>Range of frames.</returns>
        private Range GetCustomRange()
        {
            if (this.checkBoxCustomRange.Checked)
            {
                return new Range(
                    int.Parse(this.textBoxFromFrame.Text),
                    int.Parse(this.textBoxToFrame.Text));
            }
            else
            {
                return new Range();
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void SelectVideoDialog_Load(object sender, EventArgs e)
        {
            this.systemComboBox.DataSource = EyeTrackingPluginLoader.Loader.eyeTrackingsystemFactory.classesAvaiable.Select(x => x.Name).ToArray();
            this.systemComboBox.SelectedIndex = this.systemComboBox.FindStringExact(EyeTracker.Settings.EyeTrackerSystem);
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void File1Button_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Video Files (*.avi;*.mpg)|*.avi;*.mpg";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            var result = openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var fileName = openFileDialog1.FileName;

                if (fileName.Length == 0)
                {
                    throw new ArgumentException("File not valid");
                }
                string leftFileName = file1TextBox.Text;
                string rightFileName = file2TextBox.Text;

                switch (((string)((Button)sender).Tag))
                {
                    case "Left":
                        leftFileName = fileName;
                        break;
                    case "Right":
                        rightFileName = fileName;
                        break;
                }

                if (fileName.Contains("Left.") && !fileName.Contains("LeftRight."))
                {
                    leftFileName = fileName;
                    rightFileName = fileName.Replace("Left.", "Right.");
                }

                if (fileName.Contains("Right.") && !fileName.Contains("LeftRight."))
                {
                    rightFileName = fileName;
                    leftFileName = fileName.Replace("Right.", "Left.");
                }

                if (fileName.Contains("LeftRight."))
                {
                    leftFileName = fileName;
                    rightFileName = fileName;
                }

                file1TextBox.Text = leftFileName;
                file2TextBox.Text = rightFileName;
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void ButtonCalibrationLeftEye_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Calibration Files (*.cal)|*.cal";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            var result = openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var fileName = openFileDialog1.FileName;

                if (fileName.Length == 0)
                {
                    throw new ArgumentException("File not valid");
                }

                textBoxCalibration.Text = fileName;
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void CheckBoxCustomRange_CheckedChanged(object sender, EventArgs e)
        {
            this.panelCustomRange.Enabled = checkBoxCustomRange.Checked;
        }

        /// <summary>
        /// Class with an enumerate and a string to work with the combo box.
        /// </summary>
        private class VOGSystem
        {
            /// <summary>
            /// Gets or sets the name of the camera system.
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Gets or sets the code of the camera system.
            /// </summary>
            public System.Type Value { get; set; }
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void systemComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.GetCameraSystem().NumberOfVideos)
            {
                case 2:
                    file2TextBox.Visible = true;
                    break;
                case 1:
                    file2TextBox.Visible = false;
                    break;
            }
        }
    }
}