﻿//-----------------------------------------------------------------------
// <copyright file="SliderTextControl.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;

    /// <summary>
    /// User control that contains a text label, a slider and a text box. The slider and the text box are.
    /// Coordinated to as they represent the same value all the time.
    /// </summary>
    public partial class SliderTextControl : UserControl
    {
        /// <summary>
        /// Value represented in the slider and the text box.
        /// </summary>
        private double value;

        private Range range;

        /// <summary>
        /// Initializes a new instance of the SliderTextControl class.
        /// </summary>
        public SliderTextControl()
        {
            InitializeComponent();

            this.value = double.NaN;

            this.EnabledChanged += SliderTextControl_EnabledChanged;
        }

        void SliderTextControl_EnabledChanged(object sender, EventArgs e)
        {
            this.label.Enabled = this.Enabled;
            this.trackBar.Enabled = this.Enabled;
            this.textBox.Enabled = this.Enabled;
        }


        public bool EnableValueChangedEvent = true;
        /// <summary>
        /// Event raised when the value changes.
        /// </summary>
        public event EventHandler ValueChanged;
        
        /// <summary>
        /// Gets or sets the name to be shown in the text label.
        /// </summary>
        [Browsable(true)]
        [Description("Name of the value represented."), Category("Data")]  
        public string Text
        {
            get
            {
                return this.label.Text;
            }
            set
            {
                this.label.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets the range of values allowed. 
        /// </summary>
        [Browsable(true)]
        [Description("Range of values allowed."), Category("Data")]
        public Range Range
        {
            get
            {
                return this.range;
            }
            set
            {
                this.range = value;

                this.trackBar.Minimum = (int)Math.Round(this.range.Begin);
                this.trackBar.Maximum = (int)Math.Round(this.range.End);
            }
        }

        /// <summary>
        /// Gets or sets the value of the number.
        /// </summary>
        [Browsable(true)]
        [Description("Current value."), Category("Data")]  
        public double Value
        {
            get
            {
                return this.value;
            }
            set
            {
                var oldValue = this.value;

                if (oldValue != value && !this.range.IsEmpty)
                {
                    if (!this.Range.Contains(value))
                    {
                        if ( value > this.range.End )
                        {
                            this.value = this.range.End;
                        }
                        if ( value < this.range.Begin)
                        {
                            this.value = this.range.Begin;
                        }
                    }

                    this.value = value;

                    this.trackBar.Value = (int)Math.Round(this.value);
                    this.textBox.Text = ((int)Math.Round(this.value)).ToString();

                    if (EnableValueChangedEvent)
                    {
                        this.OnValueChanged(new EventArgs());
                    }
                }
            }
        }

        /// <summary>
        /// Raises the ValueChanged event.
        /// </summary>
        /// <param name="e">Event parameters.</param>
        protected void OnValueChanged(EventArgs e)
        {
            var handler = this.ValueChanged;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {

            double value = 0.0;

            var result = double.TryParse(this.textBox.Text, out value);
            if ( result)
            {
                this.Value = value;
            }
        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            this.Value = this.trackBar.Value;
        }
    }
}
