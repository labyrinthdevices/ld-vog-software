﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerQuickSettings.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using OculomotorLab.VOG;
    using OculomotorLab.VOG.ImageProcessing;

    /// <summary>
    /// Control with some quick settings of the eye tracker for a single eye.
    /// </summary>
    public partial class EyeTrackerQuickSettings : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the EyeTrackerQuickSettings class.
        /// </summary>
        public EyeTrackerQuickSettings()
        {
            this.InitializeComponent();

            this.trackBarIrisRadius.Maximum = 150;
            this.trackBarIrisRadius.Minimum = 0;

            this.trackBarTopEyelid.Maximum = 100;
            this.trackBarTopEyelid.Minimum = 0;

            this.trackBarBottomEyelid.Maximum = 100;
            this.trackBarBottomEyelid.Minimum = 0;
        }

        /// <summary>
        /// Gets or sets left or right eye.
        /// </summary>
        public Eye WhichEye { get; set; }

        /// <summary>
        /// Updates the control.
        /// </summary>
        public void UpdateValues()
        {
            if ( EyeTracker.Settings == null)
            {
                return;
            }

            if (this.WhichEye == Eye.Left)
            {
                this.trackBarPupilThreshold.Value = EyeTracker.Settings.Tracking.DarkThresholdLeftEye;
                this.textBoxPupilThreshold.Text = EyeTracker.Settings.Tracking.DarkThresholdLeftEye.ToString();

                this.trackBarIrisRadius.Value = (int)Math.Min(200, (EyeTracker.Settings.Tracking.IrisRadiusPixLeft / EyeTracker.Settings.Tracking.MaxIrisPixRad * 100.0));
                this.textBoxReflectionThreshold.Text = EyeTracker.Settings.Tracking.BrightThresholdLeftEye.ToString();

                this.trackBarReflectionThreshold.Value = EyeTracker.Settings.Tracking.BrightThresholdLeftEye;
                this.textBoxIrisRadius.Text = EyeTracker.Settings.Tracking.IrisRadiusPixLeft.ToString();
            }
            else
            {
                this.trackBarIrisRadius.Value = (int)Math.Min(200, (EyeTracker.Settings.Tracking.IrisRadiusPixRight / EyeTracker.Settings.Tracking.MaxIrisPixRad * 100.0));
                this.textBoxIrisRadius.Text = EyeTracker.Settings.Tracking.IrisRadiusPixRight.ToString();

                this.trackBarPupilThreshold.Value = EyeTracker.Settings.Tracking.DarkThresholdRightEye;
                this.textBoxPupilThreshold.Text = EyeTracker.Settings.Tracking.DarkThresholdRightEye.ToString();

                this.trackBarReflectionThreshold.Value = EyeTracker.Settings.Tracking.BrightThresholdRightEye;
                this.textBoxReflectionThreshold.Text = EyeTracker.Settings.Tracking.BrightThresholdRightEye.ToString();
            }

            this.trackBarTopEyelid.Value = (int)Math.Min(100, (EyeTracker.Settings.Tracking.TopEyelid * 100.0));
            this.trackBarBottomEyelid.Value = (int)Math.Min(100, (EyeTracker.Settings.Tracking.BottomEyelid * 100.0));

            this.textBoxTopEyelid.Text = EyeTracker.Settings.Tracking.TopEyelid.ToString();
            this.textBoxBottomEyelid.Text = EyeTracker.Settings.Tracking.BottomEyelid.ToString();
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void TrackBarPupilThreshold_Scroll(object sender, EventArgs e)
        {
            if (this.WhichEye == Eye.Left)
            {
                EyeTracker.Settings.Tracking.DarkThresholdLeftEye = this.trackBarPupilThreshold.Value;
            }
            else
            {
                EyeTracker.Settings.Tracking.DarkThresholdRightEye = this.trackBarPupilThreshold.Value;
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void textBoxPupilThreshold_TextChanged(object sender, EventArgs e)
        {
            int value = 0;
            if (int.TryParse(this.textBoxPupilThreshold.Text, out value))
            {
                value = Math.Max(Math.Min(value, 255), 0);

                if (this.WhichEye == Eye.Left)
                {
                    EyeTracker.Settings.Tracking.DarkThresholdLeftEye = value;
                }
                else
                {
                    EyeTracker.Settings.Tracking.DarkThresholdRightEye = value;
                }
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void TrackBarIrisRadius_Scroll(object sender, EventArgs e)
        {
            if (this.WhichEye == Eye.Left)
            {
                EyeTracker.Settings.Tracking.IrisRadiusPixLeft = (int)(EyeTracker.Settings.Tracking.MaxIrisPixRad * this.trackBarIrisRadius.Value / 100.0);
            }
            else
            {
                EyeTracker.Settings.Tracking.IrisRadiusPixRight = (int)(EyeTracker.Settings.Tracking.MaxIrisPixRad * this.trackBarIrisRadius.Value / 100.0);
            }
        }

        private void textBoxIrisRadius_TextChanged(object sender, EventArgs e)
        {
            int value = 0;
            if (int.TryParse(this.textBoxIrisRadius.Text, out value))
            {
                value = Math.Max(Math.Min(value, 200), 0);

                if (this.WhichEye == Eye.Left)
                {
                    EyeTracker.Settings.Tracking.IrisRadiusPixLeft = value;
                }
                else
                {
                    EyeTracker.Settings.Tracking.IrisRadiusPixRight = value;
                }
            }
        }


        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void TrackBarTopEyelid_Scroll(object sender, EventArgs e)
        {
            EyeTracker.Settings.Tracking.TopEyelid = this.trackBarTopEyelid.Value / 100.0;
        }

        private void textBoxTopEyelid_TextChanged(object sender, EventArgs e)
        {
            int value = 0;
            if (int.TryParse(this.textBoxTopEyelid.Text, out value))
            {
                value = Math.Max(Math.Min(value, 255), 0);

                EyeTracker.Settings.Tracking.TopEyelid = value;
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void TrackBarBottomEyelid_Scroll(object sender, EventArgs e)
        {
            EyeTracker.Settings.Tracking.BottomEyelid = this.trackBarBottomEyelid.Value / 100.0;
        }

        private void textBoxBottomEyelid_TextChanged(object sender, EventArgs e)
        {
            int value = 0;
            if (int.TryParse(this.textBoxBottomEyelid.Text, out value))
            {
                value = Math.Max(Math.Min(value, 255), 0);

                EyeTracker.Settings.Tracking.BottomEyelid = value;
            }
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void TrackBarReflectionThreshold_Scroll(object sender, EventArgs e)
        {
            if (this.WhichEye == Eye.Left)
            {
                EyeTracker.Settings.Tracking.BrightThresholdLeftEye = this.trackBarReflectionThreshold.Value;
            }
            else
            {
                EyeTracker.Settings.Tracking.BrightThresholdRightEye = this.trackBarReflectionThreshold.Value;
            }
        }

        private void textBoxReflectionThreshold_TextChanged(object sender, EventArgs e)
        {
            int value = 0;
            if (int.TryParse(this.textBoxReflectionThreshold.Text, out value))
            {
                value = Math.Max(Math.Min(value, 255), 0);

                if (this.WhichEye == Eye.Left)
                {
                    EyeTracker.Settings.Tracking.BrightThresholdLeftEye = value;
                }
                else
                {
                    EyeTracker.Settings.Tracking.BrightThresholdRightEye = value;
                }
            }
        }
        


    }
}
