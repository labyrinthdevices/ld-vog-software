﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using OculomotorLab.VOG.UI;

// IDEAS from
// http://www.ageektrapped.com/blog/using-the-command-pattern-in-windows-forms-clients/
// http://www.codeguru.com/csharp/.net/net_general/patterns/article.php/c15663/Implement-a-Command-Pattern-Using-C.htm

namespace OculomotorLab.VOG
{
    public enum EyeTrackerCommandName
    {
        StartTracking,
        ProcessVideo,
        Stop,
        
        PlayVideo,
        
        StartCancelCalibration,
        StartContinueCalibration,
        CancelCalibration,
        ResetReference,
        ResetCalibration,
        LoadCalibration,
        SaveCalibration,

        StartStopRecording,
    }

    public class EyeTrackerCommandManager
    {
        public IDictionary<EyeTrackerCommandName, EyeTrackerCommand> commands;

        public EyeTrackerCommandManager(EyeTracker eyeTracker)
        {
            this.commands = new Dictionary<EyeTrackerCommandName, EyeTrackerCommand>();

            commands.Add(EyeTrackerCommandName.StartContinueCalibration, new StartContinueCalibrationCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.StartCancelCalibration, new StartCancelCalibrationCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.CancelCalibration, new CancelCalibrationCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.StartStopRecording, new StartStopRecordingCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.StartTracking, new StartTrackingCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.PlayVideo, new PlayVideoCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.ProcessVideo, new ProcessVideoCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.Stop, new StopCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.LoadCalibration, new LoadCalibrationCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.SaveCalibration, new SaveCalibrationCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.ResetReference, new ResetReferenceCommand(eyeTracker));
            commands.Add(EyeTrackerCommandName.ResetCalibration, new ResetCalibrationCommand(eyeTracker));

            Application.Idle += Application_Idle;
        }

        void Application_Idle(object sender, EventArgs e)
        {
            foreach (var command in this.commands)
            {
                command.Value.Enabled = command.Value.CanExecute();
            }
        }
    }

    public abstract class EyeTrackerCommand : INotifyPropertyChanged
    {
        protected bool enabled;
        protected EyeTrackerCommandName key;
        protected string displayName;

        protected EyeTracker eyeTracker;

        public EyeTrackerCommand(string displayName, EyeTrackerCommandName key, EyeTracker eyeTracker)
        {
            this.displayName = displayName;
            this.key = key;
            this.eyeTracker = eyeTracker;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public EyeTrackerCommandName Key
        {
            get { return key; }
            protected set { key = value; }
        }

        public string DisplayName
        {
            get { return displayName; }
            protected set
            {
                if (displayName != value)
                {
                    displayName = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("DisplayName"));
                }
            }
        }

        public bool Enabled
        {
            get { return enabled; }
            set
            {
                if (enabled != value)
                {
                    enabled = value;
                    OnPropertyChanged(new PropertyChangedEventArgs("Enabled"));
                }
            }
        }

        public abstract bool CanExecute();

        public abstract void Execute();

        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, e);
        }

        public void Bind(object o)
        {
            if (o is System.Windows.Forms.Control)
            {
                var control = o as System.Windows.Forms.Control;

                control.Text = this.DisplayName;
                control.Enabled = this.Enabled;
                control.Click += delegate { this.Execute(); };

                this.PropertyChanged += (ob, e) =>
                {
                    control.Text = this.DisplayName;
                    control.Enabled = this.Enabled;
                };
                return;
            }

            if (o is System.Windows.Forms.ToolStripMenuItem)
            {
                var control = o as System.Windows.Forms.ToolStripMenuItem;

                control.Text = this.DisplayName;
                control.Enabled = this.Enabled;
                control.Click += delegate { this.Execute(); };

                this.PropertyChanged += (ob, e) =>
                {
                    control.Text = this.DisplayName;
                    control.Enabled = this.Enabled;
                };
                return;
            }

            throw new InvalidOperationException("Not posible to bind");
        }
    }

    public class StartTrackingCommand : EyeTrackerCommand
    {
        public StartTrackingCommand(EyeTracker eyeTracker)
            : base( "Start Tracking",  EyeTrackerCommandName.StartTracking, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            return this.eyeTracker.Idle;
        }

        public override void Execute()
        {
            this.eyeTracker.Start();
        }
    }

    public class PlayVideoCommand : EyeTrackerCommand
    {
        public PlayVideoCommand(EyeTracker eyeTracker)
            :base("Play video", EyeTrackerCommandName.PlayVideo, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            return this.eyeTracker.Idle;
        }

        public override void Execute()
        {
            var options = SelectVideoDialog.SelectVideo(false);

            if (options == null)
            {
                return;
            }

            this.eyeTracker.PlayVideo(options);
        }
    }

    public class ProcessVideoCommand : EyeTrackerCommand
    {
        public ProcessVideoCommand(EyeTracker eyeTracker)
            :base("Process video", EyeTrackerCommandName.ProcessVideo, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            return this.eyeTracker.Idle;
        }

        public override void Execute()
        {
            var options = SelectVideoDialog.SelectVideo(true) as ProcessVideoOptions;

            if (options == null)
            {
                return;
            }

            this.eyeTracker.ProcessVideo(options);
        }
    }

    public class StopCommand : EyeTrackerCommand
    {
        public StopCommand(EyeTracker eyeTracker)
            :base("Stop", EyeTrackerCommandName.Stop, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            return this.eyeTracker.Tracking || this.eyeTracker.PostProcessing;
        }

        public override void Execute()
        {
            this.eyeTracker.Stop();
        }
            }

    public class StartStopRecordingCommand : EyeTrackerCommand
    {
        public StartStopRecordingCommand(EyeTracker eyeTracker)
            :base("Record", EyeTrackerCommandName.StartStopRecording, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            if (this.eyeTracker.RecordingManager.Recording )
            {
                this.DisplayName = "Stop Recording";
                
                if (!this.eyeTracker.PostProcessing)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                this.DisplayName = "Start Recording";
                return this.eyeTracker.Tracking;
            }
        }

        public override void Execute()
        {
            if (!this.eyeTracker.RecordingManager.Recording)
            {
                // TODO: the first frame to record should not be the current frame number.
                // That frame may be gone already.

                this.eyeTracker.RecordingManager.StartRecording(
                    this.eyeTracker.ImageGrabber.FrameRate,
                    this.eyeTracker.ImageGrabber.FrameSize,
                    new Range(),
                    EyeTracker.Settings.RecordVideo,
                    true,
                    true,
                    false,
                    null,
                    EyeTracker.Settings.RecordAudio);
            }
            else
            {
                this.eyeTracker.RecordingManager.StopRecording();
            }
        }
    }

    public class StartCancelCalibrationCommand : EyeTrackerCommand
    {
        public StartCancelCalibrationCommand(EyeTracker eyeTracker)
            :base("Start Calibration", EyeTrackerCommandName.StartCancelCalibration, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            if (this.eyeTracker.CalibrationManager == null)
            {
                return false;
            }

            if (this.eyeTracker.CalibrationManager.Calibrating)
            {
                this.DisplayName = "Cancel Calibration";
                return true;
            }
            else
            {
                this.DisplayName = "Start Calibration";
                return this.eyeTracker.Tracking;
            }
        }

        public override void Execute()
        {
            if (!this.eyeTracker.CalibrationManager.Calibrating)
            {
                this.eyeTracker.CalibrationManager.StartCalibration();
            }
            else
            {
                this.eyeTracker.CalibrationManager.StopCalibration();
            }
        }
    }
    
    public class StartContinueCalibrationCommand : EyeTrackerCommand
    {
        public StartContinueCalibrationCommand(EyeTracker eyeTracker)
            : base("Start Calibration", EyeTrackerCommandName.StartContinueCalibration, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            if (this.eyeTracker.CalibrationManager == null)
            {
                return false;
            }

            if (this.eyeTracker.CalibrationManager.Calibrating)
            {
                if (!this.eyeTracker.CalibrationManager.Waiting)
                {

                    return false;
                }
                else
                {
                    if (this.DisplayName.IndexOf('.') != -1 && this.DisplayName.Substring(0, this.DisplayName.IndexOf('.') - 2) == "Calibrating point")
                    {
                        this.DisplayName = "Look at point " + (int.Parse(this.DisplayName.Substring(this.DisplayName.Length - 4, 1)) + 1);
                    }
                    
                    return true;
                }
                
            }
            else
            {
                this.DisplayName = "Start Calibration";
                return this.eyeTracker.Tracking;
            }
        }

        public override void Execute()
        {
            if (!this.eyeTracker.CalibrationManager.Calibrating)
            {
                    


                this.eyeTracker.CalibrationManager.StartCalibration();
                this.DisplayName = "Calibrating point 1...";
            }
            else
            {


                this.eyeTracker.CalibrationManager.ContinueCalibration();
                if (this.DisplayName.Substring(0, this.DisplayName.Length - 2) == "Look at point")
                {
                    this.DisplayName = "Calibrating point " + this.DisplayName[this.DisplayName.Length-1] + "...";
                }
            }
        }
    }

    public class CancelCalibrationCommand : EyeTrackerCommand
    {
        public CancelCalibrationCommand(EyeTracker eyeTracker)
            : base("Cancel Calibration", EyeTrackerCommandName.CancelCalibration, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            if (this.eyeTracker.CalibrationManager == null)
            {
                return false;
            }

            if (this.eyeTracker.CalibrationManager.Calibrating)
            {
                return true;
            }
            else
            {
                return this.eyeTracker.Tracking;
            }
        }

        public override void Execute()
        {
            if (this.eyeTracker.CalibrationManager.Calibrating)
            {
                this.eyeTracker.CalibrationManager.StopCalibration();
            }
        }
    }
    
    public class ResetCalibrationCommand : EyeTrackerCommand
    {
        public ResetCalibrationCommand(EyeTracker eyeTracker)
            :base("Reset Calibration", EyeTrackerCommandName.ResetReference, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            return this.eyeTracker.Tracking && !this.eyeTracker.CalibrationManager.Calibrating;
        }

        public override void Execute()
        {
            this.eyeTracker.CalibrationManager.ResetCalibration();
        }
    }

    public class ResetReferenceCommand : EyeTrackerCommand
    {
        public ResetReferenceCommand(EyeTracker eyeTracker)
            :base("Reset Reference", EyeTrackerCommandName.ResetReference, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            return this.eyeTracker.Tracking;
        }

        public override void Execute()
        {
            this.eyeTracker.CalibrationManager.ResetReference();
        }
    }

    public class LoadCalibrationCommand : EyeTrackerCommand
    {
        public LoadCalibrationCommand(EyeTracker eyeTracker)
            :base("Load Calibration", EyeTrackerCommandName.LoadCalibration, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            if (this.eyeTracker.CalibrationManager == null)
            {
                return false;
            }

            return !this.eyeTracker.CalibrationManager.Calibrating;
        }

        public override void Execute()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.AddExtension = true;
            dialog.Filter = "Calibration files (*.cal)|*.cal";
            var result = dialog.ShowDialog();
            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            this.eyeTracker.CalibrationManager.Load(dialog.FileName);
        }
    }

    public class SaveCalibrationCommand : EyeTrackerCommand
    {
        public SaveCalibrationCommand(EyeTracker eyeTracker)
            :base("Save Calibration", EyeTrackerCommandName.SaveCalibration, eyeTracker)
        {
        }

        public override bool CanExecute()
        {
            if (this.eyeTracker.CalibrationManager == null)
            {
                return false;
            }

            return !this.eyeTracker.CalibrationManager.Calibrating;
        }

        public override void Execute()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.Filter = "Calibration files (*.cal)|*.cal";
            var result = dialog.ShowDialog();
            if (result != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            this.eyeTracker.CalibrationManager.Save(dialog.FileName);
        }
    }

    /// <summary>
    /// Options for the play video command.
    /// </summary>
    public class PlayVideoOptions
    {
        /// <summary>
        /// Gets or sets the names and full paths of the video files.
        /// </summary>
        public EyeCollection<string> VideoFileNames { get; set; }

        /// <summary>
        /// Gets or sets the  names and full paths of the calibration file.
        /// </summary>
        public string CalibrationFileName { get; set; }

        /// <summary>
        /// Gets or sets the type of camera system used to record the videos.
        /// </summary>
        public string EyeTrackingSystem { get; set; }

        /// <summary>
        /// Gets or sets the range of frames to be played. Beginning and end.
        /// </summary>
        public Range CustomRange { get; set; }
    }

    /// <summary>
    /// Options for the process video command.
    /// </summary>
    public class ProcessVideoOptions : PlayVideoOptions
    {
        /// <summary>
        /// Gets or sets a value indicating whether the processed video should be saved or only the data.
        /// </summary>
        public bool SaveProcessedVideo { get; set; }
    }
}
