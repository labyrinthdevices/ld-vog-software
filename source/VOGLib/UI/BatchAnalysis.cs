﻿//-----------------------------------------------------------------------
// <copyright file="BatchAnalysis.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    /// <summary>
    /// Class to do batch processing of multiple files.
    /// </summary>
    public partial class BatchProcessing : Form
    {
        /// <summary>
        /// EyeTracker main object.
        /// </summary>
        private EyeTracker eyeTracker;

        /// <summary>
        /// List of options for each processed file.
        /// </summary>
        private List<ProcessVideoOptions> optionsList;

        int currentItem = 0;

        /// <summary>
        /// Initializes a new instance of the BatchProcessing class.
        /// </summary>
        /// <param name="eyeTracker">Main eye tracker object.</param>
        public BatchProcessing(EyeTracker eyeTracker)
        {
            if (eyeTracker == null)
            {
                throw new ArgumentNullException("eyeTracker");
            }

            this.eyeTracker = eyeTracker;
            this.eyeTracker.ProcessingVideoFinished += EyeTracker_VideoFinished;

            this.InitializeComponent();
        }

        /// <summary>
        /// Handles the event VideoFinished.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void EyeTracker_VideoFinished(object sender, EventArgs e)
        {
            if (this.listView1.Items.Count == 0)
            {
                return;
            }
            else
            {
                this.BeginInvoke((Action)(() =>
                {

                    this.listView1.Items[this.currentItem].SubItems[2].Text = "Complete";

                    this.currentItem++;

                    if (this.currentItem < this.optionsList.Count)
                    {
                        this.listView1.Items[this.currentItem].SubItems[2].Text = "Processing";
                        var options = (ProcessVideoOptions)this.optionsList.ElementAt(this.currentItem);
                        this.eyeTracker.ProcessVideo(options);
                    }
                    else
                    {
                        this.panel1.Enabled = true;
                    }
                }));
            }
        }

        /// <summary>
        /// Adds a new item to the batch processing.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            var options = (ProcessVideoOptions)SelectVideoDialog.SelectVideo(true);

            if (options == null)
            {
                return;
            }

            var item = new ProcessItem(options);
            listView1.Items.Add(item);
        }

        /// <summary>
        /// Removes an item from the batch processing list.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void ButtonRemove_Click(object sender, EventArgs e)
        {
            foreach (var item in listView1.SelectedItems)
            {
                listView1.Items.Remove((ListViewItem)item);
            }
        }

        /// <summary>
        /// Starts the batch processing.
        /// </summary>
        /// <param name="sender">Sender of the event.</param>
        /// <param name="e">Event arguments.</param>
        private void ButtonStart_Click(object sender, EventArgs e)
        {
            if (listView1.Items.Count > 0)
            {
                this.optionsList = new List<ProcessVideoOptions>();

                foreach (var item in listView1.Items)
                {
                    this.optionsList.Add(((ProcessItem)item).Options);
                }

                this.currentItem = 0;
                this.listView1.Items[this.currentItem].SubItems[2].Text = "Processing";
                var options = (ProcessVideoOptions)this.optionsList.ElementAt(this.currentItem);
                this.eyeTracker.ProcessVideo(options);
            }

            this.panel1.Enabled = false;
        }

        private void buttonLoadBatchDescription_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Batch description files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;

            var result = openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                string[] lines = System.IO.File.ReadAllLines(openFileDialog1.FileName);

                var folder = lines[0];
                var system = lines[1];
                var calibrationFile = lines[2];

                for (int i = 3; i < lines.Length; i++)
                {
                    if (lines[i].Length > 0)
                    {
                        var options = new ProcessVideoOptions();
                        options.EyeTrackingSystem = system;
                        options.CalibrationFileName = Path.Combine(folder, Path.GetFileName(calibrationFile));
                        options.VideoFileNames = new EyeCollection<string>(
                            Path.Combine(folder, Path.GetFileName(lines[i] + "-Left.avi")),
                            Path.Combine(folder, Path.GetFileName(lines[i] + "-Right.avi")));
                        //options.CustomRange = ;

                        options.SaveProcessedVideo = false;

                        var item = new ProcessItem(options);
                        this.listView1.Items.Add(item);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Batch processing items.
    /// </summary>
    [Serializable]
    internal class ProcessItem : ListViewItem
    {
        /// <summary>
        /// Initializes a new instance of the ProcessItem class.
        /// </summary>
        /// <param name="options">Options for the batch processing of the recording.</param>
        public ProcessItem(ProcessVideoOptions options)
            : base(options.VideoFileNames[0])
        {
            System.Diagnostics.Debug.Assert(options != null, "options should not be null.");

            this.Options = options;
            this.SubItems.Add(options.CalibrationFileName);
            this.SubItems.Add("Pending");
        }

        /// <summary>
        /// Gets the options for the batch processing of one recording.
        /// </summary>
        public ProcessVideoOptions Options { get; private set; }
    }
}
