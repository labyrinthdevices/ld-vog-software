﻿namespace OculomotorLab.VOG.UI
{
    partial class SelectVideoDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileLeftButton = new System.Windows.Forms.Button();
            this.systemComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.file1TextBox = new System.Windows.Forms.TextBox();
            this.file2TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fileRightButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.acceptButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.textBoxCalibration = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonCalibrationLeftEye = new System.Windows.Forms.Button();
            this.checkBoxSaveProcessedVideo = new System.Windows.Forms.CheckBox();
            this.textBoxToFrame = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxFromFrame = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxCustomRange = new System.Windows.Forms.CheckBox();
            this.panelCustomRange = new System.Windows.Forms.Panel();
            this.panelCustomRange.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileLeftButton
            // 
            this.fileLeftButton.Location = new System.Drawing.Point(541, 26);
            this.fileLeftButton.Name = "fileLeftButton";
            this.fileLeftButton.Size = new System.Drawing.Size(33, 23);
            this.fileLeftButton.TabIndex = 0;
            this.fileLeftButton.Tag = "Left";
            this.fileLeftButton.Text = "...";
            this.fileLeftButton.UseVisualStyleBackColor = true;
            this.fileLeftButton.Click += new System.EventHandler(this.File1Button_Click);
            // 
            // systemComboBox
            // 
            this.systemComboBox.FormattingEnabled = true;
            this.systemComboBox.Location = new System.Drawing.Point(118, 97);
            this.systemComboBox.Name = "systemComboBox";
            this.systemComboBox.Size = new System.Drawing.Size(262, 21);
            this.systemComboBox.TabIndex = 1;
            this.systemComboBox.SelectedIndexChanged += new System.EventHandler(this.systemComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Left eye video file";
            // 
            // file1TextBox
            // 
            this.file1TextBox.Location = new System.Drawing.Point(118, 28);
            this.file1TextBox.Name = "file1TextBox";
            this.file1TextBox.Size = new System.Drawing.Size(417, 20);
            this.file1TextBox.TabIndex = 3;
            // 
            // file2TextBox
            // 
            this.file2TextBox.Location = new System.Drawing.Point(118, 54);
            this.file2TextBox.Name = "file2TextBox";
            this.file2TextBox.Size = new System.Drawing.Size(417, 20);
            this.file2TextBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Right eye video file";
            // 
            // fileRightButton
            // 
            this.fileRightButton.Location = new System.Drawing.Point(541, 52);
            this.fileRightButton.Name = "fileRightButton";
            this.fileRightButton.Size = new System.Drawing.Size(33, 23);
            this.fileRightButton.TabIndex = 4;
            this.fileRightButton.Tag = "Right";
            this.fileRightButton.Text = "...";
            this.fileRightButton.UseVisualStyleBackColor = true;
            this.fileRightButton.Click += new System.EventHandler(this.File1Button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Recording system";
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(418, 322);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 23);
            this.acceptButton.TabIndex = 8;
            this.acceptButton.Text = "Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(499, 322);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // textBoxCalibration
            // 
            this.textBoxCalibration.Location = new System.Drawing.Point(118, 141);
            this.textBoxCalibration.Name = "textBoxCalibration";
            this.textBoxCalibration.Size = new System.Drawing.Size(417, 20);
            this.textBoxCalibration.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(41, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Calibration file";
            // 
            // buttonCalibrationLeftEye
            // 
            this.buttonCalibrationLeftEye.Location = new System.Drawing.Point(541, 139);
            this.buttonCalibrationLeftEye.Name = "buttonCalibrationLeftEye";
            this.buttonCalibrationLeftEye.Size = new System.Drawing.Size(33, 23);
            this.buttonCalibrationLeftEye.TabIndex = 10;
            this.buttonCalibrationLeftEye.Text = "...";
            this.buttonCalibrationLeftEye.UseVisualStyleBackColor = true;
            this.buttonCalibrationLeftEye.Click += new System.EventHandler(this.ButtonCalibrationLeftEye_Click);
            // 
            // checkBoxSaveProcessedVideo
            // 
            this.checkBoxSaveProcessedVideo.AutoSize = true;
            this.checkBoxSaveProcessedVideo.Location = new System.Drawing.Point(118, 250);
            this.checkBoxSaveProcessedVideo.Name = "checkBoxSaveProcessedVideo";
            this.checkBoxSaveProcessedVideo.Size = new System.Drawing.Size(132, 17);
            this.checkBoxSaveProcessedVideo.TabIndex = 16;
            this.checkBoxSaveProcessedVideo.Text = "Save processed video";
            this.checkBoxSaveProcessedVideo.UseVisualStyleBackColor = true;
            // 
            // textBoxToFrame
            // 
            this.textBoxToFrame.Location = new System.Drawing.Point(235, 3);
            this.textBoxToFrame.Name = "textBoxToFrame";
            this.textBoxToFrame.Size = new System.Drawing.Size(101, 20);
            this.textBoxToFrame.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(177, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "To frame:";
            // 
            // textBoxFromFrame
            // 
            this.textBoxFromFrame.Location = new System.Drawing.Point(77, 3);
            this.textBoxFromFrame.Name = "textBoxFromFrame";
            this.textBoxFromFrame.Size = new System.Drawing.Size(94, 20);
            this.textBoxFromFrame.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "From frame:";
            // 
            // checkBoxCustomRange
            // 
            this.checkBoxCustomRange.AutoSize = true;
            this.checkBoxCustomRange.Location = new System.Drawing.Point(118, 183);
            this.checkBoxCustomRange.Name = "checkBoxCustomRange";
            this.checkBoxCustomRange.Size = new System.Drawing.Size(96, 17);
            this.checkBoxCustomRange.TabIndex = 21;
            this.checkBoxCustomRange.Text = "Custom Range";
            this.checkBoxCustomRange.UseVisualStyleBackColor = true;
            this.checkBoxCustomRange.CheckedChanged += new System.EventHandler(this.CheckBoxCustomRange_CheckedChanged);
            // 
            // panelCustomRange
            // 
            this.panelCustomRange.Controls.Add(this.textBoxFromFrame);
            this.panelCustomRange.Controls.Add(this.label7);
            this.panelCustomRange.Controls.Add(this.textBoxToFrame);
            this.panelCustomRange.Controls.Add(this.label6);
            this.panelCustomRange.Enabled = false;
            this.panelCustomRange.Location = new System.Drawing.Point(118, 203);
            this.panelCustomRange.Name = "panelCustomRange";
            this.panelCustomRange.Size = new System.Drawing.Size(353, 30);
            this.panelCustomRange.TabIndex = 22;
            // 
            // SelectVideoDialog
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(589, 357);
            this.Controls.Add(this.panelCustomRange);
            this.Controls.Add(this.checkBoxCustomRange);
            this.Controls.Add(this.checkBoxSaveProcessedVideo);
            this.Controls.Add(this.textBoxCalibration);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonCalibrationLeftEye);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.file2TextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fileRightButton);
            this.Controls.Add(this.file1TextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.systemComboBox);
            this.Controls.Add(this.fileLeftButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SelectVideoDialog";
            this.Text = "Select video";
            this.Load += new System.EventHandler(this.SelectVideoDialog_Load);
            this.panelCustomRange.ResumeLayout(false);
            this.panelCustomRange.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button fileLeftButton;
        private System.Windows.Forms.ComboBox systemComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox file1TextBox;
        private System.Windows.Forms.TextBox file2TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button fileRightButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox textBoxCalibration;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonCalibrationLeftEye;
        private System.Windows.Forms.CheckBox checkBoxSaveProcessedVideo;
        private System.Windows.Forms.TextBox textBoxToFrame;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxFromFrame;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBoxCustomRange;
        private System.Windows.Forms.Panel panelCustomRange;
    }
}