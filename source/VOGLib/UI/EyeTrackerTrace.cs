﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerTrace.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Windows.Forms.DataVisualization.Charting;
    using OculomotorLab.VOG;

    /// <summary>
    /// Control to show the traces of the data.
    /// </summary>
    public partial class EyeTrackerTrace : UserControl
    {
        /// <summary>
        /// Last frame updated left eye.
        /// </summary>
        private long lastFrameUpdated = -1;

        #region Data series fields

        /// <summary>
        /// Series of data for the horizontal traces.
        /// </summary>
        private EyeCollection<TraceSeries> seriesH;

        /// <summary>
        /// Series of data for the vertical traces.
        /// </summary>
        private EyeCollection<TraceSeries> seriesV;

        /// <summary>
        /// Series of data for the torsion trace.
        /// </summary>
        private EyeCollection<TraceSeries> seriesT;

        /// <summary>
        /// Series of data for the left size trace.
        /// </summary>
        private EyeCollection<TraceSeries> seriesP;

        /// <summary>
        /// Series of data for the lid opening.
        /// </summary>
        private EyeCollection<TraceSeries> seriesL;

        /// <summary>
        /// Series of data for the head data.
        /// </summary>
        private TraceSeries seriesHeadZ;
        private TraceSeries seriesHeadY;
        private TraceSeries seriesHeadX;

        /// <summary>
        /// Series of data for the accelerometer data.
        /// </summary>
        private TraceSeries seriesAccelZ;
        private TraceSeries seriesAccelY;
        private TraceSeries seriesAccelX;

        /// <summary>
        /// Series of data for the GPIO data.
        /// </summary>
        private TraceSeries seriesGPIO;

        #endregion

        #region Chart area fields

        /// <summary>
        /// ChartArea for the horizontal traces.
        /// </summary>
        private TraceChartArea chartAreaHorizontal;

        /// <summary>
        /// ChartArea for the vertical traces.
        /// </summary>
        private TraceChartArea chartAreaVertical;

        /// <summary>
        /// ChartArea for the torsion traces.
        /// </summary>
        private TraceChartArea chartAreaTorsion;

        /// <summary>
        /// ChartArea for the pupil size traces.
        /// </summary>
        private TraceChartArea chartAreaPupil;

        /// <summary>
        /// ChartArea for the eyelid position traces.
        /// </summary>
        private TraceChartArea chartAreaEyelid;

        /// <summary>
        /// ChartArea for the head traces.
        /// </summary>
        private TraceChartArea chartAreaHead;

        /// <summary>
        /// ChartArea for the acceleration traces.
        /// </summary>
        private TraceChartArea chartAreaAccel;

        /// <summary>
        /// ChartArea for the head traces.
        /// </summary>
        private TraceChartArea chartAreaGPIO;

        #endregion

        public bool Initialized { get; private set; }

        /// <summary>
        /// Initializes a new instance of the EyeTrackerTrace class.
        /// </summary>
        public EyeTrackerTrace()
        {
            this.Initialized = false;
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes the graph.
        /// </summary>
        public void Init()
        {
            this.chart1.SuspendLayout();

            this.chartAreaHorizontal = new TraceChartArea(this.chart1, "H", "Horizontal");
            this.seriesH = new EyeCollection<TraceSeries>(
                new TraceSeries(this.chart1, this.chartAreaHorizontal, Eye.Left),
                new TraceSeries(this.chart1, this.chartAreaHorizontal, Eye.Right));

            this.chartAreaVertical = new TraceChartArea(this.chart1, "V", "Vertical");
            this.seriesV = new EyeCollection<TraceSeries>(
                new TraceSeries(this.chart1, this.chartAreaVertical, Eye.Left),
                new TraceSeries(this.chart1, this.chartAreaVertical, Eye.Right));

            this.chartAreaTorsion = new TraceChartArea(this.chart1, "T", "Torsion");
            this.seriesT = new EyeCollection<TraceSeries>(
                new TraceSeries(this.chart1, this.chartAreaTorsion, Eye.Left),
                new TraceSeries(this.chart1, this.chartAreaTorsion, Eye.Right));

            this.chartAreaPupil = new TraceChartArea(this.chart1, "P", "Pupil");
            this.chartAreaPupil.AxisY.Maximum = 200;
            this.chartAreaPupil.AxisY.Minimum = 0;
            this.chartAreaPupil.ShouldZoom = false;
            this.seriesP = new EyeCollection<TraceSeries>(
                new TraceSeries(this.chart1, this.chartAreaPupil, Eye.Left),
                new TraceSeries(this.chart1, this.chartAreaPupil, Eye.Right));

            this.chartAreaEyelid = new TraceChartArea(this.chart1, "L", "EyeLids");
            this.chartAreaEyelid.AxisY.Maximum = 110;
            this.chartAreaEyelid.AxisY.Minimum = -10;
            this.chartAreaEyelid.ShouldZoom = false;
            this.seriesL = new EyeCollection<TraceSeries>(
                new TraceSeries(this.chart1, this.chartAreaEyelid, Eye.Left),
                new TraceSeries(this.chart1, this.chartAreaEyelid, Eye.Right));

            this.chartAreaHead = new TraceChartArea(this.chart1, "D", "Head Vel");

            /*this.seriesHead = new EyeCollection<TraceSeries>(
                new TraceSeries(this.chart1, this.chartAreaHead, 0),
                new TraceSeries(this.chart1, this.chartAreaHead, 1),
                new TraceSeries(this.chart1, this.chartAreaHead, 2));*/

            this.seriesHeadZ = new TraceSeries(this.chart1, this.chartAreaHead, 0);
            this.seriesHeadY = new TraceSeries(this.chart1, this.chartAreaHead, 1);
            this.seriesHeadX = new TraceSeries(this.chart1, this.chartAreaHead, 2);


            this.chartAreaAccel = new TraceChartArea(this.chart1, "A", "Accel");

            this.seriesAccelZ = new TraceSeries(this.chart1, this.chartAreaAccel, 0);
            this.seriesAccelY = new TraceSeries(this.chart1, this.chartAreaAccel, 1);
            this.seriesAccelX = new TraceSeries(this.chart1, this.chartAreaAccel, 2);

            this.chartAreaGPIO = new TraceChartArea(this.chart1, "G", "GPIO");
            this.seriesGPIO = new TraceSeries(this.chart1, this.chartAreaGPIO, Eye.Right);

            this.UpdateLayout();
            this.UpdateVisibleCharts();

            this.chart1.ResumeLayout();

            this.Initialized = true;


            foreach (var chartarea in this.chart1.ChartAreas)
            {
                if (((TraceChartArea)chartarea).ShouldZoom)
                {
                    ((TraceChartArea)chartarea).UpdateRangeOffset(20);
                }
            }
        }

        /// <summary>
        /// Updates the traces with new data.
        /// </summary>
        /// <param name="dataBuffer">Data buffer.</param>
        /// <param name="currentFrameNumber">Current frame number.</param>
        public void Update(EyeTrackerData[] dataBuffer, long currentFrameNumber)
        {
            this.chart1.SuspendLayout();

            this.UpdateLayout();

            if (this.lastFrameUpdated > currentFrameNumber)
            {
                this.lastFrameUpdated = -1;
            }

            var firstFrameToUpdate = (long)Math.Max(lastFrameUpdated + 1, currentFrameNumber - EyeTracker.Settings.TraceSpan);

            int currentX = 0;
            for (long frameNumber = firstFrameToUpdate; frameNumber <= currentFrameNumber; frameNumber++)
            {
                long frameIdx = 0;
                Math.DivRem(frameNumber, dataBuffer.Length, out frameIdx);
                Math.DivRem((int)frameNumber, (int)EyeTracker.Settings.TraceSpan, out currentX);

                var qualityLabels = new EyeCollection<ToolStripLabel>(this.labelDataQualityLeft, this.labelDataQualityRight);
                if (dataBuffer[frameIdx] != null)
                {
                    foreach (var eye in new Eye[] { Eye.Left, Eye.Right })
                    {
                        var calibratedData = dataBuffer[frameIdx].EyeDataCalibrated[eye];
                        var rawData = dataBuffer[frameIdx].EyeDataRaw[eye];

                        if (double.IsNaN(calibratedData.DataQuality))
                        {
                            this.seriesH[eye].SetPoint(currentX, rawData.Pupil.Center.X, rawData.DataQuality);
                            this.seriesV[eye].SetPoint(currentX, rawData.Pupil.Center.Y, rawData.DataQuality);
                            this.seriesT[eye].SetPoint(currentX, rawData.TorsionAngle, rawData.DataQuality);
                        }
                        else
                        {
                            this.seriesH[eye].SetPoint(currentX, calibratedData.HorizontalPosition, calibratedData.DataQuality);
                            this.seriesV[eye].SetPoint(currentX, calibratedData.VerticalPosition, calibratedData.DataQuality);
                            this.seriesT[eye].SetPoint(currentX, calibratedData.TorsionalPosition, calibratedData.DataQuality);
                            this.seriesL[eye].SetPoint(currentX, calibratedData.PercentOpening, calibratedData.DataQuality);
                            this.seriesP[eye].SetPoint(currentX, calibratedData.PupilArea, calibratedData.DataQuality);
                        }
                        
                        qualityLabels[eye].Text = string.Format("{0} {1} {2:0}", "Left", " Q: ", calibratedData.DataQuality / 10);
                        qualityLabels[eye].ForeColor = (calibratedData.DataQuality < 50) ? Color.Red : Color.DarkGreen;
                    }

                    if (dataBuffer[frameIdx].HeadDataRaw.DataResult == HeadData.Result.Normal)
                    {
                        this.seriesHeadX.Points[currentX].IsEmpty = false;
                        this.seriesHeadX.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.GyroZ, double.MaxValue);
                        this.seriesHeadY.Points[currentX].IsEmpty = false;
                        this.seriesHeadY.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.GyroY, double.MaxValue);
                        this.seriesHeadZ.Points[currentX].IsEmpty = false;
                        this.seriesHeadZ.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.GyroX, double.MaxValue);
                        this.seriesGPIO.Points[currentX].IsEmpty = false;
                        this.seriesGPIO.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.GPIO0, double.MaxValue);

                        this.seriesAccelX.Points[currentX].IsEmpty = false;
                        this.seriesAccelX.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.AccelerometerZ, double.MaxValue);
                        this.seriesAccelY.Points[currentX].IsEmpty = false;
                        this.seriesAccelY.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.AccelerometerY, double.MaxValue);
                        this.seriesAccelZ.Points[currentX].IsEmpty = false;
                        this.seriesAccelZ.SetPoint(currentX, dataBuffer[frameIdx].HeadDataRaw.AccelerometerX, double.MaxValue);

                    }
                    else
                    {
                        this.seriesHeadX.Points[currentX].IsEmpty = true;
                        this.seriesHeadY.Points[currentX].IsEmpty = true;
                        this.seriesHeadZ.Points[currentX].IsEmpty = true;
                        this.seriesAccelX.Points[currentX].IsEmpty = true;
                        this.seriesAccelY.Points[currentX].IsEmpty = true;
                        this.seriesAccelZ.Points[currentX].IsEmpty = true;
                        this.seriesGPIO.Points[currentX].IsEmpty = true;
                    }


                }
            }

            this.chartAreaHorizontal.MoveCursor(currentX);
            this.chartAreaVertical.MoveCursor(currentX);
            this.chartAreaTorsion.MoveCursor(currentX);
            this.chartAreaHead.MoveCursor(currentX);
            this.chartAreaAccel.MoveCursor(currentX);
            this.chartAreaGPIO.MoveCursor(currentX);

            this.lastFrameUpdated = currentFrameNumber;

            this.chart1.ResumeLayout();
        }

        private void UpdateLayout()
        {
            if (EyeTracker.Settings != null)
            {
                var timeSpan = 1000.0;
                if (double.TryParse(this.toolStripComboBoxSpan.Text, out timeSpan))
                {
                    if (timeSpan > 5000)
                    {
                        timeSpan = 5000;
                    }

                    if (timeSpan != EyeTracker.Settings.TraceSpan)
                    {
                        EyeTracker.Settings.TraceSpan = timeSpan;
                        this.toolStripComboBoxSpan.Text = EyeTracker.Settings.TraceSpan.ToString();

                        foreach (TraceChartArea traceChartArea in this.chart1.ChartAreas)
                        {
                            traceChartArea.AxisX.Minimum = 0;
                            traceChartArea.AxisX.Maximum = timeSpan;
                        }
                    }
                }
                else
                {
                    timeSpan = EyeTracker.Settings.TraceSpan;
                    this.toolStripComboBoxSpan.Text = timeSpan.ToString();

                    foreach (TraceChartArea traceChartArea in this.chart1.ChartAreas)
                    {
                        traceChartArea.AxisX.Minimum = 0;
                        traceChartArea.AxisX.Maximum = timeSpan;
                    }
                }
            }
        }

        private void UpdateVisibleCharts()
        {
            this.chartAreaHorizontal.Visible = this.horizontalToolStripMenuItem.Checked;
            this.chartAreaVertical.Visible = this.verticalToolStripMenuItem.Checked;
            this.chartAreaTorsion.Visible = this.torsionToolStripMenuItem.Checked;
            this.chartAreaHead.Visible = this.headToolStripMenuItem.Checked;
            this.chartAreaAccel.Visible = this.accelToolStripMenuItem.Checked;
            this.chartAreaGPIO.Visible = this.GPIOToolStripMenuItem.Checked;
            this.chartAreaPupil.Visible = this.pupilToolStripMenuItem.Checked;
            this.chartAreaEyelid.Visible = this.eyeLidsToolStripMenuItem.Checked;

            int totalCharts = 0;
            foreach (var chartarea in this.chart1.ChartAreas)
            {
                if (chartarea.Visible == true)
                {
                    totalCharts++;
                }
            }

            int nChart = 0;
            ChartArea firstChart = null;
            ChartArea lastChart = null;
            foreach (var chartarea in this.chart1.ChartAreas)
            {
                var height = 100 / (float)totalCharts;
                if (chartarea.Visible == true)
                {
                    chartarea.Position = new ElementPosition(0f, height * nChart, 100f, height);
                    if (firstChart == null)
                    {
                        firstChart = chartarea;
                    }
                    else
                    {
                        chartarea.AlignWithChartArea = firstChart.Name;
                    }

                    nChart++;

                    lastChart = chartarea;
                    chartarea.AxisX.LabelStyle.Enabled = false;
                }

            }

            lastChart.AxisX.LabelStyle.Enabled = true;
        }

        private void ZoomIn()
        {

            foreach (TraceChartArea chartarea in this.chart1.ChartAreas)
            {
                if (chartarea.ShouldZoom)
                {
                    chartarea.ZoomInYaxis();
                }
            }
        }

        private void ZoomOut()
        {
            foreach (TraceChartArea chartarea in this.chart1.ChartAreas)
            {
                if (chartarea.ShouldZoom)
                {
                    chartarea.ZoomOutYaxis();
                }
            }

        }

        private void ZoomIn(TraceChartArea chartarea)
        {
            chartarea.ZoomInYaxis();
        }

        private void ZoomOut(TraceChartArea chartarea)
        {
            chartarea.ZoomOutYaxis();
        }

        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="sender">Object that generated the event.</param>
        /// <param name="e">Event arguments.</param>
        private void Chart1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                HitTestResult result = chart1.HitTest(e.X, e.Y);

                if (result.ChartArea.Name.Equals("H"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomIn(chartarea);
                }
                else if (result.ChartArea.Name.Equals("V"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomIn(chartarea);
                }
                else if (result.ChartArea.Name.Equals("T"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomIn(chartarea);
                }
                else if (result.ChartArea.Name.Equals("D"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomIn(chartarea);
                }
                else if (result.ChartArea.Name.Equals("A"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomIn(chartarea);
                }
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                HitTestResult result = chart1.HitTest(e.X, e.Y);

                if (result.ChartArea.Name.Equals("H"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomOut(chartarea);
                }
                else if (result.ChartArea.Name.Equals("V"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomOut(chartarea);
                }
                else if (result.ChartArea.Name.Equals("T"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomOut(chartarea);
                }
                else if (result.ChartArea.Name.Equals("D"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomOut(chartarea);
                }
                else if (result.ChartArea.Name.Equals("A"))
                {
                    TraceChartArea chartarea = (TraceChartArea)result.ChartArea;
                    ZoomOut(chartarea);
                }
                //this.ZoomOut();
            }
        }

        private void SelectPlots_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void torsionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void headToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void accelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void GPIOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void pupilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void eyeLidsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void soundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.UpdateVisibleCharts();
        }

        private void toolStripComboBoxSpan_TextUpdate(object sender, EventArgs e)
        {
            int timeSpan = 1000;
            int.TryParse(toolStripComboBoxSpan.Text, out timeSpan);

            if (timeSpan < 100)
            {
                timeSpan = 1000;
            }
            if (timeSpan > 5000)
            {
                timeSpan = 5000;
            }
            this.toolStripComboBoxSpan.Text = timeSpan.ToString();
            EyeTracker.Settings.TraceSpan = timeSpan;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            using (var ms = new System.IO.MemoryStream())
            {
                this.chart1.SaveImage(ms, ChartImageFormat.Bmp);
                Bitmap bm = new Bitmap(ms);
                Clipboard.SetImage(bm);
            }
        }

        private void toolStripButtonZoomIn_Click(object sender, EventArgs e)
        {
            this.ZoomIn();
        }

        private void toolStripButtonZoomOut_Click(object sender, EventArgs e)
        {
            this.ZoomOut();
        }
    }

    class TraceChartArea : ChartArea
    {
        public bool ShouldZoom = true;

        public TraceChartArea(Chart chart, string name, string yLabel)
            : base(name)
        {
            chart.ChartAreas.Add(this);

            // Set up axes
            this.AxisY.Title = yLabel;
            this.AxisY.TitleFont = new Font(FontFamily.GenericSansSerif, 16);

            this.AxisY.MinorGrid.Enabled = true;

            this.AxisY.MajorGrid.LineColor = Color.Gray;
            this.AxisY.MinorGrid.LineColor = Color.LightGray;
            this.AxisY.LineColor = Color.Gray;

            // Set up cursor
            this.CursorX.LineColor = Color.Black;

        }

        public void MoveCursor(int x)
        {
            this.CursorX.SetCursorPosition(x);
        }

        public void UpdateRangeOffset(double range)
        {
            var max = +range;
            var min = -range;

            this.AxisY.Maximum = max;
            this.AxisY.Minimum = min;

            this.AxisY.MajorTickMark.Interval = max / 2;
            this.AxisY.LabelStyle.Interval = max / 2;
            this.AxisY.MajorGrid.Interval = max / 2;
            this.AxisY.MinorGrid.Interval = max / 10;
        }

        /// <summary>
        /// Zoom in the axis (change vertical scale).
        /// </summary>
        /// <param name="axis">Which axis.</param>
        public void ZoomInYaxis()
        {
            var axis = this.AxisY;

            var offset = (axis.Maximum + axis.Minimum) / 2.0;
            var range = (axis.Maximum - axis.Minimum) / 2.0;

            if (range > 5)
            {
                range /= 2;
            }
            else
            {
                if (range > 2)
                {
                    range -= 1;
                }
                else
                {
                    if (range > .4)
                    {
                        range -= .2;
                    }
                    else
                    {
                        range -= .05;
                    }
                }
            }

            range = Math.Round(range, 2);

            axis.MajorTickMark.Interval = range / 2;
            axis.LabelStyle.Interval = range / 2;
            axis.MajorGrid.Interval = range / 2;
            axis.MinorGrid.Interval = range / 10;

            axis.Maximum = offset + range;
            axis.Minimum = offset - range;
        }

        /// <summary>
        /// Zoom out of the axis (change vertical scale).
        /// </summary>
        /// <param name="axis">Which axis.</param>
        public void ZoomOutYaxis()
        {
            var axis = this.AxisY;

            var offset = (axis.Maximum + axis.Minimum) / 2.0;
            var range = (axis.Maximum - axis.Minimum) / 2.0;

            if (range >= 5)
            {
                range *= 2;
            }
            else
            {
                if (range >= 2)
                {
                    range += 1;
                }
                else
                {
                    if (range >= .4)
                    {
                        range += .2;
                    }
                    else
                    {
                        range += .05;
                    }
                }
            }

            range = Math.Round(range, 2);

            axis.MajorTickMark.Interval = range / 2;
            axis.LabelStyle.Interval = range / 2;
            axis.MajorGrid.Interval = range / 2;
            axis.MinorGrid.Interval = range / 10;

            axis.Maximum = offset + range;
            axis.Minimum = offset - range;
        }
    }

    class TraceSeries : Series
    {
        public TraceSeries(Chart chart, ChartArea chartArea, Eye whichEye)
        {
            chart.Series.Add(this);
            this.ChartArea = chartArea.Name;

            this.ChartType = SeriesChartType.FastLine;
            this.Color = (whichEye == Eye.Left) ? Color.RoyalBlue : Color.Red;
            this.BorderWidth = 2;

            for (int i = 0; i < 5000; i++)
            {
                this.Points.AddXY(i, 0);
            }
        }

        public TraceSeries(Chart chart, ChartArea chartArea, int WhichGyroAxis)
        {
            chart.Series.Add(this);
            this.ChartArea = chartArea.Name;

            this.ChartType = SeriesChartType.FastLine;

            if (WhichGyroAxis == 0)
            {
                this.Color = Color.Red;
            }
            else if (WhichGyroAxis == 1)
            {
                this.Color = Color.Brown;
            }
            else if (WhichGyroAxis == 2)
            {
                this.Color = Color.DarkOrange;
            }
            else if (WhichGyroAxis == 3)
            {
                this.Color = Color.DarkBlue;
            }
            else if (WhichGyroAxis == 4)
            {
                this.Color = Color.DarkGreen;
            }

            this.BorderWidth = 2;

            for (int i = 0; i < 5000; i++)
            {
                this.Points.AddXY(i, 0);
            }
        }

        public void SetPoint(int x, double y, double q)
        {
            if (!double.IsNaN(q))
            {
                this.Points[x].IsEmpty = q < 30;
            }

            if (double.IsNaN(y) || double.IsInfinity(y))
            {
                this.Points[x].IsEmpty = true;
                y = 0;
            }

            this.Points[x].YValues.SetValue(y, 0);
        }
    }
}
