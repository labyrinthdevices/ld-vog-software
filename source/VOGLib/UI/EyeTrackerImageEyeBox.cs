﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerImageEyeBox.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using OculomotorLab.VOG;

    /// <summary>
    /// Control that displays the image of the eye.
    /// </summary>
    public partial class EyeTrackerImageEyeBox : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the EyeTrackerImageEyeBox class.
        /// </summary>
        public EyeTrackerImageEyeBox()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Updates the image in the control.
        /// </summary>
        /// <param name="imageEye">New image to draw.</param>
        /// <param name="thresholdDark">Threshold dark.</param>
        /// <param name="threshdoldBright">Threshold bright.</param>
        public void UpdateImageEyeBox(ProcessedImageEye imageEye, EyePhysicalModel eyeGlobe, int thresholdDark, int threshdoldBright, double topEyelid, double bottomEyelid)
        {
            this.imageBoxEye.SuspendLayout();

            if (imageEye != null)
            {
                // Draw image of the eye with tracking information
                Image<Bgr, byte> imageEyeColor = imageEye.ImageRaw.Convert<Bgr, byte>();
                ProcessedImageEye.DrawData(imageEyeColor, imageEye.EyeData, eyeGlobe, thresholdDark, threshdoldBright, topEyelid, bottomEyelid);

                this.imageBoxEye.Image = imageEyeColor;
            }

            imageBoxEye.ResumeLayout();
        }

        /// <summary>
        /// Resets the image to blank.
        /// </summary>
        public void ResetImage()
        {
            imageBoxEye.Image = null;
        }
    }
}
