﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerSettingsForm.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Windows.Forms;
    using OculomotorLab.VOG;

    /// <summary>
    /// Configuration window.
    /// </summary>
    public partial class EyeTrackerSettingsForm : Form
    {
        /// <summary>
        /// Shows the dialog.
        /// </summary>
        /// <param name="settings">Eye Tracker Settings object.</param>
        public static void Show(EyeTrackerSettings settings)
        {
            try
            {
                var configurationGui = new EyeTrackerSettingsForm(settings);
                configurationGui.Show();
                configurationGui.FormClosed += (o, e) => { configurationGui.Dispose(); };
            }
            catch(Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Initializes a new instance of the EyeTrackerSettingsForm class.
        /// </summary>
        /// <param name="settings">Eye Tracker Settings object.</param>
        private EyeTrackerSettingsForm(EyeTrackerSettings settings)
        {
            this.InitializeComponent();

            this.propertyGridGeneralSettings.SelectedObject = settings;
            this.propertyGridTrackingSettings.SelectedObject = settings.Tracking;
            this.propertyGridSystemSettings.SelectedObject = settings.EyeTrackingSystemSettings;

            settings.PropertyChanged += (o, e) =>
            {
                this.propertyGridGeneralSettings.Refresh();
                this.propertyGridTrackingSettings.Refresh();
                try
                {
                    this.propertyGridSystemSettings.SelectedObject = settings.EyeTrackingSystemSettings;
                }
                catch(Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("Error in EyeTrackerSettingsForm: " + ex.Message);
                }
                this.propertyGridSystemSettings.Refresh();
            };
        }

        /// <summary>
        /// Loads the dialog.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">Event parameters.</param>
        private void EyeTrackerConfiguration_Load(object sender, EventArgs e)
        {
            this.propertyGridGeneralSettings.ToolbarVisible = false;
            this.propertyGridGeneralSettings.PropertySort = PropertySort.CategorizedAlphabetical;

            this.propertyGridTrackingSettings.ToolbarVisible = false;
            this.propertyGridTrackingSettings.PropertySort = PropertySort.CategorizedAlphabetical;
        }
    }
}
