﻿//-----------------------------------------------------------------------
// <copyright file="NamespaceDoc.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    /// <summary>
    /// The <see cref="OculomotorLab.VOG.UI"/> namespace contains classes for ....
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    public class NamespaceDoc
    {
    }
}