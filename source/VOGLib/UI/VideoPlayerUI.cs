﻿//-----------------------------------------------------------------------
// <copyright file="VideoPlayerUI.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Windows.Forms;

    public partial class VideoPlayerUI : UserControl
    {
        private VideoPlayer videoPlayer;

        public VideoPlayerUI()
        {
            InitializeComponent();
        }

        public event ScrollEventHandler ScrollValueChanged;

        /// <summary>
        /// Update the control with the information from the videoPlayer
        /// </summary>
        /// <param name="videoPlayer"></param>
        public void Update(VideoPlayer videoPlayer)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action<VideoPlayer>(Update), videoPlayer);
            }
            else
            {
                this.videoPlayer = videoPlayer;

                // Update the player bar
                if (this.videoPlayer != null)
                {
                    this.buttonVideoPauseResume.Enabled = true;

                    if (this.videoPlayer.Playing)
                    {
                        this.buttonVideoPauseResume.Text = "Pause";
                    }
                    else
                    {
                        this.buttonVideoPauseResume.Text = "Resume";
                    }

                    this.hScrollBarPlayBack.Maximum = (int)this.videoPlayer.FrameCount;
                    this.hScrollBarPlayBack.SmallChange = 1;
                    this.hScrollBarPlayBack.LargeChange = 10;

                    // Add 1 because they are number from 0
                    this.labelFrameNumber.Text =
                        (this.videoPlayer.LastFrameNumber + 1) +
                        "/" +
                        this.videoPlayer.FrameCount;
                    this.hScrollBarPlayBack.Value = (int)this.videoPlayer.LastFrameNumber;
                }
            }
        }

        private void hScrollBarPlayBack_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.OldValue == e.NewValue)
            {
                return;
            }

            if (this.videoPlayer != null)
            {
                this.videoPlayer.Scroll(e.NewValue);
            }

            this.OnScrollValueChanged(e);
        }

        private void buttonVideoPauseResume_Click(object sender, EventArgs e)
        {
            if (this.videoPlayer.Playing)
            {
                this.videoPlayer.Pause();
            }
            else
            {
                this.videoPlayer.Play();
            }
        }

        protected void OnScrollValueChanged(ScrollEventArgs e)
        {
            var handler = this.ScrollValueChanged;
            if ( handler != null)
            {
                handler(this, e);
            }
        }
    }
}
