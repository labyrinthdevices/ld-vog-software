﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

namespace OculomotorLab.VOG
{
    public enum ConsumerState
    {
        Idle,
        Running,
        Finished,
    }

    class Consumer<T>
    {
        public delegate void ComsumeDelegate(T item);

        private Thread backgroundThread;
        private BlockingCollection<NumberedItem<T>> buffer;
        private ComsumeDelegate Consume;

        public Consumer(ComsumeDelegate consumeDelegate, int bufferSize)
        {
            this.Consume = consumeDelegate;

            this.firstItemNumberAdded = -1;
            this.lastItemNumberAdded = -1;
            this.firstItemConsumed = -1;
            this.lastItemConsumed = -1;

            this.buffer = new BlockingCollection<NumberedItem<T>>(bufferSize);

            this.State = ConsumerState.Running;
        }

        public event EventHandler<ErrorOccurredEventArgs> ErrorOcurred;

        public event EventHandler FinishedConsuming;

        public ConsumerState State { get; private set; }

        public long firstItemNumberAdded { get; private set; }
        public long lastItemNumberAdded { get; private set; }
        public long firstItemConsumed { get; private set; }
        public long lastItemConsumed { get; private set; }

        public int DroppedCount { get; private set; }
        public int AddedCount { get; private set; }
        public int BufferCount { get; private set; }

        public void Start()
        {
            this.backgroundThread = new Thread(new ThreadStart(this.ConsumerLoop));
            this.backgroundThread.Name = "EyeTracker:Consumer" + typeof(T).Name;
            this.backgroundThread.Start();
        }

        public void Stop()
        {
            if (this.buffer != null)
            {
                this.buffer.CompleteAdding();
            }

            if (this.buffer != null)
            {
                if (this.backgroundThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId)
                {
                    // Wait for the thread to finish.
                    // this will make sure all Items are saved before disposing objects.
                    this.backgroundThread.Join();
                }
            }
        }

        public void WaitFinish(int timeOut)
        {
            this.backgroundThread.Join(timeOut);
        }

        public bool TryAdd(T item, long itemNumber)
        {

            if (this.State == ConsumerState.Running && !this.buffer.IsAddingCompleted)
            {
                if (this.firstItemNumberAdded == -1)
                {
                    this.firstItemNumberAdded = itemNumber;
                }

                this.lastItemNumberAdded = itemNumber;

                // Add the Item images to the input queue for processing
                if (this.buffer.TryAdd(new NumberedItem<T>(item, itemNumber)))
                {
                    this.AddedCount++;
                    return true;
                }
                else
                {
                    // If the buffer is full return false
                    this.DroppedCount++;
                    return false;
                }
            }

            return false;
        }

        public void Add(T item, long itemNumber)
        {
            if (this.State == ConsumerState.Running  && !this.buffer.IsAddingCompleted)
            {
                if (this.firstItemNumberAdded == -1)
                {
                    this.firstItemNumberAdded = itemNumber;
                }

                this.lastItemNumberAdded = itemNumber;

                // Add the Item images to the input queue for processing
                this.buffer.Add(new NumberedItem<T>(item, itemNumber));

                this.AddedCount++;
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOcurred;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the event FinishedConsuming.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected void OnFinishedConsuming(EventArgs e)
        {
            var handler = this.FinishedConsuming;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void ConsumerLoop()
        {
            var token = new CancellationTokenSource();

            try
            {
                // Keep processing images until the buffer is marked as complete and empty
                foreach (var item in this.buffer.GetConsumingEnumerable(token.Token))
                {
                    if ( this.firstItemConsumed < 0 )
                    {
                        this.firstItemConsumed = item.ItemNumber;
                    }

                    this.lastItemConsumed = item.ItemNumber;

                    this.Consume(item.ItemData);
                }
            }
            catch (Exception ex)
            {
                this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
            }

            this.State = ConsumerState.Finished;

            System.Diagnostics.Trace.WriteLine(string.Format("Finished {0} consumer  loop : {1} frames queued; Queued: {2}-{3}, Saved: {4}-{5}", typeof(T).Name, this.AddedCount, this.firstItemNumberAdded, this.lastItemNumberAdded, this.firstItemConsumed, this.lastItemConsumed));
            this.OnFinishedConsuming(new EventArgs());
        }

        public class NumberedItem<T>
        {
            public NumberedItem(T itemData, long itemNumber)
            {
                this.ItemNumber = itemNumber;
                this.ItemData = itemData;
            }

            public long ItemNumber;
            public T ItemData;
        }
    }

}
