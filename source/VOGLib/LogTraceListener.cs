﻿//-----------------------------------------------------------------------
// <copyright file="LogTraceListener.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2015 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Diagnostics;

    /// <summary>
    /// Listener for Trace messages.
    /// </summary>
    public class LogTraceListener : TextWriterTraceListener
    {
        /// <summary>
        /// Initializes a new instance of the LogTraceListener class.
        /// </summary>
        public LogTraceListener()
            : base(
                string.Format(@"{0}\EyeTrackerLog-{1}.Log",
                    System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                    DateTime.Now.ToString("yyyyMMMdd-HHmmss")))
        {
        }

        /// <summary>
        /// Event raised when there is an event to log.
        /// </summary>
        public event EventHandler<NewLogMessageEventArgs> NewLogMessage;

        /// <summary>
        /// Writes a log message.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        public override void Write(string message)
        {
            message = DateTime.Now.ToString("HH:mm:ss") + " - " + message;

            this.OnNewLogMessage(new NewLogMessageEventArgs(message));

            base.Write(message);
        }

        /// <summary>
        /// Writes a log message with a line break at the end.
        /// </summary>
        /// <param name="message">Message to be logged.</param>
        public override void WriteLine(string message)
        {
            this.Write(message + Environment.NewLine);
        }

        /// <summary>
        /// Fire the NewLogMessage event.
        /// </summary>
        /// <param name="e">Event information.</param>
        protected void OnNewLogMessage(NewLogMessageEventArgs e)
        {
            var handler = this.NewLogMessage;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }

    /// <summary>
    /// Arguments to be sent when the event NewLogMessage is raised.
    /// </summary>
    public class NewLogMessageEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the NewLogMessageEventArgs class.
        /// </summary>
        /// <param name="message">Log message.</param>
        internal NewLogMessageEventArgs(string message)
        {
            this.Message = message;
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        public string Message { get; private set; }
    }
}
