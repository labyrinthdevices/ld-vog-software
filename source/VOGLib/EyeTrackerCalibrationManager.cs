﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerCalibration.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using System.Collections.Generic;
    using System.Collections.Concurrent;
    using System.Threading;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows.Forms;

    /// <summary>
    /// Class that contains all the calibration information.
    /// </summary>
    /// <remarks>Objects of this class can be saved to disk.</remarks>
    [Serializable]

    public class EyeTrackerCalibrationManager
    {

            /// <summary>
        /// Current state of the calibration.
            /// </summary>
        private EyeCalibrationImplementationState state;

        private EyeCalibrationImplementationState previousState;

        private CancellationTokenSource cancelTokens;

        /// <summary>
        /// Thread that runs the processing asynchronously.
        /// </summary>
        private Thread calibrationThread;

        /// <summary>
        /// Buffer of images to be processed.
        /// </summary>
        private BlockingCollection<EyeTrackerDataAndImages> inputQueue;

        /// <summary>
        /// User interface of the calibration.
        /// </summary>
        public ICalibrationUI CalibrationUI
        {
            get
            {
                if (this.implementation == null)
                {
                    return null;
                }

                return this.implementation.UserInterface;
            }
        }

        /// <summary>
        /// Actual implementation of the calibration.
        /// </summary>
        private IEyeCalibrationImplementation implementation;

        /// <summary>
        /// Image grabber that provides with the images.
        /// </summary>
        private EyeTrackerImageGrabber imageGrabber;

        /// <summary>
        /// Initializes a new instance of the EyeTrackerCalibrationManager class.
        /// </summary>
        internal EyeTrackerCalibrationManager(EyeTrackerImageGrabber imageGrabber)
        {
            this.state = EyeCalibrationImplementationState.Idle;

            this.imageGrabber = imageGrabber;
            this.imageGrabber.ImageSourcesChanged += (o, e) =>
            {
                // Keep track of when the image sources change because the physical parameters of the camera may change.
                if (imageGrabber.ImageEyeSources.Count == 2)
                {
                    if (imageGrabber.ImageEyeSources[Eye.Left] != null)
                    {
                        this.CalibrationParameters.EyeCalibrationParameters[Eye.Left].ImageSize = imageGrabber.ImageEyeSources[Eye.Left].FrameSize;
                    }

                    if (imageGrabber.ImageEyeSources[Eye.Right] != null)
                    {
                        this.CalibrationParameters.EyeCalibrationParameters[Eye.Right].ImageSize = imageGrabber.ImageEyeSources[Eye.Right].FrameSize;
                    }
                }

                if (imageGrabber.ImageEyeSources.Count == 1)
                {
                    this.CalibrationParameters.EyeCalibrationParameters[Eye.Left].ImageSize = imageGrabber.ImageEyeSources[Eye.Both].FrameSize;
                    this.CalibrationParameters.EyeCalibrationParameters[Eye.Right].ImageSize = imageGrabber.ImageEyeSources[Eye.Both].FrameSize;
                }
            };

            this.inputQueue = new BlockingCollection<EyeTrackerDataAndImages>(
                new ConcurrentQueue<EyeTrackerDataAndImages>(),
                EyeTracker.Settings.ProcessingBufferSize);

            this.ResetCalibration();

            
        }

        /// <summary>
        /// Gets a value indicating wether the calibration is ongoing.
        /// </summary>
        public bool Calibrating
        {
            get
            {
                return this.state != EyeCalibrationImplementationState.Idle;
            }
        }

        public bool Waiting
        {
            get
            {
                return this.state == EyeCalibrationImplementationState.Waiting;
            }
        }

        public bool Finalizing
        {
            get
            {
                return this.state == EyeCalibrationImplementationState.Finalizing;
            }
        }

        /// <summary>
        /// Gets calibration info for each eye.
        /// </summary>
        public CalibrationParameters CalibrationParameters { get; private set; }

        /// <summary>
        /// Gets the last calibration info for each eye.
        /// </summary>
        private CalibrationParameters lastCalibrationParameters;

        /// <summary>
        /// Event raised when the calibration is complete.
        /// </summary>
        public event EventHandler<EventArgs> CalibrationCompleted;

        /// <summary>
        /// Event raised when the calibration is complete.
        /// </summary>
        public event EventHandler<EventArgs> CalibrationAborted;

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        public event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        /// <summary>
        /// Loads a calibration object from a file.
        /// </summary>
        /// <param name="fileName">File with the calibration.</param>
        /// <returns>Calibration object.</returns>
        public void Load(string fileName)
        {
            this.CalibrationParameters = CalibrationParameters.Load(fileName);
            
            // Load the tracking settings from the calibration file to make sure the
            // new images are processed with the same parameters as the calibration
            EyeTracker.Settings.Tracking = this.CalibrationParameters.TrackingSettings;
        }

        /// <summary>
        /// Saves the calibration info to disk.
        /// </summary>
        /// <param name="fileName">File name where the calibration will be saved.</param>
        public void Save(string fileName)
        {
            if (!Directory.Exists(Path.GetDirectoryName(fileName)))
            {
                System.Diagnostics.Trace.WriteLine("Calibration could not be saved. The folder does not exist.");
                return;
            }

            try
            {
                using (var fileStream = File.Create(fileName))
                {
                    BinaryFormatter serializer = new BinaryFormatter();
                    serializer.Serialize(fileStream, this.CalibrationParameters);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error saving the calibration file.", ex);
            }
        }

        /// <summary>
        /// Starts a new calibration.
        /// </summary>
        public void StartCalibration()
        {
            if(this.implementation == null)
            this.implementation = EyeTrackingPluginLoader.Loader.eyeCalibrationImplementationFactory.Create(EyeTracker.Settings.CalibrationMethod);

            // Save the last calibration parameters just in case the calibration is canceled
            lastCalibrationParameters = this.CalibrationParameters;

            // Reset the calibration parameters. Important to use the raw position of the pupil during 
            // the calibration of the eye model.
            this.CalibrationParameters = new CalibrationParameters();

            // Save the settings.
            this.CalibrationParameters.TrackingSettings = EyeTracker.Settings.Tracking;

            // this.state = EyeCalibrationImplementationState.CalibratingEyeModel;
            if (!this.implementation.PrepareCalibration())
            {
                System.Diagnostics.Trace.WriteLine("Calibration: preparation failed");
                return;
        }

            //if (this.calibrationThread == null)
            //{
                this.calibrationThread = new Thread(new ThreadStart(this.CalibrationLoop));
                this.calibrationThread.Name = "EyeTracker:CalibrationThread";
            //}

            cancelTokens = new CancellationTokenSource();
            
            this.calibrationThread.Start();

            this.state = this.implementation.state;
        }

        public void ContinueCalibration()
        {
            this.implementation.ContinueCalibration();
            this.state = this.previousState;
        }

        /// <summary>
        /// Starts a new calibration
        /// </summary>
        public void StopCalibration()
        {
            this.CalibrationParameters = this.lastCalibrationParameters;
            this.implementation.FinalizeCalibration(true);
            if (this.implementation.state != EyeCalibrationImplementationState.Idle)
            {
                System.Diagnostics.Trace.WriteLine("An error occured, cannot finalize calibration implmentation.");
        }
            cancelTokens.Cancel();
            this.calibrationThread.Join();
            this.state = EyeCalibrationImplementationState.Idle;
        }

        /// <summary>
        /// Resets the calibration.
        /// </summary>
        public void ResetCalibration()
        {
            if (this.implementation == null)
            {
                this.implementation = EyeTrackingPluginLoader.Loader.eyeCalibrationImplementationFactory.Create(EyeTracker.Settings.CalibrationMethod);
            }

            this.CalibrationParameters = new CalibrationParameters();
        }

        /// <summary>
        /// Resets the zero reference.
        /// </summary>
        public void ResetReference()
        {
            // Save the last calibration parameters just in case the calibration is canceled
            lastCalibrationParameters = this.CalibrationParameters;

            if (this.implementation != null)
            {
                this.implementation.EyeCalibrationParameters[Eye.Left].SetReference(null);

                this.implementation.EyeCalibrationParameters[Eye.Right].SetReference(null);
            }

            this.state = EyeCalibrationImplementationState.ResettingReference;
            this.calibrationThread = new Thread(new ThreadStart(this.CalibrationLoop));
            this.calibrationThread.Name = "EyeTracker:CalibrationThread";
            //}

            cancelTokens = new CancellationTokenSource();

            this.calibrationThread.Start();
        }

        /// <summary>
        /// Processes new data.
        /// </summary>
        /// <param name="newData">New data from the last frame.</param>
        internal void ProcessNewDataAndImages(EyeTrackerDataAndImages newData)
        {
            if (!this.Calibrating || this.Waiting)
                return;
            if (this.inputQueue.IsAddingCompleted)
            {
                throw new InvalidOperationException("The processing has already been stopped, cannot process more frames.");
            }

            // Add the frame images to the input queue for processing
            this.inputQueue.TryAdd(newData);
        }

        /// <summary>
        /// Gets the calibrated data from the raw data.
        /// </summary>
        /// <param name="eyeData">Raw data.</param>
        /// <returns>Calibrated data.</returns>
        public CalibratedEyeData GetCalibratedEyeData(EyeData eyeData)
        {
            var calibrationInfo = this.CalibrationParameters.EyeCalibrationParameters[eyeData.WhichEye];

            if (eyeData == null)
            {
                throw new ArgumentNullException("eyeData");
            }

            var data = new CalibratedEyeData();

            data.HorizontalPosition = double.NaN;
            data.VerticalPosition = double.NaN;
            data.TorsionalPosition = double.NaN;

            if (eyeData.ProcessFrameResult == ProcessFrameResult.Good)
            {
                /*var eyeModel = this.CalibrationParameters.EyeCalibrationParameters[eyeData.WhichEye].EyePhysicalModel;
                if (!calibrationInfo.HasEyeModel)
                {
                    eyeModel = new EyePhysicalModel(eyeData.Pupil.Center, (float)(eyeData.Iris.Radius * 2));
                }

                var referenceX = calibrationInfo.ReferenceData.Pupil.Center.X;
                var referenceY = calibrationInfo.ReferenceData.Pupil.Center.Y;
                if (!calibrationInfo.HasReference)
                {
                    referenceX = this.CalibrationParameters.EyeCalibrationParameters[eyeData.WhichEye].ImageSize.Width / 2;
                    referenceY = this.CalibrationParameters.EyeCalibrationParameters[eyeData.WhichEye].ImageSize.Height / 2;
                }

                // Get the angle position relative to the reference
                var referenceXDeg = Math.Asin((referenceX - eyeModel.Center.X) / eyeModel.Radius) * 180 / Math.PI;
                var referenceYDeg = Math.Asin((referenceY - eyeModel.Center.Y) / eyeModel.Radius) * 180 / Math.PI;

                var xDeg = Math.Asin((eyeData.Pupil.Center.X - eyeModel.Center.X) / eyeModel.Radius) * 180 / Math.PI;
                var yDeg = Math.Asin((eyeData.Pupil.Center.Y - eyeModel.Center.Y) / eyeModel.Radius) * 180 / Math.PI;

                data.HorizontalPosition = -(xDeg - referenceXDeg);
                data.VerticalPosition = -(yDeg - referenceYDeg);*/

                data.HorizontalPosition = Polynomial_Surface_Mult(calibrationInfo.HPolynomials, eyeData.Pupil.Center.X, eyeData.Pupil.Center.Y);
                data.VerticalPosition = Polynomial_Surface_Mult(calibrationInfo.VPolynomials, eyeData.Pupil.Center.X, eyeData.Pupil.Center.Y);

                data.TorsionalPosition = eyeData.TorsionAngle;

                if (double.IsNaN(eyeData.TorsionAngle))
                {
                    data.TorsionalPosition = double.NaN;
                }

                // Pupil area in mm2 asuming 24 mm eyeblobe
                if (!calibrationInfo.EyePhysicalModel.IsEmpty)
                {
                    data.PupilArea = (eyeData.Pupil.Size.Width / calibrationInfo.EyePhysicalModel.Radius * 12) * (eyeData.Pupil.Size.Height / calibrationInfo.EyePhysicalModel.Radius * 12) * Math.PI;
                }
                else
                {
                    data.PupilArea = (eyeData.Pupil.Size.Width / eyeData.Iris.Radius * 12) * (eyeData.Pupil.Size.Height / eyeData.Iris.Radius * 12) * Math.PI;
                }

                if (calibrationInfo.HasReference)
                {
                    data.DataQuality = eyeData.DataQuality / calibrationInfo.ReferenceData.DataQuality * 100;

                    if (calibrationInfo.ReferenceData.Eyelids.Upper != null && calibrationInfo.ReferenceData.Eyelids.Lower != null)
                    {
                        data.PercentOpening =
                            (double)((eyeData.Eyelids.Lower[0].Y + eyeData.Eyelids.Lower[1].Y + eyeData.Eyelids.Lower[2].Y + eyeData.Eyelids.Lower[3].Y) -
                            (eyeData.Eyelids.Upper[0].Y + eyeData.Eyelids.Upper[1].Y + eyeData.Eyelids.Upper[2].Y + eyeData.Eyelids.Upper[3].Y))
                            /
                            (double)((calibrationInfo.ReferenceData.Eyelids.Lower[0].Y + calibrationInfo.ReferenceData.Eyelids.Lower[1].Y + calibrationInfo.ReferenceData.Eyelids.Lower[2].Y + calibrationInfo.ReferenceData.Eyelids.Lower[3].Y) -
                            (calibrationInfo.ReferenceData.Eyelids.Upper[0].Y + calibrationInfo.ReferenceData.Eyelids.Upper[1].Y + calibrationInfo.ReferenceData.Eyelids.Upper[2].Y + calibrationInfo.ReferenceData.Eyelids.Upper[3].Y))
                            * 100.0;

                        if (data.PercentOpening > 120)
                        {
                            data.PercentOpening = 120;
                        }
                    }
                }
                else
                {
                    data.DataQuality = double.NaN;
                }
            }

            return data;
        }

        // Polynomial surface multiplication
        private double Polynomial_Surface_Mult(double[] Coeffs, double HPixelVal, double VPixelVal)
        {
            double Deg;

            Deg = Coeffs[0] + Coeffs[1] * HPixelVal + Coeffs[2] * VPixelVal + Coeffs[3] * Math.Pow(HPixelVal, 2) + Coeffs[4] * HPixelVal * VPixelVal + Coeffs[5] * Math.Pow(VPixelVal, 2);

            return Deg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="calibratedEyeData"></param>
        /// <returns></returns>
        public double[] GetRotationVector(CalibratedEyeData calibratedEyeData)
        {
            return null;
        }

        /// <summary>
        /// Gets the calibrated data from the raw data.
        /// </summary>
        /// <param name="headData">Head data.</param>
        /// <returns>Calibrated data.</returns>
        public CalibratedHeadData GetCalibratedHeadData(HeadData headData)
        {
            var data = new CalibratedHeadData();
            //data.Roll = -Math.Asin(Math.Min(159, Math.Max(-159, headData.AccelerometerX - 9230.0)) / 160.0) / Math.PI * 180;
            data.Roll = headData.GyroX;
            //data.Roll = headData.AccelerometerY;
            data.YawVelocity = headData.GyroX;
            data.PitchVelocity = headData.GyroY;
            data.RollVelocity = headData.GyroZ;
            return data;
        }

        /// <summary>
        /// Starts the background thread.
        /// </summary>
        internal void Start()
        {
            //this.calibrationThread = new Thread(new ThreadStart(this.CalibrationLoop));
            //this.calibrationThread.Name = "EyeTracker:CalibrationThread";
            //this.calibrationThread.Start();
        }

        /// <summary>
        /// Calibration loop running on a separate thread.
        /// </summary>
        private void CalibrationLoop()
        {
            // var token = new CancellationTokenSource();

            // Keep processing images until the buffer is marked as complete and empty
            try
            {
                foreach (var data in this.inputQueue.GetConsumingEnumerable(this.cancelTokens.Token))
                {
                    

                try
                {
                        if (this.implementation == null)
                            continue;

                        if(this.state == EyeCalibrationImplementationState.ResettingReference)
                            {
                            if ((EyeTracker.Settings.TrackingMode == Eye.Left || EyeTracker.Settings.TrackingMode == Eye.Both) && data.Images[Eye.Left] != null)
                                {
                                if (!this.implementation.EyeCalibrationParameters[Eye.Left].HasReference)
                                    {
                                    this.implementation.UpdateReference(data.Images[Eye.Left]);
                                    }
                                }
                            if ((EyeTracker.Settings.TrackingMode == Eye.Right || EyeTracker.Settings.TrackingMode == Eye.Both) && data.Images[Eye.Right] != null)
                                {
                                if (!this.implementation.EyeCalibrationParameters[Eye.Right].HasReference)
                                    {
                                    this.implementation.UpdateReference(data.Images[Eye.Right]);
                                    }
                                }
                            this.implementation.FinalizeCalibration(false);

                            System.Diagnostics.Trace.WriteLine("Calibration finished");

                                    this.CalibrationParameters.EyeCalibrationParameters = this.implementation.EyeCalibrationParameters;
                                    EyeTracker.Settings.Tracking.LeftEyeGlobe = this.CalibrationParameters.EyeCalibrationParameters[Eye.Left].EyePhysicalModel;
                                    EyeTracker.Settings.Tracking.RightEyeGlobe = this.CalibrationParameters.EyeCalibrationParameters[Eye.Right].EyePhysicalModel;

                            // Save the calibration to disk
                            var time = DateTime.Now.ToString("yyyyMMMdd-HHmmss");
                            var folder = EyeTracker.Settings.DataFolder;
                            var sessionName = EyeTracker.Settings.SessionName;
                            var calibrationFileName = string.Format(folder + "\\" + sessionName + "-{0}.cal", time);
                            this.Save(calibrationFileName);

                            this.state = EyeCalibrationImplementationState.Idle;
                            // Notify that calibration was successfull
                            this.OnCalibrationCompleted(new EventArgs());
                            break;
                        }

                        var lefteyefinished = true;
                        var righteyefinished = true;
                        if ((EyeTracker.Settings.TrackingMode == Eye.Left || EyeTracker.Settings.TrackingMode == Eye.Both) && !this.implementation.EyeCalibrationParameters[Eye.Left].HasEyeModel
                            && !this.implementation.EyeCalibrationParameters[Eye.Left].HasReference)
                                    {
                            lefteyefinished = false;
                                }

                        if ((EyeTracker.Settings.TrackingMode == Eye.Right || EyeTracker.Settings.TrackingMode == Eye.Both) && !this.implementation.EyeCalibrationParameters[Eye.Right].HasEyeModel
                            && !this.implementation.EyeCalibrationParameters[Eye.Right].HasReference)
                                    {
                            righteyefinished = false;
                                }


                        if (lefteyefinished && righteyefinished)
                                {


                            this.implementation.FinalizeCalibration(false);

                                    System.Diagnostics.Trace.WriteLine("Calibration finished");

                                    this.CalibrationParameters.EyeCalibrationParameters = this.implementation.EyeCalibrationParameters;
                            EyeTracker.Settings.Tracking.LeftEyeGlobe = this.CalibrationParameters.EyeCalibrationParameters[Eye.Left].EyePhysicalModel;
                            EyeTracker.Settings.Tracking.RightEyeGlobe = this.CalibrationParameters.EyeCalibrationParameters[Eye.Right].EyePhysicalModel;

                                    // Save the calibration to disk
                                    var time = DateTime.Now.ToString("yyyyMMMdd-HHmmss");
                                    var folder = EyeTracker.Settings.DataFolder;
                                    var sessionName = EyeTracker.Settings.SessionName;
                                    var calibrationFileName = string.Format(folder + "\\" + sessionName + "-{0}.cal", time);
                                    this.Save(calibrationFileName);

                            this.state = EyeCalibrationImplementationState.Idle;
                                    // Notify that calibration was successfull
                                    this.OnCalibrationCompleted(new EventArgs());
                            break;
                        }

                        

                        switch (this.implementation.state)
                        {
                            case EyeCalibrationImplementationState.Waiting:
                                Thread.Sleep(1);
                                break;
                            case EyeCalibrationImplementationState.CalibratingEyeModel:

                                if (!this.implementation.EyeCalibrationParameters[Eye.Left].HasEyeModel)
                                {
                                    if (data.Images[Eye.Left] != null)
                                    {
                                        this.implementation.UpdateEyeModel(data.Images[Eye.Left]);
                                    }
                                }

                                if (!this.implementation.EyeCalibrationParameters[Eye.Right].HasEyeModel)
                                {
                                    if (data.Images[Eye.Right] != null)
                                    {
                                        this.implementation.UpdateEyeModel(data.Images[Eye.Right]);
                                }
                            }

                            break;

                            
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("Calibration error: " + ex.Message);

                        if (this.state == EyeCalibrationImplementationState.Stopping)
                    {
                        this.implementation = null;
                        this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
                        break;
                    }
                }
                    if (this.implementation.state != this.state)
                    {
                        this.previousState = this.state;
                    }
                    this.state = this.implementation.state;
                }
            }
            catch (OperationCanceledException ex)
            {

            }


            System.Diagnostics.Trace.WriteLine("Calibration loop finished.");
        }

        /// <summary>
        /// Raises the CalibrationCompleted event.
        /// </summary>
        /// <param name="e">Event parameters.</param>
        protected void OnCalibrationCompleted(EventArgs e)
        {
            // Save thesettings everytime something changes
            var handler = this.CalibrationCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the CalibrationAborted event.
        /// </summary>
        /// <param name="e">Event parameters.</param>
        protected void OnCalibrationAborted(EventArgs e)
        {
            // Save thesettings everytime something changes
            var handler = this.CalibrationAborted;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Dispose resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///  Dispose resources.
        /// </summary>
        /// <param name="disposing">True if disposing resources.</param>
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.state = EyeCalibrationImplementationState.Stopping;

                // Do not add more items to the input queue
                if (this.inputQueue != null)
                {
                    this.inputQueue.CompleteAdding();
                }

                // Wait for the processing threads to finish.
                if (this.calibrationThread != null)
                {
                    this.calibrationThread.Join();
                }
            }
        }
    }
}
