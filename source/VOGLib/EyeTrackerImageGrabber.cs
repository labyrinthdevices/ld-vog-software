﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerImageGrabber.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using System.Threading;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using OculomotorLab.VOG.ImageGrabbing;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Image grabber that grabs images in the background and fires an event to
    /// notify that a new image is available.
    /// Different eye tracking systems will provide different image sources. They can also provide a delegate to pre-process images. 
    /// </summary>
    public class EyeTrackerImageGrabber : IDisposable
    {
        /// <summary>
        /// Possible states of the processor.
        /// </summary>
        protected enum ImageGrabberState
        {
            /// <summary>
            /// Before starting to grab.
            /// </summary>
            Idle,

            /// <summary>
            /// Grabbing images.
            /// </summary>
            Grabbing,

            /// <summary>
            /// Signal to stop received, stopping the image sources.
            /// </summary>
            Stopping,

            /// <summary>
            /// Signal received to stop completely and close the thread.
            /// </summary>
            Disposing,
        }

        /// <summary>
        /// State of the grabber.
        /// </summary>
        private ImageGrabberState state;

        /// <summary>
        /// Background thread grabbing the images.
        /// </summary>
        private Thread grabbingThread;

        /// <summary>
        /// Method to be executed after images are captured. This will be system dependent.
        /// </summary>
        private PreProcessImagesDelegate preProcessImagesDelegate;

        /// <summary>
        /// Notifies listeners when a new image is about to be grabbed, the event runs in the grabber thread.
        /// Any event handler should be quick.
        /// </summary>
        internal event EventHandler<EventArgs> ImagesAboutToBeGrabbed;

        /// <summary>
        /// Notifies listeners about a new frame available, the event runs in the grabber thread.
        /// Any event handler should be quick.
        /// </summary>
        public event EventHandler<EyeCollection<ImageEye>> ImagesGrabbed;

        /// <summary>
        /// Event to notify that the image sources have changed.
        /// </summary>
        internal event EventHandler ImageSourcesChanged;

        /// <summary>
        /// Notifies listeners that there was an error.
        /// </summary>
        internal event EventHandler<ErrorOccurredEventArgs> ErrorOccurred;

        /// <summary>
        /// List of sources of images of the eyes. Typically cameras or video files.
        /// </summary>
        internal EyeCollection<IImageEyeSource> ImageEyeSources { get; private set; }
        
        /// <summary>
        /// Gets or sets the frame size in pixels coming from the camera.
        /// </summary>
        public Size FrameSize { get; private set; }

        /// <summary>
        /// Gets or sets the frame rates in frames per second coming from the camera.
        /// </summary>
        public double FrameRate { get; private set; }

        /// <summary>
        /// Frame number of the last image grabbed.
        /// </summary>
        public long CurrentFrameNumber { get; private set; }

        /// <summary>
        /// Distortion Correction Whole Image.
        /// </summary>
        private Matrix<float> Map1, Map2;

        /// <summary>
        /// Gets the diagnostics information.
        /// </summary>
        public string[] Diagnostics
        {
            get
            {
                if (this.ImageEyeSources == null)
                {
                    var text = new string[1];

                    text[0] = "No image sources.";
                    return text;
                }
                else
                {
                    var text = new string[this.ImageEyeSources.Count + 1];

                    var diagnostics = new System.Text.StringBuilder();
                    foreach (var source in this.ImageEyeSources)
                    {
                        if (source != null)
                        {
                            diagnostics.AppendFormat(
                                "{0} {1}x{2}px {3:0.0}Hz Drop:{4}",
                                source.WhichEye.ToString(),
                                source.FrameSize.Width,
                                source.FrameSize.Height,
                                source.ReportedFrameRate,
                                source.DroppedFramesCounter);
                        }
                    }
                    text[0] = diagnostics.ToString();

                    for (int i = 0; i < this.ImageEyeSources.Count; i++)
                    {
                        if (this.ImageEyeSources[i] != null)
                        {
                            text[i + 1] = this.ImageEyeSources[i].Diagnostics;
                        }
                    }

                    return text;
                }

            }
        }

        /// <summary>
        /// Gets a value indicating wether the system can move the cameras.
        /// </summary>
        public bool CanMoveCameras
        {
            get
            {
                if (this.ImageEyeSources == null)
                {
                    return false;
                }

                var movable = true;

                foreach (var camera in this.ImageEyeSources)
                {
                    var movableCamera = camera as IMovableImageEyeSource;

                    if (movableCamera == null)
                    {
                        movable = false;
                    }
                }

                return movable;
            }
        }

        /// <summary>
        /// Corrects the orientation of the image. Useful in cases where the camara is rotated.
        /// Or when there are mirrors.
        /// </summary>
        /// <param name="image">Image to be corrected.</param>
        /// <param name="isMirrored">True if the image is mirrored.</param>
        /// <param name="isUpsideDown">True if the image is upsideDown</param>
        /// <returns></returns>
        public static ImageEye CorrectImageOrientation(ImageEye image, bool isMirrored, bool isUpsideDown)
        {
            // Fix the image
            if (!isMirrored && !isUpsideDown)
            {
                // Do nothing
            }

            if (!isMirrored && isUpsideDown)
            {
                image.FlipHorizontal();
                image.FlipVertical();
            }

            if (isMirrored && !isUpsideDown)
            {
                image.FlipHorizontal();
            }

            if (isMirrored && isUpsideDown)
            {
                image.FlipVertical();
            }

            return image;
        }

        /// <summary>
        /// Starts the grabbing thread.
        /// </summary>
        internal void Start()
        {
            this.state = ImageGrabberState.Grabbing;

            this.LoadDistortionMapping();

            // Start the grabbing thread
            this.grabbingThread = new Thread(new ThreadStart(this.GrabLoop));
            this.grabbingThread.Name = "EyeTracker:GrabLoop";
            this.grabbingThread.Priority = ThreadPriority.Highest;
            this.grabbingThread.Start();
        }

        String CamCalibFilePath = @"C:\VOG\Labyrinth VOG Software\lib\CamCalib\";
        internal void LoadDistortionMapping()
        {
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1.xml");
            Map1 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2.xml");
            Map2 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));
        }

        /// <summary>
        /// Initializes the image grabber.
        /// </summary>
        internal void SetImageEyeSources(EyeCollection<IImageEyeSource> imageEyeSources, PreProcessImagesDelegate preProcessImagesDelegate)
        {
            this.StopGrabbing();

            // Save the method of the delegate used to prepare images depending on the eye tracking system
            this.preProcessImagesDelegate = preProcessImagesDelegate;

            // Get the image sources
            this.ImageEyeSources = imageEyeSources;
            this.CurrentFrameNumber = 0;

            // Verify sources

            Trace.WriteLine("Verifying grabbing system.");

            // Check same frame rate
            if (imageEyeSources.Count == 2)
            {
                if (this.ImageEyeSources[Eye.Left] != null && this.ImageEyeSources[Eye.Right] != null)
                {
                    var leftFrameRate = this.ImageEyeSources[Eye.Left].ReportedFrameRate;
                    var rightFrameRate = this.ImageEyeSources[Eye.Right].ReportedFrameRate;

                    if (Math.Abs(leftFrameRate - rightFrameRate) > 0.5)
                    {
                        throw new InvalidOperationException("The frame rate of the two cameras is not the same");
                    }

                    this.FrameRate = leftFrameRate;
                }

                if (this.ImageEyeSources[Eye.Left] != null && this.ImageEyeSources[Eye.Right] == null)
                {
                    this.FrameRate = this.ImageEyeSources[Eye.Left].ReportedFrameRate;
                }

                if (this.ImageEyeSources[Eye.Left] == null && this.ImageEyeSources[Eye.Right] != null)
                {
                    this.FrameRate = this.ImageEyeSources[Eye.Right].ReportedFrameRate;
                }

            }
            else
            {
                this.FrameRate = this.ImageEyeSources[Eye.Both].ReportedFrameRate;
            }

            // Check same frame size
            if (imageEyeSources.Count == 2)
            {
                if (this.ImageEyeSources[Eye.Left] != null && this.ImageEyeSources[Eye.Right] != null)
                {
                    var leftFrameSize = this.ImageEyeSources[Eye.Left].FrameSize;
                    var rightFrameSize = this.ImageEyeSources[Eye.Right].FrameSize;
                    if (leftFrameSize != rightFrameSize)
                    {
                        throw new InvalidOperationException("The frame rate of the two cameras is not the same");
                    }

                    this.FrameSize = leftFrameSize;
                }
                else
                {
                    if (this.ImageEyeSources[Eye.Left] != null)
                    {
                        this.FrameSize = this.ImageEyeSources[Eye.Left].FrameSize;
                    }
                    if (this.ImageEyeSources[Eye.Right] != null)
                    {

                        this.FrameSize = this.ImageEyeSources[Eye.Right].FrameSize;
                    }
                }
            }
            else
            {
                this.FrameRate = this.ImageEyeSources[Eye.Both].ReportedFrameRate;
                this.FrameSize = new System.Drawing.Size(512, 432);
            }

            this.state = ImageGrabberState.Grabbing;

            this.OnImageSourcesChanged(new EventArgs());
        }

        /// <summary>
        /// Stop the image sources from grabbing images.
        /// </summary>
        internal void StopGrabbing()
        {
            this.state = ImageGrabberState.Stopping;

            // Stop the image sources from capturing more images
            if (this.ImageEyeSources != null)
            {
                foreach (var source in this.ImageEyeSources)
                {
                    if (source != null)
                    {
                        source.StopCapture();
                    }
                }
            }

            this.ImageEyeSources = null;
        }

        /// <summary>
        /// Raises the event NewImagesToBeGrabbed.
        /// </summary>
        /// <param name="e">Current image of the eye.</param>
        protected void OnImagesAboutToBeGrabbed(EventArgs e)
        {
            var handler = this.ImagesAboutToBeGrabbed;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the event NewFrameAvailable.
        /// </summary>
        /// <param name="e">Current image of the eye.</param>
        protected void OnImagesGrabbed(EyeCollection<ImageEye> e)
        {
            var handler = this.ImagesGrabbed;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the event ErrorOccurred.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnErrorOccurred(ErrorOccurredEventArgs e)
        {
            var handler = this.ErrorOccurred;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Raises the event ImageSourcesChanged.
        /// </summary>
        /// <param name="e">Exception.</param>
        protected void OnImageSourcesChanged(EventArgs e)
        {
            var handler = this.ImageSourcesChanged;

            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes resources.
        /// </summary>
        /// <param name="disposing">Value indicating wether it should disposes resources.</param>
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.StopGrabbing();

                // This will stop the grabbing loop
                this.state = ImageGrabberState.Disposing;

                // Wait for thread to stop
                if (this.grabbingThread != null)
                {
                    if (Thread.CurrentThread != this.grabbingThread)
                    {
                        this.grabbingThread.Join();
                    }
                }

                if ( this.ImageEyeSources != null)
                {
                    foreach (var imageSource in this.ImageEyeSources)
                    {
                        if ( imageSource != null)
                        {
                            imageSource.Dispose();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loop that runs in the background thread.
        /// </summary>
        private void GrabLoop()
        {
            while (this.state != ImageGrabberState.Disposing)
            {
                try
                {
                    // Signal that an image is about to be grabbed
                    this.OnImagesAboutToBeGrabbed(new EventArgs());

                    // Grab the image from the image sources
                    var images = this.GrabImages();

                    if (images != null)
                    {
                        //images = PerformDistortionCorrection(images);

                        // Prepare the images for processing depending on the system. For instance
                        // flipping, cropping splitting...
                        images = this.preProcessImagesDelegate(images);

                        // Raise the event
                        this.OnImagesGrabbed(images);
                        continue;
                    }

                    // If there are no images sleep for a bit
                    System.Threading.Thread.Sleep(1);
                }
                catch (Exception ex)
                {
                    // If stopping ignore the error and continue.
                    if (this.state == ImageGrabberState.Stopping)
                    {
                        continue;
                    }

                    // Break the loop if disposing.
                    if (this.state == ImageGrabberState.Disposing)
                    {
                        break;
                    }

                    // In any other case propagate the error.
                    Trace.WriteLine("Error grabbing: " + ex.Message);
                    this.OnErrorOccurred(new ErrorOccurredEventArgs(ex));
                }
            }

            Trace.WriteLine("Grabber loop finished.");
        }

        private Image<Gray, byte> PerformDistortionCorrection(Image<Gray, byte> imageWhole)
        {

            //remap the image to the particular intrinsics
            //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
            //image is mapped backed, in the future this should be controllable through the flag '0'
            Image<Gray, Byte> tempImageWhole = imageWhole.CopyBlank();

            CvInvoke.cvRemap(imageWhole, tempImageWhole, Map1, Map2, 0, new MCvScalar(0));

            return tempImageWhole;
        }

        /// <summary>
        /// Grabs the images from the cameras. This method takes care of dropped frames and the
        /// image source level. It will only return groups of images with the same frame number.
        /// </summary>
        /// <returns>The images of the eyes.</returns>
        private EyeCollection<ImageEye> GrabImages()
        {
            var sources = this.ImageEyeSources;

            if (sources == null)
            {
                return null;
            }

            bool anyEmptybuffer = false;

            // If there is only one camera things are easy, no need to worry about
            // one camera skipping a frame and the other one no
            if (sources.Count == 1)
            {
                var image = sources[Eye.Both].GrabImageEye();
                this.CurrentFrameNumber = image.TimeStamp.FrameNumber;
                return new EyeCollection<ImageEye>(image);
            }
            else
            {
                // Grab the images into buffers
                foreach (var source in sources)
                {
                    if (source != null)
                    {
                        // If the queue has been marked as reset
                        // Empty the queue. This must be done this way
                        // for thread safety. 
                        if (source.queue.ShouldReset)
                        {
                            source.queue.Clear();
                            source.queue.ShouldReset = false;
                        }

                        // Only grab frames for the cameras whos buffer is empty
                        if (source.queue.Count == 0)
                        {
                            var image = source.GrabImageEye();

                            if (image != null)
                            {
                                // Add the image to the buffer
                                source.queue.Enqueue(image);
                            }
                            else
                            {
                                anyEmptybuffer = true;
                            }
                        }
                    }
                }

                // Check if all buffers have images if not go back and grab more images
                if (anyEmptybuffer)
                {
                    return null;
                }

                // The current frame will be the minimum frame number of the frames in the front of all queues
                var minFrameNumber = long.MaxValue;
                foreach (var source in sources)
                {
                    if (source != null)
                    {
                        minFrameNumber = (long)Math.Min(minFrameNumber, source.queue.Peek().TimeStamp.FrameNumber);
                    }
                }

                this.CurrentFrameNumber = minFrameNumber;

                // Get the images corresponding with the current frame number. If any camera dropped
                // that frame fill it with a fake image
                var imageArray = new ImageEye[sources.Count];

                foreach (var source in sources)
                {
                    if (source != null)
                    {
                        var buffer = source.queue;

                        if (buffer.Peek().TimeStamp.FrameNumber == this.CurrentFrameNumber)
                        {
                            imageArray[(int)source.WhichEye] = buffer.Dequeue();
                        }
                        else
                        {
                            ImageEyeTimestamp ts = new ImageEyeTimestamp();
                            ts.FrameNumber = this.CurrentFrameNumber;

                            //// TODO: fix this to create new image or use past, do not like using the future image
                            imageArray[(int)source.WhichEye] = new ImageEye(buffer.Peek().GetImageCopy<Gray, byte>(), source.WhichEye, ts);
                        }
                    }
                }

                return new EyeCollection<ImageEye>(imageArray);
            }
        }
    }

    /// <summary>
    /// Add the eye information to the queues.
    /// </summary>
    /// <typeparam name="T">Type of the elements of the queue</typeparam>
    public class ImageEyeQueue : Queue<ImageEye>
    {
        public bool ShouldReset { get; set; }
    }

    /// <summary>
    /// Delegate for methods tha preproces images.
    /// </summary>
    /// <param name="images">Images to be preprocessed.</param>
    /// <returns>Images after preprocessing.</returns>
    public delegate EyeCollection<ImageEye> PreProcessImagesDelegate(EyeCollection<ImageEye> images);
}
