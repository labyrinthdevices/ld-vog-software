﻿//-----------------------------------------------------------------------
// <copyright file="NamespaceDoc.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.Remote
{
    /// <summary>
    /// The <see cref="OculomotorLab.VOG.Remote"/> namespace contains classes for the implementation of a client/server
    /// architecture to control the eye tracker remotely.
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    public class NamespaceDoc
    {
    }
}