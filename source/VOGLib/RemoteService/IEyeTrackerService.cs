﻿//-----------------------------------------------------------------------
// <copyright file="IEyeTrackerService.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.Remote
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.ServiceModel;

    /// <summary>
    /// Interface contract for the remote interface of the eye tracker.
    /// </summary>
    [ServiceContract(Namespace = "http://oculomotorlab.org/torsion")]
    public interface IEyeTrackerService
    {
        EyeTrackerStatusSummary Status
        {
            [OperationContract]
            get;
        }

        TrackingSettings Settings
        {
            [OperationContract]
            get;
        }

        [OperationContract]
        bool ChangeSetting(string settingName, object value);

        [OperationContract]
        ImagesAndData GetCurrentImagesAndData();

        [OperationContract]
        EyeCalibrationParamteres GetCalibrationParameters();

        [OperationContract]
        void StartRecording();

        [OperationContract]
        void StopRecording();

        [OperationContract]
        void ResetReference();

        [OperationContract]
        void RecordEvent(string message);
    }
    
    public class ImagesAndData
    {
        public EyeCollection<Bitmap> Image;
        public EyeCollection<EyeData> RawData;
        public EyeCollection<CalibratedEyeData> CalibratedData;
    }

    public class EyeCalibrationParamteres
    {
        public EyeCollection<EyePhysicalModel> PhysicalModel;
        public EyeCollection<EyeData> ReferenceData;
    }

    public class EyeTrackerStatusSummary
    {
        public bool Idle;
        public bool Initializing;
        public bool Tracking;
        public bool Processing;

        public bool Recording;

        public bool Calibrating;

        public string GrabberStatus;
        public string ProcessorStatus;
        public string RecorderStatus;
    }
}
