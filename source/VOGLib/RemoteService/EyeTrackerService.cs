﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerService.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.Remote
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.ServiceModel;

    /// <summary>
    /// Service methods to allow remote control of the eye tracker.
    /// </summary>
    internal class EyeTrackerService : IEyeTrackerService
    {
        /// <summary>
        /// Gets or sets the static reference to the eye tracker.
        /// </summary>
        internal static EyeTracker EyeTracker { get; set; }

        /// <summary>
        /// Gets or sets the static reference to the eye tracker.
        /// </summary>
        internal static ServiceHost EyeTrackerHost { get; set; }

        /// <summary>
        /// Starts the service.
        /// </summary>
        /// <param name="eyeTracker">Eye tracker object.</param>
        /// <returns>The service host object.</returns>
        public static void StartService(EyeTracker eyeTracker)
        {
            try
            {
                EyeTrackerService.EyeTracker = eyeTracker;

                var host = new ServiceHost(typeof(EyeTrackerService));
                var binding = new NetTcpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                binding.Security.Mode = SecurityMode.None;
                var e = host.AddServiceEndpoint(
                    typeof(IEyeTrackerService),
                    binding,
                    "net.tcp://localhost:" + EyeTracker.Settings.ServiceListeningPort + "/EyeTrackerEndpoint");
                host.Open();

                EyeTrackerService.EyeTrackerHost = host;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error starting service: " + ex.Message);
            }
        }

        /// <summary>
        /// Stops the service.
        /// </summary>
        public static void StopService()
        {
            try
            {
                if (EyeTrackerService.EyeTrackerHost != null)
                {
                    EyeTrackerService.EyeTrackerHost.Close();
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error closing the service: " + ex.Message);
            }
        }

        public EyeTrackerStatusSummary Status
        {
            get
            {
                var status = new EyeTrackerStatusSummary();

                status.Idle = EyeTracker.Idle;
                status.Tracking = EyeTracker.Tracking;
                status.Initializing = EyeTracker.Initializing;
                status.Processing = EyeTracker.PostProcessing;

                if (EyeTracker.RecordingManager != null)
                {
                    status.Recording = EyeTracker.RecordingManager.Recording;
                }

                if (EyeTracker.CalibrationManager != null)
                {
                    status.Calibrating = EyeTracker.CalibrationManager.Calibrating;
                }

                if (EyeTracker.ProcessingStatusMessage != null)
                {
                    status.ProcessorStatus = EyeTracker.ProcessingStatusMessage;
                }
                if (EyeTracker.ImageGrabber != null)
                {
                    status.GrabberStatus = EyeTracker.ImageGrabber.Diagnostics[0];
                }
                if (EyeTracker.RecordingManager != null)
                {
                    status.RecorderStatus = EyeTracker.RecordingManager.StatusMessage;
                }

                return status;
            }
        }

        public TrackingSettings Settings
        {
            get
            {
                return EyeTracker.Settings.Tracking;
            }
        }

        public void StartRecording()
        {
            try
            {
                EyeTrackerService.EyeTracker.RecordingManager.StartRecording(
                    EyeTrackerService.EyeTracker.ImageGrabber.FrameRate,
                    EyeTrackerService.EyeTracker.ImageGrabber.FrameSize,
                    new Range(),
                    true,
                    true,
                    true,
                    false,
                    null,
                    EyeTracker.Settings.RecordAudio);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error processing remote start recording: " + ex.Message);
            }
        }

        public void StopRecording()
        {
            try
            {
                EyeTrackerService.EyeTracker.RecordingManager.StopRecording();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error processing remote start recording: " + ex.Message);
            }
        }

        public void ResetReference()
        {
            try
            {
                EyeTrackerService.EyeTracker.CalibrationManager.ResetReference();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error processing remote start recording: " + ex.Message);
            }
        }

        public bool ChangeSetting(string settingName, object value)
        {
            try
            {
                if (settingName.IndexOf('.') > 0)
                {
                    var settingParts = settingName.Split('.');
                    switch (settingParts[0])
                    {
                        case "Tracking":
                            typeof(TrackingSettings).GetProperty(settingParts[1]).SetValue(EyeTracker.Settings.Tracking, value, null);
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    typeof(EyeTrackerSettings).GetProperty(settingName).SetValue(EyeTracker.Settings, value, null);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error changing setting remotely: " + ex.Message);
                return false;
            }

            return true;
        }

        public ImagesAndData GetCurrentImagesAndData()
        {
            var imagesAndData = new ImagesAndData();
            
            if (EyeTrackerService.EyeTracker.CurrentData != null)
            {
                try
                {
                    imagesAndData.RawData = new EyeCollection<EyeData>(
                        EyeTrackerService.EyeTracker.CurrentData.Data.EyeDataRaw[Eye.Left],
                        EyeTrackerService.EyeTracker.CurrentData.Data.EyeDataRaw[Eye.Right]);

                    imagesAndData.CalibratedData = new EyeCollection<CalibratedEyeData>(
                        EyeTrackerService.EyeTracker.CurrentData.Data.EyeDataCalibrated[Eye.Left],
                        EyeTrackerService.EyeTracker.CurrentData.Data.EyeDataCalibrated[Eye.Right]
                        );
                    imagesAndData.Image = new EyeCollection<Bitmap>(
                        EyeTrackerService.EyeTracker.CurrentData.Images[Eye.Left].ImageRaw.Bitmap,
                        EyeTrackerService.EyeTracker.CurrentData.Images[Eye.Right].ImageRaw.Bitmap
                        );

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine("Error processing remote request for data: " + ex.Message);
                }
            }

            return imagesAndData;
        }

        public EyeCalibrationParamteres GetCalibrationParameters()
        {
            var calibrationParameters = new EyeCalibrationParamteres();

            try
            {
                if (EyeTrackerService.EyeTracker.CalibrationManager != null)
                {

                    calibrationParameters.PhysicalModel = new EyeCollection<EyePhysicalModel>(
                        EyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Left].EyePhysicalModel,
                        EyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Right].EyePhysicalModel
                        );
                    calibrationParameters.ReferenceData = new EyeCollection<EyeData>(
                        EyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Left].ReferenceData,
                        EyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Right].ReferenceData
                        );
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Error processing remote request for calibration parameters: " + ex.Message);
            }

            return calibrationParameters;
        }

        public void RecordEvent(string message)
        {
            EyeTracker.RecordEvent(message, null);
        }
    }
}
