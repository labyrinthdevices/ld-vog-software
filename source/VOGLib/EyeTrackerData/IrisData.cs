﻿//-----------------------------------------------------------------------
// <copyright file="IrisData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------                            
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using Emgu.CV.Structure;

    /// <summary>
    /// Data structure containing the geometric properties of the measured iris.
    /// </summary>
    [Serializable]
    public struct IrisData
    {
        /// <summary>
        /// Gets or sets the center of the circle.
        /// </summary>
        public PointF Center { get; private set; }

        /// <summary>
        /// Gets or sets the radius of the circle.
        /// </summary>
        public float Radius { get; private set; }

        /// <summary>
        /// Initializes a new instance of the CircleFSerializable class.
        /// </summary>
        /// <param name="center">Center of the circle.</param>
        /// <param name="radius">Radius of the circle.</param>
        public IrisData(PointF center, float radius)
            : this()
        {
            this.Center = center;
            this.Radius = radius;
        }

        /// <summary>
        /// Conversion from IrisData to CircleF.
        /// </summary>
        /// <param name="irisData">Iris Data.</param>
        /// <returns>New CircleF.</returns>
        public static implicit operator CircleF(IrisData irisData)
        {
            return new CircleF(irisData.Center, irisData.Radius);
        }

        /// <summary>
        /// Conversion from CircleF to IrisData.
        /// </summary>
        /// <param name="circle">The circle data.</param>
        /// <returns>The new Iris data.</returns>
        public static implicit operator IrisData(CircleF circle)
        {
            return new IrisData(circle.Center, circle.Radius);
        }
    }
}
