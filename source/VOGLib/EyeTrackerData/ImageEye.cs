﻿//-----------------------------------------------------------------------
// <copyright file="ImageEye.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using System.Runtime.Serialization;
    using System.Security.Permissions;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using OculomotorLab.VOG.ImageProcessing;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Image wrapper to add some eye tracking related functionality.
    /// </summary>
    [Serializable]
    public class ImageEye : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the ImageEye class.
        /// </summary>
        /// <param name="bitmap">Bitmap of the image.</param>
        /// <param name="whichEye">Left or right eye.</param>
        /// <param name="timestamp">Timestamp from the time the image was captured.</param>
        public ImageEye(System.Drawing.Bitmap bitmap, Eye whichEye, ImageEyeTimestamp timestamp)
            : this((Image<Gray, byte>)null, whichEye, timestamp)
        {
            Image<Gray, byte> imageTemp = null;
            try
            {
                imageTemp = new Image<Gray, byte>(bitmap);
                this.Image = imageTemp;
                imageTemp = null;
            }
            finally
            {
                if (imageTemp != null)
                {
                    imageTemp.Dispose();
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the ImageEye class.
        /// </summary>
        /// <param name="image">Image of the eye.</param>
        /// <param name="whichEye">Left or right eye.</param>
        /// <param name="timestamp">Timestamp from the time the image was captured.</param>
        public ImageEye(Image<Gray, byte> image, Eye whichEye, ImageEyeTimestamp timestamp)
        {
            this.Image = image;
            this.WhichEye = whichEye;

            timestamp.DateTimeGrabbed = System.DateTime.Now;
            this.TimeStamp = timestamp;
        }

        /// <summary>
        /// Initializes a new instance of the ImageEye class.
        /// </summary>
        /// <param name="image">Image of the eye.</param>
        public ImageEye(ImageEye image)
            :this(image.Image, image.WhichEye, image.TimeStamp)
        {
        }

        /// <summary>
        /// Initializes a new instance of the ImageEye class from a serialized object (Implementation of ISerializable).
        /// </summary>
        /// <param name="info">Serialization info.</param>
        /// <param name="context">Streaming context.</param>
        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected ImageEye(SerializationInfo info, StreamingContext context)
        {
            this.Image = new Image<Gray, byte>(info, context);

            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            this.WhichEye = (Eye)info.GetValue("WhichEye", this.WhichEye.GetType());
            this.TimeStamp = (ImageEyeTimestamp)info.GetValue("TimeStamp", this.TimeStamp.GetType());
        }

        /// <summary>
        /// Gets the image. Be very careful using this! it may cause lots of problems with threads.
        /// </summary>
        public Image<Gray, byte> Image { get; private set; }

        /// <summary>
        /// Gets Left or Right Eye.
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Distortion Correction Whole Image.
        /// </summary>
        private Matrix<float> Map1, Map2;

        /// <summary>
        /// Gets a value indicating whether the image eye actually has an image.
        /// </summary>
        public bool HasImage
        {
            get
            {
                return this.Image != null;
            }
        }

        /// <summary>
        /// Gets the size of the image of the eye in pixels.
        /// </summary>
        public Size Size
        {
            get
            {
                if (this.Image != null)
                {
                    return this.Image.Size;
                }

                return new Size();
            }
        }

        /// <summary>
        /// Gets the timestamp from the time the image was captured.
        /// </summary>
        public ImageEyeTimestamp TimeStamp { get; private set; }

        /// <summary>
        /// Merges two images into one.
        /// </summary>
        /// <param name="images">Image of the eyes.</param>
        /// <returns></returns>
        public static Image<Bgr, byte> MergeImages(EyeCollection<ProcessedImageEye> images)
        {
            var imageLeft = images[Eye.Left].ImageRaw.Convert<Bgr,byte>();
            var imageRight = images[Eye.Right].ImageRaw.Convert<Bgr, byte>();

            var imgBoth = new Image<Bgr, byte>(new Size(imageLeft.Size.Width * 2, imageLeft.Size.Height));

            if (imageRight != null)
            {
                ProcessedImageEye.DrawCross(imageRight, images[Eye.Right].EyeData);
                imgBoth.ROI = imageRight.ROI;
                imageRight.CopyTo(imgBoth);
            }

            if (imageLeft != null)
            {
                ProcessedImageEye.DrawCross(imageLeft, images[Eye.Left].EyeData);
                imgBoth.ROI = new Rectangle(imageLeft.Width, 0, imageLeft.Size.Width, imageLeft.Size.Height);
                imageLeft.CopyTo(imgBoth);
            }

            imgBoth.ROI = new Rectangle();

            return imgBoth;
        }

        /// <summary>
        /// Rotates the image 180 degrees.
        /// </summary>
        public void Rotate180()
        {
            this.Image = this.Image.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL).Flip(Emgu.CV.CvEnum.FLIP.VERTICAL);
        }

        /// <summary>
        /// Flips the image horizontally.
        /// </summary>
        public void FlipHorizontal()
        {
            this.Image = this.Image.Flip(Emgu.CV.CvEnum.FLIP.HORIZONTAL);
        }

        /// <summary>
        /// Flips the image vertically.
        /// </summary>
        public void FlipVertical()
        {
            this.Image = this.Image.Flip(Emgu.CV.CvEnum.FLIP.VERTICAL);
        }

        /// <summary>
        /// Transposes the image.
        /// </summary>
        public void Transpose()
        {
            var imageTemp = new Image<Gray, byte>(this.Size.Height, this.Size.Width);
            CvInvoke.cvTranspose(this.Image.Ptr, imageTemp.Ptr);
            this.Image = imageTemp;
        }

        /// <summary>
        /// Copies the image.
        /// </summary>
        public Image<Gray, byte> GetCopy()
        {
            return this.Image.Copy();
        }

        /// <summary>
        /// Copies a fragment of the image.
        /// </summary>
        /// <param name="roi">Region of interest limiting the fragment.</param>
        /// <returns>The fragment of the image.</returns>
        public Image<Gray, byte> GetCroppedImage(Rectangle roi)
        {
            return this.Image.Copy(roi);
        }

        String CamCalibFilePath = @"C:\VOG\Labyrinth VOG Software\lib\CamCalib\";
        internal void LoadDistortionMapping()
        {
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1.xml");
            Map1 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2.xml");
            Map2 = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));
        }

        /// <summary>
        /// Performs Distortion Correction
        /// </summary>
        /// <param name="roi">Region of interest limiting the fragment.</param>
        /// <returns>The fragment of the image.</returns>
        public void PerformDistortionCorrection()
        {
            //remap the image to the particular intrinsics
            //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
            //image is mapped backed, in the future this should be controllable through the flag '0'
            Image<Gray, Byte> tempImage = this.Image.CopyBlank();

            CvInvoke.cvRemap(this.Image, tempImage, Map1, Map2, 0, new MCvScalar(0));

            this.Image = tempImage.Copy();
        }

        /// <summary>
        /// Copies a fragment of the image and transposes it.
        /// </summary>
        /// <param name="roi">Region of interest limiting the fragment.</param>
        /// <returns>The fragment of the image.</returns>
        public Image<Gray, byte> GetCroppedTransposedImage(Rectangle roi)
        {
            var imageTemp = new Image<Gray, byte>(roi.Height, roi.Width);
            this.Image.ROI = roi;
            CvInvoke.cvTranspose(this.Image.Ptr, imageTemp.Ptr);
            this.Image.ROI = new Rectangle();

            return imageTemp;
        }

        /// <summary>
        /// Gets a copy of the image resized.
        /// </summary>
        /// <param name="scale">Number of times the resized image is larger than the original image.</param>
        /// <returns>The resized image.</returns>
        public Image<Gray, byte> GetResizedImage(float scale)
        {
            if (this.Image == null)
            {
                return null;
            }

            return this.Image.Resize(scale, Emgu.CV.CvEnum.INTER.CV_INTER_CUBIC);
        }

        /// <summary>
        /// Gets a copy of the image with a new pixel type.
        /// </summary>
        /// <typeparam name="TColor">Type of the color of the pixels, typically bgr or gray.</typeparam>
        /// <typeparam name="TDepth">Type of the pixel, typically byte or float.</typeparam>
        /// <returns>The converted image.</returns>
        public Image<TColor, TDepth> GetImageCopy<TColor, TDepth>()
            where TColor : struct, global::Emgu.CV.IColor
            where TDepth : new()
        {
            return this.Image.Convert<TColor, TDepth>();
        }

        /// <summary>
        /// Gets a binary image with white in the pixels with a value below a threshold.
        /// </summary>
        /// <param name="thresholdDark">The threshold.</param>
        /// <returns>The binary image.</returns>
        public Image<Gray, byte> ThresholdDark(double thresholdDark)
        {
            return this.Image.ThresholdBinary(new Gray(thresholdDark), new Gray(1));
        }

        /// <summary>
        /// Gets a binary image with white in the pixels with a value above a threshold.
        /// </summary>
        /// <param name="thresholdBright">The threshold.</param>
        /// <returns>The binary image.</returns>
        public Image<Gray, byte> ThresholdBright(double thresholdBright)
        {
            return this.Image.ThresholdBinary(new Gray(thresholdBright), new Gray(1));
        }

        /// <summary>
        /// Gets a segment of the image resized and thresholded with the dark pixels.
        /// </summary>
        /// <param name="thresholdDark">The threshold.</param>
        /// <param name="scale">Scaling factor.</param>
        /// <param name="roi">Region of the segment.</param>
        /// <returns>The binary image</returns>
        public Image<Gray,byte> ThresholdDarkResized(double thresholdDark, double scale, Rectangle roi)
        {
            this.Image.ROI = roi;
            var imageThreshold = this.Image.Resize(1/scale, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR).ThresholdBinaryInv(new Gray(thresholdDark), new Gray(255));
            this.Image.ROI = new Rectangle();

            return imageThreshold;
        }

        /// <summary>
        /// Fills in the object information from the stream (Implementation of ISerializable).
        /// </summary>
        /// <param name="info">Serialization info.</param>
        /// <param name="context">Streaming context.</param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("WhichEye", this.WhichEye);
            info.AddValue("TimeStamp", this.TimeStamp);

            this.Image.GetObjectData(info, context);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {

            if (disposing)
            {
                if (this.Image != null)
                {
                    this.Image.Dispose();
                }
            }
        }
    }
}
