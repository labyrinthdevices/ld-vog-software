﻿//-----------------------------------------------------------------------
// <copyright file="ProcessedImageEye.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;
    using OculomotorLab.VOG.ImageProcessing;

    /// <summary>
    /// Image processed, contains the image of the eye, the image of the iris and the data.
    /// </summary>
    [Serializable]
    public class ProcessedImageEye
    {
        /// <summary>
        /// Initializes a new instance of the ProcessedImageEye class.
        /// </summary>
        /// <param name="imageEye">Image of the eye from the current frame.</param>
        /// <param name="imageTorsion">Image of the iris from the current frame.</param>
        /// <param name="eyeData">EyeData from the current frame.</param>
        public ProcessedImageEye(ImageEye imageEye, Image<Gray, byte> imageTorsion, EyeData eyeData)
        {
            this.ImageRaw = imageEye.Image;
            this.WhichEye = imageEye.WhichEye;

            this.EyeData = eyeData;
            this.ImageTorsion = imageTorsion;
        }

        /// <summary>
        /// Initializes a new instance of the ProcessedImageEye class without data. Just with the result (error) of the
        /// processing.
        /// </summary>
        /// <param name="imageEye">Image of the eye from the current frame.</param>
        /// <param name="result">Processing result.</param>
        public ProcessedImageEye(ImageEye imageEye, ProcessFrameResult result)
        {
            this.ImageRaw = imageEye.Image;
            this.WhichEye = imageEye.WhichEye;

            this.EyeData = new EyeData(
                imageEye.WhichEye,
                imageEye.TimeStamp,
                result);
            this.ImageTorsion = null;
        }

        /// <summary>
        /// Initializes a new instance of the ProcessedImageEye class for missing images.
        /// </summary>
        /// <param name="whichEye">Left or Right eye.</param>
        /// <param name="result">Processing result.</param>
        public ProcessedImageEye(Eye whichEye, ProcessFrameResult result)
        {
            this.ImageRaw = null;
            this.WhichEye = whichEye;

            this.EyeData = new EyeData(
                whichEye,
                new ImageEyeTimestamp(),
                result);
        }

        /// <summary>
        /// Gets the raw image as it was captured by the camera. 
        /// Be very careful using this! it may cause lots of problems with threads.
        /// </summary>
        public Image<Gray, byte> ImageRaw { get; private set; }

        /// <summary>
        /// Gets Left or Right Eye.
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets or sets the EyeData.
        /// TODO: the set should be private.
        /// </summary>
        public EyeData EyeData { get; set; }

        /// <summary>
        /// Gets the image of the iris.
        /// </summary>
        public Image<Gray, byte> ImageTorsion { get; private set; }

        /// <summary>
        /// Get the image with the data drawn on it.
        /// </summary>
        /// <param name="thresholdDark">Threshold Dark.</param>
        /// <param name="threshdoldBright">Threshold Bright.</param>
        /// <returns>New image with data.</returns>
        public static void DrawData(Image<Bgr, byte> image, EyeData data, EyePhysicalModel eyeGlobe, double thresholdDark, double threshdoldBright, double topEyelid, double bottomEyelid)
        {
            if (image == null)
            {
                return;
            }

            if (thresholdDark > 0)
            {
                DrawThresdholds(image, thresholdDark, threshdoldBright);

                // Draw eyelids
                DrawEyelids(image, data, eyeGlobe, topEyelid, bottomEyelid);
            }

            DrawEyeGlobe(image, eyeGlobe, false);

            DrawCroppingBox(image);


            DrawPupil(image, data);

            DrawIris(image, data, eyeGlobe);

            DrawCross(image, data);
        }

        public static void DrawCross(Image<Bgr, byte> image, EyeData data)
        {
            var center = new Point((int)(data.Pupil.Center.X), (int)(data.Pupil.Center.Y));
            var color = (data.WhichEye == Eye.Left) ? Color.RoyalBlue : Color.Red;

            var angle = !double.IsNaN(data.TorsionAngle) ? data.TorsionAngle : 0.0;

            // Draw torsion line
            image.Draw(
                new LineSegment2D(
                    new Point(
                        (int)Math.Round((double)center.X - (Math.Sin(angle / 180 * Math.PI) * data.Iris.Radius)),
                        (int)Math.Round((double)center.Y - (Math.Cos(angle / 180 * Math.PI) * data.Iris.Radius))),
                    new Point(
                        (int)Math.Round((double)center.X + (Math.Sin(angle / 180 * Math.PI) * data.Iris.Radius)),
                        (int)Math.Round((double)center.Y + (Math.Cos(angle / 180 * Math.PI) * data.Iris.Radius)))),
                new Bgr(color),
                2);

            // Draw torsion line
            image.Draw(
                new LineSegment2D(
                    new Point(
                        (int)Math.Round((double)center.X - (Math.Cos(angle / 180 * Math.PI) * data.Iris.Radius)),
                        (int)Math.Round((double)center.Y + (Math.Sin(angle / 180 * Math.PI) * data.Iris.Radius))),
                    new Point(
                        (int)Math.Round((double)center.X + (Math.Cos(angle / 180 * Math.PI) * data.Iris.Radius)),
                        (int)Math.Round((double)center.Y - (Math.Sin(angle / 180 * Math.PI) * data.Iris.Radius)))),
                new Bgr(color),
                2);
        }

        public static void DrawThresdholds(Image<Bgr, byte> image, double thresholdDark, double threshdoldBright)
        {
            var imageThreshold = image.Convert<Gray, byte>().ThresholdBinaryInv(new Gray(thresholdDark), new Gray(255));
            image.SetValue(new Bgr(255, 255, 0), imageThreshold);

            imageThreshold = image.Convert<Gray, byte>().ThresholdBinary(new Gray(threshdoldBright), new Gray(255));
            image.SetValue(new Bgr(0, 255, 255), imageThreshold);
        }

        public static void DrawEyelids(Image<Bgr, byte> image, EyeData data, EyePhysicalModel eyeModel, double topEyeLid, double bottomEyeLid)
        {
            if (eyeModel.IsEmpty)
            {
                eyeModel = new EyePhysicalModel(data.Pupil.Center, data.Iris.Radius * 2);
            }

            var c1 = new Point(
                Math.Max(5, (int)(eyeModel.Center.X - eyeModel.Radius)),
                (int)((data.Eyelids.Upper[0].Y + data.Eyelids.Upper[1].Y +
                1.5*data.Eyelids.Lower[0].Y + 1.5*data.Eyelids.Lower[1].Y) / 5.0));

            var c2 = new Point(
                Math.Min(image.Width - 5,
                (int)(eyeModel.Center.X + eyeModel.Radius)),
                (int)((data.Eyelids.Upper[3].Y + 1.5*data.Eyelids.Lower[3].Y + data.Eyelids.Upper[2].Y + 1.5*data.Eyelids.Lower[2].Y) / 5.0));

            if (data.Eyelids.Upper != null)
            {
                var upperEyelid = (Point[])Array.ConvertAll(data.Eyelids.Upper, point => new Point((int)Math.Round(point.X), (int)Math.Round(point.Y)));

                var eyelidOffsetTop = (int)Math.Round(topEyeLid * 100);
                var top = new Point[8];
                top[0] = c1;
                top[5] = c2;
                top[6] = c1;
                top[7] = c2;
                Array.Copy(upperEyelid, 0, top, 1, 4);
              //  image.DrawPolyline(top, false, new Bgr(Color.Green), 2);

                for (int i = 0; i < top.Length; i++)
                {
                    top[i].Y += eyelidOffsetTop;
                }

                //image.DrawPolyline(top, false, new Bgr(Color.Green), 2);

                var top2 = FitParabola(top, eyeModel);

                image.DrawPolyline(top2, false, new Bgr(Color.Orange), 2);
            }




            if (data.Eyelids.Lower != null)
            {
                var lowerEyelid = (Point[])Array.ConvertAll(data.Eyelids.Lower, point => new Point((int)Math.Round(point.X), (int)Math.Round(point.Y)));

                var eyelidOffsetBottom = (int)Math.Round(bottomEyeLid * 100);
                var bottom = new Point[8];
                bottom[0] = c1;
                bottom[5] = c2;
                bottom[6] = c1;
                bottom[7] = c2;
                Array.Copy(lowerEyelid, 0, bottom, 1, 4);
                //image.DrawPolyline(bottom, false, new Bgr(Color.Green), 2);


                for (int i = 0; i < bottom.Length; i++)
                {
                    bottom[i].Y -= eyelidOffsetBottom;
                }

                //image.DrawPolyline(bottom, false, new Bgr(Color.Green), 2);
                var bottom2 = FitParabola(bottom, eyeModel);

                image.DrawPolyline(bottom2, false, new Bgr(Color.Orange), 2);
            }
        }

        public static Point[] FitParabola(Point[] top, EyePhysicalModel eyeModel)
        {
            var s0 = top.Length;
            
            var s1 = 0.0;
            for (int i = 0; i < top.Length; i++) s1 = s1 + top[i].X;

            var s2 = 0.0;
            for (int i = 0; i < top.Length; i++) s2 = s2 + Math.Pow(top[i].X, 2);

            var s3 = 0.0;
            for (int i = 0; i < top.Length; i++) s3 = s3 + Math.Pow(top[i].X, 3);

            var s4 = 0.0;
            for (int i = 0; i < top.Length; i++) s4 = s4 + Math.Pow(top[i].X, 4);

            var d = new Image<Gray, double>(1, 3);
            d.Data[0, 0, 0] = 0.0;
            for (int i = 0; i < top.Length; i++) d.Data[0, 0, 0] += Math.Pow(top[i].X, 2) * top[i].Y;
            d.Data[1, 0, 0] = 0.0;
            for (int i = 0; i < top.Length; i++) d.Data[1, 0, 0] += top[i].X * top[i].Y;
            d.Data[2, 0, 0] = 0.0;
            for (int i = 0; i < top.Length; i++) d.Data[2, 0, 0] += top[i].Y;

            var A = new Image<Gray, double>(3, 3);

            A.Data[0, 0, 0] = s4;
            A.Data[1, 0, 0] = s3;
            A.Data[2, 0, 0] = s2;

            A.Data[0, 1, 0] = s3;
            A.Data[1, 1, 0] = s2;
            A.Data[2, 1, 0] = s1;

            A.Data[0, 2, 0] = s2;
            A.Data[1, 2, 0] = s1;
            A.Data[2, 2, 0] = s0;

            var Ainv = new Image<Gray, double>(3, 3);
            CvInvoke.cvInvert(A, Ainv, Emgu.CV.CvEnum.SOLVE_METHOD.CV_LU);

            var a1 = Ainv.Data[0, 0, 0] * d.Data[0, 0, 0] + Ainv.Data[1, 0, 0] * d.Data[1, 0, 0] + Ainv.Data[2, 0, 0] * d.Data[2, 0, 0];
            var a2 = Ainv.Data[0, 1, 0] * d.Data[0, 0, 0] + Ainv.Data[1, 1, 0] * d.Data[1, 0, 0] + Ainv.Data[2, 1, 0] * d.Data[2, 0, 0];
            var a3 = Ainv.Data[0, 2, 0] * d.Data[0, 0, 0] + Ainv.Data[1, 2, 0] * d.Data[1, 0, 0] + Ainv.Data[2, 2, 0] * d.Data[2, 0, 0];

            var top2 = new Point[21];
            var x1 = (eyeModel.Center.X - eyeModel.Radius);
            var step = (eyeModel.Radius * 2) / 20;
            for (int i = 0; i < top2.Length; i++)
            {
                top2[i].X = (int)(i * step + x1);
                top2[i].Y = (int)(a1 * top2[i].X * top2[i].X + a2 * top2[i].X + a3);
            }
            return top2;
        }

        public static void DrawPupil(Image<Bgr, byte> image, EyeData data)
        {
            // Draw pupil circle
            image.Draw(data.Pupil, new Bgr(Color.RoyalBlue), 2);
        }

        public static void DrawIris(Image<Bgr, byte> image, EyeData data, EyePhysicalModel eyeModel)
        {
            var iris = data.Iris;

            // Draw iris circle
            var points = new Point[36];
            for (int i = 0; i < points.Length; i++)
            {
                var theta = 2 * Math.PI / points.Length * i;

                points[i].X = (int)Math.Round(Math.Cos(theta) * data.Iris.Radius);
                points[i].Y = (int)Math.Round(Math.Sin(theta) * data.Iris.Radius);

                // TODO: FIX THIS
                if ((EyeTracker.Settings == null || EyeTracker.Settings.Tracking.UseGeometricTransformation) && !eyeModel.IsEmpty)
                {
                    points[i] = RotatePoint(points[i], eyeModel, data.Pupil.Center);
                }
                else
                {
                    points[i].X = (int)Math.Round(points[i].X + data.Iris.Center.X);
                    points[i].Y = (int)Math.Round(points[i].Y + data.Iris.Center.Y);
                }
            }

            image.DrawPolyline(points, true, new Bgr(Color.RoyalBlue), 2);
        }

        private static Point RotatePoint(Point point, EyePhysicalModel eyeModel, PointF center)
        {
            var angle = Math.Atan2((center.Y - eyeModel.Center.Y), (center.X - eyeModel.Center.X));
            var ecc = Math.Asin(Math.Sqrt((center.Y - eyeModel.Center.Y) * (center.Y - eyeModel.Center.Y) + (center.X - eyeModel.Center.X) * (center.X - eyeModel.Center.X)) / eyeModel.Radius);
            var q = new Quaternions(Math.Cos(ecc / 2), -Math.Sin(angle) * Math.Sin(ecc / 2), Math.Cos(angle) * Math.Sin(ecc / 2), 0);

            var x = point.X;
            var y = point.Y;

            double
                 t2 = q.W * q.X,
                 t3 = q.W * q.Y,
                 t4 = q.W * q.Z,
                 t5 = -q.X * q.X,
                 t6 = q.X * q.Y,
                 t7 = q.X * q.Z,
                 t8 = -q.Y * q.Y,
                 t9 = q.Y * q.Z,
                 t10 = -q.Z * q.Z;

            double rr = Math.Sqrt(x * x + y * y) / eyeModel.Radius;

            if (rr <= 1)
            {
                var z = Math.Sqrt(1 - rr * rr) * eyeModel.Radius;

                // var p2 = q.RotatePoint(p);
                double xx = 2.0 * ((t8 + t10) * x + (t6 - t4) * y + (t3 + t7) * z) + x;
                double yy = 2.0 * ((t4 + t6) * x + (t5 + t10) * y + (t9 - t2) * z) + y;
                double zz = 2.0 * ((t7 - t3) * x + (t2 + t9) * y + (t5 + t8) * z) + z;

                point.X = (int)Math.Round(xx + eyeModel.Center.X);
                point.Y = (int)Math.Round(yy + eyeModel.Center.Y);
            }

            return point;
        }

        public static void DrawCroppingBox(Image<Bgr, byte> image)
        {
            if (EyeTracker.Settings != null)
            {
                // Draw cropping box
                var eyeROI = new Rectangle(
                    EyeTracker.Settings.Tracking.Cropping.Left,
                    EyeTracker.Settings.Tracking.Cropping.Top,
                    image.Size.Width - EyeTracker.Settings.Tracking.Cropping.Left - EyeTracker.Settings.Tracking.Cropping.Width,
                    image.Size.Height - EyeTracker.Settings.Tracking.Cropping.Top - EyeTracker.Settings.Tracking.Cropping.Height);

                image.Draw(
                    eyeROI,
                    new Bgr(Color.RoyalBlue),
                    2);
            }
        }

        public static void DrawEyeGlobe(Image<Bgr, byte> image, EyePhysicalModel globe, bool detailed)
        {
            if (detailed)
            {
                for (int i = 0; i <= 90; i = i + 10)
                {
                    var circle = globe.CircleF;
                    circle = new CircleF(circle.Center, (float)((1 / (1 + (1 - Math.Cos(i / 180 * Math.PI)) / 2)) * Math.Sin(i / 90.0 * (Math.PI / 2.0)) * circle.Radius));
                    image.Draw(circle, new Bgr(Color.White), 1);
                }
            }
            else
            {
                var circle = globe.CircleF;
                circle = new CircleF(circle.Center, circle.Radius);
                image.Draw(circle, new Bgr(Color.White), 1);
            }
        }
    }
}
