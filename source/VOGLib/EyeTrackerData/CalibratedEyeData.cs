﻿//-----------------------------------------------------------------------
// <copyright file="CalibratedEyeData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------                            
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using Emgu.CV.Structure;

    /// <summary>
    /// Calibrated data. Calculated from the raw data using the CalibrationInfo.
    /// </summary>
    public struct CalibratedEyeData
    {
        /// <summary>
        /// Gets or sets the horizontal position of the eye, in degrees, relative to the reference position during calibration.
        /// </summary>
        public double HorizontalPosition { get; set; }

        /// <summary>
        /// Gets or sets the vertical position of the eye, in degrees, relative to the reference position during calibration.
        /// </summary>
        public double VerticalPosition { get; set; }

        /// <summary>
        /// Gets or sets the torsional position of the eye, in degrees, relative to the reference position during calibration.
        /// </summary>
        public double TorsionalPosition { get; set; }

        /// <summary>
        /// Gets or sets the pupil Area, in squared milimeters. 
        /// </summary>
        public double PupilArea { get; set; }

        /// <summary>
        /// Gets or sets the percent opening of the eyelids. 100% being the opening during calibration.
        /// </summary>
        public double PercentOpening { get; set; }

        /// <summary>
        /// Gets or sets the data quality metric, from 0 to 100.
        /// </summary>
        public double DataQuality { get; set; }
    }


    /// <summary>
    /// Calibrated head data. Calculated from the raw data using the HeadCalibration.
    /// </summary>
    public struct CalibratedHeadData
    {
        /// <summary>
        /// Gets or sets the head roll angle in degrees, relative to gravity.
        /// </summary>
        public double Roll { get; set; }

        /// <summary>
        /// Gets or sets the head pitch angle in degrees, relative to gravity.
        /// </summary>
        public double Pitch { get; set; }

        /// <summary>
        /// Gets or sets the head yaw angle in degrees, relative to the reference position during calibration.
        /// </summary>
        public double Yaw { get; set; }

        public double RollVelocity { get; set; }

        public double PitchVelocity { get; set; }

        public double YawVelocity { get; set; }
    }
}
