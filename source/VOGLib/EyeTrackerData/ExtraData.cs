﻿//-----------------------------------------------------------------------
// <copyright file="ExtraData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------    
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Structure for the generic extra data.
    /// </summary>
    public struct ExtraData
    {
        public int Int0;
        public int Int1;
        public int Int2;
        public int Int3;
        public int Int4;
        public int Int5;
        public int Int6;
        public int Int7;
        public int Int8;
        public int Int9;

        public double Double0;
        public double Double1;
        public double Double2;
        public double Double3;
        public double Double4;
        public double Double5;
        public double Double6;
        public double Double7;
        public double Double8;
    }
}
