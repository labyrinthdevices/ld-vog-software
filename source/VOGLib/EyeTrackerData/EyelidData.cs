﻿//-----------------------------------------------------------------------
// <copyright file="EyelidData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------                            
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using Emgu.CV.Structure;

    /// <summary>
    /// Data structure containing the data about the eyelids.
    /// </summary>
    [Serializable]
    public class EyelidData
    {
        /// <summary>
        /// Initializes a new item of the EyelidData class.
        /// </summary>
        public EyelidData()
        {
            this.Upper = new PointF[4];
            this.Lower = new PointF[4];
        }

        /// <summary>
        /// Gets or sets the eyelid points of the upper eyelid contour.
        /// </summary>
        public PointF[] Upper { get; set; }

        /// <summary>
        /// Gets or sets the eyelid points of the lower eyelid contour.
        /// </summary>
        public PointF[] Lower { get; set; }
    }
}
