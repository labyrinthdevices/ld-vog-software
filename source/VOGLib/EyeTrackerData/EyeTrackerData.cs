﻿//-----------------------------------------------------------------------
// <copyright file="EyelidData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------    
namespace OculomotorLab.VOG
{
    using System;

    /// <summary>
    /// Class containing all the data related to one frame.
    /// </summary>
    public class EyeTrackerData
    {
        /// <summary>
        /// Gets or sets the raw data from the right eye.
        /// </summary>
        public EyeCollection<EyeData> EyeDataRaw { get; set; }

        /// <summary>
        /// GEts or sets the calibrated data for the left eye.
        /// </summary>
        public EyeCollection<CalibratedEyeData> EyeDataCalibrated { get; set; }

        /// <summary>
        /// Gets or sets the raw data from the head sensor.
        /// </summary>
        public HeadData HeadDataRaw { get; set; }

        /// <summary>
        /// Gets or sets the calibrated head data.
        /// </summary>
        public CalibratedHeadData HeadDataCalibrated { get; set; }
        
        /// <summary>
        /// Extra generic data for different systems to use as they please.
        /// </summary>
        public ExtraData ExtraData {get; set;}

        public string GetStringLine()
        {
            var data = this;

            var leftUpperEyelid = (data.EyeDataRaw[Eye.Left].Eyelids.Upper == null) ? 0 : data.EyeDataRaw[Eye.Left].Eyelids.Upper[0].Y + data.EyeDataRaw[Eye.Left].Eyelids.Upper[1].Y + data.EyeDataRaw[Eye.Left].Eyelids.Upper[2].Y + data.EyeDataRaw[Eye.Left].Eyelids.Upper[3].Y;

            var leftLowerEyelid = (data.EyeDataRaw[Eye.Left].Eyelids.Lower == null) ? 0 : data.EyeDataRaw[Eye.Left].Eyelids.Lower[0].Y + data.EyeDataRaw[Eye.Left].Eyelids.Lower[1].Y + data.EyeDataRaw[Eye.Left].Eyelids.Lower[2].Y + data.EyeDataRaw[Eye.Left].Eyelids.Lower[3].Y;

            var rightUpperEyelid = (data.EyeDataRaw[Eye.Right].Eyelids.Upper == null) ? 0 : data.EyeDataRaw[Eye.Right].Eyelids.Upper[0].Y + data.EyeDataRaw[Eye.Right].Eyelids.Upper[1].Y + data.EyeDataRaw[Eye.Right].Eyelids.Upper[2].Y + data.EyeDataRaw[Eye.Right].Eyelids.Upper[3].Y;

            var rightLowerEyelid = (data.EyeDataRaw[Eye.Right].Eyelids.Lower == null) ? 0 : data.EyeDataRaw[Eye.Right].Eyelids.Lower[0].Y + data.EyeDataRaw[Eye.Right].Eyelids.Lower[1].Y + data.EyeDataRaw[Eye.Right].Eyelids.Lower[2].Y + data.EyeDataRaw[Eye.Right].Eyelids.Lower[3].Y;

            var headdataresult = 1;
            if (data.HeadDataRaw.DataResult == HeadData.Result.Missed)
                headdataresult = 0;

            string line =
                string.Format("{0}", data.EyeDataRaw[Eye.Left].Timestamp.FrameNumberRaw) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Timestamp.Seconds) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Center.X) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Center.Y) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Size.Width) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Size.Height) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Pupil.Angle) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].Iris.Radius) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].TorsionAngle) + " " +
                string.Format("{0:0.0000}", leftUpperEyelid) + " " +
                string.Format("{0:0.0000}", leftLowerEyelid) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Left].DataQuality) + " " +

                string.Format("{0}", data.EyeDataRaw[Eye.Right].Timestamp.FrameNumberRaw) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Timestamp.Seconds) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Center.X) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Center.Y) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Size.Width) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Size.Height) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Pupil.Angle) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].Iris.Radius) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].TorsionAngle) + " " +
                string.Format("{0:0.0000}", rightUpperEyelid) + " " +
                string.Format("{0:0.0000}", rightLowerEyelid) + " " +
                string.Format("{0:0.0000}", data.EyeDataRaw[Eye.Right].DataQuality) + " " +

                string.Format("{0:0.0000}", data.HeadDataRaw.AccelerometerX) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.AccelerometerY) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.AccelerometerZ) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GyroX) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GyroY) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GyroZ) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.Temperature) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.MagnetometerX) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.MagnetometerY) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.MagnetometerZ) + " " +
                string.Format("{0:0.0000}", data.HeadDataRaw.GPIO0) + " " +

                string.Format("{0}", headdataresult) + " " +    // JB added this 4-9-2015
                string.Format("{0}", data.EyeDataRaw[Eye.Left].Timestamp.FrameNumber) + " " +
                string.Format("{0}", data.EyeDataRaw[Eye.Right].Timestamp.FrameNumber) + " " +
                string.Format("{0}", data.HeadDataRaw.TimeStamp.FrameNumber) + " " +

               // MAR Added this on 2015-10-26
               string.Format("{0:0.0000}", data.EyeDataCalibrated[Eye.Left].HorizontalPosition) + " " +
               string.Format("{0:0.0000}", data.EyeDataCalibrated[Eye.Left].VerticalPosition) + " " +
               string.Format("{0:0.0000}", data.EyeDataCalibrated[Eye.Left].TorsionalPosition) + " " +
               string.Format("{0:0.0000}", data.EyeDataCalibrated[Eye.Right].HorizontalPosition) + " " +
               string.Format("{0:0.0000}", data.EyeDataCalibrated[Eye.Right].VerticalPosition) + " " +
               string.Format("{0:0.0000}", data.EyeDataCalibrated[Eye.Right].TorsionalPosition) + " " +
               string.Format("{0:0.0000}", data.HeadDataRaw.SoftwareTrigger) + " " +
               string.Format("{0:0.0000}", data.HeadDataRaw.NumSerBytesInBuffer) + " " +
               string.Format("{0}", data.HeadDataRaw.CPUClockTime);         // Added by Mehdi on 2019-12-23

            /*
                string.Format("{0}", data.ExtraData.Int0) + " " +
                string.Format("{0}", data.ExtraData.Int1) + " " +
                string.Format("{0}", data.ExtraData.Int2) + " " +
                string.Format("{0}", data.ExtraData.Int3) + " " +
                string.Format("{0}", data.ExtraData.Int4) + " " +
                string.Format("{0}", data.ExtraData.Int5) + " " +
                string.Format("{0}", data.ExtraData.Int6) + " " +
                string.Format("{0}", data.ExtraData.Int7) + " " +

                string.Format("{0:0.0000}", data.ExtraData.Double0) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double1) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double2) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double3) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double4) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double5) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double6) + " " +
                string.Format("{0:0.0000}", data.ExtraData.Double7) + " ";*/

            return line;
        }
    }

    /// <summary>
    /// Class containing all the data related to one frame and the images of the frame.
    /// </summary>
    public class EyeTrackerDataAndImages
    {
        public EyeTrackerDataAndImages(EyeTrackerData eyeTrackerData, EyeCollection<ProcessedImageEye> processedImages)
        {
            this.Data = eyeTrackerData;
            this.Images = processedImages;
        }

        public EyeCollection<ProcessedImageEye> Images { get; private set; }

        public EyeTrackerData Data { get; private set; }
    }
}
