﻿//-----------------------------------------------------------------------
// <copyright file="EyeData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------                            
namespace OculomotorLab.VOG
{
    using System;
    using System.Drawing;
    using Emgu.CV.Structure;

    /// <summary>
    /// Possible results of processing a frame.
    /// </summary>
    public enum ProcessFrameResult
    {
        /// <summary>
        /// Undefined result.
        /// </summary>
        Undefined,

        /// <summary>
        /// No errors data obtained correctly.
        /// </summary>
        Good,

        /// <summary>
        /// Pupil could not be found.
        /// </summary>
        MissingPupil,

        /// <summary>
        /// Iris could not be found.
        /// </summary>
        MissingIris,

        /// <summary>
        /// The current frame could not be captured.
        /// </summary>
        FrameDropped,

        /// <summary>
        /// Processing error.
        /// </summary>
        ProcessingError,
    }

    /// <summary>
    /// Class containing of the eye movement data from one frame.
    /// </summary>
    /// <remarks>
    /// Best Practices on serialization from http://msdn.microsoft.com/en-us/library/ms229752(v=vs.110).aspx
    /// To ensure proper versioning behavior, follow these rules when modifying a type from version to version:
    /// Never remove a serialized field.
    /// Never apply the NonSerializedAttribute attribute to a field if the attribute was not applied to the field in the previous version.
    /// Never change the name or the type of a serialized field.
    /// When adding a new serialized field, apply the OptionalFieldAttribute attribute.
    /// When removing a NonSerializedAttribute attribute from a field (that was not serializable in a previous version), apply the OptionalFieldAttribute attribute.
    /// For all optional fields, set meaningful defaults using the serialization callbacks unless 0 or nullas defaults are acceptable.
    /// To ensure that a type will be compatible with future serialization engines, follow these guidelines:
    /// Always set the VersionAdded property on the OptionalFieldAttribute attribute correctly.
    /// Avoid branched versioning.</remarks>
    [Serializable]
    public class EyeData
    {
        /// <summary>
        /// Initializes a new instance of the EyeData class.
        /// </summary>
        public EyeData()
        {
            this.Eyelids = new EyelidData();
        }

        /// <summary>
        /// Initializes a new instance of the EyeData class.
        /// </summary>
        public EyeData(
            Eye whichEye,
            ImageEyeTimestamp timestamp,
            ProcessFrameResult result)
            : this()
        {
            this.WhichEye = whichEye;
            this.Timestamp = timestamp;
            this.ProcessFrameResult = result;
        }

        /// <summary>
        /// Initializes a new instance of the EyeData class.
        /// </summary>
        public EyeData(
            Eye whichEye,
            ImageEyeTimestamp timestamp,
            PupilData pupil,
            IrisData iris,
            double torsionAngle,
            EyelidData eyelids,
            double dataQuality,
            ProcessFrameResult result)
            : this()
        {
            this.WhichEye = whichEye;
            this.Timestamp = timestamp;
            this.Pupil = pupil;
            this.Iris = iris;
            this.TorsionAngle = torsionAngle;
            this.Eyelids = eyelids;
            this.DataQuality = dataQuality;
            this.ProcessFrameResult = result;
        }

        /// <summary>
        /// Gets or sets left or right eye.
        /// </summary>
        public Eye WhichEye { get; private set; }

        /// <summary>
        /// Gets or sets the timestamp from the frame.
        /// </summary>
        public ImageEyeTimestamp Timestamp { get; private set; }

        /// <summary>
        /// Gets or sets the pupil position and shape information.
        /// </summary>
        public PupilData Pupil { get; private set; }

        /// <summary>
        /// Gets or sets the iris position and shape information.
        /// </summary>
        public IrisData Iris { get; private set; }

        /// <summary>
        /// Gets or sets the torsion angle.
        /// </summary>
        public double TorsionAngle { get; private set; }

        /// <summary>
        /// Gets or sets the eyelid points of the eyelid contours.
        /// </summary>
        public EyelidData Eyelids { get; private set; }

        /// <summary>
        /// Gets or sets a value of data quality.
        /// </summary>
        public double DataQuality { get; private set; }

        /// <summary>
        /// Gets or sets the result of the processing.
        /// </summary>
        public ProcessFrameResult ProcessFrameResult { get; private set; }
    }
}
