﻿//-----------------------------------------------------------------------
// <copyright file="HeadData.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------                            
namespace OculomotorLab.VOG
{
    using System;

    /// <summary>
    /// Data structure containing the data about head movement.
    /// </summary>
    [Serializable]
    public struct HeadData
    {
        public enum Result
        {
            Normal,
            Missed,
            Empty,
            Terminated
        }
        public Result DataResult { get; set; }

        public ImageEyeTimestamp TimeStamp { get; set; }

        public double AccelerometerX { get; set; }
        public double AccelerometerY { get; set; }
        public double AccelerometerZ { get; set; }
        public double Temperature { get; set; }
        public double GyroX { get; set; }
        public double GyroY { get; set; }
        public double GyroZ { get; set; }
        public double MagnetometerX { get; set; }
        public double MagnetometerY { get; set; }
        public double MagnetometerZ { get; set; }
        public uint GPIO0 { get; set; }
        public String CPUClockTime {get; set; }         // Added by Mehdi on 2019-12-23
        public double NumSerBytesInBuffer { get; set; }
        public int SoftwareTrigger { get; set; }
    }
}
