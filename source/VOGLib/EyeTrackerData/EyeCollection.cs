﻿//-----------------------------------------------------------------------
// <copyright file="EyeCollection.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Inmmutable collection of objects related to the left eye, the right eye or both
    /// </summary>
    /// <typeparam name="T">Type of the elements in the collection.</typeparam>
    [Serializable]
    public class EyeCollection<T> : IEnumerable<T>
    {
        /// <summary>
        /// List of elements.
        /// </summary>
        protected T[] list;

        /// <summary>
        /// Initializes a new instance of the EyeCollection class
        /// </summary>
        public EyeCollection(T[] objects)
        {
            if (objects == null)
            {
                throw new ArgumentNullException("Objects");
            }

            if (objects.Length < 1 || objects.Length > 2)
            {
                throw new InvalidOperationException("Only one or two images;");
            }

            this.list = (T[])objects.Clone();
        }

        /// <summary>
        /// Initializes a new instance of the EyeCollection class
        /// </summary>
        public EyeCollection(T objectBothEyes)
        {
            this.list = new T[] { objectBothEyes };
        }

        /// <summary>
        /// Initializes a new instance of the CollectionEye class
        /// </summary>
        public EyeCollection(T objectLeftEye, T objectRightEye)
        {
            this.list = new T[] { objectLeftEye, objectRightEye };
        }

        /// <summary>
        /// Enables to index the collection with the Eye enumaration.
        /// </summary>
        /// <param name="whichEye">Left eye, right eye or both.</param>
        /// <returns>The corresponding object.</returns>
        public virtual T this[Eye whichEye]
        {
            get
            {
                switch (whichEye)
                {
                    case Eye.Left:
                        if (this.list.Length != 2)
                        {
                            throw new InvalidOperationException("The collection has more than 1 image.");
                        }
                        
                        return this.list[0];
                    case Eye.Right:
                        if (this.list.Length != 2)
                        {
                            throw new InvalidOperationException("The collection has more than 1 image.");
                        }

                        return this.list[1];
                    case Eye.Both:
                        if (this.list.Length != 1)
                        {
                            throw new InvalidOperationException("The collection has more than 1 image.");
                        }

                        return this.list[0];
                }

                throw new InvalidOperationException("The collection has more than 1 image.");
            }
        }


        /// <summary>
        /// Enables to index the collection.
        /// </summary>
        /// <param name="idx">Index number of the object.</param>
        /// <returns>The corresponding object.</returns>
        public virtual T this[int idx]
        {
            get
            {
                return this.list[idx];
            }
        }


        /// <summary>
        /// Gets the number of elements in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                return this.list.Length;
            }
        }

        public EyeCollection<Tout> ConvertElements<Tout>()
            where Tout : class
        {
            var objects = new Tout[this.Count];
            for (int i = 0; i < this.Count; i++)
            {
                objects[i] = this[i] as Tout;
            }

            return new EyeCollection<Tout>(objects);
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            // call the generic version of the method
            return this.GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < list.Length; i++)
            {
                yield return list[i];
            }
        }

        public override string ToString()
        {
            var s = new System.Text.StringBuilder();
            for (int i = 0; i < list.Length; i++)
            {
                if ( i>0)
                {
                    s.Append(";");
                }
                s.Append(list[i].ToString());
            }

            return s.ToString();
        }
    }
}
