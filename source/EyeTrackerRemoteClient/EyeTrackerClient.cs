﻿//-----------------------------------------------------------------------
// <copyright file="EyeTrackerClient.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.Remote
{
    using System;
    using System.Drawing;
    using System.Diagnostics;
    using System.ServiceModel;

    /// <summary>
    /// Creates a client that can control remotely (or locally from a different program) the eye tracker.
    /// </summary>
    public class EyeTrackerClient
    {
        /// <summary>
        /// Initializes a new instance of the EyeTrackerClient class.
        /// </summary>
        /// <param name="hostname">Name of the host running the eye tracker.</param>
        /// <param name="port">TCP port where the host is listening.</param>
        public static IEyeTrackerService GetEyeTrackerClient(string hostname, int port)
        {
            try
            {
                var binding = new NetTcpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                binding.Security.Mode = SecurityMode.None;

                var proxy = ChannelFactory<IEyeTrackerService>.CreateChannel(
                    binding,
                    new EndpointAddress("net.tcp://" + hostname + ":" + port + "/EyeTrackerEndpoint"));

                return proxy;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
