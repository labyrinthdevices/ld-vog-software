﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace OculomotorLab.VOG.Remote
{
    /// <summary>
    /// This class is just for matlab. It did not work with the interface. I don't know why.
    /// </summary>
    class EyeTrackerClientMatlab
    {
        IEyeTrackerService proxy;

        public EyeTrackerClientMatlab(string IP, int port)
        {
            this.proxy = EyeTrackerClient.GetEyeTrackerClient(IP, port);
        }

        public EyeTrackerStatusSummary Status
        {
            get { return this.proxy.Status; }
        }

        public TrackingSettings Settings
        {
            get { return this.proxy.Settings; }
        }

        public bool ChangeSetting(string settingName, object value)
        {
            return this.proxy.ChangeSetting(settingName, value);
        }


        public ImagesAndData GetCurrentImagesAndData()
        {
            return this.proxy.GetCurrentImagesAndData();
        }

        public EyeCalibrationParamteres GetCalibrationParameters()
        {
            return this.proxy.GetCalibrationParameters();
        }

        public void StartRecording()
        {
            this.proxy.StartRecording();
        }

        public void StopRecording()
        {
            this.proxy.StopRecording();
        }

        public void ResetReference()
        {
            this.proxy.ResetReference();
        }

        public void RecordEvent(string message)
        {
            this.proxy.RecordEvent(message);
        }
    }
}
