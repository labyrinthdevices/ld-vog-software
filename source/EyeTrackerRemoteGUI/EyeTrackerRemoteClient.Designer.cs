﻿namespace OculomotorLab.VOG.Remote
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConnect = new System.Windows.Forms.Button();
            this.buttonStartRecording = new System.Windows.Forms.Button();
            this.buttonStopRecording = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.eyeTrackerImageEyeBoxLeft = new OculomotorLab.VOG.UI.EyeTrackerImageEyeBox();
            this.eyeTrackerImageEyeBoxRight = new OculomotorLab.VOG.UI.EyeTrackerImageEyeBox();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.labelDataRight = new System.Windows.Forms.Label();
            this.labelDataLeft = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelImageGrabbingStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelTimestamp = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelProcessingTimeLeftEye = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelRecording = new System.Windows.Forms.ToolStripStatusLabel();
            this.buttonResetReference = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonConnect
            // 
            this.buttonConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnect.Location = new System.Drawing.Point(15, 47);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(230, 63);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // buttonStartRecording
            // 
            this.buttonStartRecording.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartRecording.Location = new System.Drawing.Point(12, 157);
            this.buttonStartRecording.Name = "buttonStartRecording";
            this.buttonStartRecording.Size = new System.Drawing.Size(230, 56);
            this.buttonStartRecording.TabIndex = 1;
            this.buttonStartRecording.Text = "Start Recording";
            this.buttonStartRecording.UseVisualStyleBackColor = true;
            this.buttonStartRecording.Click += new System.EventHandler(this.buttonStartRecording_Click);
            // 
            // buttonStopRecording
            // 
            this.buttonStopRecording.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStopRecording.Location = new System.Drawing.Point(12, 219);
            this.buttonStopRecording.Name = "buttonStopRecording";
            this.buttonStopRecording.Size = new System.Drawing.Size(230, 59);
            this.buttonStopRecording.TabIndex = 2;
            this.buttonStopRecording.Text = "Stop Recording";
            this.buttonStopRecording.UseVisualStyleBackColor = true;
            this.buttonStopRecording.Click += new System.EventHandler(this.buttonStopRecording_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.eyeTrackerImageEyeBoxLeft, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.eyeTrackerImageEyeBoxRight, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(248, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(774, 357);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // eyeTrackerImageEyeBoxLeft
            // 
            this.eyeTrackerImageEyeBoxLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eyeTrackerImageEyeBoxLeft.Location = new System.Drawing.Point(390, 3);
            this.eyeTrackerImageEyeBoxLeft.Name = "eyeTrackerImageEyeBoxLeft";
            this.eyeTrackerImageEyeBoxLeft.Size = new System.Drawing.Size(381, 351);
            this.eyeTrackerImageEyeBoxLeft.TabIndex = 16;
            // 
            // eyeTrackerImageEyeBoxRight
            // 
            this.eyeTrackerImageEyeBoxRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eyeTrackerImageEyeBoxRight.Location = new System.Drawing.Point(3, 3);
            this.eyeTrackerImageEyeBoxRight.Name = "eyeTrackerImageEyeBoxRight";
            this.eyeTrackerImageEyeBoxRight.Size = new System.Drawing.Size(381, 351);
            this.eyeTrackerImageEyeBoxRight.TabIndex = 15;
            // 
            // textBoxIP
            // 
            this.textBoxIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIP.Location = new System.Drawing.Point(53, 12);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(192, 29);
            this.textBoxIP.TabIndex = 10;
            this.textBoxIP.Text = "127.0.0.1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 24);
            this.label1.TabIndex = 11;
            this.label1.Text = "IP:";
            // 
            // labelError
            // 
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelError.Location = new System.Drawing.Point(12, 369);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(229, 100);
            this.labelError.TabIndex = 12;
            // 
            // labelDataRight
            // 
            this.labelDataRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDataRight.AutoSize = true;
            this.labelDataRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDataRight.Location = new System.Drawing.Point(247, 384);
            this.labelDataRight.Name = "labelDataRight";
            this.labelDataRight.Size = new System.Drawing.Size(171, 24);
            this.labelDataRight.TabIndex = 13;
            this.labelDataRight.Text = "DATA RIGHT EYE:";
            // 
            // labelDataLeft
            // 
            this.labelDataLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDataLeft.AutoSize = true;
            this.labelDataLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDataLeft.Location = new System.Drawing.Point(247, 419);
            this.labelDataLeft.Name = "labelDataLeft";
            this.labelDataLeft.Size = new System.Drawing.Size(161, 24);
            this.labelDataLeft.TabIndex = 14;
            this.labelDataLeft.Text = "DATA LEFT EYE:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelImageGrabbingStatus,
            this.toolStripStatusLabelTimestamp,
            this.toolStripStatusLabelProcessingTimeLeftEye,
            this.toolStripStatusLabelRecording});
            this.statusStrip1.Location = new System.Drawing.Point(0, 481);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1034, 24);
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabelImageGrabbingStatus
            // 
            this.toolStripStatusLabelImageGrabbingStatus.Name = "toolStripStatusLabelImageGrabbingStatus";
            this.toolStripStatusLabelImageGrabbingStatus.Size = new System.Drawing.Size(42, 19);
            this.toolStripStatusLabelImageGrabbingStatus.Text = "0.00Hz";
            // 
            // toolStripStatusLabelTimestamp
            // 
            this.toolStripStatusLabelTimestamp.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabelTimestamp.Name = "toolStripStatusLabelTimestamp";
            this.toolStripStatusLabelTimestamp.Size = new System.Drawing.Size(112, 19);
            this.toolStripStatusLabelTimestamp.Text = "Camera not started";
            // 
            // toolStripStatusLabelProcessingTimeLeftEye
            // 
            this.toolStripStatusLabelProcessingTimeLeftEye.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabelProcessingTimeLeftEye.Name = "toolStripStatusLabelProcessingTimeLeftEye";
            this.toolStripStatusLabelProcessingTimeLeftEye.Size = new System.Drawing.Size(137, 19);
            this.toolStripStatusLabelProcessingTimeLeftEye.Text = "Processing Time = 0ms;";
            this.toolStripStatusLabelProcessingTimeLeftEye.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabelRecording
            // 
            this.toolStripStatusLabelRecording.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
            this.toolStripStatusLabelRecording.Name = "toolStripStatusLabelRecording";
            this.toolStripStatusLabelRecording.Size = new System.Drawing.Size(160, 19);
            this.toolStripStatusLabelRecording.Text = "Frames dropped video rec: 0";
            // 
            // buttonResetReference
            // 
            this.buttonResetReference.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonResetReference.Location = new System.Drawing.Point(11, 293);
            this.buttonResetReference.Name = "buttonResetReference";
            this.buttonResetReference.Size = new System.Drawing.Size(230, 59);
            this.buttonResetReference.TabIndex = 16;
            this.buttonResetReference.Text = "Reset reference";
            this.buttonResetReference.UseVisualStyleBackColor = true;
            this.buttonResetReference.Click += new System.EventHandler(this.buttonResetReference_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 505);
            this.Controls.Add(this.buttonResetReference);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.labelDataLeft);
            this.Controls.Add(this.labelDataRight);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.buttonStopRecording);
            this.Controls.Add(this.buttonStartRecording);
            this.Controls.Add(this.buttonConnect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonStartRecording;
        private System.Windows.Forms.Button buttonStopRecording;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Label labelDataRight;
        private System.Windows.Forms.Label labelDataLeft;
        private UI.EyeTrackerImageEyeBox eyeTrackerImageEyeBoxRight;
        private UI.EyeTrackerImageEyeBox eyeTrackerImageEyeBoxLeft;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelImageGrabbingStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelTimestamp;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelProcessingTimeLeftEye;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRecording;
        private System.Windows.Forms.Button buttonResetReference;
    }
}

