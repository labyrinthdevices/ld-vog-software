﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OculomotorLab.VOG.Remote
{
    public partial class Form1 : Form
    {
        IEyeTrackerService eyeTracker;
        Timer updateTimer;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.updateTimer = new System.Windows.Forms.Timer();
            this.updateTimer.Tick += updateTimer_Tick;
            this.updateTimer.Interval = 300;
            this.updateTimer.Enabled = true;

            this.textBoxIP.Text = EyeTrackerRemoteGUI.Properties.Settings.Default.LastIP;
        }

        void updateTimer_Tick(object sender, EventArgs e)
        {
            if (this.eyeTracker != null)
            {
                try
                {
                    // Get the status of the tracker
                    var eyeTrackerStatus = this.eyeTracker.Status;

                    this.toolStripStatusLabelImageGrabbingStatus.Text = eyeTrackerStatus.GrabberStatus;
                    this.toolStripStatusLabelProcessingTimeLeftEye.Text = eyeTrackerStatus.ProcessorStatus;
                    this.toolStripStatusLabelRecording.Text = eyeTrackerStatus.RecorderStatus;

                    if (eyeTrackerStatus.Tracking)
                    {
                        var settings = this.eyeTracker.Settings;

                        var calibrationParameters = this.eyeTracker.GetCalibrationParameters();
                        var imagesAndData = this.eyeTracker.GetCurrentImagesAndData();

                        var bitmap = imagesAndData.Image[Eye.Left];

                        this.labelDataLeft.Text = "DATA LEFT EYE: " +
                            "H: " + Math.Round(imagesAndData.CalibratedData[Eye.Left].HorizontalPosition) + " " +
                            "V: " + Math.Round(imagesAndData.CalibratedData[Eye.Left].VerticalPosition) + " " +
                            "T: " + Math.Round(imagesAndData.CalibratedData[Eye.Left].TorsionalPosition);

                        var imageLeftEye = new ProcessedImageEye(
                            new ImageEye(bitmap, Eye.Right, imagesAndData.RawData[Eye.Left].Timestamp),
                            null, imagesAndData.RawData[Eye.Left]);
                        this.eyeTrackerImageEyeBoxLeft.UpdateImageEyeBox(imageLeftEye,
                            calibrationParameters.PhysicalModel[Eye.Left],
                            settings.DarkThresholdLeftEye,
                            settings.BrightThresholdLeftEye,
                            settings.TopEyelid, 
                            settings.BottomEyelid);

                        bitmap = imagesAndData.Image[Eye.Right];

                        this.labelDataRight.Text = "DATA RIGHT EYE: " +
                            "H: " + Math.Round(imagesAndData.CalibratedData[Eye.Right].HorizontalPosition) + " " +
                            "V: " + Math.Round(imagesAndData.CalibratedData[Eye.Right].VerticalPosition) + " " +
                            "T: " + Math.Round(imagesAndData.CalibratedData[Eye.Right].TorsionalPosition);

                        var imageRightEye = new ProcessedImageEye(
                            new ImageEye(bitmap, Eye.Right, imagesAndData.RawData[Eye.Right].Timestamp),
                            null, imagesAndData.RawData[Eye.Right]);
                        this.eyeTrackerImageEyeBoxRight.UpdateImageEyeBox(imageRightEye,
                            calibrationParameters.PhysicalModel[Eye.Right],
                            settings.DarkThresholdRightEye,
                            settings.BrightThresholdRightEye,
                            settings.TopEyelid,
                            settings.BottomEyelid);
                    }

                    this.buttonConnect.Enabled = false;
                    this.textBoxIP.Enabled = true;

                    this.buttonStartRecording.Enabled = !eyeTrackerStatus.Recording && eyeTrackerStatus.Tracking;
                    this.buttonStopRecording.Enabled = eyeTrackerStatus.Recording && eyeTrackerStatus.Tracking;
                    this.buttonResetReference.Enabled = eyeTrackerStatus.Tracking;

                }
                catch (Exception ex)
                {
                    this.labelError.Text = ex.Message;
                    this.labelError.ForeColor = Color.Red;

                    this.eyeTracker = null;
                }
            }
            else
            {
                this.buttonConnect.Enabled = true;
                this.textBoxIP.Enabled = true;

                this.buttonStartRecording.Enabled = false;
                this.buttonStopRecording.Enabled = false;
                this.buttonResetReference.Enabled = false;
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            try
            {
                EyeTrackerRemoteGUI.Properties.Settings.Default.LastIP = this.textBoxIP.Text;
                EyeTrackerRemoteGUI.Properties.Settings.Default.Save();


                string hostname = this.textBoxIP.Text;
                int port = 9000;
                this.eyeTracker = EyeTrackerClient.GetEyeTrackerClient(hostname, port);

                this.labelError.Text = string.Empty;
            }
            catch(Exception ex)
            {
                this.labelError.Text = ex.Message;
                this.labelError.ForeColor = Color.Red;
            }
        }

        private void buttonStartRecording_Click(object sender, EventArgs e)
        {
            this.eyeTracker.StartRecording();
        }

        private void buttonStopRecording_Click(object sender, EventArgs e)
        {
            this.eyeTracker.StopRecording();
        }

        private void buttonResetReference_Click(object sender, EventArgs e)
        {
            this.eyeTracker.ResetReference();
        }
    }
}
