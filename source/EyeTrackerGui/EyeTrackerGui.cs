//-----------------------------------------------------------------------
// <copyright file="EyeTrackerGui.cs" company="Jonhs Hopkins University">
//     Copyright (c) 2014 Jorge Otero-Millan, Oculomotor lab, Johns Hopkins University. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace OculomotorLab.VOG.UI
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    using System.Threading;
    using System.Diagnostics;
    using System.Runtime.InteropServices;

    using Emgu.CV;
    using Emgu.CV.Structure;
    using Emgu.CV.Util;

    using OculomotorLab.VOG;
    using OculomotorLab.VOG.ImageGrabbing;

    /// <summary>
    /// Main graphical user interface of the eye tracker
    /// </summary>
    public partial class EyeTrackerGui : Form
    {
        #region Fields

        /// <summary>
        /// Eye tracker object
        /// </summary>
        private EyeTracker eyeTracker;

        /// <summary>
        /// Message to notify that the execution of the timer event is finished
        /// </summary>
        private ManualResetEvent timerFinished = new ManualResetEvent(false);

        /// <summary>
        /// Frame number of the last data added to the buffer.
        /// </summary>
        private long lastFrameInBufferData = 0;

        /// <summary>
        /// Current data from the right eye
        /// </summary>                    
        private EyeTrackerData[] bufferData = new EyeTrackerData[10000];

        /// <summary>
        /// Command manager. All main commands are managed by this object. It is possible to 
        /// bind multiple controls to the same command and they will be enable or disabled
        /// automatically.
        /// </summary>
        private EyeTrackerCommandManager commandManager;

        /// <summary>
        /// Controls the cameras of the eye tracker. Move, center, etc.
        /// </summary>
        private TwoCamerasControl cameraControllerUI;

        /// <summary>
        /// Controls the calibration.
        /// </summary>
        private ICalibrationUI calibrationUI;

        /// <summary>
        /// Data and images from the last frame.
        /// </summary>
        private EyeTrackerDataAndImages lastDataAndImages;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the EyeTrackerGui class
        /// </summary>
        public EyeTrackerGui()
        {
            this.InitializeComponent();
            this.KeyPreview = true;
        }

        #endregion

        #region Display and acquiring chess board info
        Capture _Capture; // capture device
        Image<Bgr, Byte> img; // image captured
        Image<Gray, Byte> Gray_Frame; // image for processing
        const int width = 9;//9 //width of chessboard no. squares in width - 1
        const int height = 6;//6 // height of chess board no. squares in height - 1
        Size patternSize = new Size(width, height); //size of chess board to be detected
        PointF[] corners; //corners found from chessboard
        Bgr[] line_colour_array = new Bgr[width * height]; // just for displaying colored lines of detected chessboard

        static Image<Gray, Byte>[] Frame_array_buffer = new Image<Gray, byte>[100]; //number of images to calibrate camera over
        int frame_buffer_savepoint = 0;
        bool start_Flag = false;
        #endregion

        #region Current mode variables
        public enum Mode
        {
            Calculating_Intrinsics,
            Calibrated,
            BothCalibrated,
            SavingFrames
        }
        Mode currentMode = Mode.SavingFrames;
        #endregion

        #region Getting the camera calibration
        MCvPoint3D32f[][] corners_object_list = new MCvPoint3D32f[Frame_array_buffer.Length][];
        PointF[][] corners_points_list = new PointF[Frame_array_buffer.Length][];

        IntrinsicCameraParameters IC = new IntrinsicCameraParameters();
        ExtrinsicCameraParameters[] EX_Param;

        #endregion

        #region Private Methods

        private void Initialize()
        {
            this.Hide();

            try
            {
                this.eyeTracker = EyeTracker.Initialize();
                this.eyeTracker.NewDataAvailable += this.EyeTracker_NewDataAvailable;
                this.eyeTracker.LogTraceListener.NewLogMessage += this.EyeTracker_NewLogMessage;

                this.eyeTracker.SettingChanged += (o, e) =>
                {
                    if (e.NeedsRestarting && !this.eyeTracker.Idle)
                    {
                        var result = MessageBox.Show(
                            "Changing the setting " + e.PropertyName + " requires to stop the tracking. Do you want to stop?",
                            "Do you want to stop?",
                            MessageBoxButtons.YesNo);

                        if (result == System.Windows.Forms.DialogResult.Yes)
                        {
                            this.eyeTracker.Stop();
                        }
                        else
                        {
                            MessageBox.Show("Setting will take effect the next time you start tracking.");
                        }
                    }
                };

                commandManager = new EyeTrackerCommandManager(this.eyeTracker);

                //commandManager.commands[EyeTrackerCommandName.StartCancelCalibration].Bind(this.calibrateToolStripMenuItem);
                //commandManager.commands[EyeTrackerCommandName.StartCancelCalibration].Bind(this.buttonCalibrate);

                commandManager.commands[EyeTrackerCommandName.StartContinueCalibration].Bind(this.buttonCalibrate);
                commandManager.commands[EyeTrackerCommandName.StartContinueCalibration].Bind(this.calibrateToolStripMenuItem);

                commandManager.commands[EyeTrackerCommandName.CancelCalibration].Bind(this.abortCalibrationToolStripMenuItem);

                commandManager.commands[EyeTrackerCommandName.LoadCalibration].Bind(this.loadCalibrationToolStripMenuItem);
                commandManager.commands[EyeTrackerCommandName.SaveCalibration].Bind(this.saveCalibrationToolStripMenuItem);

                commandManager.commands[EyeTrackerCommandName.ResetReference].Bind(this.buttonResetReference);
                commandManager.commands[EyeTrackerCommandName.ResetReference].Bind(this.resetReferenceToolStripMenuItem);
                commandManager.commands[EyeTrackerCommandName.ResetCalibration].Bind(this.resetCalibrationToolStripMenuItem);

                commandManager.commands[EyeTrackerCommandName.StartStopRecording].Bind(this.startRecordingToolStripMenuItem);
                commandManager.commands[EyeTrackerCommandName.StartStopRecording].Bind(this.buttonRecord);

                commandManager.commands[EyeTrackerCommandName.StartTracking].Bind(this.startTrackingToolStripMenuItem);
                commandManager.commands[EyeTrackerCommandName.ProcessVideo].Bind(this.processVideoToolStripMenuItem);

                commandManager.commands[EyeTrackerCommandName.Stop].Bind(this.stopToolStripMenuItem);

                commandManager.commands[EyeTrackerCommandName.PlayVideo].Bind(this.playVideoToolStripMenuItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show("GUI:Load: " + ex.ToString());
            }
            this.Show();
        }

        private void UpdateGraphics()
        {
            // Update the enabled and disabled elements of the UI
            this.UpdateUI();

            if (this.eyeTracker.Initializing)
            {
                return;
            }


            try
            {
                // This will force the UI to wait until this drawing is done before closing
                this.timerRefreshUI.Stop();

                switch (this.tabSoftStim.SelectedTab.Name)
                {
                    case ("tabSetup"):
                        this.UpdateTabSetup();
                        break;

                    case ("tabCalibration"):
                        this.UpdateTabCalibration();
                        break;

                    case ("tabViewer"):
                        this.UpdateTabViewer();
                        break;

                    case ("tabDebug"):
                        this.UpdateTabDebug();
                        break;

                    case "tabTiming":
                        this.UpdateTabTiming();
                        break;

                    case "tabDistortion":
                        //this.UpdateTabDistortion();
                        break;
                }

                // Update Status Bar
                this.UpdateStatusBar();

                // Camera controller
                if (this.cameraControllerUI == null)
                {
                    this.cameraControllerUI = new TwoCamerasControl();
                    this.cameraControllerUI.EyeTracker = this.eyeTracker;
                    this.panelCameraControl.Controls.Add(this.cameraControllerUI as Control);
                }
                else
                {
                    this.cameraControllerUI.Enabled = this.eyeTracker.ImageGrabber.CanMoveCameras;
                }

                this.videoPlayerUI.Update(this.eyeTracker.VideoPlayer);
            }
            catch (Exception ex)
            {
                MessageBox.Show("GUI:Load: " + ex.ToString());
            }
            finally
            {
                this.timerRefreshUI.Start();
            }
        }

        private void UpdateTabSetup()
        {
            if (this.lastDataAndImages != null &&
                this.lastDataAndImages.Images != null)
            {
                // Update Eye Images
                if (this.lastDataAndImages.Images[Eye.Left] != null)
                {

                    //var imageTest = Properties.Resources.ImageTest;
                    //var imageLeft = new Image<Bgr, Byte>(imageTest); 

                    var imageLeft = this.lastDataAndImages.Images[Eye.Left].ImageRaw.Convert<Bgr, byte>();

                    if (radioDistortCorrectEyeTrackingYes.Checked)
                    {
                        imageLeft = Perform_Distortion_Correction(imageLeft, Eye.Left);
                    }

                    if (EyeTracker.Settings.OverlayData)
                    {
                        ProcessedImageEye.DrawData(imageLeft, this.lastDataAndImages.Images[Eye.Left].EyeData,
                            this.eyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Left].EyePhysicalModel,
                            EyeTracker.Settings.Tracking.DarkThresholdLeftEye,
                            EyeTracker.Settings.Tracking.BrightThresholdLeftEye, 
                            EyeTracker.Settings.Tracking.TopEyelid,
                            EyeTracker.Settings.Tracking.BottomEyelid);
                    }

                    this.imageBoxLeftEye.Image = imageLeft;
                    this.imageBoxIrisLeftEye.Image = this.lastDataAndImages.Images[Eye.Left].ImageTorsion;
                }
                else
                {
                    this.imageBoxLeftEye.Image = null;
                    this.imageBoxIrisLeftEye.Image = null;
                }

                if (this.lastDataAndImages.Images[Eye.Right] != null)
                {
                    var imageRight = this.lastDataAndImages.Images[Eye.Right].ImageRaw.Convert<Bgr, byte>();

                    if (radioDistortCorrectEyeTrackingYes.Checked)
                    {
                        imageRight = Perform_Distortion_Correction(imageRight, Eye.Right);
                    }

                    if (EyeTracker.Settings.OverlayData)
                    {
                        ProcessedImageEye.DrawData(imageRight, this.lastDataAndImages.Images[Eye.Right].EyeData,
                            this.eyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Right].EyePhysicalModel,
                            EyeTracker.Settings.Tracking.DarkThresholdRightEye,
                            EyeTracker.Settings.Tracking.BrightThresholdRightEye,
                            EyeTracker.Settings.Tracking.TopEyelid,
                            EyeTracker.Settings.Tracking.BottomEyelid);
                    }

                    this.imageBoxRightEye.Image = imageRight;
                    this.imageBoxIrisRightEye.Image = this.lastDataAndImages.Images[Eye.Right].ImageTorsion;
                }
                else
                {
                    this.imageBoxRightEye.Image = null;
                    this.imageBoxIrisRightEye.Image = null;
                }

                // Update Iris reference Images
                if (this.eyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Left] != null)
                {
                    this.imageBoxIrisRefLeftEye.Image = this.eyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Left].ImageTorsionReference;
                }
                else
                {
                    this.imageBoxIrisLeftEye.Image = null;
                    this.imageBoxIrisRefLeftEye.Image = null;
                }

                if (this.eyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Right] != null)
                {
                    this.imageBoxIrisRefRightEye.Image = this.eyeTracker.CalibrationManager.CalibrationParameters.EyeCalibrationParameters[Eye.Right].ImageTorsionReference;
                }
                else
                {
                    this.imageBoxIrisRightEye.Image = null;
                    this.imageBoxIrisRefRightEye.Image = null;
                }
            }
            else
            {
                this.imageBoxIrisLeftEye.Image = null;
                this.imageBoxIrisRefLeftEye.Image = null;
                this.imageBoxIrisRightEye.Image = null;
                this.imageBoxIrisRefRightEye.Image = null;
            }
        }

        private void UpdateTabCalibration()
        {
            if (this.calibrationUI != null)
            {
                this.calibrationUI.UpdateUI();
            }
        }

        private void UpdateTabViewer()
        {
            if (this.lastDataAndImages != null &&
                this.lastDataAndImages.Images != null &&
                this.lastDataAndImages.Images[Eye.Left] != null &&
                this.lastDataAndImages.Images[Eye.Right] != null)
            {
                long currentIdx;
                Math.DivRem(this.lastFrameInBufferData, this.bufferData.Length, out currentIdx);

                var imageLeft = this.lastDataAndImages.Images[Eye.Left].ImageRaw.Convert<Bgr,byte>();
                ProcessedImageEye.DrawThresdholds(imageLeft, EyeTracker.Settings.Tracking.DarkThresholdLeftEye, EyeTracker.Settings.Tracking.BrightThresholdLeftEye);
                this.eyeTrackerImageEyeBoxLeftEyeSmall.Image = imageLeft;

                var imageRight = this.lastDataAndImages.Images[Eye.Right].ImageRaw.Convert<Bgr,byte>();
                ProcessedImageEye.DrawThresdholds(imageRight, EyeTracker.Settings.Tracking.DarkThresholdRightEye, EyeTracker.Settings.Tracking.BrightThresholdRightEye);
                this.eyeTrackerImageEyeBoxRightEyeSmall.Image = imageRight;

                var calibratedDataLeftEye = this.lastDataAndImages.Data.EyeDataCalibrated[Eye.Left];
                var calibratedDataRightEye = this.lastDataAndImages.Data.EyeDataCalibrated[Eye.Right];
            }

            // Update Traces
            this.eyeTrackerTrace.Update(this.bufferData, this.lastFrameInBufferData);
        }

        private void UpdateTabDebug()
        {
            this.tabDebug.SuspendLayout();

            if (EyeTracker.Settings.Debug)
            {
                try
                {
                    var CROSSCORR = true;
                    var CURVATURE = false;
                    if (CROSSCORR)
                    {
                        // Cross correlations
                        var maxTorsion = EyeTracker.Settings.Tracking.MaxTorsion;
                        var xcorrSize = (int)Math.Round(maxTorsion * 10);
                        // Initialize charts
                        if (this.chartBottom.Series.Count == 0)
                        {
                            var seriesLeft = new System.Windows.Forms.DataVisualization.Charting.Series("Left");
                            seriesLeft.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                            var seriesRight = new System.Windows.Forms.DataVisualization.Charting.Series("Right");
                            seriesRight.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;

                            this.chartBottom.Series.Add(seriesLeft);
                            this.chartBottom.Series.Add(seriesRight);

                            for (int i = 0; i < xcorrSize; i++)
                            {
                                this.chartBottom.Series[0].Points.AddXY((i - xcorrSize / 2.0) / xcorrSize * maxTorsion * 2.0, 0);
                                this.chartBottom.Series[1].Points.AddXY((i - xcorrSize / 2.0) / xcorrSize * maxTorsion * 2.0, 0);
                            }

                            this.chartBottom.Series[0].Color = Color.RoyalBlue;
                            this.chartBottom.Series[1].Color = Color.Red;

                            this.chartBottom.ChartAreas[0].AxisX.MajorTickMark.Interval = 5;
                            this.chartBottom.ChartAreas[0].AxisX.LabelStyle.Interval = 5;
                            this.chartBottom.ChartAreas[0].AxisX.MajorGrid.Interval = 5;
                            this.chartBottom.ChartAreas[0].AxisX.Minimum = -maxTorsion;
                            this.chartBottom.ChartAreas[0].AxisX.Maximum = +maxTorsion;
                            this.chartBottom.ChartAreas[0].AxisX.MinorGrid.Interval = 1;
                            this.chartBottom.ChartAreas[0].AxisX.MinorGrid.Enabled = true;
                            this.chartBottom.ChartAreas[0].AxisX.MinorGrid.LineColor = Color.Gray;

                            this.chartBottom.ChartAreas[0].AxisY.Maximum = 255;
                            this.chartBottom.ChartAreas[0].AxisY.Minimum = 0;
                        }

                        var imageCorrLeft = EyeTracker.Diagnostics.DebugImageManager.GetImage("crosscorrelation", Eye.Left);
                        if (imageCorrLeft != null)
                        {
                            var leftXcorr = imageCorrLeft.Resize(1, xcorrSize, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);

                            for (int i = 0; i < xcorrSize; i++)
                            {
                                this.chartBottom.Series[0].Points[i].SetValueY(leftXcorr.Data[i, 0, 0]);
                            }
                        }

                        var imageCorrRight = EyeTracker.Diagnostics.DebugImageManager.GetImage("crosscorrelation", Eye.Right);
                        if (imageCorrRight != null)
                        {
                            var rightXcorr = imageCorrRight.Resize(1, xcorrSize, Emgu.CV.CvEnum.INTER.CV_INTER_LINEAR);
                            for (int i = 0; i < xcorrSize; i++)
                            {
                                this.chartBottom.Series[1].Points[i].SetValueY(rightXcorr.Data[i, 0, 0]);
                            }
                        }

                        this.chartBottom.Invalidate();
                    }
                    if (CURVATURE)
                    {
                    }

                    foreach (var key in EyeTracker.Diagnostics.DebugImageManager.GetImageNames())
                    {
                        if (!this.listBox1.Items.Contains(key))
                        {
                            this.listBox1.Items.Add(key);
                        }
                    }

                    var imagesLeft = new System.Collections.Generic.List<Emgu.CV.UI.ImageBox>(new Emgu.CV.UI.ImageBox[4] { 
                        this.imageBoxLeft1,this.imageBoxLeft2,this.imageBoxLeft3,this.imageBoxLeft4
                    });
                    var imagesRight = new System.Collections.Generic.List<Emgu.CV.UI.ImageBox>(new Emgu.CV.UI.ImageBox[4] {
                        this.imageBoxRight1,this.imageBoxRight2,this.imageBoxRight3,this.imageBoxRight4
                    });

                    var selectedKeys = this.listBox1.SelectedItems;
                    for (int i = 0; i < 4; i++)
                    {
                        if (i < selectedKeys.Count)
                        {
                            imagesLeft[i].Image = EyeTracker.Diagnostics.DebugImageManager.GetImage((string)selectedKeys[i], Eye.Left);
                            imagesRight[i].Image = EyeTracker.Diagnostics.DebugImageManager.GetImage((string)selectedKeys[i], Eye.Right);
                        }
                        else
                        {
                            imagesLeft[i].Image = null;
                            imagesRight[i].Image = null;
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.ToString());
                }

                this.tabDebug.ResumeLayout();
            }
        }

        private void UpdateTabTiming()
        {
            this.labelTiming.Text = EyeTracker.Diagnostics.Timer.ToString();
            var diagnostics = new System.Text.StringBuilder();
            for (int i = 1; i < this.eyeTracker.ImageGrabber.Diagnostics.Length; i++)
            {
                diagnostics.Append(this.eyeTracker.ImageGrabber.Diagnostics[i]);
            }
            this.labelDiagnosticsGrabbing.Text = diagnostics.ToString();
        }

        //camera intrinsics left eye
        Matrix<float> Map1_Left, Map2_Left;
        Matrix<float> Map1_Right, Map2_Right;
        private void UpdateTabDistortion()
        {
            var imageTest = Properties.Resources.ImageTest;

            var imageLeft = this.lastDataAndImages.Images[Eye.Left].ImageRaw.Convert<Bgr, byte>();
            //var imageLeft = new Image<Bgr, Byte>(imageTest);
            this.imageLeftDistortion.Image = imageLeft;


            var imageRight = this.lastDataAndImages.Images[Eye.Right].ImageRaw.Convert<Bgr, byte>();
            //var imageRight = new Image<Bgr, Byte>(imageTest);
            this.imageRightDistortion.Image = imageRight;

            //lets get a frame from our capture device

            if (radioLeftImageCalib.Checked)
            {
                img = imageLeft;
            }
            else if (radioRightImageCalib.Checked)
            {
                img = imageRight;
            }
            
            Gray_Frame = img.Convert<Gray, Byte>();

            //apply chess board detection
            if (currentMode == Mode.SavingFrames)
            {

                corners = CameraCalibration.FindChessboardCorners(Gray_Frame, patternSize, Emgu.CV.CvEnum.CALIB_CB_TYPE.ADAPTIVE_THRESH);
                //we use this loop so we can show a color image rather than a gray: //CameraCalibration.DrawChessboardCorners(Gray_Frame, patternSize, corners);

                if (corners != null) //chess board found
                {
                    //make measurements more accurate by using FindCornerSubPixel
                    Gray_Frame.FindCornerSubPix(new PointF[1][] { corners }, new Size(11, 11), new Size(-1, -1), new MCvTermCriteria(30, 0.1));

                    //if go button has been pressed start acquiring frames else we will just display the points
                    if (start_Flag)
                    {
                        Frame_array_buffer[frame_buffer_savepoint] = Gray_Frame.Copy(); //store the image
                        frame_buffer_savepoint++;//increase buffer position

                        numCurrentFrameDistortion.Value = frame_buffer_savepoint;

                        //check the state of buffer
                        if (frame_buffer_savepoint == Frame_array_buffer.Length) currentMode = Mode.Calculating_Intrinsics; //buffer full
                    }

                    //dram the results
                    img.Draw(new CircleF(corners[0], 3), new Bgr(Color.Yellow), 1);
                    for (int i = 1; i < corners.Length; i++)
                    {
                        img.Draw(new LineSegment2DF(corners[i - 1], corners[i]), line_colour_array[i], 2);
                        img.Draw(new CircleF(corners[i], 3), new Bgr(Color.Yellow), 1);
                    }
                    //calibrate the delay based on size of buffer
                    //if buffer small you want a big delay if big small delay
                    Thread.Sleep(1000);//allow the user to move the board to a different position
                }
                corners = null;
            }
            if (currentMode == Mode.Calculating_Intrinsics)
            {
                //we can do this in the loop above to increase speed
                for (int k = 0; k < Frame_array_buffer.Length; k++)
                {

                    corners_points_list[k] = CameraCalibration.FindChessboardCorners(Frame_array_buffer[k], patternSize, Emgu.CV.CvEnum.CALIB_CB_TYPE.ADAPTIVE_THRESH);
                    //for accuracy
                    Gray_Frame.FindCornerSubPix(corners_points_list, new Size(11, 11), new Size(-1, -1), new MCvTermCriteria(30, 0.1));

                    //Fill our objects list with the real world measurements for the intrinsic calculations
                    List<MCvPoint3D32f> object_list = new List<MCvPoint3D32f>();
                    for (int i = 0; i < height; i++)
                    {
                        for (int j = 0; j < width; j++)
                        {
                            object_list.Add(new MCvPoint3D32f(j * 20.0F, i * 20.0F, 0.0F));
                        }
                    }
                    corners_object_list[k] = object_list.ToArray();
                }

                //our error should be as close to 0 as possible
                Thread.Sleep(2000);

                MCvTermCriteria chessboardcriteria = new MCvTermCriteria(30, 0.001);

                double error = CameraCalibration.CalibrateCamera(corners_object_list, corners_points_list, Gray_Frame.Size, IC, Emgu.CV.CvEnum.CALIB_TYPE.CV_CALIB_RATIONAL_MODEL, chessboardcriteria, out EX_Param);
                //If Emgu.CV.CvEnum.CALIB_TYPE == CV_CALIB_USE_INTRINSIC_GUESS and/or CV_CALIB_FIX_ASPECT_RATIO are specified, some or all of fx, fy, cx, cy must be initialized before calling the function
                //if you use FIX_ASPECT_RATIO and FIX_FOCAL_LEGNTH options, these values needs to be set in the intrinsic parameters before the CalibrateCamera function is called. Otherwise 0 values are used as default.
                MessageBox.Show("Intrinsic Calculation Error: " + error.ToString(), "Results", MessageBoxButtons.OK, MessageBoxIcon.Information); //display the results to the user
                currentMode = Mode.Calibrated;

                lblDistortionStatus.Text = "Calibrated!";
                lblDistortionStatus.ForeColor = Color.DarkGreen;
                
                //calculate the camera intrinsics
                Matrix<float> Map1, Map2;
                IC.InitUndistortMap(img.Width, img.Height, out Map1, out Map2);

                if (radioLeftImageCalib.Checked)
                {
                    Map1_Left = Map1;
                    Map2_Left = Map2;
                }
                else if (radioRightImageCalib.Checked)
                {
                    Map1_Right = Map1;
                    Map2_Right = Map2;                
                }


            }
            if (currentMode == Mode.Calibrated)
            {
                //remap the image to the particular intrinsics
                //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
                //image is mapped backed, in the future this should be controllable through the flag '0'
                Image<Bgr, Byte> temp = img.CopyBlank();

                if (radioLeftImageCalib.Checked)
                {
                    CvInvoke.cvRemap(img, temp, Map1_Left, Map2_Left, 0, new MCvScalar(0));
                }
                else if (radioRightImageCalib.Checked)
                {
                    CvInvoke.cvRemap(img, temp, Map1_Right, Map2_Right, 0, new MCvScalar(0));
                }
                
                img = temp.Copy();

                //set up to allow another calculation
                SetButtonState(true);
                start_Flag = false;
            }
            if (currentMode == Mode.BothCalibrated)
            {
                //remap the image to the particular intrinsics
                //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
                //image is mapped backed, in the future this should be controllable through the flag '0'
                Image<Bgr, Byte> tempLeft = imageLeft.CopyBlank();
                Image<Bgr, Byte> tempRight = imageRight.CopyBlank();

                CvInvoke.cvRemap(imageLeft, tempLeft, Map1_Left, Map2_Left, 0, new MCvScalar(0));
                CvInvoke.cvRemap(imageRight, tempRight, Map1_Right, Map2_Right, 0, new MCvScalar(0));

                imageLeft = tempLeft.Copy();
                imageRight = tempRight.Copy();

                //set up to allow another calculation
                SetButtonState(true);
                start_Flag = false;
            }
            if (currentMode == Mode.BothCalibrated)
            {
                imageLeftDistortion.Image = imageLeft;
                imageRightDistortion.Image = imageRight;
            }
            else {
                if (radioLeftImageCalib.Checked)
                {
                    imageLeftDistortion.Image = img;
                }
                else if (radioRightImageCalib.Checked)
                {
                    imageRightDistortion.Image = img;
                } 
            }

        }

        private Image<Bgr, Byte> Perform_Distortion_Correction(Image<Bgr, Byte> eyeImage, Eye eye)
        {
            //remap the image to the particular intrinsics
            //In the current version of EMGU any pixel that is not corrected is set to transparent allowing the original image to be displayed if the same
            //image is mapped backed, in the future this should be controllable through the flag '0'

            Image<Bgr, Byte> tempEyeImage = eyeImage.CopyBlank();
            if (eye == Eye.Left)
            {
                CvInvoke.cvRemap(eyeImage, tempEyeImage, Map1_Left, Map2_Left, 0, new MCvScalar(0));
            }
            else if (eye == Eye.Right)
            {
                CvInvoke.cvRemap(eyeImage, tempEyeImage, Map1_Right, Map2_Right, 0, new MCvScalar(0));
            }

            eyeImage = tempEyeImage.Copy();
            return eyeImage;
        }

        private void Start_BTN_Click(object sender, EventArgs e)
        {
            if (currentMode != Mode.SavingFrames) currentMode = Mode.SavingFrames;
            Start_BTN.Enabled = false;
            //set up the arrays needed
            Frame_array_buffer = new Image<Gray, byte>[(int)numFrameBufferDistortion.Value];
            corners_object_list = new MCvPoint3D32f[Frame_array_buffer.Length][];
            corners_points_list = new PointF[Frame_array_buffer.Length][];
            frame_buffer_savepoint = 0;
            //allow the start
            start_Flag = true;
            lblDistortionStatus.Text = "Calibrating...";
            lblDistortionStatus.ForeColor = Color.DarkGreen;
        }

        /// <summary>
        /// Used to safely set the button state from capture thread
        /// </summary>
        /// <param name="state"></param>
        delegate void SetButtonStateDelegate(bool state);
        private void SetButtonState(bool state)
        {
            if (Start_BTN.InvokeRequired)
            {
                try
                {
                    // update textbox asynchronously
                    SetButtonStateDelegate ut = new SetButtonStateDelegate(SetButtonState);
                    //if (this.IsHandleCreated && !this.IsDisposed)
                    this.BeginInvoke(ut, new object[] { state });
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                Start_BTN.Enabled = state;
            }
        }

        private void UpdateUI()
        {
            bool CanFinish = !((this.eyeTracker.RecordingManager != null && this.eyeTracker.RecordingManager.Recording) || this.eyeTracker.PostProcessing);

            if (!this.eyeTrackerTrace.Initialized)
            {
                this.eyeTrackerTrace.Init();
            }

            // Mouse cursor
            if (this.eyeTracker.Initializing)
            {
                this.Cursor = Cursors.WaitCursor;
            }
            else
            {
                this.Cursor = Cursors.Default;
            }

            // Session and folder info
            textBoxSession.Text = (EyeTracker.Settings == null) ? string.Empty : EyeTracker.Settings.SessionName;
            linkLabelDataFolder.Text = (EyeTracker.Settings == null) ? string.Empty : EyeTracker.Settings.DataFolder;
            textBoxSession.Enabled = this.eyeTracker.Idle || this.eyeTracker.Tracking;

            // QuickSettings
            this.eyeTrackerQuickSettingsLeftEye.UpdateValues();
            this.eyeTrackerQuickSettingsRightEye.UpdateValues();
            this.eyeTrackerQuickSettingsLeftEye.Enabled = this.eyeTracker.Tracking;
            this.eyeTrackerQuickSettingsRightEye.Enabled = this.eyeTracker.Tracking;

            // Player
            this.panelPlayer.Visible = this.eyeTracker.VideoPlayer != null && !this.eyeTracker.PostProcessing;
            this.panelPlayer.Enabled = this.eyeTracker.VideoPlayer != null && !this.eyeTracker.PostProcessing;

            // Menu Configuration
            this.configurationToolStripMenuItem.Enabled = this.eyeTracker.Tracking || this.eyeTracker.Idle;

            // Record buttons
            if (this.eyeTracker.RecordingManager.Recording)
            {
                this.buttonRecord.BackColor = Color.Salmon;
            }
            else
            {
                this.buttonRecord.BackColor = SystemColors.Control;
            }

            // Calibration
            var currentCalibrationUI = this.calibrationUI;
            var newCalibrationUI = this.eyeTracker.CalibrationManager.CalibrationUI;
            if ((currentCalibrationUI == null) || (currentCalibrationUI != newCalibrationUI))
            {
                this.tabCalibration.Controls.Clear();
                var calibrationControl = newCalibrationUI as UserControl;

                if (newCalibrationUI != null)
                {
                    calibrationControl.Dock = DockStyle.Fill;
                    calibrationControl.Location = new Point(0, 0);
                    calibrationControl.Size = this.tabCalibration.ClientSize;
                    this.tabCalibration.Controls.Add(calibrationControl);
                    this.calibrationUI = newCalibrationUI;

                    if (this.eyeTracker.CalibrationManager.Calibrating)
                    {
                        this.tabSoftStim.SelectTab(this.tabCalibration);
                    }
                    if(newCalibrationUI.btnAbortCalibration != null)
                        commandManager.commands[EyeTrackerCommandName.CancelCalibration].Bind(newCalibrationUI.btnAbortCalibration);
                    if (newCalibrationUI.btnStartCalibration != null)
                        commandManager.commands[EyeTrackerCommandName.StartContinueCalibration].Bind(newCalibrationUI.btnStartCalibration);
                    

                }
            }
        }


        /// <summary>
        /// Updates the information of the status bar
        /// </summary>
        private void UpdateStatusBar()
        {
            if (this.eyeTracker.Initializing)
            {
                return;
            }

            try
            {
                if (this.lastDataAndImages == null)
                {
                    return;
                }

                toolStripStatusLabelImageGrabbingStatus.Text = this.eyeTracker.ImageGrabber.Diagnostics[0];

                var statusString = "";
                if (this.lastDataAndImages.Images[Eye.Left] != null)
                {
                    statusString = statusString + string.Format("L-{0:0.00}", this.lastDataAndImages.Images[Eye.Left].EyeData.Timestamp.Seconds);
                }
                if (this.lastDataAndImages.Images[Eye.Right] != null)
                {
                    statusString = statusString + string.Format(" R-{0:0.00}", this.lastDataAndImages.Images[Eye.Right].EyeData.Timestamp.Seconds);
                }

                this.toolStripStatusLabelTimestamp.Text = statusString;

                this.toolStripStatusLabelProcessingTimeLeftEye.Text = this.eyeTracker.ProcessingStatusMessage;

                this.toolStripStatusLabelRecording.Text = this.eyeTracker.RecordingManager.StatusMessage;

                this.statusStrip1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("GUI:Update Status Bar: " + ex.ToString());
            }
        }

        #endregion

        #region Main event handlers

        /// <summary>
        /// Loads the EyeTrackerGUI.
        /// </summary>
        /// <param name="sender">Parent window.</param>
        /// <param name="e">Event parameters.</param>
        private void EyeTrackerGUI_Load(object sender, EventArgs e)
        {
            this.Initialize();
        }

        private void timerRefreshUI_Tick(object sender, EventArgs e)
        {
            // Update the UI
            try
            {
                this.UpdateGraphics();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.ToString(), "timerRefreshUI_Tick");
            }
        }

        private void EyeTracker_NewLogMessage(object sender, NewLogMessageEventArgs e)
        {
            this.richTextBox1.BeginInvoke((Action)(() =>
            {
                try
                {
                    if (e.Message.Contains("ERROR"))
                    {
                        this.richTextBox1.AppendText(e.Message, Color.Red);
                        this.richTextBoxLogLarge.AppendText(e.Message, Color.Red);
                    }
                    else
                    {
                        this.richTextBox1.AppendText(e.Message);
                        this.richTextBoxLogLarge.AppendText(e.Message);
                    }
                    this.richTextBox1.ScrollToCaret();
                    this.richTextBoxLogLarge.ScrollToCaret();
                }
                catch (Exception ex)
                {

                }
            }));
        }

        private void EyeTracker_NewDataAvailable(object sender, NewDataAvailableEventArgs e)
        {
            this.lastDataAndImages = e.EyeTrackerDataAndImages;

            long currentIdx = 0;

            if (e.EyeTrackerDataAndImages.Images[Eye.Left] != null)
            {
                Math.DivRem(e.EyeTrackerDataAndImages.Images[Eye.Left].EyeData.Timestamp.FrameNumber, this.bufferData.Length, out currentIdx);
            }

            if (e.EyeTrackerDataAndImages.Images[Eye.Right] != null)
            {
                Math.DivRem(e.EyeTrackerDataAndImages.Images[Eye.Right].EyeData.Timestamp.FrameNumber, this.bufferData.Length, out currentIdx);
            }


            lock (this)
            {
                if (e.EyeTrackerDataAndImages.Images[Eye.Left] != null)
                {
                    this.lastFrameInBufferData = e.EyeTrackerDataAndImages.Images[Eye.Left].EyeData.Timestamp.FrameNumber;
                }

                if (e.EyeTrackerDataAndImages.Images[Eye.Right] != null)
                {
                    this.lastFrameInBufferData = e.EyeTrackerDataAndImages.Images[Eye.Right].EyeData.Timestamp.FrameNumber;
                }

                if (currentIdx >= 0)
                {
                    this.bufferData[currentIdx] = e.EyeTrackerDataAndImages.Data;
                }
                else
                {
                    this.eyeTracker.Stop();
                }
            }
        }

        private void EyeTrackerGUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool CanFinish = !((this.eyeTracker.RecordingManager != null && this.eyeTracker.RecordingManager.Recording) || this.eyeTracker.PostProcessing);

            try
            {
                if (!CanFinish)
                {
                    e.Cancel = true;
                    return;
                }

                if (this.timerRefreshUI != null)
                {
                    this.timerRefreshUI.Stop();
                }

                if (this.timerFinished != null)
                {
                    this.timerFinished.WaitOne(1000);
                }

                if (eyeTracker != null)
                {
                    this.eyeTracker.Stop();
                    this.eyeTracker.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("GUI:Error Closing: " + ex.ToString());
            }
        }

        #endregion

        #region Menu event handlers

        private void ConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EyeTrackerSettingsForm.Show(EyeTracker.Settings);
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fullScreenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void openSoundRecorderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("SoundRecorder.exe");
        }

        private void textBoxSession_TextChanged(object sender, EventArgs e)
        {
            EyeTracker.Settings.SessionName = textBoxSession.Text;
        }

        private void linkLabelDataFolder_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", EyeTracker.Settings.DataFolder);
        }

        private void openLogFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));

        }

        private void batchAnalysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var batchForm = new BatchProcessing(this.eyeTracker);

            batchForm.Show();
        }

        private void convertVideoToRGBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            var videoReader = new Capture(dialog.FileName);

            using (var videoWriter = new VideoWriter(dialog.FileName + "out2.avi", 100, videoReader.Width, videoReader.Height, true))
            {
                while (true)
                {
                    var img = videoReader.QueryGrayFrame();
                    if (img == null)
                    {
                        return;
                    }
                    videoWriter.WriteFrame(img.Convert<Bgr, byte>());
                }
            }
        }

        #endregion

        private void EyeTrackerGui_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.eyeTracker.RecordEvent("KEYPRESS", e.KeyChar);
        }

        String CamCalibFilePath = @"C:\VOG\Labyrinth VOG Software\lib\CamCalib\";
        private void btnSaveLeftCamCalib_Click(object sender, EventArgs e)
        {
            Matrix<float> mat = Map1_Left;
            StringBuilder sb = new StringBuilder();

            (new XmlSerializer(typeof(Matrix<float>))).Serialize(new StringWriter(sb), mat);
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.LoadXml(sb.ToString());
            xDoc1.Save(CamCalibFilePath + "Map1_Left.xml");


            mat = Map2_Left;
            sb.Clear();
            (new XmlSerializer(typeof(Matrix<float>))).Serialize(new StringWriter(sb), mat);
            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.LoadXml(sb.ToString());
            xDoc2.Save(CamCalibFilePath + "Map2_Left.xml");
        }

        private void btnLoadLeftCamCalib_Click(object sender, EventArgs e)
        {
            radioLeftImageCalib.Checked = true;

            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1_Left.xml");
            Map1_Left = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2_Left.xml");
            Map2_Left = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));

            currentMode = Mode.Calibrated;

        }

        private void btnSaveRightCamCalib_Click(object sender, EventArgs e)
        {
            Matrix<float> mat = Map1_Right;
            StringBuilder sb = new StringBuilder();

            (new XmlSerializer(typeof(Matrix<float>))).Serialize(new StringWriter(sb), mat);
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.LoadXml(sb.ToString());
            xDoc1.Save(CamCalibFilePath + "Map1_Right.xml");

            mat = Map2_Right;
            sb.Clear();
            (new XmlSerializer(typeof(Matrix<float>))).Serialize(new StringWriter(sb), mat);
            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.LoadXml(sb.ToString());
            xDoc2.Save(CamCalibFilePath + "Map2_Right.xml");
        }

        private void btnLoadRightCamCalib_Click(object sender, EventArgs e)
        {
            radioRightImageCalib.Checked = true;

            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1_Right.xml");
            Map1_Right = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2_Right.xml");
            Map2_Right = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));

            currentMode = Mode.Calibrated;
        }

        private void btnLoadCalibBothEyes_Click(object sender, EventArgs e)
        {
            XmlDocument xDoc1 = new XmlDocument();
            xDoc1.Load(CamCalibFilePath + "Map1_Left.xml");
            Map1_Left = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc1));

            XmlDocument xDoc2 = new XmlDocument();
            xDoc2.Load(CamCalibFilePath + "Map2_Left.xml");
            Map2_Left = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc2));

            XmlDocument xDoc3 = new XmlDocument();
            xDoc3.Load(CamCalibFilePath + "Map1_Right.xml");
            Map1_Right = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc3));

            XmlDocument xDoc4 = new XmlDocument();
            xDoc4.Load(CamCalibFilePath + "Map2_Right.xml");
            Map2_Right = (Matrix<float>)
            (new XmlSerializer(typeof(Matrix<float>))).Deserialize(new XmlNodeReader(xDoc4));

            currentMode = Mode.BothCalibrated;

            groupDistortCorrectEyeTracking.Visible = true;
            radioDistortCorrectEyeTrackingYes.Checked = true;

        }

        private void btnAnalyze_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog openFileDialogAlignmentAnalyze = new OpenFileDialog();
            openFileDialogAlignmentAnalyze.InitialDirectory = EyeTracker.Settings.DataFolder;


            openFileDialogAlignmentAnalyze.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialogAlignmentAnalyze.FilterIndex = 2;

            if (openFileDialogAlignmentAnalyze.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string fname = openFileDialogAlignmentAnalyze.FileName;
                    var scriptsfolder = EyeTracker.Settings.DataFolder + "\\Scripts";

                    textVOGDataFilename.Text = fname;

                    int CoordSys = 0;
                    if (radioLRZ.Checked)
                    {
                        CoordSys = 0;
                    }
                    else if (radiopitchedXYZ.Checked)
                    {
                        CoordSys = 1;
                    }
                    else if (radioXYZ.Checked)
                    {
                        CoordSys = 2;
                    }

                    double PitchEyeAngle = Convert.ToDouble(numEyeAngleCorrection.Value);
                    double Axis1Pol = Convert.ToDouble(numPolXorLARP.Value);
                    double Axis2Pol = Convert.ToDouble(numPolYorRALP.Value);
                    double Axis3Pol = Convert.ToDouble(numPolZorLHRH.Value);

                    int EyeDataSmoothing = 0;
                    if (radioDataSmoothingOff.Checked)
                    {
                        EyeDataSmoothing = 0;
                    }
                    else if (radioDataSmoothingOn.Checked)
                    {
                        EyeDataSmoothing = 1;
                    }

                    MLApp.MLApp matlab = new MLApp.MLApp();
                    object result = null;
                    matlab.Execute("clear all");
                    matlab.Execute("clc all");
                    matlab.Execute(@"cd " + scriptsfolder);
                    matlab.Feval("Analyze_Head_And_Eye_Data", 1, out result, fname, CoordSys, PitchEyeAngle, Axis1Pol, Axis2Pol, Axis3Pol, EyeDataSmoothing);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not open file for analysis.");
                }
            }
        }

        private void radioLRZ_CheckedChanged(object sender, EventArgs e)
        {
            lblEyePolXorLARP.Text = "LARP";
            lblEyePolYorRALP.Text = "RALP";
            lblEyePolZorLHRH.Text = "LHRH";
        }

        private void radiopitchedXYZ_CheckedChanged(object sender, EventArgs e)
        {
            lblEyePolXorLARP.Text = "X";
            lblEyePolYorRALP.Text = "Y";
            lblEyePolZorLHRH.Text = "Z";
        }

        private void radioXYZ_CheckedChanged(object sender, EventArgs e)
        {
            lblEyePolXorLARP.Text = "X";
            lblEyePolYorRALP.Text = "Y";
            lblEyePolZorLHRH.Text = "Z";
        }

        private void numPolXorLARP_ValueChanged(object sender, EventArgs e)
        {
            if ((numPolXorLARP.Value != 1) && (numPolXorLARP.Value != -1))
            {
                numPolXorLARP.Value = 1;
            }
        }

        private void numPolYorRALP_ValueChanged(object sender, EventArgs e)
        {
            if ((numPolYorRALP.Value != 1) && (numPolYorRALP.Value != -1))
            {
                numPolYorRALP.Value = 1;
            }
        }

        private void numPolZorLHRH_ValueChanged(object sender, EventArgs e)
        {
            if ((numPolZorLHRH.Value != 1) && (numPolZorLHRH.Value != -1))
            {
                numPolZorLHRH.Value = 1;
            }
        }

        private void btnCloseMatlabWindows_Click(object sender, EventArgs e)
        {
            MLApp.MLApp matlab = new MLApp.MLApp();
            matlab.Execute("close all");
        }

        private void radioSoftwareFlagOn_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSoftwareFlagOn.Checked)
            {
                eyeTracker.SoftwareTrigger = true;
            }
            else
            {
                eyeTracker.SoftwareTrigger = false;
            }
        }

        private void radioSoftwareFlagOff_CheckedChanged(object sender, EventArgs e)
        {
            if (radioSoftwareFlagOff.Checked)
            {
                eyeTracker.SoftwareTrigger = false;
            }
            else
            {
                eyeTracker.SoftwareTrigger = true;
            }
        }

    }
}